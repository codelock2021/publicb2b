
"use strict";
var CLS_LOADER = '<svg viewBox="0 0 20 20" class="Polaris-Spinner Polaris-Spinner--colorInkLightest Polaris-Spinner--sizeSmall" aria-label="Loading" role="status"><path d="M7.229 1.173a9.25 9.25 0 1 0 11.655 11.412 1.25 1.25 0 1 0-2.4-.698 6.75 6.75 0 1 1-8.506-8.329 1.25 1.25 0 1 0-.75-2.385z"></path></svg>';
var CLS_DELETE = '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg>';
var CLS_MINUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 80 80" focusable="false" aria-hidden="true"><path d="M39.769,0C17.8,0,0,17.8,0,39.768c0,21.956,17.8,39.768,39.769,39.768   c21.965,0,39.768-17.812,39.768-39.768C79.536,17.8,61.733,0,39.769,0z M13.261,45.07V34.466h53.014V45.07H13.261z" fill-rule="evenodd" fill="#DE3618"></path></svg>';
var CLS_PLUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><path d="M17 9h-6V3a1 1 0 1 0-2 0v6H3a1 1 0 1 0 0 2h6v6a1 1 0 1 0 2 0v-6h6a1 1 0 1 0 0-2" fill-rule="evenodd"></path></svg>';
var CLS_CIRCLE_MINUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 80 80" focusable="false" aria-hidden="true"><path d="M39.769,0C17.8,0,0,17.8,0,39.768c0,21.956,17.8,39.768,39.769,39.768   c21.965,0,39.768-17.812,39.768-39.768C79.536,17.8,61.733,0,39.769,0z M13.261,45.07V34.466h53.014V45.07H13.261z" fill-rule="evenodd" fill="#DE3618"></path></svg>';
var CLS_CIRCLE_PLUS = '<svg class="Polaris-Icon__Svg" viewBox="0 0 510 510" focusable="false" aria-hidden="true"><path d="M255,0C114.75,0,0,114.75,0,255s114.75,255,255,255s255-114.75,255-255S395.25,0,255,0z M382.5,280.5h-102v102h-51v-102    h-102v-51h102v-102h51v102h102V280.5z" fill-rule="evenodd" fill="#3f4eae"></path></svg>';
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    cname = (cname != undefined) ? cname : 'flash_message';
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function flashNotice($message, $class) {
    $class = ($class != undefined) ? $class : '';

    var flashmessageHtml = '<div class="inline-flash-wrapper animated bounceInUp inline-flash-wrapper--is-visible ourFlashmessage"><div class="inline-flash ' + $class + '  "><p class="inline-flash__message">' + $message + '</p></div></div>';

    if ($('.ourFlashmessage').length) {
        $('.ourFlashmessage').remove();
    }
    $("body").append(flashmessageHtml);

    setTimeout(function () {
        if ($('.ourFlashmessage').length) {
            $('.ourFlashmessage').remove();
        }
    }, 3000);
}
function changeTab(evt, id) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("Polaris-Tabs_tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("Polaris-Tabs__Tab");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace("Polaris-Tabs__Tab--selected", "");
    }
    document.getElementById(id).style.display = "block";
    evt.currentTarget.className += " Polaris-Tabs__Tab--selected";
}
function loading_show($selector) {
    $($selector).addClass("Polaris-Button--loading").html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner">' + CLS_LOADER + '</span><span>Loading</span></span>').fadeIn('fast').attr('disabled', 'disabled');
}
function loading_hide($selector, $buttonName, $buttonIcon) {
    if ($buttonIcon != undefined) {
        $buttonIcon = '<span class="Polaris-Button__Icon"><span class="Polaris-Icon">' + $buttonIcon + '</span></span>'
    } else {
        $buttonIcon = '';
    }
    $($selector).removeClass("Polaris-Button--loading").html('<span class="Polaris-Button__Content">' + $buttonIcon + '<span>' + $buttonName + '</span></span>').removeAttr("disabled");
}

function table_loader(selector, colSpan) {
    $(selector).html('<tr><td colspan="' + colSpan + '" style="text-align:center;"><div class="loader-spinner"></div></td></tr>')
}
function redirect403() {
    window.location = "https://www.shopify.com/admin/apps";
}
var loadShopifyAJAX = null;
var js_loadShopifyDATA = function js_loadShopifyDATA(listingID, pageno) {
    if (loadShopifyAJAX && loadShopifyAJAX.readyState != 4) {
        loadShopifyAJAX.abort();
    }
    var searchKEY = $("#" + listingID + "SearchKeyword").val();
    var searchKEYLEN = (searchKEY != undefined) ? searchKEY.length : 0;
    if (searchKEYLEN == 0 || searchKEYLEN >= 3) {
        var shopifyApi = $('#' + listingID).attr('data-apiName');
        var limit = $("#" + listingID + "limit").val();
        var from = $('#' + listingID).data('from');
        var routineNAME = 'take_' + from + '_shopify_data';
        var fields = $('#' + listingID).attr('data-fields');
        fields = (fields != undefined) ? fields : '*';
        var searchFields = $('#' + listingID).attr('data-search');
        pageno = (pageno != undefined) ? pageno : 1;
        loadShopifyAJAX = $.ajax({
            url: "ajax_call.php",
            type: "post",
            dataType: "json",
            data: {
                routine_name: routineNAME,
                shopify_api: shopifyApi,
                fields: fields,
                store: store,
                limit: limit,
                pageno: pageno,
                search_key: searchKEY,
                listing_id: listingID,
                search_fields: searchFields,
                pagination_method: js_loadShopifyDATA.name
            },
            beforeSend: function () {
                var listingTH = $('#' + listingID + ' thead tr th').length;
                table_loader("table#" + listingID + " tbody", listingTH);
            },
            success: function (comeback) {
                if (comeback['code'] != undefined && comeback['code'] == '403') {
                    redirect403();
                } else if (comeback['outcome'] == 'true') {
                    $('table#' + listingID + ' tbody').html(comeback['html']);
                    $('#' + listingID + 'Pagination').html(comeback['pagination_html']);
                } else {
                }
            }
        });
    }
}
function footer_total_purchase(){
        var total = 0;
        $('.total_amount').each(function () {
            total += parseFloat($(this).val());
            console.log("total" + total);
        });
        $('.grand_amount').val(total.toFixed(2) || 0);
        $('.price_set').val(total.toFixed(2));
        var qty = 0;
        $('.qty').each(function () {
            if ($(this).val() != "") {
                qty += parseInt($(this).val());
            }
        });
        $('.total_quantity').val(qty.toFixed(2));
        $('.qty_set').val(qty.toFixed(2));
        
        var taxable = 0;
        $('.total').each(function () {
            taxable += parseFloat($(this).val());
        });
        $('.total_taxable').val(taxable.toFixed(2));
        var igst = 0;
        $('.igst').each(function () {
            igst += parseFloat($(this).val() || 0);
        });
        $('.total_igst').val(igst.toFixed(2));
        var sgst = 0;
        $('.sgst').each(function () {
            sgst += parseFloat($(this).val());
        });
        $('.total_sgst').val(sgst.toFixed(2));
        var cgst = 0;
        $('.cgst').each(function () {
            cgst += parseFloat($(this).val());
        });
        $('.total_cgst').val(cgst.toFixed(2));
        var all_total = 0;
        all_total = parseFloat($('#freight').val()) +parseFloat($('#packing').val()) + parseFloat($('#insurance').val()) + parseFloat($('.grand_amount').val()) || 0; 
        $('.all_total').val(all_total.toFixed(2));
    }
function OrderDetails(order_id, store) {
    var store_id = store_id;
    var order_id = order_id;
    var shopifyApi = "orders";
    var routineNAME = "order_detail";
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        data: {routine_name: routineNAME, shopify_api: shopifyApi, order_id: order_id, store: store},
        success: function ($return_arary) {
            if ($return_arary['code'] != undefined && $return_arary['code'] == '403') {
                redirect403();
            } else if ($return_arary['outcome'] == 'true') {
                $('.block2').append($return_arary["html"]["table"]);
                $('.block1').append($return_arary["html"]["product"]);
                $('.block3').append($return_arary["html"]["customer"]);
            } else {
            }
        }
    });
}
function replaceTableStatus(tableName, primaryKeyId, status, thisObj) {
    var class_name = 'listingTableLoader', button_name = '';
    $.ajax({
        url: "responce.php",
        type: "post",
        dataType: "json",
        data: {method_name: "replace_table_status", shop: shop, table_name: tableName, primary_key_id: primaryKeyId, status: status},
        success: function (response) {
            if (response['result'] == 'success') {
                flashNotice(response['message']);
                $(thisObj).attr('onclick', response['onclickfn'])
                        .find('.Polaris-custom-icon.Polaris-Icon.Polaris-Icon--hasBackdrop').html(response['svg_icon'])
                        .children('span').attr('data-hover', response['data_hover']);
            } else {
                flashNotice(response['message']);
            }
        },
        error: function () {
        }
    });
}
function removeFromTable(tableName, ID, pageno, tableId, api_name, is_delete) {
    var is_delete = (is_delete == undefined) ? 'Record' : is_delete;
    var Ajaxdelete = function Ajaxdelete() {
        var el = is_delete;
        $.ajax({
            url: "ajax_call.php",
            type: "post",
            dataType: "json",
            data: {routine_name: 'remove_from_table', store: store, table_name: tableName, id: ID, api_name: api_name},
            beforeSend: function () {
                loading_show('.save_loader_show' + ID);
            },
            success: function (response) {
                loading_hide('save_loader_show' + ID, '', CLS_DELETE);
                if (response['result'] == 'success') {
                    if (pageno == undefined || pageno < 0 || response['total_record'] <= 0) {
                        setCookie('flash_message', response['message'], 2);
                        location.reload();
                    } else if (pageno > 0) {
                        $(is_delete).closest("tr").css("background", "tomato");
                        $(is_delete).closest("tr").fadeOut(800, function () {
                            $(this).remove();
                        });
                        flashNotice(response['message']);
                    }
                    if (response['total_method'] != undefined) {
                        $('#totalShippingMethod').html(response['total_method']);
                    }
                } else {
                    window.location = 'dealers-listing.php';
                    setCookie('flash_message', response['message'], 2);
                }


            }
        });
    }
//   if (mode == 'live') {
//        ShopifyApp.Modal.confirm({
//            title: "Delete " + is_delete + " ?",
//            message: "Are you sure want to delete the " + is_delete + " ? This action cannot be reversed.",
//            okButton: "Delete " + is_delete,
//            cancelButton: "Cancel",
//            style: "danger"
//        }, function (result) {
//            if (result) {
//                $('.ui-button.close-modal.btn-destroy-no-hover').addClass("ui-button ui-button--destructive js-btn-loadable is-loading disabled");
//                Ajaxdelete();
//            }
//        });
//    } else {
    var r = confirm("Are you sure want to delete!");
    if (r == true) {
        Ajaxdelete();
//        }
    }

}
$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
}
$(document).ready(function () {
    $('#rating').rating({
        min: 0,
        max: 5,
        step: 1,
        size: 'xs',
        showClear: false
    });
    $(".star").hover(function () {
        $(".rate-our-app").show();
        $(".dismiss-button-rating").hide();
    });
    $('table[data-listing="true"]').each(function () {
        var tableId = $(this).attr('id');
        js_loadShopifyDATA(tableId);

    });
    var flashmessage = getCookie("flash_message");
    var flashClass = getCookie("flash_class");
    if (flashmessage != '') {
        flashClass = (flashClass == null || flashClass == undefined) ? '' : flashClass;
        setCookie('flash_message', '', -2);
        flashNotice(flashmessage, flashClass);
    }
    $(".spectrumColor").spectrum({
        showButtons: false
    });
    $(".spectrumColor").on('move.spectrum', function (e, color) {
        var id = $(this).data('id');
        var hexVal = color.toHexString();
        $("[data-id='" + id + "']").val(hexVal);
    });
    $(document).on("submit", "#addClientstore_settingFrm", function (e) {
        e.preventDefault();
        var frmData = $(this).serialize();
        frmData += '&' + $.param({"method_name": 'set_store_settings', 'shop': shop});
        $.ajax({
            url: "responce.php",
            type: "post",
            dataType: "json",
            data: frmData,
            beforeSend: function () {
                loading_show('save_loader_show');
            },
            success: function (response) {
                if (response['code'] != undefined && response['code'] == '403') {
                    redirect403();
                } else if (response['result'] == 'success') {
                    $("#errorsmsgBlock").hide();
                    flashNotice(response['message']);
                } else if (response['msg_contented'] != undefined && response['msg_manage'] != undefined) {
                    $("#errorBlockInfo").html(response['msg_contented']);
                    $("#errorBlockManage").html(response['msg_manage']);
                    $("#errorsmsgBlock").show();
                    $("html, body").animate({scrollTop: 0}, "slow");
                } else {
                    flashNotice(response['message']);
                }
                loading_hide('save_loader_show', 'Save');
            }
        });
    });
    $(document).on("submit", "#imageExmapleFrm", function (e) {
        e.preventDefault();
        var frmData = new FormData($(this)[0]);
        frmData.append('shop', shop);
        frmData.append('method_name', 'image_file_example');
        $.ajax({
            url: "responce.php",
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: frmData,
            beforeSend: function () {
                loading_show('save_loader_show');
            },
            success: function (response) {
                if (response['code'] != undefined && response['code'] == '403') {
                    redirect403();
                } else if (response['result'] == 'success') {
                    $("#errorsmsgBlock").hide();
                    flashNotice(response['message']);
                } else if (response['msg_contented'] != undefined && response['msg_manage'] != undefined) {
                    $("#errorBlockInfo").html(response['msg_contented']);
                    $("#errorBlockManage").html(response['msg_manage']);
                    $("#errorsmsgBlock").show();
                    $("html, body").animate({scrollTop: 0}, "slow");
                } else {
                    flashNotice(response['message']);
                }
                loading_hide('save_loader_show', 'Save');
            }
        });
    });
    $(document).on("click", ".logout", function (event) {
        event.preventDefault();
        logout();
    });
    function logout()
    {
        $.ajax({
            url: "ajax_call.php",
            type: 'POST',
            data: {
                action: 'logout'
            },
            success: function (response)
            {
                if (response.trim() == 'success')
                {
                    window.location.href = 'index.php';
                }
            }

        });
    }
   $(document).on("submit", "#register_frm", function (e) {
        e.preventDefault();
        var frmData = new FormData($(this)[0]);
        frmData.append('store', store);
        frmData.append('routine_name', 'allbutton_details');
        $.ajax({
            url: "ajax_call.php",
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: frmData,
            beforeSend: function () {
                loading_show('.save_loader_show');

            },
            success: function (response) {
                console.log(response);
                if (response['code'] != undefined && response['code'] == '403') {
                    redirect403();
                }
                else {
                    if (response['for_data'] == 'supplier') {
                        window.location.href = 'supplier-listing.php?store='+store;
                    } else if (response['for_data'] == 'purchase') {
                        window.location.href = 'purchase-listing.php?store='+store;
                    } else if (response['for_data'] == 'purchase_return') {
                        window.location.href = 'puchase-return.php?store='+store;
                    } else if (response['for_data'] == 'dealer') {
                        window.location.href = 'dealers-listing.php?store='+store;
                    } else if (response['for_data'] == 'sales') {
                        window.location.href = 'sales-listing.php?store='+store;
                    } else if (response['for_data'] == 'sales_retrun') {
                        window.location.href = 'sales-return-listing.php?store='+store;
                    } else if (response['for_data'] == 'dealers_orders_update') {
                        window.location.href = 'dealer-order.php?store='+store;
                    } 
                    else {
                        window.location.href = '';
                    }
                    loading_hide('.save_loader_show', 'save');
                }
            }
        });
    });
});
function get_textarea_value(routine_name, id, for_data) {
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        data: {'routine_name': routine_name, 'store': store, 'id': id, 'for_data': for_data},
        success: function (comeback) {
            if (comeback['code'] != undefined && comeback['code'] == '403') {
                redirect403();
            } else if (comeback['outcome'] == 'true') {
                $('#supplier_name').val(comeback['data']['supplier_name']);
                $('#supplier_contact_person').val(comeback['data']['supplier_contact_person']);
                $('#supplier_email').val(comeback['data']['supplier_email']);
                $('#supplier_address').val(comeback['data']['supplier_address']);
                $('#supplier_landmark').val(comeback['data']['supplier_landmark']);
                $('#supplier_phone').val(comeback['data']['supplier_phone']);
                $('#supplier_mobile').val(comeback['data']['supplier_mobile']);
                $('#supplier_city').val(comeback['data']['supplier_city']);
                $('#supplier_state').val(comeback['data']['supplier_state']);
                $('#supplier_country').val(comeback['data']['supplier_country']);
                $('#supplier_pincode').val(comeback['data']['supplier_pincode']);
                $('#supplier_pan').val(comeback['data']['supplier_pan']);
                $('#supplier_tin').val(comeback['data']['supplier_tin']);
                $('#supplier_vat').val(comeback['data']['supplier_vat']);
                $('#supplier_gst').val(comeback['data']['supplier_gst']);
                $('#supplier_bank_name').val(comeback['data']['supplier_bank_name']);
                $('#supplier_bank_account_no').val(comeback['data']['supplier_bank_account_no']);
                $('#supplier_bank_branch').val(comeback['data']['supplier_bank_branch']);
                $('#supplier_ifsc_code').val(comeback['data']['supplier_ifsc_code']);
                $('#supplier_remarks').val(comeback['data']['supplier_remarks']);
                $('#supplier_status').val(comeback['data']['supplier_status']);

            } else {
            }
        }
    });
}
function get_purchase_value(routine_name, store, id, for_data) {
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        data: {'routine_name': routine_name, 'store': store, 'id': id, 'for_data': for_data},
        success: function (comeback) {
            if (comeback['code'] != undefined && comeback['code'] == '403') {
                redirect403();
            } else if (comeback['outcome'] == 'true') {
                $('#freight').val(comeback['data']['freight']);
                $('#packing').val(comeback['data']['packing']);
                $('#insurance').val(comeback['data']['insurance']);
                $('#po_date').val(comeback['data']['po_date']);
                $('#supplier_id').val(comeback['data']['supplier_id']);
                $('#rec_ref_code').val(comeback['data']['rec_ref_code']);
                $('#supplier_address').val(comeback['data']['supplier_address']);
                $('#remarks').val(comeback['data']['remarks']);
                $('#reverse_charge').val(comeback['data']['reverse_charge']);
                $('#supplier_invoice_no').val(comeback['data']['supplier_invoice_no']);
                $('#supplier_invoice_date').val(comeback['data']['supplier_invoice_date']);
                $('.edit-product').html(comeback["data"]["str"]);
                footer_total_purchase();
            } else {
            }  
        }
    });
}   
function searchData(listingID){
        if (window.location.href.indexOf("/sales-return-create") > -1) {
            var searchKEY = $('#dealer_id').val();
        } else if(window.location.href.indexOf("/sales_return_edit") > -1)  {
           var searchKEY = $('#dealer_id').val();
        }else{
             var searchKEY = $('#supplier_id').val();  
        }
        var searchKEYLEN = (searchKEY != undefined) ? searchKEY.length : 0;
        if (searchKEYLEN >= 1) {
            var shopifyApi = $('#' + listingID).attr('data-apiName');
            var limit = 10;
            var from = $('#' + listingID).data('from');
            var routineNAME = 'take_' + from + '_shopify_data';
            var fields = $('#' + listingID).attr('data-fields');
            fields = (fields != undefined) ? fields : '*';
            var searchFields = $('#' + listingID).attr('data-search');
            loadShopifyAJAX = $.ajax({
                url: "ajax_call.php",
                type: "post",
                dataType: "json",
                data: {
                    routine_name: routineNAME,
                    shopify_api: shopifyApi,
                    fields: fields,
                    store: store,
                    limit: limit,
                    search_key: searchKEY,
                    listing_id: listingID,
                    search_fields: searchFields,
                    pagination_method: js_loadShopifyDATA.name
                },
                beforeSend: function () {
                    $("#" + listingID).show();
                    var listingTH = $('#' + listingID + ' thead tr th').length;
                    table_loader("table#" + listingID + " tbody", listingTH);
                },
                success: function (comeback) {
                    if (comeback['code'] != undefined && comeback['code'] == '403') {
                        redirect403();
                    } else if (comeback['outcome'] == 'true') {
                        $('table#' + listingID + ' tbody').html(comeback['html']);
                        $('#' + listingID + 'Pagination').html(comeback['pagination_html']);
                        $(".purchase-return-skutable").css("display", "block");
                    } else {
                    }
                }
            });
        } else {
            alert("Please select return date than click on search.");
        }
}
function SearchEditData(listingID,purchase_id){
          if (window.location.href.indexOf("/sales-return-create") > -1) {
            var searchKEY = $('#dealer_id').val();
        } else if(window.location.href.indexOf("/sales_return_edit") > -1)  {
           var searchKEY = $('#dealer_id').val();
        }else{
             var searchKEY = $('#supplier_id').val();  
        }
        console.log(searchKEY);
        var searchKEYLEN = (searchKEY != undefined) ? searchKEY.length : 0;
        if (searchKEYLEN >= 1) {
            var shopifyApi = $('#' + listingID).attr('data-apiName');
            var limit = 10;
            var from = $('#' + listingID).data('from');
            var routineNAME = 'take_' + from + '_shopify_data';
            var fields = $('#' + listingID).attr('data-fields');
            fields = (fields != undefined) ? fields : '*';
            var searchFields = $('#' + listingID).attr('data-search');
            loadShopifyAJAX = $.ajax({
                url: "ajax_call.php",
                type: "post",
                dataType: "json",
                data: {
                    routine_name: routineNAME,
                    shopify_api: shopifyApi,
                    fields: fields,
                    store: store,
                    limit: limit,
                    search_key: purchase_id,
                    listing_id: listingID,
                    search_fields: searchFields,
                    pagination_method: js_loadShopifyDATA.name
                },
                beforeSend: function () {
                    $("#" + listingID).show();
                    var listingTH = $('#' + listingID + ' thead tr th').length;
                    table_loader("table#" + listingID + " tbody", listingTH);
                },
                success: function (comeback) {
                        if (comeback['code'] != undefined && comeback['code'] == '403') {
                        redirect403();
                    } else if (comeback['outcome'] == 'true') {
                        $('table#' + listingID + ' tbody').html(comeback['html']);
                        $('#' + listingID + 'Pagination').html(comeback['pagination_html']);
                        $(".purchase-return-skutable").css("display", "block");
                    } else {
                    }
                }
            });
        } else {
            alert("Please select return date than click on search.");
        }
    
}
function salesreturn_product_edit_listing(pid){
         if (pid.length > 0) {
                $.ajax({
                    url: "ajax_call.php",
                    type: "POST",
                    data: {routine_name: "multipleSelectRecord", id: pid, store: store},
                    success: function (response) {
                        var response = JSON.parse(response);
                            var startnum = 1;
                            var str = '';
                            str += '<tr class="input_' + response.data.id + '" value= '+response.data.id+'>';
                            str += '<input type="hidden" value=' + response.data.id + ' name="skuid[]"/>';
                            str += '<td>' + startnum + '</td>';
                            startnum++;
                            str += '<td><input type="text" class="form-control"  name="barcode[]"/ readonly value=' + response.data.barcode + ' ></td>';
                            str += '<td><input type="text" class="form-control skuInput"  name="sku[]"/ readonly value=' + response.data.sku + ' ></td>';
                            str += '<td><input type="text"  class="form-control category_name" name="category_name[]" readonly value=' + response.data.name + ' > </td>';
                             str += '<td><input type="text"  class="form-control vendor" name="v.sku[]" readonly  > </td>';
                            str += '<td><input type="text"  class="form-control"  name="metal[]" readonly value=' + response.data.metal + ' >  </td>';
                            str += '<td><input type="text"  class="form-control"  name="invchallan_no[]" readonly value=' + response.data.sales_date + ' >  </td>';
                            str += '<td><input type="text"  class="form-control"  name="invchallan_no[]" readonly value=' + response.data.voucher_no + ' >  </td>';
                            str += '<td><input type="text"  class="form-control" name="weight[]"  readonly value=' + response.data.weight + ' >  </td>';
                            str += '<td class="cls-text-section"><input type="number" class="form-control qtySale"  name="qty[]" value=' + response.data.return_qty + ' >\n\
                                <input type="number"  class="form-control mrp"  name="mrp[]" readonly value=' + response.data.mrp + ' > </td>';
//                            str += '<td class="cls-discount"><input type="text"  class="form-control discount" name="discount[]" readonly value=' + response.data.discount + '>  </td>';
                            str += '<td><input type="text"  class="form-control taxation"  name="taxation[]" readonly value=' + response.data.taxation + ' >  </td>';
                            str += '<td><input type="number"  class="form-control cgst"  name="cgst[]" readonly value=' + response.data.cgst + ' >   </td>';
                            str += '<td><input type="text"  class="form-control sgst"  name="sgst[]" readonly value=' + response.data.sgst + ' >  </td>';
                            str += '<td><input type="number"  class="form-control igst"  name="igst[]" readonly value=' + response.data.igst + '  >   </td>';
                            str += '<td><input type="number"  class="form-control grandtotal"  name="grandtotal[]" readonly  value=' + response.data.grandtotal + ' >   </td>';
                            str += '<td> <button class=" btn-zap btnDeleteSales" value= '+response.data.id+' type="button">× </button></td>';
                            str += '</tr>';
                            $(".dataresult").append(str);
                            footer_total_sales();
                    },
                });
            }
        else {
            var id = this.id;
            var splitid = id.split("_");
            var pid = splitid[1];
            $('.btnDeleteSales').closest('.input_' + pid).find(".btnDeleteSales").trigger('click');
            footer_total_sales();
        }
}
function salesreturn_productlisting(pid){
         if (pid.length > 0) {
                $.ajax({
                    url: "ajax_call.php",
                    type: "POST",
                    data: {routine_name: "multipleSelectRecord",store: store, id: pid},
                    success: function (response) {
                        var response = JSON.parse(response);
                        console.log("hello");
                            var startnum = 1;
                            var str = '';
                            str += '<tr class="input_' + response.data.id + '" value= '+response.data.id+'>';
                            str += '<input type="hidden" value=' + response.data.id + ' name="skuid[]"/>';
                            str += '<td>' + startnum + '</td>';
                            startnum++;
                            str += '<td><input type="text" class="form-control"  name="barcode[]"/ readonly value=' + response.data.barcode + ' ></td>';
                            str += '<td><input type="text" class="form-control skuInput"  name="sku[]"/ readonly value=' + response.data.sku + ' ></td>';
                            str += '<td><input type="text"  class="form-control category_name" name="category_name[]" readonly value=' + response.data.name + ' > </td>';
                             str += '<td><input type="text"  class="form-control vendor" name="v.sku[]" readonly  > </td>';
                            str += '<td><input type="text"  class="form-control"  name="metal[]" readonly value=' + response.data.metal + ' >  </td>';
                            str += '<td><input type="text"  class="form-control"  name="invchallan_no[]" readonly value=' + response.data.sales_date + ' >  </td>';
                            str += '<td><input type="text"  class="form-control"  name="invchallan_no[]" readonly value=' + response.data.voucher_no + ' >  </td>';
                            str += '<td><input type="text"  class="form-control" name="weight[]"  readonly value=' + response.data.weight + ' >  </td>';
                            str += '<td class="cls-text-section"><input type="number" class="form-control qtySale"  name="qty[]" value=' + response.data.remain_qty + ' >\n\
                                <input type="number"  class="form-control mrp"  name="mrp[]" readonly value=' + response.data.mrp + ' > </td>';
                            str += '<input type="hidden"  class="form-control discount" name="discount[]" readonly value=' + response.data.discount + '>';
                            str += '<td><input type="text"  class="form-control taxation"  name="taxation[]" readonly value=' + response.data.taxation + ' >  </td>';
                            str += '<td><input type="number"  class="form-control cgst"  name="cgst[]" readonly value=' + response.data.cgst + ' >   </td>';
                            str += '<td><input type="text"  class="form-control sgst"  name="sgst[]" readonly value=' + response.data.sgst + ' >  </td>';
                            str += '<td><input type="number"  class="form-control igst"  name="igst[]" readonly value=' + response.data.igst + '  >   </td>';
                            str += '<td><input type="number"  class="form-control grandtotal"  name="grandtotal[]" readonly  value=' + response.data.grandtotal + ' >   </td>';
                            str += '<td> <button class=" btn-zap btnDeleteSales" value= '+response.data.id+' type="button">× </button></td>';
                            str += '</tr>';
                            $(".dataresult").append(str);
                            footer_total_sales();
                    },
                });
            }
        else {
            var id = this.id;
            var splitid = id.split("_");
            var pid = splitid[1];
            $('.btnDeleteSales').closest('.input_' + pid).find(".btnDeleteSales").trigger('click');
            footer_total_sales();
        }
}
function get_dealer_value(routine_name, id, for_data) {
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        data: {'routine_name': routine_name, 'store': store, 'id': id, 'for_data': for_data},
        success: function (comeback) {
            if (comeback['code'] != undefined && comeback['code'] == '403') {
                redirect403();
            } else if (comeback['outcome'] == 'true') {

                $('#firstname').val(comeback['data']['firstname']);
                $('#lastname').val(comeback['data']['lastname']);
                $('#contact_person').val(comeback['data']['contact_person']);
                $('#email').val(comeback['data']['email']);
                $('#username').val(comeback['data']['username']);
                $('#password').val(comeback['data']['password']);
                if (comeback["data"]["same_address"] == "1") {
                    $("#same_address").val("1").prop("checked", "checked");
                }

                $('#address').val(comeback['data']['address']);
                $('#billing_address').val(comeback['data']['billing_address']);
                $('#landmark').val(comeback['data']['landmark']);
                $('#billing_landmark').val(comeback['data']['billing_landmark']);
                $('#city').val(comeback['data']['city']);
                $('#billing_city').val(comeback['data']['billing_city']);
                $('#state').val(comeback['data']['state']);
                $('#billing_state').val(comeback['data']['billing_state']);
                $('#country').val(comeback['data']['country']);
                $('#billing_country').val(comeback['data']['billing_country']);
                $('#phone').val(comeback['data']['phone']);
                $('#billing_phone').val(comeback['data']['billing_phone']);
                $('#mobile').val(comeback['data']['mobile']);
                $('#billing_mobile').val(comeback['data']['billing_mobile']);
                $('#postal_code').val(comeback['data']['postal_code']);
                $('#gst').val(comeback['data']['gst']);
                $('#billing_gst').val(comeback['data']['billing_gst']);
                $('#pan').val(comeback['data']['pan']);
                $('#tin').val(comeback['data']['tin']);
                $('#vat').val(comeback['data']['vat']);
                $('#bank_name').val(comeback['data']['bank_name']);
                $('#bank_account_no').val(comeback['data']['bank_account_no']);
                $('#bank_branch').val(comeback['data']['bank_branch']);
                $('#ifsc_code').val(comeback['data']['ifsc_code']);
                $('#approved').val(comeback['data']['approved']);
                $('#discount').val(comeback['data']['discount']);
                $('#active').val(comeback['data']['active']);

            } else {
            }
        }
    });
}
   function footer_total_sales() {
        var total = 0;
        $('.grandtotal').each(function () {
            total += parseInt($(this).val());
        });
        $('.grand_amount').val(total.toFixed(2));
        var qty = 0;
        $('.qtySale').each(function () {
            if ($(this).val() != "") {
                qty += parseInt($(this).val()|| 0);
            }
        });
        $('.total_qty').val(qty.toFixed(2) );
        var taxable = 0;
        $('.taxation').each(function () {
            taxable += parseInt($(this).val()|| 0);
        });
        $('.total_taxable').val(taxable.toFixed(2) );
        var discount = 0;
        $('.discount').each(function () {
            discount += parseInt($(this).val()|| 0);
        });
        $('.total_discount').val(discount.toFixed(2) );
        var igst = 0;
        $('.igst').each(function () {
            igst += parseInt($(this).val());
        });
        $(".price_ret_set").val(total.toFixed(2));
        $(".qty_set").val(qty.toFixed(2));
        $('.total_igst').val(igst.toFixed(2));
        $('.total_cgst').val(0);
        $('.total_sgst').val(0);
    }
function get_sales_value(routine_name, id, for_data) {
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        data: {'routine_name': routine_name, 'store': store, 'id': id, 'for_data': for_data},
        success: function (comeback) {
            console.log(comeback["data"]["str"]);
            if (comeback['code'] != undefined && comeback['code'] == '403') {
                redirect403();
            } else if (comeback['outcome'] == 'true') {
                if (comeback.sales_type == "0") {
                    $("#sales_type_tax").val("0").prop("checked", "checked");
                } else {
                    $("#sales_type_delivery").val("1").prop("checked", "checked");
                }
                if (comeback.invoice_type == "1") {
                    $("#invoice_type").val("1").prop("checked", "checked");
                }
                $('#sales_date').val(comeback['data']['sales_date']);
                $('#dos').val(comeback['data']['dos']);
                $('#packing').val(comeback['data']['packing']);
                $('#insurance').val(comeback['data']['insurance']);
                $('#freight').val(comeback['data']['freight']);
                $('#reverse_charge').val(comeback['data']['reverse_charge']);
                $('.edit-sales').html(comeback['data']['str']);
                $('#ref_no').val(comeback['data']['ref_no']);
                $('#transport_mode').val(comeback['data']['transport_mode']);
                $('#vehicle_no').val(comeback['data']['vehicle_no']);
                $('#billing_address').val(comeback['data']['billing_address']);
                $('#shipping_address').val(comeback['data']['shipping_address']);
                $('#remark').val(comeback['data']['remark']);
                footer_total_sales();
            } else {
            }
        }
    });
}
function get_sales_return_value(routine_name, store, id,sales_id, for_data) {
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        data: {'routine_name': routine_name, 'store': store, 'sales_id': sales_id, 'id': id, 'for_data': for_data},
        success: function (comeback) {
            if (comeback['code'] != undefined && comeback['code'] == '403') {
                redirect403();
            } else if (comeback['outcome'] == 'true') {
                if (comeback.so_type == "0") {
                    $("#so_type_direct").val("0").prop("checked", "checked");
                } else {
                    $("#so_type_onaooroval").val("1").prop("checked", "checked");
                }

                $('#return_date').val(comeback['data']['return_date']);
                $('#ref_num').val(comeback['data']['ref_num']);
                $('#qty_ret').val(comeback['data']['qty_ret']);
                $('#price_ret').val(comeback['data']['price_ret']);
                $('#reason').val(comeback['data']['reason']);
                $('#dealer_id').val(comeback['data']['dealer_id']);

            } else {
            }
        }
    });
}
function get_api_data(routineName, shopify_api) {
    var routineName = routineName;
    var shopify_api = shopify_api;
    $.ajax({
        url: "ajax_call.php",
        type: "POST",
        dataType: "json",
        data: {
            routine_name: routineName,
            shopify_api: shopify_api,
            store: store,
        },
        success: function ($response) {
            if ($response['code'] != undefined && $response['code'] == '403') {
                redirect403();
            } else if ($response['data'] == 'true') {
                $('.numberConvertBlog').html($response["total_record_blog"]);
                $('.numberConvertCollection').html($response["total_record_collection"]);
                $('.numberConvertProduct').html($response["total_record_product"]);
                $('.numberConvertPages').html($response["total_record_pages"]);
            } else {
            }
        }
    })
}

 function update_status(id,$this,table,tables,return_tables) {
        var routineNAME = "update_status";
        var thisObj = $this;
        $.ajax({
            url: "ajax_call.php",
            type: "post",
            dataType: "json",
            data: {routine_name: routineNAME, id: id,table:table,store:store,tables :tables, return_tables: return_tables},
         success: function (response) {
                var dataResult = JSON.parse(response); 
                console.log(dataResult);
                if(dataResult.status == 1){
                      $(thisObj).closest("tr").css("background", "tomato");
                        $(thisObj).closest("tr").fadeOut(800, function() {
                                 $(this).closest('tr').remove();   
                            });
                }
            }
        });
    }
function update_status_sales(id,$this,sales,sales_sku,sales_return){
      var routineNAME = "update_status_sales";
      var thisObj = $this;
      $.ajax({
            url: "ajax_call.php",
            type: "post",
            dataType: "json",
            data: {routine_name: routineNAME, id: id,store:store,sales:sales,sales_sku:sales_sku,sales_return:sales_return},
         success: function (response) {
                var dataResult = JSON.parse(response); 
                console.log(dataResult);
                if(dataResult.status == 1){
                      $(thisObj).closest("tr").css("background", "tomato");
                        $(thisObj).closest("tr").fadeOut(800, function() {
                                 $(this).closest('tr').remove();   
                            });
                }
            }
        });
}
$(document).on("submit", "#addblog_frm", function (e) {
    e.preventDefault();
    var form_data = $("#addblog_frm")[0];
    var form_data = new FormData(form_data);
    form_data.append('images', $("#ImagePreview").attr("src"));
    form_data.append('store', store);
    form_data.append('routine_name', 'addblog');
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');
        },
        success: function (response) {
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                window.location.href = "blog_post.php?store=" + store;
            }
            loading_hide('.save_loader_show', 'Save');
        }
    });
});
$(document).on("submit", "#addproduct_frm", function (e) {
    e.preventDefault();
    var form_data = $("#addproduct_frm")[0];
    var form_data = new FormData(form_data);

    form_data.append('images', $("#ImagePreview").attr("src"));
    form_data.append('store', store);
    form_data.append('routine_name', 'addproduct');
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');
        },
        success: function (response) {
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                window.location.href = "products.php?store=" + store;
            }
            loading_hide('.save_loader_show', 'Save');
        }
    });
});
$(document).on("submit", "#addpages_frm", function (e) {
    e.preventDefault();
    var form_data = $("#addpages_frm")[0];
    var form_data = new FormData(form_data);
    form_data.append('store', store);
    form_data.append('routine_name', 'addpages');
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');
        },
        success: function (response) {
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                window.location.href = "pages.php?store" + store;
            }
            loading_hide('.save_loader_show', 'Save');
        }
    });
});
$(document).on("submit", "#addcollection_frm", function (e) {
    e.preventDefault();
    var form_data = $("#addcollection_frm")[0];
    var form_data = new FormData(form_data);
    form_data.append('store', store);
    form_data.append('routine_name', 'addcollections');
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');
        },
        success: function (response) {
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                window.location.href = "pages.php?store" + store;
            }
            loading_hide('.save_loader_show', 'Save');
        }
    });
});

function readURL(input) {
    $(".imagesBlock").css("display", "block");
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#ImagePreview').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}


function preview_image(selector, page)
{
    $('.imagesBlock').css({display: 'block'});
    $('.imagesBlockEdit').css({display: 'block'});
    var product_id = $('#product_id').val();
    var id = 1, last_id = '', last_cid = '';
    var img_srcs = [];
    var img_titles = [];

    $.each(selector.files, function (vpb_o_, file)
    {
        if (file.name.length > 0)
        {
            if (!file.type.match('image.*')) {
                return true;
            }
            else
            {
                var reader = new FileReader();
                if (page == 'add') {
                    reader.onload = function (e)
                    {
                        $('#multiImagePreview').append(
                                '<div class="column">' +
                                '<div class="image-box">' +
                                '<img class="btn-delete removeAddImage" src="http://cdn1.iconfinder.com/data/icons/diagona/icon/16/101.png">' +
                                '<img class="thumbnail multi-image-preview" src="' + e.target.result + '" \
                               title="' + escape(file.name) + '" /><input type="hidden" name="selected_imges[]" id="jsImg" value="' + file.name + '"></div></div>');
                    }
                } else {
                    reader.onload = function (e)
                    {
                        img_srcs.push(e.target.result);
                        img_titles.push(escape(file.name));
                    }
                }
                reader.readAsDataURL(file);
            }
        }
        else {
            return false;
        }
    });

    if (page == 'update') {
        setTimeout(function () {
            var old_images = $('#oldImages').val();
            var frmData = new FormData();
            $.each(img_srcs, function (index, value) {
                frmData.append('images[]', value);
                frmData.append('title[]', img_titles[index]);
            });
            frmData.append('shop', shop);
            frmData.append('product_id', product_id);
            frmData.append('image_ids', old_images);
            frmData.append('method_name', 'upload_product_images');

            if (frmData != '') {
                $.ajax({
                    url: "ajax_responce.php",
                    type: "post",
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    data: frmData,
                    beforeSend: function () {
                        loading_show('.image_loader');
                    },
                    success: function (response) {
                        var old_img_arr = JSON.parse(old_images);
                        if (response['result'] == 'success') {
                            $.each(response['images']['product']['images'], function (index, value) {
                                if ($.inArray(value.id, old_img_arr) == -1) {
                                    old_img_arr.push(value.id);
                                    $('#multiImagePreview').append(
                                            '<div class="column style="display:none;">' +
                                            '<div class="image-box-edit selectMainImage" data-id="' + value.id + '"><i class="btn-select-image"><input type="radio" name="select_main_image" class="select_main_image" value="' + value.id + '" style="display:none;"></i>' +
                                            '<img class="btn-delete-edit removeAddImage" data-id="' + value.id + '" src="http://cdn1.iconfinder.com/data/icons/diagona/icon/16/101.png">' +
                                            '<img class="thumbnail  multi-image-preview addedNewImage" src="' + value.src + '" \
                       title="' + value.alt + '" /><input type="hidden" name="selected_imges[]" id="jsImg" value="' + value.id + '"></div></div>');
                                }
                            });
                            old_img_arr.toString();
                            $('#oldImages').val("[" + old_img_arr + "]");
                            loading_hide('.image_loader', '');
                        } else {
                            flashNotice(response['msg']);
                        }
                        loading_hide('.image_loader', '');
                    }
                });
            }
        }, 1000);
    }


}

$(document).on("submit", "#frm", function (e) {
    e.preventDefault();
    var form_data = new FormData($("#frm")[0]);
    form_data.append('routine_name', 'supplier_create');
    form_data.append('store', store);

    $.ajax({
        url: "ajax_call.php",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');
        },
        success: function (response) {
            var dataResult = JSON.parse(response);
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                if (dataResult.dataResult == "fail") {
                    dataResult.errors.supplier_name !== undefined ? $(".supplier_name").html(dataResult.errors.supplier_name) : $(".supplier_name").html("");
                    dataResult.errors.supplier_city !== undefined ? $(".supplier_city").html(dataResult.errors.supplier_city) : $(".supplier_city").html("");
                    dataResult.errors.supplier_state !== undefined ? $(".supplier_state").html(dataResult.errors.supplier_state) : $(".supplier_state").html("");
                    dataResult.errors.supplier_pincode !== undefined ? $(".supplier_pincode").html(dataResult.errors.supplier_pincode) : $(".supplier_pincode").html("");
                    dataResult.errors.supplier_address !== undefined ? $(".supplier_address").html(dataResult.errors.supplier_address) : $(".supplier_address").html("");
                } else {
                    $(".supplier_name").html("");
                    $(".supplier_city").html("");
                    $(".supplier_state").html("");
                    $(".supplier_pincode").html("");
                    $(".supplier_address").html("");
                    $("#frm")[0].reset();
                    window.location.href = "supplier-listing.php?store="+store;
                    loading_hide('.save_loader_show', 'save');
                }

            }
        }
    });
});
$(document).on("submit", "#purchase_create", function (e) {
    e.preventDefault();
    var form_data = new FormData($("#purchase_create")[0]);
    form_data.append('routine_name', 'purchase_create');
    form_data.append('store', store);
var thisObj =this;
    $.ajax({
        url: "ajax_call.php",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');

        },
        success: function (response) {
            var dataResult = JSON.parse(response);
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                if (dataResult.dataResult == "fail") {
                    dataResult.errors.po_date !== undefined ? $(".po_date").html(dataResult.errors.po_date) : $(".po_date").html("");
                    dataResult.errors.supplier_id !== undefined ? $(".supplier_id").html(dataResult.errors.supplier_id) : $(".supplier_id").html("");
                    dataResult.errors.sku !== undefined ? $(".sku").html(dataResult.errors.sku) : $(".sku").html("");
                    
                } else {
                    $(".po_date").html("");
                    $(".supplier_id").html("");
                    $(".sku").html("");
                    $("#purchase_create")[0].reset();
                    window.location.href = "purchase-listing.php?store="+store;
                    loading_hide('.save_loader_show', 'save');
                }

            }
        }
    });
});
$(document).on("submit", "#purchase_return_create", function (e) {
    e.preventDefault();
    var form_data = new FormData($("#purchase_return_create")[0]);
    form_data.append('routine_name', 'purchase_return_create');
    form_data.append('store', store);
           var thisObj = this;

    $.ajax({
        url: "ajax_call.php",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');

        },
        success: function (response) {
            var dataResult = JSON.parse(response);
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                if (dataResult.dataResult == "fail") {
                    dataResult.errors.return_date !== undefined ? $(".return_date").html(dataResult.errors.return_date) : $(".return_date").html("");
                    dataResult.errors.qty_ret !== undefined ? $(".qty_ret").html(dataResult.errors.qty_ret) : $(".qty_ret").html("");
                    dataResult.errors.price_ret !== undefined ? $(".price_ret").html(dataResult.errors.price_ret) : $(".price_ret").html("");
                    dataResult.errors.sku !== undefined ? $(".sku").html(dataResult.errors.sku) : $(".sku").html("");
                    if (dataResult.errors.update_qty) {
                        $(thisObj).closest("tr").find(".qty").css("border", "2px solid red");
                        alert("Please  Enter Valid Quantity"); 
                    } else {
                        $(thisObj).closest("tr").find(".qty").css("border", "1px solid #ced4da");
                    }
                } else {
                    $(".return_date").html("");
                    $(".qty_ret").html("");
                    $(".price_ret").html("");
                    $(".sku").html("");
                    $("#purchase_return_create")[0].reset();
                    window.location.href = "puchase-return.php?store="+store;;
                    loading_hide('.save_loader_show', 'save');
                }

            }
        }
    });
});
$(document).on("submit", "#sales", function (e) {
    e.preventDefault();
    var form_data = new FormData($("#sales")[0]);
    form_data.append('routine_name', 'sales');
    form_data.append('store', store);
    $.ajax({
        url: "ajax_call.php",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');

        },
        success: function (response) {
            var dataResult = JSON.parse(response);
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                if (dataResult.dataResult == "fail") {
                    dataResult.errors.dealer_id !== undefined ? $(".dealer_id").html(dataResult.errors.dealer_id) : $(".dealer_id").html("");
                    dataResult.errors.billing_address !== undefined ? $(".billing_address").html(dataResult.errors.billing_address) : $(".billing_address").html("");
                    dataResult.errors.shipping_address !== undefined ? $(".shipping_address").html(dataResult.errors.shipping_address) : $(".shipping_address").html("");
                    dataResult.errors.sales_date !== undefined ? $(".sales_date").html(dataResult.errors.sales_date) : $(".sales_date").html("");

                } else {
                    $(".dealer_id").html("");
                    $(".billing_address").html("");
                    $(".shipping_address").html("");
                    $(".sales_date").html("");
                    $("#sales")[0].reset();
                    window.location.href = "sales-listing.php?store="+store;
                    loading_hide('.save_loader_show', 'save');
                }

            }
        }
    });
});
$(document).on("submit", "#sales_order", function (e) {
    e.preventDefault();
    var form_data = new FormData($("#sales_order")[0]);
    form_data.append('routine_name', 'sales_order');
    form_data.append('store', store);

    $.ajax({
        url: "ajax_call.php",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');

        },
        success: function (response) {
            var dataResult = JSON.parse(response);
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                if (dataResult.dataResult == "fail") {
                    dataResult.errors.sales_date !== undefined ? $(".sales_date").html(dataResult.errors.sales_date) : $(".sales_date").html("");
                    dataResult.errors.dealer_id !== undefined ? $(".dealer_id").html(dataResult.errors.dealer_id) : $(".dealer_id").html("");
                    dataResult.errors.billing_address !== undefined ? $(".billing_address").html(dataResult.errors.billing_address) : $(".billing_address").html("");
                    dataResult.errors.shipping_address !== undefined ? $(".shipping_address").html(dataResult.errors.shipping_address) : $(".shipping_address").html("");


                } else {
                    $(".sales_date").html("");
                    $(".dealer_id").html("");
                    $(".billing_address").html("");
                    $(".shipping_address").html("");
                    $("#sales_order")[0].reset();
                    window.location.href = "sales-listing.php?store="+store;
                    loading_hide('.save_loader_show', 'save');
                }

            }
        }
    });
});
$(document).on("submit", "#sales_return", function (e) {
    e.preventDefault();
    var form_data = new FormData($("#sales_return")[0]);
    form_data.append('routine_name', 'sales_return');
    form_data.append('store', store);

    $.ajax({
        url: "ajax_call.php",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');

        },
        success: function (response) {
            var dataResult = JSON.parse(response);
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                if (dataResult.dataResult == "fail") {
                    dataResult.errors.return_date !== undefined ? $(".return_date").html(dataResult.errors.return_date) : $(".return_date").html("");
                    dataResult.errors.qty_ret !== undefined ? $(".qty_ret").html(dataResult.errors.qty_ret) : $(".qty_ret").html("");
                    dataResult.errors.price_ret !== undefined ? $(".price_ret").html(dataResult.errors.price_ret) : $(".price_ret").html("");
                } else {
                    $(".return_date").html("");
                    $(".qty_ret").html("");
                    $(".price_ret").html("");
                    $("#sales_return")[0].reset();
                    window.location.href = "sales-return-listing.php?store="+store;
                    loading_hide('.save_loader_show', 'save');
                }
            }
        }
    });
});
$(document).on("submit", "#dealer", function (e) {
    e.preventDefault();
    var form_data = new FormData($("#dealer")[0]);
    form_data.append('routine_name', 'dealer');
    form_data.append('store', store);

    $.ajax({
        url: "ajax_call.php",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        beforeSend: function () {
            loading_show('.save_loader_show');
        },
        success: function (response) {
            var dataResult = JSON.parse(response);
            if (response['code'] != undefined && response['code'] == '403') {
                redirect403();
            }
            else {
                if (dataResult.dataResult == "fail") {
                    dataResult.errors.firstname !== undefined ? $(".firstname").html(dataResult.errors.firstname) : $(".firstname").html("");
                } else {
                    $(".firstname").html("");
                    $("#dealer")[0].reset();
                    window.location.href = "dealers-listing.php?store="+store;
                    loading_hide('.save_loader_show', 'save');
                }

            }
        }
    });
});



$(document).ready(function () {
    $('.filter input').on("keyup", function () {
        var listingID = $(this).parents("table").attr('id');
        var searchKEY = $(this).val();
        var searchKEYLEN = (searchKEY != undefined) ? searchKEY.length : 0;
        if (searchKEYLEN == 0 || searchKEYLEN >= 3) {
            var shopifyApi = $(this).attr('data-apiName');
            var limit = 10;
            var from = $(this).data('from');
            var routineNAME = 'take_' + from + '_shopify_data';
            var fields = $(this).attr("name");
            var searchFields = $(this).attr("name");
            loadShopifyAJAX = $.ajax({
                url: "ajax_call.php",
                type: "post",
                dataType: "json",
                data: {
                    routine_name: routineNAME,
                    shopify_api: shopifyApi,
                    fields: fields,
                    store: store,
                    limit: limit,
                    search_key: searchKEY,
                    listing_id: listingID,
                    search_fields: searchFields,
                    pagination_method: js_loadShopifyDATA.name
                },
                beforeSend: function () {
                    var listingTH = $('#' + listingID + ' thead tr th').length;
                    table_loader("table#" + listingID + " tbody", listingTH);
                },
                success: function (comeback) {
                    if (comeback['code'] != undefined && comeback['code'] == '403') {
                        redirect403();
                    } else if (comeback['outcome'] == 'true') {
                        $('table#' + listingID + ' tbody').html(comeback['html']);
                        $('#' + listingID + 'Pagination').html(comeback['pagination_html']);
                    } else {
                    }
                }
            });
        }
    });
    $('#supplier_id').on("change", function () {
        var st = $("#supplier_id").val();
        $.ajax({
            url: "ajax_call.php",
            type: "post",
            data: {supplier_id: st, routine_name: 'get_supplier_address',store: store},
            success: function (response) {
                var dataResult = JSON.parse(response);
                if (response['code'] != undefined && response['code'] == '403') {
                    redirect403();
                }
                else {
                    $("#supplier_address").val(dataResult["data"]["supplier_address"]);
                }
            },
        });
    });
    $('#dealer_id').on("change", function () {
        var st = $("#dealer_id").val();
        $.ajax({
            url: "ajax_call.php",
            type: "post",
            data: {dealer_id: st, routine_name: 'get_billing_address',store:store},
            success: function (response) {
                var dataResult = JSON.parse(response);
                if (response['code'] != undefined && response['code'] == '403') {
                    redirect403();
                }
                else {
                    $("#shipping_address").val(dataResult["shipping_address"]);
                    $("#billing_address").val(dataResult["billing_address"]);
                }
            },
        });
    });

    $('#same_address').val(this.checked);

    $('#same_address').change(function () {
        if (this.checked) {
            var address = $('#address').val();
            var landmark = $('#landmark').val();
            var city = $('#city').val();
            var state = $('#state').val();
            var country = $('#country').val();
            var phone = $('#phone').val();
            var mobile = $('#mobile').val();
            var postal_code = $('#postal_code').val();
            var gst = $('#gst').val();

            $("#billing_address").val(address);
            $("#billing_landmark").val(landmark);
            $("#billing_city").val(city);
            $("#billing_state").val(state);
            $("#billing_country").val(country);
            $("#billing_phone").val(phone);
            $("#billing_mobile").val(mobile);
            $("#billing_postal_code").val(postal_code);
            $("#billing_gst").val(gst);

        }
    });

    $('#searchbtn').on("click", function () {
        var listingID = $("#" + $(this).attr('tableId') + " table").attr('id');
      searchData(listingID);
    });  
    
    $(document).on("click", ".salesModalClick", function () {
           var sku_value = $(this).data("sku");
           var thisObj = this;
            $.ajax({
            url: "ajax_call.php",
            type: "post",
            data: {sku: sku_value, routine_name: 'fill_sales_modal_sku', store:store},
            success: function (response) {
                var dataResult = JSON.parse(response);
                $(".fill_sku").html(dataResult);
            },
        });
        });

     $("#addSku").click(function () {
        var str = '';
        var dddd = $("#numberDemo").val();
        if ($('.skuInput').length != '0') {
            var numItems = $('.skuInput').length;
            numItems = parseInt(numItems) + parseInt(1);
            var startnum = 2;
            for (var i = 1; i <= dddd; i++) {
                numItems++;
                str += '<tr>';
                str += '<td>' + numItems + '</td>';
                str += '<td><input type="text" class="form-control skuInput set_sku"  name="sku[]"/></td>';
                str += '<td><input type="text"  class="form-control category_name" readonly name="category_name[]" > </td>';
                str += '<td><input type="text"  class="form-control vendor" name="v_sku[]" >  </td>';
              str += '<td class="cls-text-section "><input type="number" class="form-control qty " name="qty[]">\n\<input type="number" class="form-control price" name="price[]"  step=0.01> </td>';
              str += '<td><input type="number"  class="form-control total"  name="taxable[]" step=0.01 > </td>';
              str += '<td> <input type="number"  class="form-control taxable_rate"  name="taxable_rate[]"></td>';
                str += '<td><input type="number"  class="form-control cgst"  name="cgst[]"  step=0.01 > </td>';
                str += '<td><input type="number"  class="form-control sgst" name="sgst[]"  step=0.01>  </td>';
                str += '<td><input type="number"  class="form-control igst"  name="igst[]"  step=0.01>  </td>';
                str += '<td><input type="number"  class="form-control total_amount"  name="grand_total[]"  step=0.01>   </td>';
                str += '<td> <button class="btnDeleteRow btn-zap" type="button" >× </button></td>';
                str += '</tr>';
            }
            $('.dataresult tr:last').after(str);
        }
        else {
            var numItems = $('.skuInput').length;
            dddd = parseInt(numItems) + parseInt(dddd);
            var startnum = 2;
            for (var i = 1; i <= dddd; i++) {
                str += '<tr>';
                str += '<td>' + startnum + '</td>';
                startnum++;
                str += '<td><input type="text" class="form-control skuInput set_sku"  name="sku[]"/></td>';
                str += '<td><input type="text"  class="form-control category_name" name="category_name[]" > </td>';
                str += '<td><input type="text"  class="form-control vendor" name="v_sku[]" >  </td>';
                str += '<td class="cls-text-section "><input type="number" class="form-control qty " name="qty[]"  >\n\<input type="number" class="form-control price" name="price[]"  step=0.01> </td>';
                str += '<td><input type="number"  class="form-control total"  name="taxable[]"  step=0.01 > </td>';
                str += '<td> <input type="number"  class="form-control taxable_rate"  name="taxable_rate[]"></td>';
                str += '<td><input type="number"  class="form-control cgst"  name="cgst[]"  step=0.01> </td>';
                str += '<td><input type="number"  class="form-control sgst" name="sgst[]"  step=0.01>  </td>';
                str += '<td><input type="number"  class="form-control igst"  name="igst[]"  step=0.01>  </td>';
                str += '<td><input type="number"  class="form-control total_amount"  name="grand_total[]"  step=0.01>      </td>';
                str += '<td><button class="btnDeleteRow btn-zap" type="button">× </button></td>';
                str += '</tr>';
            }

            $(".dataresult").html(str);
        }

    });
     $("#addSku_purchase_return").click(function () {
        var str = '';
        var dddd = $("#numberDemo").val();
        if ($('.skuInput').length != '0') {
            var numItems = $('.skuInput').length;
            numItems = parseInt(numItems) + parseInt(1);
            var startnum = 2;
            for (var i = 1; i <= dddd; i++) {
                numItems++;
                str += '<tr>';
                str += '<td>' + numItems + '</td>';
                str += '<td><input type="text" class="form-control skuInput set_barcode"  name="barcode[]"/></td>';
                str += '<td><input type="text" class="form-control skuInput sku" readonly name="sku[]"/></td>';
                str += '<td><input type="text"  class="form-control category_name"  readonly readonly name="category_name[]" > </td>';
                str += '<td><input type="text"  class="form-control" name="v_sku[]" >  </td>';
              str += '<td class="cls-text-section "><input type="number" class="form-control qty " name="qty[]">\n\<input type="number" class="form-control price " name="price[]"> </td>';
                str += '<td><input type="text"  class="form-control total" readonly name="taxable[]" > </td>';
                str += '<td><input type="text"  class="form-control cgst" readonly  name="cgst[]" > </td>';
                str += '<td><input type="text"  class="form-control sgst" readonly name="sgst[]">  </td>';
                str += '<td><input type="text"  class="form-control igst"  readonly name="igst[]" >  </td>';
                str += '<td><input type="number"  class="form-control total" readonly name="total[]" >   </td>';
                str += '<td> <button class="btnDeleteRow btn-zap" type="button">× </button></td>';
                str += '</tr>';
            }
            $('.dataresult tr:last').after(str);
        }
        else {
            var numItems = $('.skuInput').length;
            dddd = parseInt(numItems) + parseInt(dddd);
            var startnum = 2;
            for (var i = 1; i <= dddd; i++) {
                str += '<tr>';
                str += '<td>' + startnum + '</td>';
                startnum++;
                str += '<td><input type="text" class="form-control skuInput set_barcode"  name="barcode[]"/></td>';
                str += '<td><input type="text" class="form-control skuInput sku" readonly name="sku[]"/></td>';
                str += '<td><input type="text"  class="form-control category_name" readonly name="category_name[]" > </td>';
                str += '<td><input type="text"  class="form-control" name="v_sku[]" >  </td>';
                str += '<td class="cls-text-section "><input type="number" class="form-control qty " name="qty[]">\n\<input type="number" class="form-control price " name="price[]"> </td>';
              str += '<td><input type="text"  class="form-control total" readonly name="taxable[]" > </td>';
                str += '<td><input type="text"  class="form-control cgst" readonly name="cgst[]" > </td>';
                str += '<td><input type="text"  class="form-control sgst" readonly name="sgst[]">  </td>';
                str += '<td><input type="text"  class="form-control igst" readonly  name="igst[]" >  </td>';
                str += '<td><input type="number"  class="form-control total_amount" readonly name="grand_total[]" >   </td>';
                str += '<td> <button class="btnDeleteRow btn-zap" type="button">× </button></td>';
                str += '</tr>';
            }

            $(".dataresult").html(str);
        }

    });
    
    $(document).ready(function () {

        $(document).on("change", ".qty", function () {
            console.log("change qty");
            var category_name = $(this).closest("tr").find(".category_name").val().toLowerCase();
            var supplier_name = $(this).closest("form").find("#supplier_id").val();
            if(supplier_name.length > 0){
                var thisObj = this;
                 $.ajax({
                    url: "ajax_call.php",
                    type: "post",
                    data: {supplier_id: supplier_name, routine_name: 'get_supplier_address',store: store},
                    success: function (response) {
                        var dataResult = JSON.parse(response);
                        var supplier_state = dataResult["data"]["supplier_state"];
                        var collection_1array = new Array('rain-bow','anklets', 'armlets', 'bangles', 'brooches', 'bracelets', 'button', 'cufflink', 'charms', 'chains', 'earrings', 'gold earring', 'gold pendant', 'hair clips', 'hathphool', 'jewellery-set', 'mens brooches', 'mens belts', 'mens mracelets', 'mens-chains', 'mala', 'mens-necklaces', 'mens-pendants', 'mens-rings', 'angalsutra-set', 'maang-tikka', 'necklace', 'night-wear', 'nose-pin', 'necklace-set', 'personalize bracelet', 'pendants', 'personalize-mangalsutra', 'personalize pendant', 'pendant sets', 'rudraksha', 'rings', 'rakhi', 'silver bracelets', 'silver earring', 'silver necklace', 'silver necklace Set', 'silver pendant', 'silver pendant set', 'waist belts');
                        var collection_2array = new Array('mix-foil-ballon', 'bedsheet', 'co-ord-set', 'dresses', 'face mask', 'fusion-set', 'jumpsuit', 'kurti', 'kurta', 'shirts', 'mens shirt cut piece', 'mens tshirts', 'night-suit', 'nehru-jacket', 'pagdi', 'pyjama', 'pillow', 'quilt', 'shacket', 'sweater', 'tops', 'throw', 'yoga mat');
                        var collection_6array = new Array('carpet', 'dhurrie', 'face roller', 'jewellery box', 'organizer', 'tray set', 'tissue holder', 'wall decoratives');
                        var collection_9array = new Array('bench', 'chair', 'coaster', 'floor cushion', 'foot stool', 'marble stool', 'marble table', 'ottoman stool', 'pen holder', 'table', 'tray');
                        if(supplier_state == "Rajasthan" || supplier_state == "rajasthan"){
                              var cgstGet = 1.5;
                            var sgstGet = 1.5;
                            var igstGet = 0;
                        if (jQuery.inArray(category_name, collection_1array) > 0) {
                            var cgstGet = 1.5;
                            var sgstGet = 1.5;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_2array) > 0) {
                            var cgstGet = 2.5;
                            var sgstGet = 2.5;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_6array) > 0) {
                            var cgstGet = 6;
                            var sgstGet = 6;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_9array) > 0) {
                            var cgstGet = 9;
                            var sgstGet = 9;
                            var igstGet = 0;
                        }
                      }else{
                           var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 3;
                         if (jQuery.inArray(category_name, collection_1array) > 0) {
                              var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 3;
                        }
                        if (jQuery.inArray(category_name, collection_2array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 5;
                        }
                        if (jQuery.inArray(category_name, collection_6array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 12;
                        }
                        if (jQuery.inArray(category_name, collection_9array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 18;
                        }  
                      }
                      console.log(igstGet   + "igst");
                       console.log(sgstGet + "sgst");
                        if (sgstGet ==  0) {
                            var taxable_rate = igstGet;
                        }else{
                            var taxable_rate = sgstGet;
                        }
                        console.log(taxable_rate + "taxable_rate");
                        $(thisObj).closest("tr").find(".taxable_rate").val(taxable_rate);
                        var qttt = $(thisObj).closest(".cls-text-section").find(".qty").val();
                        var privccc = $(thisObj).closest(".cls-text-section").find(".price").val();
                        var tot = parseFloat(qttt).toFixed(2) * parseFloat(privccc).toFixed(2);
                           $(thisObj).closest("tr").find(".total").val(tot.toFixed(2) || 0);
                        var igstnum = (igstGet / 100) * parseFloat(tot).toFixed(2) || 0;
                        var cgstnum = (cgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
                        var sgstnum = (sgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
                                  
                          console.log(sgstnum + ".....sgstnum");
                        $(thisObj).closest("tr").find(".cgst").val( parseFloat(cgstnum).toFixed(2) || 0);
                        $(thisObj).closest("tr").find(".sgst").val(parseFloat(sgstnum).toFixed(2) || 0);
                        $(thisObj).closest("tr").find(".igst").val(parseFloat(igstnum).toFixed(2) || 0);
                           var total_amount = parseFloat(tot) +parseFloat(cgstnum) +parseFloat(sgstnum)+ parseFloat(igstnum) || 0;
                        $(thisObj).closest("tr").find(".total_amount").val( total_amount.toFixed(2) || 0);
                        footer_total_purchase();
                    },
                });
            }else{
                alert("Please select supplier");
            }
        });

        $(document).on("change", ".price", function () {
             var category_name = $(this).closest("tr").find(".category_name").val().toLowerCase();
             console.log(category_name + "   category_name");
            var supplier_name = $(this).closest("form").find("#supplier_id").val();
            console.log(supplier_name + "  supplier_name");
            if(supplier_name.length > 0){
                var thisObj = this;
                 $.ajax({
                    url: "ajax_call.php",
                    type: "post",
                    data: {supplier_id: supplier_name, routine_name: 'get_supplier_address', store: store},
                    success: function (response) {
                        var dataResult = JSON.parse(response);
                        console.log(dataResult);
                       var supplier_state = dataResult["data"]["supplier_state"];
                       console.log(supplier_state);
                         var collection_1array = new Array('rain-bow','anklets', 'armlets', 'bangles', 'brooches', 'bracelets', 'button', 'cufflink', 'charms', 'chains', 'earrings', 'gold earring', 'gold pendant', 'hair clips', 'hathphool', 'jewellery-set', 'mens brooches', 'mens belts', 'mens mracelets', 'mens-chains', 'mala', 'mens-necklaces', 'mens-pendants', 'mens-rings', 'angalsutra-set', 'maang-tikka', 'necklace', 'night-wear', 'nose-pin', 'necklace-set', 'personalize bracelet', 'pendants', 'personalize-mangalsutra', 'personalize pendant', 'pendant sets', 'rudraksha', 'rings', 'rakhi', 'silver bracelets', 'silver earring', 'silver necklace', 'silver necklace Set', 'silver pendant', 'silver pendant set', 'waist belts');
                        var collection_2array = new Array('mix-foil-ballon', 'bedsheet', 'co-ord-set', 'dresses', 'face mask', 'fusion-set', 'jumpsuit', 'kurti', 'kurta', 'shirts', 'mens shirt cut piece', 'mens tshirts', 'night suit', 'nehru-jacket', 'pagdi', 'pyjama', 'pillow', 'quilt', 'shacket', 'sweater', 'tops', 'throw', 'yoga mat');
                        var collection_6array = new Array('carpet', 'dhurrie', 'face roller', 'jewellery box', 'organizer', 'tray set', 'tissue holder', 'wall decoratives');
                        var collection_9array = new Array('bench', 'chair', 'coaster', 'floor cushion', 'foot stool', 'marble stool', 'marble table', 'ottoman stool', 'pen holder', 'table', 'tray');
                         if(supplier_state == "Rajasthan" || supplier_state == "rajasthan"){
                              var cgstGet = 1.5;
                            var sgstGet = 1.5;
                            var igstGet = 0;
                            console.log(jQuery.inArray(category_name, collection_1array) + " 1OR 0");
                        if (jQuery.inArray(category_name, collection_1array) > 0) {
                            var cgstGet = 1.5;
                            var sgstGet = 1.5;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_2array) > 0 ) {
                            var cgstGet = 2.5;
                            var sgstGet = 2.5;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_6array) > 0) {
                            var cgstGet = 6;
                            var sgstGet = 6;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_9array) > 0) {
                            var cgstGet = 9;
                            var sgstGet = 9;
                            var igstGet = 0;
                        }
                      }else{
                          var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 3;
                         if (jQuery.inArray(category_name, collection_1array) > 0) {
                              var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 3;
                        }
                        if (jQuery.inArray(category_name, collection_2array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 5;
                        }
                        if (jQuery.inArray(category_name, collection_6array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 12;
                        }
                        if (jQuery.inArray(category_name, collection_9array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 18;
                        }  
                      }
                     
                    var qttt = $(thisObj).closest(".cls-text-section").find(".qty").val();
                        var privccc = $(thisObj).closest(".cls-text-section").find(".price").val();
                        var tot = parseFloat(qttt).toFixed(2) * parseFloat(privccc).toFixed(2);
                           $(thisObj).closest("tr").find(".total").val(tot.toFixed(2) || 0);
                        var igstnum = (igstGet / 100) * parseFloat(tot).toFixed(2) || 0;
                        var cgstnum = (cgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
                        var sgstnum = (sgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
                                  
                          console.log(sgstnum + ".....sgstnum");
                        $(thisObj).closest("tr").find(".cgst").val( parseFloat(cgstnum).toFixed(2) || 0);
                        $(thisObj).closest("tr").find(".sgst").val(parseFloat(sgstnum).toFixed(2) || 0);
                        $(thisObj).closest("tr").find(".igst").val(parseFloat(igstnum).toFixed(2) || 0);
                        var total_amount = parseFloat(tot) +parseFloat(cgstnum) +parseFloat(sgstnum)+ parseFloat(igstnum) || 0;
                        $(thisObj).closest("tr").find(".total_amount").val( total_amount.toFixed(2) || 0);
                        footer_total_purchase();
                        
                    },
                });
            }else{
                alert("Please select supplier");
            }
        }); 
      $(document).on("click", ".btnDeleteRow", function () {
         var id = $(this).val();
             $('#chkbox_'+ id).prop('checked',false);     
               $(this).closest('tr').remove();
            footer_total_purchase();
        });

        $(document).on("click", ".btnDeleteRowCls", function () {
            alert("This item cannot be remove");
        });

    });

     $("#addSkusale").click(function () {
        var str = '';
        var dddd = $("#numberSale").val();
        if ($('.skuInput').length != '0') {
            var numItems = $('.skuInput').length;
            numItems = parseInt(numItems) + parseInt(1);
            var startnum = 2;
            for (var i = 1; i <= dddd; i++) {
                numItems++;
                str += '<tr>';
                str += '<td>' + startnum + '</td>';
                startnum++;
                str += '<td><input type="text" class="form-control set_barcode"  name="barcode[]"/></td>';
                str += '<td><input type="text" class="form-control skuInput sku set_sku"  name="sku[]"/></td>';
                str += '<td><input type="text"  class="form-control category_name" name="name[]" > </td>';
//                str += '<td><input type="text"  class="form-control vendor" name="v.sku[]" >  </td>';
                str += '<td><input type="text"  class="form-control"  name="metal[]" >  </td>';
                str += '<td><input type="text"  class="form-control"  name="weight[]" >  </td>';
                str += '<td class="cls-text-section"><input type="number" class="form-control qtySale "  name="qty[]"><input type="number"  class="form-control mrp"  name="mrp[]"  step=0.01> </td>';
                str += '  <td> <input type="number" class="form-control discount_rate"  name="discount_rate[]" step=0.1> </td>';
                str += '<td><input type="number"  class="form-control discount" name="discount[]"  step=0.01>  </td>';
                str += '<td><input type="number"  class="form-control taxation"  name="taxation[]"  step=0.01 >  </td>';
                str += '<td><input type="number"  class="form-control cgst"  name="cgst[]"  step=0.01>   </td>';
                str += '<td><input type="number"  class="form-control sgst"  name="sgst[]"  step=0.01>  </td>';
                str += '<td><input type="number"  class="form-control igst"  name="igst[]"  step=0.01>   </td>';
                str += '<td><input type="number"  class="form-control grandtotal"  name="grandtotal[]" step=0.01 >   </td>';
                str += '<td> <button class="btnDeleteSales btn-zap" type="button">× </button></td>';
                str += '</tr>';
            }
            $('.dataresult tr:last').after(str);
        }
        else {
            var numItems = $('.skuInput').length;
            dddd = parseInt(numItems) + parseInt(dddd);
            var startnum = 2;
            for (var i = 1; i <= dddd; i++) {
                str += '<tr>';
                str += '<td>' + startnum + '</td>';
                startnum++;
                str += '<td><input type="text" class="form-control set_barcode"  name="barcode[]"/></td>';
                str += '<td><input type="text" class="form-control skuInput sku set_sku"  name="sku[]"/></td>';
                str += '<td><input type="text"  class="form-control category_name " name="name[]" > </td>';
//                   str += '<td><input type="text"  class="form-control vendor" name="v.sku[]" >  </td>';
                str += '<td><input type="text"  class="form-control"  name="metal[]" >  </td>';
                str += '<td><input type="text"  class="form-control" name="weight[]" >  </td>';
                str += '<td class="cls-text-section"><input type="number" class="form-control qtySale"  name="qty[]"><input type="number"  class="form-control mrp"  name="mrp[]"  step=0.01> </td>';
                str += '  <td> <input type="number" class="form-control discount_rate"  name="discount_rate[]" step=0.1> </td>';
                str += '<td><input type="number"  class="form-control discount" name="discount[]"  step=0.01>  </td>';
                str += '<td><input type="number"  class="form-control taxation"  name="taxation[]" step=0.01 >  </td>';
                str += '<td><input type="number"  class="form-control cgst"  name="cgst[]"  step=0.01>   </td>';
                str += '<td><input type="number"  class="form-control sgst"  name="sgst[]" step=0.01 >  </td>';
                str += '<td><input type="number"  class="form-control igst"  name="igst[]" step=0.01 >   </td>';
                str += '<td><input type="number"  class="form-control grandtotal"  name="grandtotal[]"  step=0.01>   </td>';
                str += '<td> <button class="btnDeleteSales btn-zap" type="button">× </button></td>';
                str += '</tr>';
            }

            $(".dataresult").html(str);
        }

    });
    $("#addSkusale_return").click(function () {
        var str = '';
        var dddd = $("#numberSale").val();
        if ($('.skuInput').length != '0') {
            var numItems = $('.skuInput').length;
            numItems = parseInt(numItems) + parseInt(1);
            var startnum = 2;
            for (var i = 1; i <= dddd; i++) {
                numItems++;
                str += '<tr>';
                str += '<td>' + startnum + '</td>';
                startnum++;
                str += '<td><input type="text" class="form-control set_barcode"  name="barcode[]"/></td>';
                str += '<td><input type="text" class="form-control skuInput sku set_sku"  name="sku[]"/></td>';
                str += '<td><input type="text"  class="form-control category_name" name="name[]" > </td>';
                str += '<td><input type="text"  class="form-control vendor" name="v.sku[]" >  </td>';
                str += '<td><input type="text"  class="form-control"  name="metal[]" >  </td>';
                str += '<td><input type="text"  class="form-control"  name="invchallan_date[]" >  </td>';
                str += '<td><input type="text"  class="form-control"  name="invchallan_no[]" >  </td>';
                str += '<td><input type="text"  class="form-control"  name="weight[]" >  </td>';
                str += '<td class="cls-text-section"><input type="number" class="form-control qtySale "  name="qty[]"><input type="number"  class="form-control mrp"  name="mrp[]"  step=0.01> </td>';
//                str += '  <td> <input type="number" class="form-control discount_rate"  name="discount_rate[]" step=0.1> </td>';
//                str += '<td><input type="number"  class="form-control discount" name="discount[]"  step=0.01>  </td>';
                str += '<td><input type="number"  class="form-control taxation"  name="taxation[]"  step=0.01 >  </td>';
                str += '<td><input type="number"  class="form-control cgst"  name="cgst[]"  step=0.01>   </td>';
                str += '<td><input type="number"  class="form-control sgst"  name="sgst[]"  step=0.01>  </td>';
                str += '<td><input type="number"  class="form-control igst"  name="igst[]"  step=0.01>   </td>';
                str += '<td><input type="number"  class="form-control grandtotal"  name="grandtotal[]" step=0.01 >   </td>';
                str += '<td> <button class="btnDeleteSales btn-zap" type="button">× </button></td>';
                str += '</tr>';
            }
            $('.dataresult tr:last').after(str);
        }
        else {
            var numItems = $('.skuInput').length;
            dddd = parseInt(numItems) + parseInt(dddd);
            var startnum = 2;
            for (var i = 1; i <= dddd; i++) {
                str += '<tr>';
                str += '<td>' + startnum + '</td>';
                startnum++;
                str += '<td><input type="text" class="form-control set_barcode"  name="barcode[]"/></td>';
                str += '<td><input type="text" class="form-control skuInput sku set_sku"  name="sku[]"/></td>';
                str += '<td><input type="text"  class="form-control category_name " name="name[]" > </td>';
                str += '<td><input type="text"  class="form-control vendor" name="v.sku[]" >  </td>';
                str += '<td><input type="text"  class="form-control"  name="metal[]" >  </td>';
                str += '<td><input type="text"  class="form-control"  name="invchallan_date[]" >  </td>';
                str += '<td><input type="text"  class="form-control" name="weight[]" >  </td>';
                str += '<td class="cls-text-section"><input type="number" class="form-control qtySale"  name="qty[]"><input type="number"  class="form-control mrp"  name="mrp[]"  step=0.01> </td>';
//                str += '<td> <input type="number" class="form-control discount_rate"  name="discount_rate[]" step=0.1> </td>';
//                str += '<td><input type="number"  class="form-control discount" name="discount[]"  step=0.01>  </td>';
                str += '<td><input type="number"  class="form-control taxation"  name="taxation[]" step=0.01 >  </td>';
                str += '<td><input type="number"  class="form-control cgst"  name="cgst[]"  step=0.01>   </td>';
                str += '<td><input type="number"  class="form-control sgst"  name="sgst[]" step=0.01 >  </td>';
                str += '<td><input type="number"  class="form-control igst"  name="igst[]" step=0.01 >   </td>';
                str += '<td><input type="number"  class="form-control grandtotal"  name="grandtotal[]"  step=0.01>   </td>';
                str += '<td> <button class="btnDeleteSales btn-zap" type="button">× </button></td>';
                str += '</tr>';
            }

            $(".dataresult").html(str);
        }

    });
    
    $(document).ready(function () {
$(document).on("change", ".qtySale", function () {
             var category_name = $(this).closest("tr").find(".category_name").val();
            var dealer_name = $(this).closest("form").find("#dealer_id").val();
            if(dealer_name.length > 0){
                var thisObj = this;
                 $.ajax({
                    url: "ajax_call.php",
                    type: "post",
                    data: {dealer_id: dealer_name, routine_name: 'get_billing_address' , store: store},
                    success: function (response) {
                        var dataResult = JSON.parse(response);
                       var dealer_city = dataResult["state"];
                       console.log(dealer_city);
                        var collection_1array = new Array('rain-bow','anklets', 'armlets', 'bangles', 'brooches', 'bracelets', 'button', 'cufflink', 'charms', 'chains', 'earrings', 'gold earring', 'gold pendant', 'hair clips', 'hathphool', 'jewellery-set', 'mens brooches', 'mens belts', 'mens mracelets', 'mens-chains', 'mala', 'mens-necklaces', 'mens-pendants', 'mens-rings', 'angalsutra-set', 'maang-tikka', 'necklace', 'night-wear', 'nose-pin', 'necklace-set', 'personalize bracelet', 'pendants', 'personalize-mangalsutra', 'personalize pendant', 'pendant sets', 'rudraksha', 'rings', 'rakhi', 'silver bracelets', 'silver earring', 'silver necklace', 'silver necklace Set', 'silver pendant', 'silver pendant set', 'waist belts');
                        var collection_2array = new Array('mix-foil-ballon', 'bedsheet', 'co-ord-set', 'dresses', 'face mask', 'fusion-set', 'jumpsuit', 'kurti', 'kurta', 'shirts', 'mens shirt cut piece', 'mens tshirts', 'night suit', 'nehru-jacket', 'pagdi', 'pyjama', 'pillow', 'quilt', 'shacket', 'sweater', 'tops', 'throw', 'yoga mat');
                        var collection_6array = new Array('carpet', 'dhurrie', 'face roller', 'jewellery box', 'organizer', 'tray set', 'tissue holder', 'wall decoratives');
                        var collection_9array = new Array('bench', 'chair', 'coaster', 'floor cushion', 'foot stool', 'marble stool', 'marble table', 'ottoman stool', 'pen holder', 'table', 'tray');
                        if(dealer_city == "Rajasthan" || dealer_city == "rajasthan"){
                             var cgstGet = 1.5;
                            var sgstGet = 1.5;
                            var igstGet = 0;
                           if (jQuery.inArray(category_name, collection_1array) > 0) {
                            var cgstGet = 1.5;
                            var sgstGet = 1.5;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_2array) > 0 ) {
                            var cgstGet = 2.5;
                            var sgstGet = 2.5;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_6array) > 0) {
                            var cgstGet = 6;
                            var sgstGet = 6;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_9array) > 0) {
                            var cgstGet = 9;
                            var sgstGet = 9;
                            var igstGet = 0;
                        }
                      }else{
                          console.log("not in rajsthan");
                          var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 3;
                         if (jQuery.inArray(category_name, collection_1array) > 0) {
                              var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 3;
                        }
                        if (jQuery.inArray(category_name, collection_2array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 5;
                        }
                        if (jQuery.inArray(category_name, collection_6array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 12;
                        }
                        if (jQuery.inArray(category_name, collection_9array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 18;
                        }  
                      }
                      var qttt = $(thisObj).closest(".cls-text-section").find(".qtySale").val();
                        var privccc = $(thisObj).closest(".cls-text-section").find(".mrp").val();
                        var tot = parseFloat(qttt).toFixed(2) * parseFloat(privccc).toFixed(2) || 0;
                        var discount_rate = $(thisObj).closest("tr").find(".discount_rate").val() || 0;
                        if(discount_rate > 0){
                            var discount = tot * (discount_rate/100) || 0;      
                            $(thisObj).closest("tr").find(".discount").val(discount.toFixed(2) || 0);
                            var tot = tot - discount;
                            $(thisObj).closest("tr").find(".taxation").val(tot.toFixed(2) || 0);
                        }
                        $(thisObj).closest("tr").find(".taxation").val(tot.toFixed(2) || 0);
                        var igstnum = (igstGet / 100) * parseFloat(tot).toFixed(2) || 0;
                        var cgstnum = (cgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
                        var sgstnum = (sgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
                                  
                        console.log(sgstnum + ".....sgstnum");
                        $(thisObj).closest("tr").find(".cgst").val( parseFloat(cgstnum).toFixed(2) || 0);
                        $(thisObj).closest("tr").find(".sgst").val(parseFloat(sgstnum).toFixed(2) || 0);
                        $(thisObj).closest("tr").find(".igst").val(parseFloat(igstnum).toFixed(2) || 0);
                        var total_amount = parseFloat(tot) +parseFloat(cgstnum) +parseFloat(sgstnum)+ parseFloat(igstnum) || 0;
                        $(thisObj).closest("tr").find(".grandtotal").val( total_amount.toFixed(2) || 0);
                        footer_total_sales();
                    },
                });
            }else{
                alert("Please select dealer");
            }
            
        });

        $(document).on("change", ".mrp", function () {
            var category_name = $(this).closest("tr").find(".category_name").val();
            var dealer_name = $(this).closest("form").find("#dealer_id").val();
            if(dealer_name.length > 0){
                var thisObj = this;
                 $.ajax({
                    url: "ajax_call.php",
                    type: "post",
                    data: {dealer_id: dealer_name, routine_name: 'get_billing_address'},
                    success: function (response) {
                        var dataResult = JSON.parse(response);
                       var dealer_city = dataResult["state"];
                       console.log(dealer_city);
                         var collection_1array = new Array('rain-bow','anklets', 'armlets', 'bangles', 'brooches', 'bracelets', 'button', 'cufflink', 'charms', 'chains', 'earrings', 'gold earring', 'gold pendant', 'hair clips', 'hathphool', 'jewellery-set', 'mens brooches', 'mens belts', 'mens mracelets', 'mens-chains', 'mala', 'mens-necklaces', 'mens-pendants', 'mens-rings', 'angalsutra-set', 'maang-tikka', 'necklace', 'night-wear', 'nose-pin', 'necklace-set', 'personalize bracelet', 'pendants', 'personalize-mangalsutra', 'personalize pendant', 'pendant sets', 'rudraksha', 'rings', 'rakhi', 'silver bracelets', 'silver earring', 'silver necklace', 'silver necklace Set', 'silver pendant', 'silver pendant set', 'waist belts');
                        var collection_2array = new Array('mix-foil-ballon', 'bedsheet', 'co-ord-set', 'dresses', 'face mask', 'fusion-set', 'jumpsuit', 'kurti', 'kurta', 'shirts', 'mens shirt cut piece', 'mens tshirts', 'night suit', 'nehru-jacket', 'pagdi', 'pyjama', 'pillow', 'quilt', 'shacket', 'sweater', 'tops', 'throw', 'yoga mat');
                        var collection_6array = new Array('carpet', 'dhurrie', 'face roller', 'jewellery box', 'organizer', 'tray set', 'tissue holder', 'wall decoratives');
                        var collection_9array = new Array('bench', 'chair', 'coaster', 'floor cushion', 'foot stool', 'marble stool', 'marble table', 'ottoman stool', 'pen holder', 'table', 'tray');
                        if(dealer_city == "Rajasthan" || dealer_city == "rajasthan"){
                             var cgstGet = 1.5;
                            var sgstGet = 1.5;
                            var igstGet = 0;
                           if (jQuery.inArray(category_name, collection_1array) > 0) {
                            var cgstGet = 1.5;
                            var sgstGet = 1.5;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_2array) > 0) {
                            var cgstGet = 2.5;
                            var sgstGet = 2.5;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_6array) > 0) {
                            var cgstGet = 6;
                            var sgstGet = 6;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_9array) > 0) {
                            var cgstGet = 9;
                            var sgstGet = 9;
                            var igstGet = 0;
                        }
                      }else{
                          console.log("not in rajsthan");
                           var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 3;
                         if (jQuery.inArray(category_name, collection_1array) > 0) {
                              var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 3;
                        }
                        if (jQuery.inArray(category_name, collection_2array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 5;
                        }
                        if (jQuery.inArray(category_name, collection_6array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 12;
                        }
                        if (jQuery.inArray(category_name, collection_9array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 18;
                        }  
                      }
                       var qttt = $(thisObj).closest(".cls-text-section").find(".qtySale").val();
                        var privccc = $(thisObj).closest(".cls-text-section").find(".mrp").val();
                        var tot = parseFloat(qttt).toFixed(2) * parseFloat(privccc).toFixed(2) || 0;
                        var discount_rate = $(thisObj).closest("tr").find(".discount_rate").val() || 0;
                        if(discount_rate > 0){
                            var discount = tot * (discount_rate/100) || 0;      
                            $(thisObj).closest("tr").find(".discount").val(discount.toFixed(2) || 0);
                            var tot = tot - discount;
                            $(thisObj).closest("tr").find(".taxation").val(tot.toFixed(2) || 0);
                        }
                        $(thisObj).closest("tr").find(".taxation").val(tot.toFixed(2) || 0);
                        var igstnum = (igstGet / 100) * parseFloat(tot).toFixed(2) || 0;
                        var cgstnum = (cgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
                        var sgstnum = (sgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
                                  
                        console.log(sgstnum + ".....sgstnum");
                        $(thisObj).closest("tr").find(".cgst").val( parseFloat(cgstnum).toFixed(2) || 0);
                        $(thisObj).closest("tr").find(".sgst").val(parseFloat(sgstnum).toFixed(2) || 0);
                        $(thisObj).closest("tr").find(".igst").val(parseFloat(igstnum).toFixed(2) || 0);
                        var total_amount = parseFloat(tot) +parseFloat(cgstnum) +parseFloat(sgstnum)+ parseFloat(igstnum) || 0;
                        $(thisObj).closest("tr").find(".grandtotal").val( total_amount.toFixed(2) || 0);
                        footer_total_sales();
//                        
//                        
//                        var qttt = $(thisObj).closest(".cls-text-section").find(".qtySale").val();
//                        var privccc = $(thisObj).closest(".cls-text-section").find(".mrp").val();
//                        var tot = parseFloat(qttt).toFixed(2) * parseFloat(privccc).toFixed(2) || 0;
//                       
//                        $(thisObj).closest("tr").find(".taxation").val(tot.toFixed(2) || 0);
//                        var igstnum = (igstGet / 100) * parseFloat(tot).toFixed(2) || 0;
//                        var cgstnum = (cgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
//                        var sgstnum = (sgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
//                        $(thisObj).closest("tr").find(".cgst").val( parseFloat(cgstnum).toFixed(2) || 0);
//                        $(thisObj).closest("tr").find(".sgst").val(parseFloat(sgstnum).toFixed(2) || 0);
//                        $(thisObj).closest("tr").find(".igst").val(parseFloat(igstnum).toFixed(2) || 0);
//                        var total_amount = parseFloat(tot) +parseFloat(cgstnum) +parseFloat(sgstnum)+ parseFloat(igstnum) || 0;
//                        $(thisObj).closest("tr").find(".grandtotal").val( total_amount.toFixed(2) || 0);
//                        footer_total_sales();
                    },
                });
            }else{
                alert("Please select dealer");
            }
        });
         $(document).on("change", ".discount_rate", function () {
                var category_name = $(this).closest("tr").find(".category_name").val();
            var dealer_name = $(this).closest("form").find("#dealer_id").val();
            if(dealer_name.length > 0){
                var thisObj = this;
                 $.ajax({
                    url: "ajax_call.php",
                    type: "post",
                    data: {dealer_id: dealer_name, routine_name: 'get_billing_address'},
                    success: function (response) {
                        var dataResult = JSON.parse(response);
                        var dealer_city = dataResult["state"];
                        var collection_1array = new Array('rain-bow', 'anklets', 'armlets', 'bangles', 'brooches', 'bracelets', 'button', 'cufflink', 'charms', 'chains', 'earrings', 'gold earring', 'gold pendant', 'hair clips', 'hathphool', 'jewellery-set', 'mens brooches', 'mens belts', 'mens mracelets', 'mens-chains', 'mala', 'mens-necklaces', 'mens-pendants', 'mens-rings', 'angalsutra-set', 'maang-tikka', 'necklace', 'night-wear', 'nose-pin', 'necklace-set', 'personalize bracelet', 'pendants', 'personalize-mangalsutra', 'personalize pendant', 'pendant sets', 'rudraksha', 'rings', 'rakhi', 'silver bracelets', 'silver earring', 'silver necklace', 'silver necklace Set', 'silver pendant', 'silver pendant set', 'waist belts');
                        var collection_2array = new Array('mix-foil-ballon', 'bedsheet', 'co-ord-set', 'dresses', 'face mask', 'fusion-set', 'jumpsuit', 'kurti', 'kurta', 'shirts', 'mens shirt cut piece', 'mens tshirts', 'night suit', 'nehru-jacket', 'pagdi', 'pyjama', 'pillow', 'quilt', 'shacket', 'sweater', 'tops', 'throw', 'yoga mat');
                        var collection_6array = new Array('carpet', 'dhurrie', 'face roller', 'jewellery box', 'organizer', 'tray set', 'tissue holder', 'wall decoratives');
                        var collection_9array = new Array('bench', 'chair', 'coaster', 'floor cushion', 'foot stool', 'marble stool', 'marble table', 'ottoman stool', 'pen holder', 'table', 'tray');
                        if(dealer_city == "Rajasthan" || dealer_city == "rajasthan"){
                             var cgstGet = 1.5;
                            var sgstGet = 1.5;
                            var igstGet = 0;
                           if (jQuery.inArray(category_name, collection_1array) > 0) {
                            var cgstGet = 1.5;
                            var sgstGet = 1.5;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_2array) > 0) {
                            var cgstGet = 2.5;
                            var sgstGet = 2.5;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_6array) > 0) {
                            var cgstGet = 6;
                            var sgstGet = 6;
                            var igstGet = 0;
                        }
                        if (jQuery.inArray(category_name, collection_9array) > 0) {
                            var cgstGet = 9;
                            var sgstGet = 9;
                            var igstGet = 0;
                        }
                      }else{
                          console.log("not in rajsthan");
                           var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 3;
                         if (jQuery.inArray(category_name, collection_1array) > 0) {
                              var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 3;
                        }
                        if (jQuery.inArray(category_name, collection_2array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 5;
                        }
                        if (jQuery.inArray(category_name, collection_6array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 12;
                        }
                        if (jQuery.inArray(category_name, collection_9array) > 0) {
                               var cgstGet = 0;
                            var sgstGet = 0;
                            var igstGet = 18;
                        }  
                      }
                       var qttt = $(thisObj).closest("tr").find(".qtySale").val();
                        var privccc = $(thisObj).closest("tr").find(".mrp").val();
                        var tot = parseFloat(qttt).toFixed(2) * parseFloat(privccc).toFixed(2) || 0;
                        var discount_rate = $(thisObj).closest("tr").find(".discount_rate").val() || 0;
                        if(discount_rate > 0){
                            var discount = tot * (discount_rate/100) || 0;      
                            $(thisObj).closest("tr").find(".discount").val(discount.toFixed(2) || 0);
                            var tot = tot - discount;
                            $(thisObj).closest("tr").find(".taxation").val(tot.toFixed(2) || 0);
                        }
                        $(thisObj).closest("tr").find(".taxation").val(tot.toFixed(2) || 0);
                        var igstnum = (igstGet / 100) * parseFloat(tot).toFixed(2) || 0;
                        var cgstnum = (cgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
                        var sgstnum = (sgstGet / 100) *  parseFloat(tot).toFixed(2) || 0;
                                  
                        console.log(sgstnum + ".....sgstnum");
                        $(thisObj).closest("tr").find(".cgst").val( parseFloat(cgstnum).toFixed(2) || 0);
                        $(thisObj).closest("tr").find(".sgst").val(parseFloat(sgstnum).toFixed(2) || 0);
                        $(thisObj).closest("tr").find(".igst").val(parseFloat(igstnum).toFixed(2) || 0);
                        var total_amount = parseFloat(tot) +parseFloat(cgstnum) +parseFloat(sgstnum)+ parseFloat(igstnum) || 0;
                        $(thisObj).closest("tr").find(".grandtotal").val( total_amount.toFixed(2) || 0);
                        footer_total_sales();
                    },
                });
            }else{
                alert("Please select dealer");
            }
        });
        $(document).on("change", ".qtySale", function () {
            footer_total_sales();
        });
     
//        $(document).on("change", ".qty", function () {
//            footer_total_purchase();
//        });

        $(document).on("click", ".btnDeleteSales", function () {
            $(this).closest('tr').remove();
            footer_total_sales();
        });
        $(document).on("click", ".btnDeleteRowSales", function () {
            alert("This item cannot be remove");
        });
    });
    $('tbody').on('click', '.chkbox', function () {
        if ($(this).is(":checked")) {
            var id = this.id;
            var splitid = id.split("_");
            var pid = splitid[1];
            console.log(pid);
            salesreturn_productlisting(pid);
        }
    });
    function get_purchase_return_value(routine_name, store, id,purchase_id, for_data) {
    $.ajax({
        url: "ajax_call.php",
        type: "post",
        dataType: "json",
        data: {'routine_name': routine_name, 'store': store, 'id': id, 'purchase_id':purchase_id,'for_data': for_data},
        success: function (comeback) {
            if (comeback['code'] != undefined && comeback['code'] == '403') {
                redirect403();
            } else if (comeback['outcome'] == 'true') {
                if (comeback.po_type == "0") {
                    $("#po_typedirect").val("0").prop("checked", "checked");
                } else {
                    $("#po_typeapproval").val("1").prop("checked", "checked");
                }
                $('#supplier_id').val(comeback['data']['supplier_id']);
                $('#return_date').val(comeback['data']['return_date']);
                $('#ref_num').val(comeback['data']['ref_num']);
                $('#qty_ret').val(comeback['data']['qty_ret']);
                $('#price_ret').val(comeback['data']['price_ret']);
                $('#reason').val(comeback['data']['reason']);
                $(".dataresult").html(comeback['data']['str']);
                footer_total_purchase();
            } else {
            }
        }
    });
}
     function get_productreturn_listing(pid){
        console.log(pid + "sku_id");
         if (pid.length > 0) {
                $.ajax({
                    url: "ajax_call.php",
                    type: "POST",
                    data: {routine_name: "selectForPurchase", id: pid, store: store},
                    success: function (response) {
                        var response = JSON.parse(response);
                        var startnum = 1;
                        var str = '';
                        str += '<tr class="input_' + response.data.id + '">';
                        str += '<input type="hidden" value=' + response.data.id + ' name="sku_id[]"/>';
                        str += '<td>' + startnum + '</td>';
                        startnum++;
                        str += '<td><input type="text" class="form-control skuInput"  name="barcode[]" readonly value=' + response.data.barcode + ' ></td>';
                        str += '<td><input type="text" class="form-control skuInput"  name="sku[]" readonly value=' + response.data.sku + '  ></td>';
                        str += '<td><input type="text"  class="form-control category_name" name="category_name[]"  readonly value=' + response.data.category_name + '> </td>';
                        str += '<td><input type="text"  class="form-control" name="v_sku[]" readonly value=' + response.data.v_sku + ' >  </td>';
                         str += '<td><input type="text"  class="form-control" name="return_date[]" readonly value=' + response.data.return_date + ' >  </td>';
                        str += '<td class="cls-text-section "><input type="number" class="form-control qty " name="qty[]" value=' + response.data.remain_qty + '>\n\<input type="number" class="form-control price " name="price[]" value=' + response.data.price + ' step=0.01> </td>';
                        str += '<td><input type="number"  class="form-control total" readonly name="taxable[]" value=' + response.data.taxable + ' step=0.01> </td>';
                        str += '<td><input type="number"  class="form-control cgst"  name="cgst[]" readonly value=' + response.data.cgst + '  step=0.01> </td>';
                        str += '<td><input type="number"  class="form-control sgst" name="sgst[]" readonly value=' + response.data.sgst + ' step=0.01>  </td>';
                        str += '<td><input type="number"  class="form-control igst"  name="igst[]" readonly value=' + response.data.igst + '  step=0.01>  </td>';
                        str += '<td><input type="number"  class="form-control total_amount"  name="grand_total[]" readonly  value=' + response.data.grand_total + ' >   </td>';
                        str += '<td> <button class="btnDeleteRow btn-zap" type="button" value=' + response.data.id + '>× </button></td>';
                        str += '</tr>';
                        $(".dataresult").append(str);
                        footer_total_purchase();
                    },
                });
            }
         else {
            var id = this.id;
            var splitid = id.split("_");
            var pid = splitid[1];
            $('.btnDeleteRow').closest('.input_' + pid).find(".btnDeleteRow").trigger('click');
            footer_total_purchase();
        }
        
    }
//    $('tbody').on('click', '.chkboxforPurchase', function () {
//        if ($(this).is(":checked")) {
//            var id = this.id;
//            var splitid = id.split("_");
//            var pid = splitid[1];
//            console.log(pid);
//            if (pid.length > 0) {
//                $.ajax({
//                    url: "ajax_call.php",
//                    type: "POST",
//                    data: {routine_name: "selectForPurchase", id: pid,store:store},
//                    success: function (response) {
//                        var response = JSON.parse(response);
//                        var startnum = 1;
//                        var str = '';
//                        str += '<tr class="input_' + response.data.id + '">';
//                        str += '<input type="hidden" value=' + response.data.id + ' name="sku_id[]"/>';
//                        str += '<td>' + startnum + '</td>';
//                        startnum++;
//                        str += '<td><input type="text" class="form-control skuInput"  name="barcode[]" readonly value=' + response.data.barcode + ' ></td>';
//                        str += '<td><input type="text" class="form-control skuInput"  name="sku[]" readonly value=' + response.data.sku + '  ></td>';
//                        str += '<td><input type="text"  class="form-control" name="category_name[]"  readonly value=' + response.data.category_name + '> </td>';
//                        str += '<td><input type="text"  class="form-control" name="v_sku[]" readonly value=' + response.data.v_sku + ' >  </td>';
//                        str += '<td class="cls-text-section "><input type="number" class="form-control qty " name="qty[]" value=' + response.data.remain_qty + '>\n\<input type="number" class="form-control price " name="price[]" value=' + response.data.price + '> </td>';
//                        str += '<td><input type="text"  class="form-control total" readonly name="taxable[]" value=' + response.data.taxable + ' > </td>';
//                        str += '<td><input type="text"  class="form-control cgst"  name="cgst[]" readonly value=' + response.data.cgst + '  > </td>';
//                        str += '<td><input type="text"  class="form-control sgst" name="sgst[]" readonly value=' + response.data.sgst + ' >  </td>';
//                        str += '<td><input type="text"  class="form-control igst"  name="igst[]" readonly value=' + response.data.igst + '  >  </td>';
//                        str += '<td><input type="number"  class="form-control total_amount"  name="grand_total[]" readonly  value=' + response.data.grand_total + ' >   </td>';
//                        str += '<td> <button class="btnDeleteRow btn-zap" type="button">× </button></td>';
//                        str += '</tr>';
//                        $(".dataresult").append(str);
//                        footer_total_purchase();
//                    },
//                });
//            }
//        } else {
//            var id = this.id;
//            var splitid = id.split("_");
//            var pid = splitid[1];
//            $('.btnDeleteRow').closest('.input_' + pid).find(".btnDeleteRow").trigger('click');
//            footer_total_purchase();
//        }
//    });
         $('tbody').on('click', '.chkboxforPurchase', function () {
        if ($(this).is(":checked")) {
            var id = this.id;
            var splitid = id.split("_");
            var pid = splitid[1];
            console.log(pid);
            get_productreturn_listing(pid);
        }
    });
       $(document).on("change", ".set_sku", function () {
           var sku_value = $(this).val();
           var thisObj = this;
            $.ajax({
            url: "ajax_call.php",
            type: "post",
            data: {sku: sku_value, routine_name: 'get_category_name',store:store},
            success: function (response) {
                var dataResult = JSON.parse(response);
                if(dataResult["data"]["check_sku"] == ''){
                    $(thisObj).closest("tr").find(".set_sku").css("border", "2px solid red");
                    $("#purchase-create").prop('disabled', true);
                }else{
                     $(thisObj).closest("tr").find(".set_sku").css("border", "1px solid #ced4da");
                     $("#purchase-create").prop('disabled', false);
                }
                if(dataResult["data"]["check_sku"])
                $(thisObj).closest("tr").find(".category_name").val(dataResult["data"]["category_name"]);
                $(thisObj).closest("tr").find(".vendor").val(dataResult["data"]["vendor"]);
            },
        });
        });
      $(document).on("change", ".set_barcode", function () {
           var sku_value = $(this).val();
           var thisObj = this;
            $.ajax({
            url: "ajax_call.php",
            type: "post",
            data: {sku: sku_value, routine_name: 'get_sku_category_name', store: store},
            success: function (response) {
                var dataResult = JSON.parse(response);
                console.log(dataResult["data"]);
                if(dataResult["data"]["check_sku"] == ''){
                    $(thisObj).closest("tr").find(".set_barcode").css("border", "2px solid red");
                    $("#purchase-create").prop('disabled', true);
                }else{
                    $(thisObj).closest("tr").find(".category_name").val(dataResult["data"]["category_name"]);
                    $(thisObj).closest("tr").find(".sku").val(dataResult["data"]["sku"]);
                    $(thisObj).closest("tr").find(".mrp").val(dataResult["data"]["price"]);
                      $(thisObj).closest("tr").find(".vendor").val(dataResult["data"]["vendor"]);
                    $(thisObj).closest("tr").find(".set_barcode").css("border", "1px solid #ced4da");
                    $("#purchase-create").prop('disabled', false);
                }
            },
        });
        });
        $(document).on("click", ".modalformClick", function () {
           var sku_value = $(this).data("sku");
           var thisObj = this;
            $.ajax({
            url: "ajax_call.php",
            type: "post",
            data: {sku: sku_value, routine_name: 'fill_modal_sku', store:store},
            success: function (response) {
                var dataResult = JSON.parse(response);
                $(".fill_sku").html(dataResult);
            },
        });
        });
});

   $(document).on("click", ".login", function () {
      var email = $("#email").val();
        var password = $("#password").val();

   $.ajax({
            url: "../user/ajax_call.php",
            type: "post",
              data: { routine_name: "sign_in", email: email, password: password ,store:store},
            success: function (response) {
                  var dataResult = JSON.parse(response);
                   $(".login_msg").html(dataResult["data"].errors);
                    if(dataResult["data"].dataResult == "success"){
                          $(".login_msg").css("color","green");
                           setTimeout(function(){ 
                          window.location.href = "index.php?store="+store;
                    }, 3000); 
                    }
                   
            },
        });
   });
   
   $(document).ready(function () {
        $("#countries_id").change(function(){
           var st = $("#countries_id option:selected").attr('data-id');
        $.ajax({
            url: "ajax_call.php",
            type: "post",
            data: {countries_id:st, routine_name: 'get_state',store: store} ,
            success: function (response) {
                    var dataResult = JSON.parse(response);
                    $("#states_id").html(dataResult.data).selectpicker('refresh');
            },
        });
    });
    var st = $("#countries_id option:selected").attr('data-id');
        $.ajax({
            url: "ajax_call.php",
            type: "post",
            data: {countries_id:st, routine_name: 'get_state',store:store} ,
            success: function (response) {
                var dataResult = JSON.parse(response);
                   $("#states_id").html(dataResult.data).selectpicker('refresh');
            },
        });
        
   });
   
    