<?php
function cls_api_call($password, $store, $api_endpoint, $query = array(), $type = 'GET', $request_headers = array()) {
    $url = "https://" . $store . $api_endpoint;
    if (strpos($url, '2020-01') === false) {
        $log_data = array(
            'date' => date('Y-m-d H:i:s'),
            'function_name' => 'shopify_call',
            'url' => $url,
            'debug_backtrace' => debug_backtrace()
        );
//            generate_log('shopify_api_version_correct', json_encode($log_data));
    }
    if (!empty($query) && !is_null($query) && in_array($type, array('GET', 'DELETE'))) {
        $url = $url . "?" . http_build_query($query);
    }
    if (strpos($url, 'page=') !== false) {
        $log_data = array(
            'date' => date('Y-m-d H:i:s'),
            'function_name' => 'shopify_call',
            'url' => $url,
            'debug_backtrace' => debug_backtrace()
        );
//            generate_log('shopify_rest_api_deprecation', json_encode($log_data));
    }
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, TRUE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
    $request_headers[] = "";
    if (!is_null($password))
        $request_headers[] = "X-Shopify-Access-Token: " . $password;
    curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
    if ($type != 'GET' && in_array($type, array('POST', 'PUT'))) {
        if (is_array($query))
            $query = http_build_query($query);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
    }
    $response = curl_exec($curl);
    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    $header = substr($response, 0, $header_size);
    $body = substr($response, $header_size);
    $error_number = curl_errno($curl);
    $error_message = curl_error($curl);
    curl_close($curl);
    if ($error_number) {
        return $error_message;
    } else {
        $response = preg_split("@\r?\n\r?\nHTTP/@u", $response);
        $response = (count($response) > 1 ? 'HTTP/' : '') . array_pop($response);
        $response = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);

        $headers = array();
        $header_data = explode("\n", $response[0]);
        $headers['status'] = $header_data[0]; // Does not contain a key, have to explicitly set
        array_shift($header_data); // Remove status, we've already set it above
        foreach ($header_data as $part) {
            $h = explode(":", $part, 2);
            $key = (strcasecmp(trim($h[0]), "link") == 0) ? strtolower(trim($h[0])) : trim($h[0]);
            $headers[$key] = trim($h[1]);
        }
        if (!empty($headers['link'])) {
            prepare_next_prev($headers);
        }

        if (isset($headers['X-Shopify-API-Deprecated-Reason'])) {
            $log_data = array(
                'date' => date('Y-m-d H:i:s'),
                'function_name' => 'shopify_call',
                'headers' => $headers,
                'url' => $url,
                'debug_backtrace' => debug_backtrace()
            );
//                generate_log('shopify_api_deprecation', json_encode($log_data));
        }

        if (isset($headers['X-Shopify-Api-Version-Warning'])) {
            $log_data = array(
                'date' => date('Y-m-d H:i:s'),
                'function_name' => 'shopify_call',
                'headers' => $headers,
                'url' => $url,
                'debug_backtrace' => debug_backtrace()
            );
//                generate_log('shopify_api_version_deprecation_warning', json_encode($log_data));
        }

        return array('headers' => $headers, 'response' => $response[1]);
    }
}

function cls_api_call1($password, $store, $shopify_endpoint, $query = array(), $type = '', $request_headers = array()) {
    $cls_shopify_url = "https://" . $api_key . ":" . $password . "@" . $store . $shopify_endpoint;

    if (!is_array($type) && !is_object($type)) {
        (array) $type;
    }
    if (!is_null($query) && in_array($type, array('GET', 'DELETE')))
        $cls_shopify_url = $cls_shopify_url . "?" . http_build_query($query);

    $curl = curl_init($cls_shopify_url);
    curl_setopt($curl, CURLOPT_HEADER, TRUE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
    $request_headers[] = "";
    if (!is_null($password))
        $request_headers[] = "X-Shopify-Access-Token: " . $password;
    curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);


    if ($type != 'GET' && in_array($type, array('POST', 'PUT'))) {
        if (is_array($query))
            $query = http_build_query($query);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
    }
    $comeback = curl_exec($curl);


    $error_number = curl_errno($curl);
    $error_message = curl_error($curl);
    curl_close($curl);
    if ($error_number) {
        return $error_message;
    } else {
        $comeback = preg_split("/\r\n\r\n|\n\n|\r\r/", $comeback, 2);
        $headers = array();
        $header_data = explode("\n", $comeback[0]);
        $headers['status'] = $header_data[0];
        array_shift($header_data);
        foreach ($header_data as $part) {
            $h = explode(":", $part);
            $headers[trim($h[0])] = trim($h[1]);
        }

        return array('headers' => $headers, 'response' => $comeback[1]);
    }
}

function shopify_call($token, $shop, $api_endpoint, $query = array(), $method = 'GET', $request_headers = array()) {
    $url = "https://" . $shop . $api_endpoint;
    if (!empty($query) && !is_null($query) && in_array($method, array('GET', 'DELETE'))) {
        $url = $url . "?" . http_build_query($query);
    } else {
        $url = $url;
    }
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, TRUE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($curl, CURLOPT_TIMEOUT, 60);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    //$request_headers[] = "";
    if (!is_null($token)) {
        $request_headers[] = "X-Shopify-Access-Token: " . $token;
    }
    curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
    if ($method != 'GET' && in_array($method, array('POST', 'PUT'))) {
        if (is_array($query)) {
            $query = json_encode($query);
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
    }
    $response = curl_exec($curl);
    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    $header = substr($response, 0, $header_size);
    $body = substr($response, $header_size);
    $error_number = curl_errno($curl);
    $error_message = curl_error($curl);
    curl_close($curl);
    if ($error_number) {
        return $error_message;
        
        
        
    } else {
        return array('headers' => $header, 'response' => $body);
    }
}

function cls_api_graphql_call($api_key, $password, $store, $shopify_endpoint, $query = '', $type = '', $request_headers = array()) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $shopify_endpoint,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $type,
        CURLOPT_POSTFIELDS => "{ 
    productVariants(first: 10, query: \"sku:$query\") {
      edges {
      node {
        id
        title
        sku
        price
         product {
        collections(first: 10) {
          edges {
            node {
              handle
              title
            }
          }
        }
      }
        barcode
        inventoryItem {
        id
        }
        displayName
        inventoryQuantity
        product {
          id
          vendor
          title
        }
      }
    }
    }
  }",
        CURLOPT_HTTPHEADER => array(
            //    "authorization: Basic MzhlNzBmNmExNzVlMjczN2Y0NWM3ZmQ2NjYzMjc5ZDI6c2hwcGFfODc0ZjcyMjU3ZWUxYmE2MmY5ZTA0MzA1MmE4MTI0NmY=",
            "cache-control: no-cache",
            "content-type: application/graphql",
            "X-Shopify-Access-Token:$password"
        ),
    ));

    $comeback = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    return array('response' => $comeback);
}
function cls_api_graphql_barcode_call($api_key, $password, $store, $shopify_endpoint, $query = '', $type = '', $request_headers = array()) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $shopify_endpoint,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $type,
        CURLOPT_POSTFIELDS => "{ 
    productVariants(first: 10, query: \"barcode:$query\") {
      edges {
      node {
        id
        title
        sku
        price
         product {
        collections(first: 10) {
          edges {
            node {
              handle
              title
            }
          }
        }
      }
        barcode
        inventoryItem {
        id
        }
        displayName
        inventoryQuantity
        product {
          id
          vendor
          title
        }
      }
    }
    }
  }",
        CURLOPT_HTTPHEADER => array(
            //    "authorization: Basic MzhlNzBmNmExNzVlMjczN2Y0NWM3ZmQ2NjYzMjc5ZDI6c2hwcGFfODc0ZjcyMjU3ZWUxYmE2MmY5ZTA0MzA1MmE4MTI0NmY=",
            "cache-control: no-cache",
            "content-type: application/graphql",
            "X-Shopify-Access-Token:$password"
        ),
    ));

    $comeback = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    return array('response' => $comeback);
}
function cls_inventory_level_graphql_call($api_key, $password, $store, $shopify_endpoint, $query = '', $type = '', $request_headers = array()) {

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $shopify_endpoint,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $type,
        CURLOPT_POSTFIELDS => "{
    inventoryItem (id:\"$query\"){
      id
      inventoryLevels(first:10) {
        edges {
          node {
            id
            available
            location {
              id
             
            }
          }
        }
      }
    }
  }",
     
        CURLOPT_HTTPHEADER => array(
            //    "authorization: Basic MzhlNzBmNmExNzVlMjczN2Y0NWM3ZmQ2NjYzMjc5ZDI6c2hwcGFfODc0ZjcyMjU3ZWUxYmE2MmY5ZTA0MzA1MmE4MTI0NmY=",
            "cache-control: no-cache",
            "content-type: application/graphql",
            "X-Shopify-Access-Token:$password"
        ),
    ));
 
    $comeback = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
   
    return array('response' => $comeback);
}
?>