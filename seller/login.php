<?php include "../user/cls_header.php"; 
    ?>

<body class="theme-orange">
<div class="authentication">
    <div class="card">
        <div class="body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header slideDown">
                        <h1>B2B Management</h1>
                        <ul class="list-unstyled l-social">
<!--                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-facebook-box"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-linkedin-box"></i></a></li>                            
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a></li>-->
                        </ul>
                    </div>                        
                </div>
                 <h6 class="cls_login_msg"><span class="error login_msg"></span></h6>
                <form class="col-lg-12" id="sign_in" method="POST">
                    <h5 class="title">Sign in to your Account</h5>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="email" id="email">
                            <label class="form-label">Email</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                                <input type="password" class="form-control" name="password" id="password">
                            <label class="form-label">Password</label>
                        </div>
                    </div>
                    <div>
                        <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-cyan">
                        <label for="rememberme">Remember Me</label>
                    </div>                        
                </form>
                <div class="col-lg-12">
                    <button  type="submit" name="submit"  class="btn btn-raised btn-primary waves-effect login">SIGN IN</button>
                    <button  class="btn btn-raised btn-default waves-effect">SIGN UP</button>                        
                </div>
                <div class="col-lg-12 m-t-20">
                    <a class="" href="forgot-password.php">Forgot Password?</a>
                </div>                    
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="../assets/bundles/libscripts.bundle.js"></script>    
<script src="../assets/bundles/vendorscripts.bundle.js"></script>
<script src="../assets/bundles/mainscripts.bundle.js"></script>
</body>

</html>