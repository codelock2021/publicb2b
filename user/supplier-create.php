<!doctype html>
<html class="no-js " lang="en">
<?php
include_once 'cls_header.php';
 $obj_Client_functions = new Client_functions($_SESSION['store']);

?>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>
    <!-- Right Sidebar -->
    <!-- Top Bar -->
<?php include 'topbar.php';
include 'sidebar.php';
include 'ri8sidebar.php';
    ?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-11 col-md-11 col-sm-12">
                <h1>Create Supplier
                </h1>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12">
               <a href="supplier-listing.php?store=<?php echo $_SESSION['store'];?>" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                        <div class="body">
                            <form name="frm" id="frm" method="POST" enctype="multipart/form-data"> 
                            <div class="row clearfix">
                                <div class="col-sm-12"> 
                                  <div class="col-sm-12">
                                      
                                    <div class="form-group">
                                    <select class="form-control show-tick" name="supplier_type">
                                            <option value="">Supplier Type</option>
                                            <option value="0">Un-registered</option>
                                            <option value="1">Registered</option>
                                            <option value="2">Composition</option>
                                        </select>
                                    </div>
                                       <span class="error supplier_type"></span>
                                </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Name" name="supplier_name" />
                                        </div>
                                         <span class="error supplier_name"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Contact Person" name="supplier_contact_person" />
                                        </div>
                                         <span class="error supplier_contact_person"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Email" name="supplier_email" />
                                        </div>
                                         <span class="error supplier_email"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea rows="4" class="form-control no-resize" placeholder="Address" name="supplier_address"></textarea>
                                        </div>
                                         <span class="error supplier_address"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Landmark" name="supplier_landmark"/>
                                        </div>
                                         <span class="error supplier_landmark"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Phone" name="supplier_phone" />
                                        </div>
                                         <span class="error supplier_phone"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Mobile" name="supplier_mobile" />
                                        </div>
                                         <span class="error supplier_mobile"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                 <div class="form-line">
                                        <select name="supplier_country" class="form-control show-tick"  id="countries_id" style="width: 100%;" >
                                                <option value="" selected disabled></option>
                                                <?php
                                                  $comeback = $obj_Client_functions->select_result(TABLE_COUNTRY, '*');
                                                

                                                  $data_value = (isset($comeback['data']) && !empty($comeback['data']) ? $comeback['data'] : '');
                                                  foreach ($data_value as $value) {
                                                ?>
                                                <option data-id="<?php echo $value['id'];?>" <?php if($value['id'] == "101") { echo "selected='selected'"; } ?> name="<?php echo $value['name'];?>" value="<?php echo $value['name'];?>" ><?php echo $value['name']; ?></option>
                                                  <?php 
                                                  }  
                                                  ?>     
                                        </select>
                                            <label class="form-label">Country</label>
                                        </div>
                                         </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                 <div class="form-line">
                                            <select name="supplier_state" class="form-control show-tick" id="states_id" style="width: 100%;">
                                                    <option value="" selected disabled>--Select State--</option>
                                            </select>
                                        </div>
                                             <span class="error supplier_state"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="City" name="supplier_city" />
                                        </div>
                                         <span class="error supplier_city"></span>
                                    </div>
                                </div>
<!--                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="City" name="supplier_city" />
                                        </div>
                                         <span class="error supplier_city"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="State" name="supplier_state"/>
                                        </div>
                                         <span class="error supplier_state"></span>
                                    </div>
                                </div>-->
<!--                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Country" name="supplier_country"/>
                                        </div>
                                         <span class="error supplier_country"></span>
                                    </div>
                                </div>-->
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Pincode" name="supplier_pincode" />
                                        </div>
                                         <span class="error supplier_pincode"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Pan"   name="supplier_pan"/>
                                        </div>
                                         <span class="error supplier_pan"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="TIN" name="supplier_tin" />
                                        </div>
                                         <span class="error supplier_tin"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="VAT"   name="supplier_vat"/>
                                        </div>
                                         <span class="error supplier_vat"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="GST" name="supplier_gst" />
                                        </div>
                                         <span class="error supplier_gst"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Bank name" name="supplier_bank_name" />
                                        </div>
                                         <span class="error supplier_bank_name"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Bank Account No" name="supplier_bank_account_no"/>
                                        </div>
                                         <span class="error supplier_bank_account_no"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Bank Branch" name="supplier_bank_branch" />
                                        </div>
                                         <span class="error supplier_bank_branch"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="IFSC Code" name="supplier_ifsc_code" />
                                        </div>
                                         <span class="error supplier_ifsc_code"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea rows="4" class="form-control no-resize" placeholder="Remarks" name="supplier_remarks"></textarea>
                                        </div>
                                         <span class="error supplier_remarks"></span>
                                    </div>
                                </div>
                                   <div class="col-sm-12">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="supplier_status">
                                            <option value="">Status</option>
                                            <option value="0">Active</option>
                                            <option value="1">InActive</option>
                                        </select>
                                    </div>
                                        <span class="error supplier_status"></span>
                                </div>

                            </div>
                            <button type="submit" name="submit"  id="supplier-create" class="btn  btn-raised bg-teal waves-effect">Create </button>
                      </form>
                        </div>
                </div>
            </div>
        </div>
        <!-- #END# Input --> 
     
    </div>
</section>
</div>
</div>
  
</body>


</html>