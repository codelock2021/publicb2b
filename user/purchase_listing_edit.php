<!doctype html>
<html class="no-js " lang="en">
<?php
$id = isset($_GET['pid']) ? $_GET['pid'] : '';
$sid = isset($_GET['sid']) ? $_GET['sid'] : '';
include_once 'cls_header.php';
$obj_Client_functions = new Client_functions($_SESSION['store']);
?>
<script>
var id = "<?php echo $id; ?>";
</script>

<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>
    <!-- Right Sidebar -->
    <!-- Top Bar -->
    <?php  include 'topbar.php';
include 'sidebar.php';
include 'ri8sidebar.php';
    ?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-11 col-md-11 col-sm-12">
                <h1>Create Goods Receive Note
                </h1>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12">
              <a href="purchase-listing.php?store=<?php echo $_SESSION['store'];?>" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                   <form class="m-t" id="register_frm" name="register_frm" method="POST"  enctype="multipart/form-data" onsubmit="">
                       <input type="hidden" id="" name="id" value="<?php echo $id; ?>">
                                <input type="hidden" id="" name="for_data" value="<?php echo 'purchase'; ?>">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-1 col-md-2">
                                <div class="col-sm-2">
                                    <label>P.O.Type</label>
                                    <input name="po_type" type="radio" class="with-gap"   value="1" checked  id="po_type"/>
                                    <label for="radio_3">Direct</label>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <div class="col-sm-2">
                                            <label>Taxation</label>
                                            <input  name="taxation" type="radio" class="with-gap"  value="1" checked id="taxation"/>
                                            <label for="radio_4">Exclude TAX</label>
                                        </div>
                            </div>
                            <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="Freight" name="freight" id="freight" />
                                            </div>
                                              <span class="error freight"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="Packing" name="packing" id="packing"/>
                                            </div>
                                              <span class="error packing"></span>
                                        </div>
                                    </div>
                                     <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="Insurance" name="insurance" id="insurance"/>
                                            </div>
                                              <span class="error insurance"></span>
                                        </div>
                                    </div>
                            <div class="col-sm-3">
                                <label >Purchase Date</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="date" class="form-control" placeholder="Purchase Date" name="po_date" id="po_date"/>
                                    </div>
                                      <span class="error po_date"></span>
                                </div>
                            </div>
                           
                            <div class="col-sm-3">
                                 <label >Supplier Invoice Date</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="date" class="form-control" placeholder="Supplier Invoice Date" name="supplier_invoice_date" id="supplier_invoice_date"/>
                                    </div>
                                      <span class="error supplier_invoice_date"></span>
                                </div>
                            </div>
                              <div class="col-sm-6">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="supplier_id" id="supplier_id">
                      
                                        <?php
                                            $comeback = $obj_Client_functions->select_result(TABLE_SUPPLIERS, '*');
                                            $options = '<option value="">Supplier Name</option>';
                                            foreach($comeback["data"] as $data){
                                            $data = (object)$data;
                                            ?>
                                            <option <?php if($sid==$data->id) { echo "selected='selected'"; } ?>  value="<?php echo $data->id;?>"><?php echo $data->supplier_name.'('.$data->generate_code.')'; ?></option>
                                       <?php }
                                       echo $options;
                                        ?>
                                        </select>
                                    </div>
                                    <span class="error supplier_id"></span>
                                </div>
                         
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea rows="4" class="form-control no-resize" placeholder="Supplier Address" name="supplier_address" id="supplier_address"></textarea>
                                    </div>
                                      <span class="error supplier_address"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea rows="4" class="form-control no-resize" placeholder="Remarks" name="remarks" id="remarks"></textarea>
                                    </div>
                                      <span class="error remarks"></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="reverse_charge" id="reverse_charge">
                                            <option value="">Reverse Charge</option>
                                              <?php
                                                $where_query_arr = array(["", "id", "=", "$id"]);
                                                $comeback = $obj_Client_functions->select_result(TABLE_PURCHASE, '*');
                                                $comeback = (object)$comeback["data"][0];
                                                $comeback = (isset($comeback->reverse_charge) && $comeback->reverse_charge != '') ? $comeback->reverse_charge:'';  
                                              ?>
                                            <option value="" >Status</option>
                                             <option value="1" <?php if($comeback=="1")  {echo "selected";}?>>No</option>
                                              <option value="2" <?php if($comeback=="2")  {echo "selected";}?>>Yes</option>
                                        </select>
                                    </div>
                                       <span class="error reverse_charge"></span>
                                </div> 
                            </div>
                             <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Supplier Invoice No" name="supplier_invoice_no" id="supplier_invoice_no" />
                                    </div>
                                      <span class="error supplier_invoice_no"></span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                               <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Rec. Ref. Code" name="rec_ref_code" id="rec_ref_code" />
                                    </div>
                                     <span class="error rec_ref_code"></span>
                                </div>
                            </div>

                        </div>
                         <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card">
                                        <div class="body">
                                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                   <th>SNo.</th>
                                                        <th>SKU</th>
                                                        <th>Category</th>
                                                        <th>V. SKU  </th>
                                                        <th>Qty/Our.Price   </th>
                                                        <th>Taxable</th>
                                                        <th>CGST</th>
                                                        <th>SGST</th>
                                                        <th>IGST    </th>
                                                        <th>Total Amt   </th>
                                                </tr>
                                            </thead>
                                            <tbody class="edit-product">
                                               
                                            </tbody>
                                             <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>Total</th>
                                                <th><input type="number"  class="form-control total_quantity" name="total_quantity"  step=0.01 > </th>
                                                <th><input type="number"  class="form-control total_taxable" name="total_taxable"  step=0.01></th>
                                                <th><input type="number"  class="form-control total_cgst" name="total_cgst"  step=0.01></th>
                                                <th><input type="number"  class="form-control total_sgst" name="total_sgst"  step=0.01></th>
                                                <th><input type="number"  class="form-control total_igst" name="total_igst"  step=0.01 ></th>
                                                <th><input type="number"  class="form-control grand_amount" name="grand_amount"  step=0.01></th>
                                                  <input type="hidden"  class="form-control all_total" name="all_total" >
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="col-sm-12">
                               <div class="row clearfix">   
                               <div class="col-sm-4">
                                <div class="form-group">
                                        <button type="submit" name="submit"  id="purchase-create" class="btn  btn-raised bg-teal waves-effect">Update </button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- #END# Input --> 
    </div>
</section>
</body>
</html>
<script>
    var routine_name = 'purhcase_select';
    get_purchase_value(routine_name,store, id, "purchase");
</script>