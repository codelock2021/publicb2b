
<?php

if (DB_OBJECT == 'mysql') {
    include ABS_PATH . "/collection/mongo_mysql/mysql/common_function.php";
} else {
    include ABS_PATH . "/collection/mongo_mysql/mongo/common_function.php";
}
include_once ABS_PATH . '/collection/form_validation.php';
include_once ABS_PATH . '/user/cls_load_language_file.php';
include_once '../append/Login.php';

class Client_functions extends common_function {

    public $cls_errors = array();
    public $msg = array();

    public function __construct($shop = '') {
        parent::__construct($shop);
        $this->db = $GLOBALS['conn'];
    }

    function cls_get_shopify_list($shopify_api_name_arr = array(), $shopify_url_param_array = [], $type = '', $shopify_is_object = 1) {
            $shopinfo = $this->current_store_obj;
        $store_user_id = $shopinfo[0]['store_user_id'];
        $store = $shopinfo[0]['shop_name'];
        $password = $shopinfo[0]['password'];
      
        $shopify_url_array = array_merge(array('/admin/' . CLS_API_VERSIION), $shopify_api_name_arr);
        $shopify_main_url = implode('/', $shopify_url_array) . '.json';
        $shopify_data_list = cls_api_call($password, $store, $shopify_main_url, $shopify_url_param_array, $type);
        if ($shopify_is_object) {
            return json_decode($shopify_data_list['response']);
        } else {
            return json_decode($shopify_data_list['response'], TRUE);
        }
    }

    function take_api_shopify_data() {
        $comeback = array('outcome' => 'false', 'report' => CLS_SOMETHING_WENT_WRONG);
        if (isset($_POST['store']) && $_POST['store'] != '' && isset($_POST['shopify_api'])) {
            $shopify_api = $_POST['shopify_api'];
            $shopinfo = $this->current_store_obj;
            $pages = defined('PAGE_PER') ? PAGE_PER : 10;
            $limit = isset($_POST['limit']) ? $_POST['limit'] : $pages;
            $page_no = isset($_POST['pageno']) ? $_POST['pageno'] : '1';
            $shopify_url_param_array = array(
                'limit' => $limit,
                'pageno' => $page_no
            );
            $shopify_api_name_arr = array('main_api' => $shopify_api, 'count' => 'count');
            $filtered_count = $total_product_count = $this->cls_get_shopify_list($shopify_api_name_arr)->count;

            $search_word = isset($_POST['search_keyword']) ? $_POST['search_keyword'] : '';
            if ($search_word != '') {
                $shopify_url_param_array = array_merge($shopify_url_param_array, $this->make_api_search_query($search_word, $_POST['search_fields']));
                $filtered_count = $this->cls_get_shopify_list($shopify_api_name_arr, $shopify_url_param_array)->count;
            }
            $shopify_api_name_arr = array('main_api' => $shopify_api);
            $api_shopify_data_list = $this->cls_get_shopify_list($shopify_api_name_arr, $shopify_url_param_array);
            $tr_html = array();
            if (count($api_shopify_data_list->$shopify_api) > 0) {
                $tr_html = call_user_func(array($this, 'make_api_data_' . $_POST['listing_id']), $api_shopify_data_list);
            }
            $total_pages = ceil($filtered_count / $limit);
            $pagination_html = $this->pagination_btn_html($total_pages, $page_no, $_POST['pagination_method'], $_POST['listing_id']);
            $comeback = array(
                "outcome" => 'true',
                "total_record" => intval($total_product_count),
                "recordsFiltered" => intval($filtered_count),
                'pagination_html' => $pagination_html,
                'html' => $tr_html
            );
            return $comeback;
        }
    }

    function make_api_data_collectionData($api_data_list) {

        $shopinfo = $this->current_store_obj;
        $tr_html = '<tr class="Polaris-ResourceList__ItemWrapper"> <td colspan="5"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Data not found</p></center></td></tr>';
        $prifix = '<td>';
        $sufix = '</td>';
        $html = '';
        foreach ($api_data_list as $detail_obj) {
            foreach ($detail_obj as $i => $collections) {
                $html .= '<tr>';
                $html .= $prifix . $collections->id . $sufix;
                $html .= $prifix . $collections->title . $sufix;
                $html .= $prifix . $collections->body_html . $sufix;
                $html .= $prifix .
                        '<div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                        <div class="Polaris-ButtonGroup__Item">
                                            <a href="' . SITE_CLIENT_URL . 'collection_details.php?collection_id=' . $collections->id . '" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="View">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="Polaris-ButtonGroup__Item">
                                            <a href="" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="Edit">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 469.331 469.331"><path d="M438.931,30.403c-40.4-40.5-106.1-40.5-146.5,0l-268.6,268.5c-2.1,2.1-3.4,4.8-3.8,7.7l-19.9,147.4   c-0.6,4.2,0.9,8.4,3.8,11.3c2.5,2.5,6,4,9.5,4c0.6,0,1.2,0,1.8-0.1l88.8-12c7.4-1,12.6-7.8,11.6-15.2c-1-7.4-7.8-12.6-15.2-11.6   l-71.2,9.6l13.9-102.8l108.2,108.2c2.5,2.5,6,4,9.5,4s7-1.4,9.5-4l268.6-268.5c19.6-19.6,30.4-45.6,30.4-73.3   S458.531,49.903,438.931,30.403z M297.631,63.403l45.1,45.1l-245.1,245.1l-45.1-45.1L297.631,63.403z M160.931,416.803l-44.1-44.1   l245.1-245.1l44.1,44.1L160.931,416.803z M424.831,152.403l-107.9-107.9c13.7-11.3,30.8-17.5,48.8-17.5c20.5,0,39.7,8,54.2,22.4   s22.4,33.7,22.4,54.2C442.331,121.703,436.131,138.703,424.831,152.403z" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="Polaris-ButtonGroup__Item">
                                         <a href="" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="Delete">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                         </a>       
                                        </div>
                    </div> ' . $sufix;

                $html .= '</tr>';
            }
        }
        return $html;
    }

    function make_api_data_orderData($api_data_list) {
        $shopinfo = $this->current_store_obj;
        $tr_html = '<tr class="Polaris-ResourceList__ItemWrapper"> <td colspan="5"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Data not found</p></center></td></tr>';
        $prifix = '<td>';
        $sufix = '</td>';
        $html = '';
        foreach ($api_data_list as $detail_obj) {
            foreach ($detail_obj as $i => $orders) {
                $html .= '<tr>';
                $html .= $prifix . $orders->name . $sufix;
                $html .= $prifix . $orders->email . $sufix;
                $html .= $prifix . $orders->checkout_id . $sufix;
                $html .= $prifix . $orders->current_subtotal_price . $sufix;
                $html .= $prifix . $orders->currency . $sufix;
                $html .= $prifix . $orders->gateway . $sufix;
                $html .= $prifix .
                        '<div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                        <div class="Polaris-ButtonGroup__Item">
                                            <a href="' . SITE_CLIENT_URL . 'order_detail.php?order_id=' . $orders->id . '" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="View">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="Polaris-ButtonGroup__Item">
                                            <a href="" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="Edit">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 469.331 469.331"><path d="M438.931,30.403c-40.4-40.5-106.1-40.5-146.5,0l-268.6,268.5c-2.1,2.1-3.4,4.8-3.8,7.7l-19.9,147.4   c-0.6,4.2,0.9,8.4,3.8,11.3c2.5,2.5,6,4,9.5,4c0.6,0,1.2,0,1.8-0.1l88.8-12c7.4-1,12.6-7.8,11.6-15.2c-1-7.4-7.8-12.6-15.2-11.6   l-71.2,9.6l13.9-102.8l108.2,108.2c2.5,2.5,6,4,9.5,4s7-1.4,9.5-4l268.6-268.5c19.6-19.6,30.4-45.6,30.4-73.3   S458.531,49.903,438.931,30.403z M297.631,63.403l45.1,45.1l-245.1,245.1l-45.1-45.1L297.631,63.403z M160.931,416.803l-44.1-44.1   l245.1-245.1l44.1,44.1L160.931,416.803z M424.831,152.403l-107.9-107.9c13.7-11.3,30.8-17.5,48.8-17.5c20.5,0,39.7,8,54.2,22.4   s22.4,33.7,22.4,54.2C442.331,121.703,436.131,138.703,424.831,152.403z" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="Polaris-ButtonGroup__Item">
                                         <a href="" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="Delete">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                         </a>       
                                        </div>
                    </div> ' . $sufix;

                $html .= '</tr>';
            }
        }
        return $html;
    }

    function make_api_data_orders_products($api_data_list) {
        $items = count($api_data_list->order->line_items);
        $payment_html = $product_html = $table_html = $customer_html = '';
        $shopinfo = $this->current_store_obj;
        $return_arary = array();
        $table_html .= '<div class="Polaris-Stack">
                        <div class="Polaris-Stack__Item"><span class="Polaris-Badge">' . $api_data_list->order->financial_status . '</span></div>
                </div>
                <div class="Polaris-Card__Section">
                     <div class="Polaris-DataTable">
                        <div class="table-responsive">
                            <table id="orders_products" data-listing="true" data-from="api" data-apiName="orders" class="table">
                                <thead>
                                    <tr>
                                        <th>Detail</th>
                                        <th>Sub Detail</th>
                                        <th>Amount</th>                                                   
                                    </tr>
                                </thead>
                                <tbody id="orderDataTable">      
                                <tr>
                                    <td>subtotal</td>
                                    <td>' . $items . 'items' . '</td>
                                    <td>' . $api_data_list->order->total_line_items_price . '</td>
                                </tr>';
        foreach ($api_data_list->order->discount_codes as $i => $discount_codes) {
            $table_html .= '<tr>
                                    <td>Discount</td>
                                    <td>' . $discount_codes->code . '</td>
                                    <td>' . $discount_codes->amount . '</td>
                                </tr>';
        }
        $table_html .= '<tr>
                                    <td>Shipping</td>
                                    <td></td>
                                    <td>' . $api_data_list->order->total_shipping_price_set->shop_money->amount . '</td>
                                </tr>';
        foreach ($api_data_list->order->tax_lines as $i => $tax_lines) {
            $table_html .=' <tr>
                                    <td>Tax</td>
                                    <td>' . $tax_lines->title . '' . $tax_lines->rate . '</td>
                                    <td>' . $tax_lines->price . '</td>
                                </tr>';
        }
        $table_html .= '</tbody>
                            </table>
                        </div> 
                     </div>
                </div>
                <div class="Polaris-Card__Section">
                    <span class="">Paid by customer</span>
                        <span class="totalamount offset-7">' . $api_data_list->order->total_price . '</span>
                </div>';

        foreach ($api_data_list->order->line_items as $i => $product) {
            $fulfillment_status = ($api_data_list->order->fulfillment_status != "") ? $api_data_list->order->fulfillment_status : 'Unfulfilled';
            $amount = $api_data_list->order->total_line_items_price;
            $product_html .= '<div class="Polaris-Stack">
                        <div class="Polaris-Stack__Item"><span class="Polaris-Badge">' . $fulfillment_status . '</span></div>
                </div>
                <div class="Polaris-Card__Section">
                    <div class="pname">' . $product->name . '</div>
                    <div class="pprice offset-1">' . $product->price . '</div>
                    <div class="multiplication">*<span>' . $product->quantity . '</span></div>
                    <div class="total offset-2">' . $amount . '</div>
                </div>
                <div class="Polaris-Card__Section ">
                    <button class="Polaris-Button" type="button"><span class="Polaris-Button__Content"><span class="Polaris-Button__Text">Mark as Fulfilled</span></span></button>
                </div>';
        }
        $orders_count = ($api_data_list->order->customer->orders_count == 0) ? 'No Order' : $api_data_list->order->customer->orders_count;
        $customer_html .=' <div class="Polaris-Card">                                            
                <div class="Polaris-Card__Section">
                    <div class="Polaris-TextContainer">
                        <b><div class="Polaris-Card__Header">CUSTOMER</div></b>
                        <div class="Polaris-Stack__Item">' . $api_data_list->order->customer->email . '</div>
                        <div class="Polaris-Stack__Item">' . $orders_count . '</div>
                    </div>
                </div>
                <div class="Polaris-Card__Section">
                    <div class="Polaris-TextContainer">
                        <div class="Polaris-Stack__Item">CONTACT INFORMATION</div>
                        <div class="Polaris-Stack__Item">' . $api_data_list->order->customer->email . '</div>
                        <div class="Polaris-Stack__Item">' . $api_data_list->order->customer->default_address->phone . '</div>
                        <div class="Polaris-Stack__Item">' . $api_data_list->order->customer->default_address->address1 . '</div>
                        <div class="Polaris-Stack__Item">' . $api_data_list->order->customer->default_address->address2 . '</div>
                    </div>
                </div>
                <div class="Polaris-Card__Section">
                    <div class="Polaris-TextContainer">
                        <div class="Polaris-Stack__Item">BILLING ADDRESS</div>
                        <div class="Polaris-Stack__Item">' . $api_data_list->order->billing_address->address1 . '</div>
                        <div class="Polaris-Stack__Item">' . $api_data_list->order->billing_address->address2 . '</div>
                    </div>
                </div>
            </div>';

        $return_arary["table"] = $table_html;
        $return_arary["product"] = $product_html;
        $return_arary["customer"] = $customer_html;
        return $return_arary;
    }

    function make_api_data_productData($api_data_list) {
        $shopinfo = $this->current_store_obj;
        $tr_html = '<tr class="Polaris-ResourceList__ItemWrapper"> <td colspan="5"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Data not found</p></center></td></tr>';
        $prifix = '<td>';
        $sufix = '</td>';
        $html = '';
        foreach ($api_data_list as $detail_obj) {
            foreach ($detail_obj as $i => $products) {
                $image = ($products->image == '') ? CLS_NO_IMAGE : $products->image->src;
                $html .= '<tr>';
                $html .= $prifix . '<img src="' . $image . '" width="50px" height="50px" >' . $sufix;
                $html .= $prifix . $products->id . $sufix;
                $html .= $prifix . $products->title . $sufix;
                $html .= $prifix . $products->vendor . $sufix;
                $html .= $prifix .
                        '<div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                        <div class="Polaris-ButtonGroup__Item">
                                            <a href="" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="View">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="Polaris-ButtonGroup__Item">
                                            <a href="products_edit.php?product_id=' . $products->id . '" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="Edit">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 469.331 469.331"><path d="M438.931,30.403c-40.4-40.5-106.1-40.5-146.5,0l-268.6,268.5c-2.1,2.1-3.4,4.8-3.8,7.7l-19.9,147.4   c-0.6,4.2,0.9,8.4,3.8,11.3c2.5,2.5,6,4,9.5,4c0.6,0,1.2,0,1.8-0.1l88.8-12c7.4-1,12.6-7.8,11.6-15.2c-1-7.4-7.8-12.6-15.2-11.6   l-71.2,9.6l13.9-102.8l108.2,108.2c2.5,2.5,6,4,9.5,4s7-1.4,9.5-4l268.6-268.5c19.6-19.6,30.4-45.6,30.4-73.3   S458.531,49.903,438.931,30.403z M297.631,63.403l45.1,45.1l-245.1,245.1l-45.1-45.1L297.631,63.403z M160.931,416.803l-44.1-44.1   l245.1-245.1l44.1,44.1L160.931,416.803z M424.831,152.403l-107.9-107.9c13.7-11.3,30.8-17.5,48.8-17.5c20.5,0,39.7,8,54.2,22.4   s22.4,33.7,22.4,54.2C442.331,121.703,436.131,138.703,424.831,152.403z" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="Polaris-ButtonGroup__Item">
                                         <a href="" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="Delete">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                         </a>       
                                        </div>
                    </div> ' . $sufix;

                $html .= '</tr>';
            }
        }
        return $html;
    }

    function make_api_data_pagesData($api_data_list) {
        $shopinfo = $this->current_store_obj;
        $tr_html = '<tr class="Polaris-ResourceList__ItemWrapper"> <td colspan="5"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Data not found</p></center></td></tr>';
        $prifix = '<td>';
        $sufix = '</td>';
        $html = '';
        foreach ($api_data_list as $pages_obj) {
            foreach ($pages_obj as $i => $pages) {
                $html .= '<tr>';
                $html .= '<tr>'; $html .= $prifix . $pages->id . $sufix;
                $html .= $prifix . $pages->title . $sufix;
                $html .= $prifix . $pages->shop_id . $sufix;
                $html .= $prifix . $pages->body_html . $sufix;
                $html .= $prifix .
                        '<div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                        <div class="Polaris-ButtonGroup__Item">
                                            <a href="" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="View">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="Polaris-ButtonGroup__Item">
                                            <a href="pages_edit.php?page_id=' . $pages->id . '" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="Edit">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 469.331 469.331"><path d="M438.931,30.403c-40.4-40.5-106.1-40.5-146.5,0l-268.6,268.5c-2.1,2.1-3.4,4.8-3.8,7.7l-19.9,147.4   c-0.6,4.2,0.9,8.4,3.8,11.3c2.5,2.5,6,4,9.5,4c0.6,0,1.2,0,1.8-0.1l88.8-12c7.4-1,12.6-7.8,11.6-15.2c-1-7.4-7.8-12.6-15.2-11.6   l-71.2,9.6l13.9-102.8l108.2,108.2c2.5,2.5,6,4,9.5,4s7-1.4,9.5-4l268.6-268.5c19.6-19.6,30.4-45.6,30.4-73.3   S458.531,49.903,438.931,30.403z M297.631,63.403l45.1,45.1l-245.1,245.1l-45.1-45.1L297.631,63.403z M160.931,416.803l-44.1-44.1   l245.1-245.1l44.1,44.1L160.931,416.803z M424.831,152.403l-107.9-107.9c13.7-11.3,30.8-17.5,48.8-17.5c20.5,0,39.7,8,54.2,22.4   s22.4,33.7,22.4,54.2C442.331,121.703,436.131,138.703,424.831,152.403z" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="Polaris-ButtonGroup__Item">
                                         <a href="" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="Delete">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                         </a>       
                                        </div>
                    </div> ' . $sufix;

                $html .= '</tr>';
            }
        }
        return $html;
    }

    function phpvalidation_supplier_create() {
        $error_array = array();
        if (isset($_POST['supplier_name']) && (ctype_alpha(str_replace(' ', '', $_POST['supplier_name'])) == '')) {
            $error_array['supplier_name'] = "Please Enter Supplier Name.";
        }
        if (isset($_POST['supplier_address']) && ($_POST['supplier_address']) == '') {
            $error_array['supplier_address'] = "Please Enter Supplier Address.";
        }
        if (isset($_POST['supplier_city']) && (ctype_alpha($_POST['supplier_city']) == '')) {
            $error_array['supplier_city'] = "Please Enter Supplier City.";
        }
        if (isset($_POST['supplier_state']) && (ctype_alpha($_POST['supplier_state']) == '')) {
            $error_array['supplier_state'] = "Please Enter Supplier State.";
        }
        if (isset($_POST['supplier_pincode']) && (ctype_digit($_POST['supplier_pincode']) == '')) {
            $error_array['supplier_pincode'] = "Please Enter Supplier Pincodess.";
        }
        return $error_array;
    }

    function supplier_create() {
           
        $shopinfo = $this->current_store_obj;
        $store_user_id = $shopinfo[0]['store_user_id'];
        
        $response = array();
        $error_array = $this->phpvalidation_supplier_create();
        if (!empty($error_array)) {
            $result['dataResult'] = "fail";
            $result['errors'] = $error_array;
        } else {
            $result['dataResult'] = "Success";
            if ( $_POST['supplier_type'] == 0) {
                $supplier_type = 'Un-registered';
            } elseif ($_POST['supplier_type'] == 1) {
                $supplier_type = 'Registered';
            } else {
                $supplier_type = 'Composition';
            }
            $words = explode(" ", $_POST['supplier_name']);
                $acronym = "";
                foreach ($words as $w) {
                    $acronym .= $w[0];
                } 
                $length = 10;    
                $supplier_password =  substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefgsijklmnopqrstuvwxyz'),1,$length);
            $fields_arr = array(
                '`id`' => '',
                '`store_user_id`' => $store_user_id,
                '`supplier_type`' => $_POST['supplier_type'],
                '`supplier_name`' => $_POST['supplier_name'],
                '`supplier_contact_person`' => $_POST['supplier_contact_person'],
                '`supplier_email`' => $_POST['supplier_email'],
                '`supplier_password`' =>$supplier_password,
                '`supplier_phone`' => $_POST['supplier_phone'],
                '`supplier_mobile`' => $_POST['supplier_mobile'],
                '`supplier_address`' => $_POST['supplier_address'],
                '`supplier_landmark`' => $_POST['supplier_landmark'],
                '`supplier_city`' => $_POST['supplier_city'],
                '`supplier_state`' => $_POST['supplier_state'],
                '`supplier_country`' => $_POST['supplier_country'],
                '`supplier_pincode`' => $_POST['supplier_pincode'],
                '`supplier_pan`' => $_POST['supplier_pan'],
                '`supplier_tin`' => $_POST['supplier_tin'],
                '`supplier_gst`' => $_POST['supplier_gst'],
                '`supplier_vat`' => $_POST['supplier_vat'],
                '`supplier_bank_name`' => $_POST['supplier_bank_name'],
                '`supplier_bank_account_no`' => $_POST['supplier_bank_account_no'],
                '`supplier_bank_branch`' => $_POST['supplier_bank_branch'],
                '`supplier_ifsc_code`' => $_POST['supplier_ifsc_code'],
                '`supplier_remarks`' => $_POST['supplier_remarks'],
                '`supplier_status`' => $_POST['supplier_status'],
                '`created_at`' => date('Y-m-d H:i:s'),
                '`updated_at`' => date('Y-m-d H:i:s')
            );
            $supplier_data = $this->post_data(TABLE_SUPPLIERS, array($fields_arr));
            $id = $supplier_data['last_insert_id'];
            $numlength = strlen((string)$id);
            if($numlength == 1){
                $numlength = '0000'.$id;
            }elseif($numlength == 2){
                $numlength = '000'.$id;
            }elseif($numlength == 3){
                $numlength = '00'.$id;
            }elseif($numlength == 4){
                $numlength = '0'.$id;
            }else{
                $numlength = $id;
                
            }
            $generate_code = $acronym .'-' .$_POST['supplier_city'] .'-'.$numlength.':'. $supplier_type;
            $fields = array(
                '`generate_code`' => $generate_code,
            );
            $where_query = array(
                ["", "id", "=", $id],
            );
            $comeback = $this->put_data(TABLE_SUPPLIERS, $fields, $where_query);
           
        }

        return json_encode($result);
    }

    function phpvalidation_purchase_create() {
        $error_array = array();       
        if (isset($_POST['po_date']) && $_POST['po_date'] == '') {
            $error_array['po_date'] = "Please Enter Purchase Order Date .";
        }
        if (isset($_POST['supplier_id']) && $_POST['supplier_id'] == '') {
            $error_array['supplier_id'] = "Please Enter Supplier Name .";
        }
        return $error_array;
    }

    function purchase_create() {
        $shopinfo = $this->current_store_obj;
        $store_user_id = $shopinfo[0]['store_user_id'];
        $store = $shopinfo[0]['shop_name'];
        $password = $shopinfo[0]['password'];
        $response = array();
        $error_array = $this->phpvalidation_purchase_create();

        if (!empty($error_array)) {
            $result['dataResult'] = "fail";
            $result['errors'] = $error_array;
        } else {
            $result['dataResult'] = "Success";

            $fields_arr = array(
                '`id`' => '',
                 '`store_user_id`' => $store_user_id,
                '`po_type`' => $_POST['po_type'],
                '`taxation`' => $_POST['taxation'],
                '`freight`' => $_POST['freight'],
                '`packing`' => $_POST['packing'],
                '`insurance`' => $_POST['insurance'],
                '`po_date`' => $_POST['po_date'],
                '`supplier_id`' => $_POST['supplier_id'],
                '`rec_ref_code`' => $_POST['rec_ref_code'],
                '`supplier_address`' => $_POST['supplier_address'],
                '`remarks`' => $_POST['remarks'],
                '`reverse_charge`' => $_POST['reverse_charge'],
                '`supplier_invoice_no`' => $_POST['supplier_invoice_no'],
                '`supplier_invoice_date`' => $_POST['supplier_invoice_date'],
                '`supplier_invoice_date`' => $_POST['supplier_invoice_date'],
                '`created_at`' => date('Y-m-d H:i:s'),
                '`updated_at`' => date('Y-m-d H:i:s')
            );
            $purchase_data = $this->post_data(TABLE_PURCHASE, array($fields_arr));
//          $purchase_data = json_decode($purchase_data);
            $purchase_id = $purchase_data['last_insert_id'];
            $numlength = strlen((string) $purchase_id);
            if ($numlength == 1) {
                $numlength = '000' . $purchase_id;
            } elseif ($numlength == 2) {
                $numlength = '00' . $purchase_id;
            } elseif ($numlength == 3) {
                $numlength = '0' . $purchase_id;
            } else {
                $numlength = $purchase_id;
            }
            $year = date('y');
            $nextYear = $year + 1;
            $voucher_no = "GRN/" . $year . "-" . $nextYear . "/" . $numlength;

            foreach ($_POST['sku'] as $key => $skuvalue) {
                $category_name = $_POST['category_name'][$key];
                $v_sku = $_POST['v_sku'][$key];
                $qty = $_POST['qty'][$key];
                $price = $_POST['price'][$key];
                $taxable = $_POST['taxable'][$key];
                $cgst = $_POST['cgst'][$key];
                $sgst = $_POST['sgst'][$key];
                $igst = $_POST['igst'][$key];
                $grand_total = $_POST['grand_total'][$key];
               
                if ($skuvalue != ''){
                    $sku = $skuvalue;
                    $end_point = "https://".$store."/admin/api/graphql.json";
                    $shopify_data_list = cls_api_graphql_call(CLS_API_KEY, $password, $store, $end_point, $sku, 'POST');
                
                    $decode_data = json_decode($shopify_data_list["response"]);
                    $checksku = $decode_data->data->productVariants->edges;
                    if (!empty($checksku)) {
                       
                        $barcode = isset($decode_data->data->productVariants->edges[0]->node->barcode) ? $decode_data->data->productVariants->edges[0]->node->barcode : '';
                        $inventoryItem_id = isset($decode_data->data->productVariants->edges[0]->node->inventoryItem->id) ? $decode_data->data->productVariants->edges[0]->node->inventoryItem->id : '';
                      
                       
//                        $inventory_level_api = array('api_name' => 'inventory_levels');
//                        $fields = array('inventory_item_ids' => $inventoryItem_id);
//                        $get_level = $this->cls_get_shopify_list($inventory_level_api, $fields, 'GET');
                        $get_level = cls_inventory_level_graphql_call(CLS_API_KEY, $password, $store, $end_point, $inventoryItem_id, 'POST');
                        $decode_inventory_level = json_decode($get_level["response"]);

                        $get_available = (isset($decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->available) && $decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->available !== "" ) ? $decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->available : '';
                        $get_location_id = (isset($decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->location->id) && $decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->location->id !== "" ) ? $decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->location->id : '';

                        $get_available = $get_available + $qty;
                        if ($get_location_id != '') {
                            $set_api = array('api_name' => 'inventory_levels', 'set' => 'set');
                            $get_location_id = str_replace("gid://shopify/Location/", "", $get_location_id);
                            $inventoryItem_id = str_replace("gid://shopify/InventoryItem/", "", $inventoryItem_id);
                            $filed_arr = array('location_id' => $get_location_id, 'inventory_item_id' => $inventoryItem_id, 'available' => (int) $get_available);

                            $update_inve = $this->cls_get_shopify_list($set_api, $filed_arr, 'POST');
                          
                            $fields_arr = array(
                                '`id`' => '',
                                 '`store_user_id`' => $store_user_id,
                                '`purchase_id`' =>  $purchase_id,
                                '`supplier_id`' =>   $_POST['supplier_id'],
                                '`category_name`' => $category_name,
                                '`voucher_no`' => $voucher_no,
                                '`sku`' =>$sku,
                                '`barcode`' =>$barcode,
                                '`v_sku`' => $v_sku,
                                '`qty`' => $qty,
                                '`price`' => $price,
                                '`taxable`' => $taxable,
                                '`cgst`' => $cgst,
                                '`sgst`' =>$sgst,
                                '`igst`' =>$igst,
                                '`grand_total`' => $grand_total,
                                '`created_at`' => date('Y-m-d H:i:s'),
                                '`updated_at`' => date('Y-m-d H:i:s')
                            );
                            $this->post_data(TABLE_SKU_MASTER, array($fields_arr));
                            
                        }
                    } else {
                        $result['dataResult'] = "fail";
                        $result['errors']['sku']= "Invalid sku";
                    }
                } 
            }
          
            $fields = array(
                                '`voucher_no`' => $voucher_no,
                                '`qty`' => $_POST['total_quantity'],
                                '`price`' => $_POST['total_taxable'],
                                '`taxable`' => $_POST['total_taxable'],
                                '`cgst`' => $_POST['total_cgst'],
                                '`sgst`' => $_POST['total_sgst'],
                                '`igst`' => $_POST['total_igst'],
                                '`grand_total`' => $_POST['grand_amount'],
                                '`all_total`' => $_POST['all_total'],    
                            );
                            $where_query = array(
                                ["", "id", "=", $purchase_id],
                            );
                            $comeback = $this->put_data(TABLE_PURCHASE, $fields, $where_query);
                                }
        return json_encode($result);
    }

    function phpvalidation_purchase_return_create() {
        $error_array = array();        
        if (isset($_POST['return_date']) && $_POST['return_date'] == '') {
            $error_array['return_date'] = "Please Enter Return Date .";
        }      
           
        return $error_array;
    }

   function purchase_return_create() {
       
         $shopinfo = $this->current_store_obj;
        $store_user_id = $shopinfo[0]['store_user_id'];
   
        $store = $shopinfo[0]['shop_name'];
        $password = $shopinfo[0]['password'];
        $response = array();
        $error_array = $this->phpvalidation_purchase_return_create();
        if (!empty($error_array)) {
            $result['dataResult'] = "fail";
            $result['errors'] = $error_array;
        } else {
            $result['dataResult'] = "Success";
            if(isset($_POST['barcode']) && $_POST['barcode'] !== ''){
            foreach ($_POST['barcode'] as $key => $barcodevalue) {
                $sku_id = $_POST['sku_id'][$key];
                $category_name = $_POST['category_name'][$key];
                $post_qty = $_POST['qty'][$key];
                $post_taxable = $_POST['taxable'][$key];
                $grand_total = $_POST['grand_total'][$key];
                $sku = isset($_POST['sku']) ? $_POST['sku'] : '';
                $supplier_id = isset($_POST['supplier_id']) ? $_POST['supplier_id'] : '';
                $where_query = array(["", "barcode", "=",$barcodevalue], ["AND", "id", "=",$sku_id],["AND", "status", "=",1]);
                $comeback = $this->select_result(TABLE_SKU_MASTER, '*', $where_query);
                $getbarcode = (isset($comeback["data"][0]["barcode"]) && !empty($comeback["data"][0]["barcode"])) ?$comeback["data"][0]["barcode"] : '';
                $getsku = (isset($comeback["data"][0]["sku"]) && !empty($comeback["data"][0]["sku"])) ?$comeback["data"][0]["sku"] : '';
                $total = (isset($comeback["data"][0]["total"]) && !empty($comeback["data"][0]["total"])) ?$comeback["data"][0]["total"] : '';
                $cgst = (isset($comeback["data"][0]["cgst"]) && !empty($comeback["data"][0]["cgst"])) ?$comeback["data"][0]["cgst"] : '';
                $sgst = (isset($comeback["data"][0]["sgst"]) && !empty($comeback["data"][0]["sgst"])) ?$comeback["data"][0]["sgst"] : '';
                $igst = (isset($comeback["data"][0]["igst"]) && !empty($comeback["data"][0]["igst"])) ?$comeback["data"][0]["igst"] : '';
                $qty = (isset($comeback["data"][0]["qty"]) && !empty($comeback["data"][0]["qty"])) ?$comeback["data"][0]["qty"] : '';
                $return_qty = (isset($comeback["data"][0]["return_qty"]) && !empty($comeback["data"][0]["return_qty"])) ?$comeback["data"][0]["return_qty"] : '';
                $grand_total = (isset($comeback["data"][0]["grand_total"]) && !empty($comeback["data"][0]["grand_total"])) ?$comeback["data"][0]["grand_total"] : '';
                $sku_id = (isset($comeback["data"][0]["id"]) && !empty($comeback["data"][0]["id"])) ?$comeback["data"][0]["id"] : '';
                $purchase_id = (isset($comeback["data"][0]["purchase_id"]) && !empty($comeback["data"][0]["purchase_id"]) ?$comeback["data"][0]["purchase_id"] : '');
                if ($barcodevalue == $getbarcode) {
                     $result['dataResult'] = "Success";
                     if ($barcodevalue != ''){
                        $end_point = "https://".$store."/admin/api/graphql.json";
                        $shopify_data_list = cls_api_graphql_barcode_call(CLS_API_KEY, $password, $store, $end_point, $barcodevalue, 'POST');
               
                        $decode_data = json_decode($shopify_data_list["response"]);
                        $checkbarcode = $decode_data->data->productVariants->edges[0]->node->barcode;
                        if (!empty($checkbarcode)) {
                            $inventoryItem_id = isset($decode_data->data->productVariants->edges[0]->node->inventoryItem->id) ? $decode_data->data->productVariants->edges[0]->node->inventoryItem->id : '';
//                            $inventoryItem_id = str_replace("gid://shopify/InventoryItem/", "", $inventoryItem_id);

//                            $inventory_level_api = array('api_name' => 'inventory_levels');
//                            $fields = array('inventory_item_ids' => $inventoryItem_id);
//                            $get_level = $this->cls_get_shopify_list($inventory_level_api, $fields, 'GET');
//                            $inventory_levels = $get_level->inventory_levels[0];
//                            $get_location_id = (isset($inventory_levels->location_id) && $inventory_levels->location_id !== '' ) ? $inventory_levels->location_id : '';
//                            $get_available = (isset($inventory_levels->available) && $inventory_levels->available !== '' ) ? $inventory_levels->available : '';
                                $get_level = cls_inventory_level_graphql_call(CLS_API_KEY, $password, $store, $end_point, $inventoryItem_id, 'POST');

                                $decode_inventory_level = json_decode($get_level["response"]);
                                $get_available = (isset($decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->available) && $decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->available !== "" ) ? $decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->available : '';
                                $get_location_id = (isset($decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->location->id) && $decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->location->id !== "" ) ? $decode_inventory_level->data->inventoryItem->inventoryLevels->edges[0]->node->location->id : '';

                                $get_available = $get_available - $post_qty;
                            if ($get_location_id != '') {
                                $set_api = array('api_name' => 'inventory_levels', 'set' => 'set');
                                    $get_location_id = str_replace("gid://shopify/Location/", "", $get_location_id);
                                    $inventoryItem_id = str_replace("gid://shopify/InventoryItem/", "", $inventoryItem_id);
                                    $filed_arr = array('location_id' => $get_location_id, 'inventory_item_id' => $inventoryItem_id, 'available' => (int) $get_available);
                                    $update_inve = $this->cls_get_shopify_list($set_api, $filed_arr, 'POST');

                                    $fields_arr = array(
                                    '`id`' => '',
                                    '`sku_id`' => $sku_id,
                                    '`store_user_id`' => $store_user_id,
                                    '`purchase_id`' => $purchase_id,
                                    '`supplier_id`' => $_POST['supplier_id'],
                                    '`return_date`' => $_POST['return_date'],
                                    '`ref_num`' => $_POST['ref_num'],
                                    '`qty`' => $post_qty,
                                    '`taxable`' =>$post_taxable,
                                    '`cgst`' =>$_POST['total_cgst'],
                                    '`sgst`' => $_POST['total_sgst'],
                                    '`igst`' =>$_POST['total_igst'],
                                    '`grand_total`' =>$_POST['grand_amount'],
                                    '`reason`' => $_POST['reason'],
                                    '`created_at`' => date('Y-m-d H:i:s'),
                                    '`updated_at`' => date('Y-m-d H:i:s')
                                );
                                 $purchase_return_data = $this->post_data(TABLE_PURCHASE_RETURN, array($fields_arr));
                                 $purchase_return_id = $purchase_return_data['last_insert_id'];
                                    $numlength = strlen((string) $purchase_return_id);
                                    if ($numlength == 1) {
                                        $numlength = '000' . $purchase_return_id;
                                    } elseif ($numlength == 2) {
                                        $numlength = '00' . $purchase_return_id;
                                    } elseif ($numlength == 3) {
                                        $numlength = '0' . $purchase_return_id;
                                    } else {
                                        $numlength = $purchase_return_id;
                                    }
                                    $year = date('y');
                                    $nextYear = $year + 1;
                                    $voucher_no = "GRTN/" . $year . "-" . $nextYear . "/" . $numlength;
                                    $fields = array(
                                        '`voucher_no`' => $voucher_no,
                                    );
                                    $where_query = array(
                                        ["", "id", "=", $purchase_return_id],
                                    );
                                     $comeback = $this->put_data(TABLE_PURCHASE_RETURN, $fields, $where_query);
                                              
                                     $update_qty = $qty - $post_qty;
                                  
                                     if($update_qty == 0){
                                         $post_qty = $post_qty + (int)$return_qty;
                                        $fields = array(
                                            'return_qty' => $post_qty,
                                            'status' => 0,
                                        );
                                          $where_query = array(
                                        ["", "id", "=", $sku_id],
                                    );
                                          $comeback = $this->put_data(TABLE_SKU_MASTER, $fields, $where_query);
                                     }  elseif($update_qty > 0) {
                                        $post_qty = $post_qty + (int)$return_qty;

                                         $fields = array(
                                            'return_qty' => $post_qty,
                                            'status' => 1,
                                             
                                        );
                                           $where_query = array(
                                            ["", "id", "=", $sku_id],
                                        );
                                           $comeback = $this->put_data(TABLE_SKU_MASTER, $fields, $where_query);
                                    }elseif($update_qty <= 0) {
                                         $result['dataResult'] = "fail";
                                        $result['errors']['qty'] = "Please enter valid Quantity";
                                        $result['errors']['update_qty'] = $update_qty;
                                     }
                                        
                                  
                                }
                            } else {
                            $result['dataResult'] = "fail";
                            $result['errors']['sku'] = "Invalid Barcode uhu";
                        }
                    }
                }else{
                          $result['dataResult'] = "fail";
                          $result['errors']['sku'] = "Invalid Barcode";
                    }
            }
        }else{
            $fields_arr = array(
                    '`id`' => '',
                    '`store_user_id`' => $store_user_id,
//                  '`po_type`' => $_POST['po_type'],
                    '`supplier_id`' => $_POST['supplier_id'],
                    '`return_date`' => $_POST['return_date'],
                    '`ref_num`' => $_POST['ref_num'],
                    '`qty`' => $_POST['total_quantity'],
                    '`taxable`' => $_POST['total_taxable'],
                    '`cgst`' => $_POST['total_cgst'],
                    '`sgst`' => $_POST['total_sgst'],
                    '`igst`' => $_POST['total_igst'],
                    '`grand_total`' => $_POST['grand_amount'],
                    '`reason`' => $_POST['reason'],
                    '`created_at`' => date('Y-m-d H:i:s'),
                    '`updated_at`' => date('Y-m-d H:i:s')
                );
                $this->post_data(TABLE_PURCHASE_RETURN, array($fields_arr));
            }
            
        }
        return json_encode($result);
    }

    function phpvalidation_sales() {
        $error_array = array();
        if (isset($_POST['billing_address']) && $_POST['billing_address'] == '') {
            $error_array['billing_address'] = "Please enter billing address.";
        }

        if (isset($_POST['shipping_address']) && $_POST['shipping_address'] == '') {
            $error_array['shipping_address'] = "Please enter shipping address.";
        }

        if (isset($_POST['sales_date']) && $_POST['sales_date'] == '') {
            $error_array['sales_date'] = "Please select sales date.";
        }

        if (isset($_POST['dealer_id']) && ($_POST['dealer_id']) == '') {
            $error_array['dealer_id'] = "Please select dealer.";
        }
        return $error_array;
    }

    function sales() {
          $shopinfo = $this->current_store_obj;
        $store_user_id = $shopinfo[0]['store_user_id'];
   
        $store = $shopinfo[0]['shop_name'];
        $password = $shopinfo[0]['password'];
        $response = array();
        $error_array = $this->phpvalidation_sales();
        if (!empty($error_array)) {
            $result['dataResult'] = "fail";
            $result['errors'] = $error_array;
        } else {
            if (isset($_POST['invoice_type']) && $_POST['invoice_type'] != '') {
                $_POST['invoice_type'] = 1;
            } else {
                $_POST['invoice_type'] = 0;
            }
            $fields_arr = array(
                '`id`' => '',
                 '`store_user_id`' => $store_user_id,
                '`dealer_id`' => $_POST['dealer_id'],
                '`invoice_type`' => $_POST['invoice_type'],
                '`billing_address`' => $_POST['billing_address'],
                '`shipping_address`' => $_POST['shipping_address'],
                '`vat`' => $_POST['vat'],
                '`freight`' => $_POST['freight'],
                '`sales_date`' => $_POST['sales_date'],
                '`qty`' => $_POST['qty'],
                '`grandtotal`' => $_POST['grandtotal'],
                '`created_at`' => date('Y-m-d H:i:s'),
                '`updated_at`' => date('Y-m-d H:i:s')
            );
             $this->post_data(TABLE_SALES_ORDER, array($fields_arr));
            $result['dataResult'] = "Success";
        }

        return json_encode($result);
    }

    function phpvalidation_sales_order() {
        $error_array = array();
        
        if (isset($_POST['sales_date']) && ($_POST['sales_date']) == '') {
            $error_array['sales_date'] = "Please enter date of sales .";
        }
        if (isset($_POST['dealer_id']) && $_POST['dealer_id'] == '') {
            $error_array['dealer_id'] = "Please select dealer.";
        }
        if (isset($_POST['billing_address']) && $_POST['billing_address'] == '') {
            $error_array['billing_address'] = "Please enter billing address.";
        }
        if (isset($_POST['shipping_address']) && $_POST['shipping_address'] == '') {
            $error_array['shipping_address'] = "Please enter shipping address.";
        }
        return $error_array;
    }

    function sales_order() {
         $shopinfo = $this->current_store_obj;
        $store_user_id = $shopinfo[0]['store_user_id'];
        $store = $shopinfo[0]['shop_name'];
        $password = $shopinfo[0]['password'];
        $response = array();
        $error_array = $this->phpvalidation_sales_order();
        if (!empty($error_array)) {
            $result['dataResult'] = "fail";
            $result['errors'] = $error_array;
        } else {
            if (isset($_POST['invoice_type']) && $_POST['invoice_type'] != '') {
                $_POST['invoice_type'] = 1;
            } else {
                $_POST['invoice_type'] = 0;
            }
           $result['dataResult'] = "Success";
                                      
           $fields_arr = array(
                '`id`' => '',
                 '`store_user_id`' => $store_user_id,
                '`sales_type`' => $_POST['sales_type'],
                '`invoice_type`' => $_POST['invoice_type'],
                '`sales_date`' => $_POST['sales_date'],
                '`dos`' => $_POST['dos'],
                '`packing`' => $_POST['packing'],
                '`insurance`' => $_POST['insurance'],
                '`freight`' => $_POST['freight'],
                '`reverse_charge`' => $_POST['reverse_charge'],
                '`dealer_id`' => $_POST['dealer_id'],
                '`ref_no`' => $_POST['ref_no'],
                '`transport_mode`' => $_POST['transport_mode'],
                '`vehicle_no`' => $_POST['vehicle_no'],
                '`billing_address`' => $_POST['billing_address'],
                '`shipping_address`' => $_POST['shipping_address'],
                '`remark`' => $_POST['remark'],
                '`created_at`' => date('Y-m-d H:i:s'),
                '`updated_at`' => date('Y-m-d H:i:s')
            );

            $sales_data = $this->post_data(TABLE_SALES_ORDER, array($fields_arr));
//                            $sales_data = json_decode($sales_data);
            $sales_id = $sales_data['last_insert_id'];
            $numlength = strlen((string) $sales_id);
            if ($numlength == 1) {
                $numlength = '000' . $sales_id;
            } elseif ($numlength == 2) {
                $numlength = '00' . $sales_id;
            } elseif ($numlength == 3) {
                $numlength = '0' . $sales_id;
            } else {
                $numlength = $sales_id;
            }
            $year = date('y');
            $nextYear = $year + 1;
            $voucher_no = "GRN/" . $year . "-" . $nextYear . "/" . $numlength;
           
            foreach ($_POST['barcode'] as $key => $barcodevalue) {
                
                $name = $_POST['name'][$key];
                $metal = $_POST['metal'][$key];
                $weight = $_POST['weight'][$key];
                $qty = (isset($_POST['qty'][$key]) && !empty($_POST['qty'][$key]) ? $_POST['qty'][$key] : 0);
                $mrp = $_POST['mrp'][$key];
                $discount_rate = $_POST['discount_rate'][$key];
                $discount = $_POST['discount'][$key];
                $taxation = $_POST['taxation'][$key];
                $cgst = $_POST['cgst'][$key];
                $sgst = $_POST['sgst'][$key];
                $igst = $_POST['igst'][$key];
                $grandtotal = $_POST['grandtotal'][$key];
               
                if ($barcodevalue != '') {
                    $end_point = "https://".$store."/admin/api/graphql.json";
                    $shopify_data_list = cls_api_graphql_barcode_call(CLS_API_KEY, $password, $store, $end_point,$barcodevalue, 'POST');
                    $decode_data = json_decode($shopify_data_list["response"]);
                    $checkbarcode = (isset($decode_data->data->productVariants->edges[0]->node->barcode) && !empty($decode_data->data->productVariants->edges[0]->node->barcode)) ? $decode_data->data->productVariants->edges[0]->node->barcode : '' ;
                    

                    if (!empty($checkbarcode)) {
                         $sku = isset($decode_data->data->productVariants->edges[0]->node->sku) ? $decode_data->data->productVariants->edges[0]->node->sku : '';
                         $barcode = isset($decode_data->data->productVariants->edges[0]->node->barcode) ? $decode_data->data->productVariants->edges[0]->node->barcode : '';
                        $inventoryItem_id = isset($decode_data->data->productVariants->edges[0]->node->inventoryItem->id) ? $decode_data->data->productVariants->edges[0]->node->inventoryItem->id : '';
                        $inventoryItem_id = str_replace("gid://shopify/InventoryItem/", "", $inventoryItem_id);
                        
                        $inventory_level_api = array('api_name' => 'inventory_levels');
                        $fields = array('inventory_item_ids' => $inventoryItem_id);
                        $get_level = $this->cls_get_shopify_list($inventory_level_api, $fields, 'GET');
                        $inventory_levels = $get_level->inventory_levels[0];
                       
                        $get_location_id = (isset($inventory_levels->location_id) && $inventory_levels->location_id !== '' ) ? $inventory_levels->location_id : '';
                        $get_available = (isset($inventory_levels->available) && $inventory_levels->available !== '' ) ? $inventory_levels->available : '';
                        $get_available = (int)$get_available - (int)$qty;
                      

                        if ($get_location_id != '') {
                            $set_api = array('api_name' => 'inventory_levels', 'set' => 'set');
                            $filed_arr = array('location_id' => $get_location_id, 'inventory_item_id' => $inventoryItem_id, 'available' => (int) $get_available);
                            $update_inve = $this->cls_get_shopify_list($set_api, $filed_arr, 'POST');
                            $fields_arr = array(
                                '`id`' => '',
                                 '`store_user_id`' => $store_user_id,
                                '`sales_id`' =>  $sales_id,
                                '`dealer_id`' =>  $_POST['dealer_id'],
                                '`name`' => $name,
                                '`sku`' =>$sku,
                                '`barcode`' =>$barcode,
                                '`metal`' => $metal,
                                '`weight`' => $weight,
                                '`mrp`' => $mrp,
                                '`discount_rate`' => $discount_rate,
                                '`discount`' => $discount,
                                '`taxation`' => $taxation,
                                 '`qty`' => $qty,
                                '`cgst`' => $cgst,
                                '`sgst`' =>$sgst,
                                '`igst`' => $igst,
                                '`grandtotal`' => $grandtotal,
                                '`created_at`' => date('Y-m-d H:i:s'),
                                '`updated_at`' => date('Y-m-d H:i:s')
                            );
                            $this->post_data(TABLE_SALES_SKU_MASTER, array($fields_arr));
                            $fields = array(
                                '`voucher_no`' => $voucher_no,
                                '`qty`' => $_POST['total_qty'],
                                '`all_total`' => $_POST['all_total'],
                                '`discount`' => $_POST['total_discount'],
                                '`taxation`' => $_POST['total_taxable'],
                                '`cgst`' => $_POST['total_cgst'],
                                '`sgst`' => $_POST['total_sgst'],
                                '`igst`' => $_POST['total_igst'],
                                '`grandtotal`' => $_POST['grand_amount'],
                            );
                            $where_query = array(
                                ["", "id", "=", $sales_id],
                            );
                            $comeback = $this->put_data(TABLE_SALES_ORDER, $fields, $where_query);
                        }
                    } else {
                        $result['dataResult'] = "fail";
                        $result['errors']['sku']= "Invalid sku";
                    }
                } 
            }
                        }
        return json_encode($result);
    }

    function phpvalidation_sales_return() {
        $error_array = array();
        
        if (isset($_POST['return_date']) && $_POST['return_date'] == '') {
            $error_array['return_date'] = "Please enter return date.";
        }
        return $error_array;
    }

    function sales_return() {
        $shopinfo = $this->current_store_obj;
        $store_user_id = $shopinfo[0]['store_user_id'];
        $store = $shopinfo[0]['shop_name'];
        $password = $shopinfo[0]['password'];
        $response = array();
        $error_array = $this->phpvalidation_sales_return();
        if (!empty($error_array)) {
            $result['dataResult'] = "fail";
            $result['errors'] = $error_array;
        } else {
            $result['dataResult'] = "Success";
           
            if(isset($_POST['barcode']) && $_POST['barcode'] !== ''){
                $fields_arr = array(
                                    '`id`' => '',
//                                    '`sales_sku_id`' =>$id,
//                                    '`sales_id`' =>$sales_id,
                                    '`store_user_id`' => $store_user_id,
                                    '`so_type`' => $_POST['so_type'],
                                    '`dealer_id`' => $_POST['dealer_id'],
                                    '`return_date`' => $_POST['return_date'],
                                    '`ref_num`' => $_POST['ref_num'],
                                    '`qty_ret`' => $_POST['qty_ret'],
                                    '`price_ret`' => $_POST['total_taxable'],
                                    '`reason`' => $_POST['reason'],
                                     '`qty`' => $_POST['total_qty'],
                                    '`taxable`' => $_POST['total_taxable'],
//                                    '`discount`' => $_POST['total_discount'],
                                    '`cgst`' => $_POST['total_cgst'],
                                    '`sgst`' => $_POST['total_sgst'],
                                    '`igst`' => $_POST['total_igst'],
                                    '`grand_total`' => $_POST['grand_amount'],
                                    '`created_at`' => date('Y-m-d H:i:s'),
                                    '`updated_at`' => date('Y-m-d H:i:s')
                                );
                              $sales__return_data = $this->post_data(TABLE_SALES_ORDER_RETURN, array($fields_arr));
                              $sales_return_id = $sales__return_data['last_insert_id'];
            foreach ($_POST['barcode'] as $key => $barcodevalue) {
                 $store = $shopinfo[0]['shop_name'];
                $password = $shopinfo[0]['password'];
                $grand_total = $_POST['grandtotal'][$key];
                $post_qty = $_POST['qty'][$key];
                $sales_sku_id = $_POST['skuid'][$key];
                $barcode = isset($_POST['barcode']) ? $_POST['barcode'] : '';
                $dealer_id = isset($_POST['dealer_id']) ? $_POST['dealer_id'] : '';
                $where_query = array(["", "id", "=","$sales_sku_id"], ["AND", "dealer_id", "=","$dealer_id"],["AND", "status", "=","1"]);
                $comeback = $this->select_result(TABLE_SALES_SKU_MASTER, '*', $where_query);
                $getbarcode = (isset($comeback["data"][0]["barcode"]) && !empty($comeback["data"][0]["barcode"])) ?$comeback["data"][0]["barcode"] : '';
                $getsku = (isset($comeback["data"][0]["sku"]) && !empty($comeback["data"][0]["sku"])) ?$comeback["data"][0]["sku"] : '';
                $qty = (isset($comeback["data"][0]["qty"]) && !empty($comeback["data"][0]["qty"])) ?$comeback["data"][0]["qty"] : '';
                 $return_qty = (isset($comeback["data"][0]["return_qty"]) && !empty($comeback["data"][0]["return_qty"])) ?$comeback["data"][0]["return_qty"] : '';
                $id = (isset($comeback["data"][0]["id"]) && !empty($comeback["data"][0]["id"])) ?$comeback["data"][0]["id"] : '';
                $sales_id = (isset($comeback["data"][0]["sales_id"]) && !empty($comeback["data"][0]["sales_id"])) ?$comeback["data"][0]["sales_id"] : '';
             
                if ($barcodevalue == $getbarcode) {
                       $result['dataResult'] = "Success";
                       $fields = array(
                        'status' => 0,
                    );
                    $where_query = array(
                        ["", "id", "=", $id],
                    );
                    $comeback = $this->put_data(TABLE_SALES_SKU_MASTER, $fields, $where_query);
                     if ($barcodevalue != ''){
                        $end_point = "https://".$store."/admin/api/graphql.json";
                        $shopify_data_list = cls_api_graphql_barcode_call(CLS_API_KEY, $password, $store, $end_point, $barcodevalue, 'POST');

                        $decode_data = json_decode($shopify_data_list["response"]);
                        $checkbarcode = $decode_data->data->productVariants->edges[0]->node->barcode;
                        if (!empty($checkbarcode)) {
                            $inventoryItem_id = isset($decode_data->data->productVariants->edges[0]->node->inventoryItem->id) ? $decode_data->data->productVariants->edges[0]->node->inventoryItem->id : '';
                            $inventoryItem_id = str_replace("gid://shopify/InventoryItem/", "", $inventoryItem_id);

                            $inventory_level_api = array('api_name' => 'inventory_levels');
                            $fields = array('inventory_item_ids' => $inventoryItem_id);
                            $get_level = $this->cls_get_shopify_list($inventory_level_api, $fields, 'GET');
                            $inventory_levels = $get_level->inventory_levels[0];
                            $get_location_id = (isset($inventory_levels->location_id) && $inventory_levels->location_id !== '' ) ? $inventory_levels->location_id : '';
                            $get_available = (isset($inventory_levels->available) && $inventory_levels->available !== '' ) ? $inventory_levels->available : '';
                          
                            $get_available = (int)$get_available + (int)$qty;
                            if ($get_location_id != '') {
                                $set_api = array('api_name' => 'inventory_levels', 'set' => 'set');
                                $filed_arr = array('location_id' => $get_location_id, 'inventory_item_id' => $inventoryItem_id, 'available' => (int) $get_available);
                                $update_inve = $this->cls_get_shopify_list($set_api, $filed_arr, 'POST');
                                $fields_arr_purchasereturn = array(
                                   
                                     '`sales_sku_id`' =>$id,
                                    '`sales_id`' =>$sales_id
                                );
                                     $where_query_purchasereturn = array(
                                        ["", "id", "=", $sales_return_id],
                                    );
                                      $comeback = $this->put_data(TABLE_SALES_ORDER_RETURN, $fields_arr_purchasereturn, $where_query_purchasereturn);
//                               $sales_return_id = $sales__return_data['last_insert_id'];
                                    $numlength = strlen((string) $sales_return_id);
                                    if ($numlength == 1) {
                                        $numlength = '000' . $sales_return_id;
                                    } elseif ($numlength == 2) {
                                        $numlength = '00' . $sales_return_id;
                                    } elseif ($numlength == 3) {
                                        $numlength = '0' . $sales_return_id;
                                    } else {
                                        $numlength = $sales_return_id;
                                    }
                                    $year = date('y');
                                    $nextYear = $year + 1;
                                    $voucher_no = "GRTN/" . $year . "-" . $nextYear . "/" . $numlength;
                                    $fields = array(
                                        '`voucher_no`' => $voucher_no,
                                    );
                                    $where_query = array(
                                        ["", "id", "=", $sales_return_id],
                                    );
                                     $comeback = $this->put_data(TABLE_SALES_ORDER_RETURN, $fields, $where_query);
                                     $update_qty = $qty - (int)$return_qty;
                                     
                                     if($update_qty >= $post_qty){
                                        
                                         $post_qty = $post_qty + (int)$return_qty;
                                         $update_qty = $qty -$post_qty ;
                                     if($update_qty == 0){
                                        $fields = array(
                                            'return_qty' => $post_qty,
                                            'status' => 0,
                                        );
                                          $where_query = array(
                                            ["", "id", "=", $id],
                                        );
                                          $comeback = $this->put_data(TABLE_SALES_SKU_MASTER, $fields, $where_query);
                                     }  elseif($update_qty > 0) {
                                        $post_qty = $post_qty + (int)$return_qty;

                                         $fields = array(
                                            'return_qty' => $post_qty,
                                            'status' => 1,
                                             
                                        );
                                            $where_query = array(
                                            ["", "id", "=", $id],
                                        );
                                           $comeback = $this->put_data(TABLE_SALES_SKU_MASTER, $fields, $where_query);
                                    }
                                     }else{
                                          $result['dataResult'] = "fail";
                                    alert( "Please enter valid Quantity");
                                        $result['errors']['update_qty'] = $update_qty;
                                     }
                            }
                        } else {
                            $result['dataResult'] = "fail";
                            $result['errors']['sku'] = "Invalid Barcode tt";
                        }
                    }
                }
                else{
                          $result['dataResult'] = "fail";
                          $result['errors']['sku'] = "Invalid Barcode";
                    }
            }
             }else{
                 $fields_arr = array(
                                    '`id`' => '',
//                                    '`sales_sku_id`' =>$id,
//                                    '`sales_id`' =>$sales_id,
                                     '`store_user_id`' => $store_user_id,    
                                    '`so_type`' => $_POST['so_type'],
                                    '`dealer_id`' => $_POST['dealer_id'],
                                    '`return_date`' => $_POST['return_date'],
                                    '`ref_num`' => $_POST['ref_num'],
                                    '`qty_ret`' => $_POST['qty_ret'],
                                    '`price_ret`' => $_POST['price_ret'],
                                    '`reason`' => $_POST['reason'],
                                     '`qty`' => $_POST['total_qty'],
                                    '`taxable`' => $_POST['total_taxable'],
//                                    '`discount`' => $_POST['total_discount'],
                                    '`cgst`' => $_POST['total_cgst'],
                                    '`sgst`' => $_POST['total_sgst'],
                                    '`igst`' => $_POST['total_igst'],
                                    '`grand_total`' => $_POST['grand_amount'],
                                    '`created_at`' => date('Y-m-d H:i:s'),
                                    '`updated_at`' => date('Y-m-d H:i:s')
                                );
                             $sales__return_data =  $this->post_data(TABLE_SALES_ORDER_RETURN, array($fields_arr));
                               $sales_return_id = $sales__return_data['last_insert_id'];
                                    $numlength = strlen((string) $sales_return_id);
                                    if ($numlength == 1) {
                                        $numlength = '000' . $sales_return_id;
                                    } elseif ($numlength == 2) {
                                        $numlength = '00' . $sales_return_id;
                                    } elseif ($numlength == 3) {
                                        $numlength = '0' . $sales_return_id;
                                    } else {
                                        $numlength = $sales_return_id;
                                    }
                                    $year = date('y');
                                    $nextYear = $year + 1;
                                    $voucher_no = "GRTN/" . $year . "-" . $nextYear . "/" . $numlength;
                                    $fields = array(
                                        '`voucher_no`' => $voucher_no,
                                    );
                                    $where_query = array(
                                        ["", "id", "=", $sales_return_id],
                                    );
                                     $comeback = $this->put_data(TABLE_SALES_ORDER_RETURN, $fields, $where_query);
            }
        }
        return json_encode($result);
    }
    
    
    function phpvalidation_dealer() {
        $error_array = array();

         if (isset($_POST['firstname']) && (ctype_alpha(str_replace(' ', '', $_POST['firstname'])) == '')) {
            $error_array['firstname'] = "please enter first name";
        }
        return $error_array;
    }

    function dealer() {
        $shopinfo = $this->current_store_obj;
        $store_user_id = $shopinfo[0]['store_user_id'];
       
        $response = array();
        $error_array = $this->phpvalidation_dealer();
        if (!empty($error_array)) {
            $result['dataResult'] = "fail";
            $result['errors'] = $error_array;
        } else {
            $result['dataResult'] = "Success";
            $words = explode(" ", $_POST['firstname']);
            $acronym = "";
            foreach ($words as $w) {
                $acronym .= $w[0];
            }
            $same_address = (isset($_POST["same_address"])) ? 1 : 0;
            if (isset($_POST["same_address"])) {
                $fields_arr = array(
                '`id`' => '',
                    '`store_user_id`' => $store_user_id,
                    '`firstname`' => $_POST['firstname'],
                    '`lastname`' => $_POST['lastname'],
                    '`contact_person`' => $_POST['contact_person'],
                    '`email`' => $_POST['email'],
                    '`username`' => $_POST['username'],
                    '`password`' => $_POST['password'],
                    '`same_address`' => $same_address,
                    '`address`' => $_POST['address'],
                    '`landmark`' => $_POST['landmark'],
                    '`city`' => $_POST['city'],
                    '`state`' => $_POST['state'],
                    '`country`' => $_POST['country'],
                    '`phone`' => $_POST['phone'],
                    '`mobile`' => $_POST['mobile'],
                    '`postal_code`' => $_POST['postal_code'],
                    '`gst`' => $_POST['gst'],
                    '`pan`' => $_POST['pan'],
                    '`tin`' => $_POST['tin'],
                    '`vat`' => $_POST['vat'],
                    '`bank_name`' => $_POST['bank_name'],
                    '`bank_account_no`' => $_POST['bank_account_no'],
                    '`bank_branch`' => $_POST['bank_branch'],
                    '`ifsc_code`' => $_POST['ifsc_code'],
                    '`approved`' => $_POST['approved'],
                    '`discount`' => $_POST['discount'],
                    '`active`' => $_POST['active'],
                    '`billing_address`' => $_POST['address'],
                    '`billing_landmark`' => $_POST['landmark'],
                    '`billing_city`' => $_POST['city'],
                    '`billing_state`' => $_POST['state'],
                    '`billing_country`' => $_POST['country'],
                    '`billing_phone`' => $_POST['phone'],
                    '`billing_mobile`' => $_POST['mobile'],
                    '`billing_postal_code`' => $_POST['postal_code'],
                    '`billing_gst`' => $_POST['gst'],
                    '`created_at`' => date('Y-m-d H:i:s'),
                    '`updated_at`' => date('Y-m-d H:i:s')
                );
          }else{
            $fields_arr = array(
                           '`id`' => '',
                            '`store_user_id`' => $store_user_id,
                           '`firstname`' => $_POST['firstname'],
                           '`lastname`' => $_POST['lastname'],
                           '`contact_person`' => $_POST['contact_person'],
                           '`email`' => $_POST['email'],
                           '`username`' => $_POST['username'],
                           '`password`' => $_POST['password'],
                           '`same_address`' =>$same_address,
                           '`address`' => $_POST['address'],
                           '`landmark`' => $_POST['landmark'],
                           '`city`' => $_POST['city'],
                           '`state`' => $_POST['state'],
                           '`country`' => $_POST['country'],
                           '`phone`' => $_POST['phone'],
                           '`mobile`' => $_POST['mobile'],
                           '`postal_code`' => $_POST['postal_code'],
                           '`gst`' => $_POST['gst'],
                           '`pan`' => $_POST['pan'],
                           '`tin`' => $_POST['tin'],
                           '`vat`' => $_POST['vat'],
                           '`bank_name`' => $_POST['bank_name'],
                           '`bank_account_no`' => $_POST['bank_account_no'],
                           '`bank_branch`' => $_POST['bank_branch'],
                           '`ifsc_code`' => $_POST['ifsc_code'],
                           '`approved`' => $_POST['approved'],
                           '`discount`' => $_POST['discount'],
                           '`active`' => $_POST['active'],
                           '`billing_address`' => $_POST['billing_address'],
                           '`billing_landmark`' => $_POST['billing_landmark'],
                           '`billing_city`' => $_POST['billing_city'],
                           '`billing_state`' => $_POST['billing_state'],
                           '`billing_country`' => $_POST['billing_country'],
                           '`billing_phone`' => $_POST['billing_phone'],
                           '`billing_mobile`' => $_POST['billing_mobile'],
                           '`billing_postal_code`' => $_POST['billing_postal_code'],
                           '`billing_gst`' => $_POST['billing_gst'],
                           '`created_at`' => date('Y-m-d H:i:s'),
                           '`updated_at`' => date('Y-m-d H:i:s')
                       ); 
                             }
            $result = $this->post_data(TABLE_DEALER, array($fields_arr));
             $id = $result['last_insert_id'];
            $numlength = strlen((string)$id);
            if($numlength == 1){
                $numlength = '0000'.$id;
            }elseif($numlength == 2){
                $numlength = '000'.$id;
            }elseif($numlength == 3){
                $numlength = '00'.$id;
            }elseif($numlength == 4){
                $numlength = '0'.$id;
            }else{
                $numlength = $id;
                
            }
                $generate_code = $acronym .'-' .$_POST['city'] .'-'.$numlength;
                  $fields = array(
                '`generate_code`' => $generate_code,
            );
            $where_query = array(
                ["", "id", "=", $id],
            );
            $comeback = $this->put_data(TABLE_DEALER, $fields, $where_query);
        }
        return json_encode($result);
    }

    function allbutton_details() {
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        if (isset($_POST["for_data"]) && $_POST["for_data"] == "blogpost") {
            $id = isset($_POST['blogpost_id']) ? $_POST['blogpost_id'] : '';
            $where_query = array(["", "blogpost_id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_BLOGPOST_MASTER, '*', $where_query);
            if (empty($comeback["data"])) {
                $fields_arr = array(
                    '`blogpost_id`' => $_POST['blogpost_id'],
                    '`description`' => $_POST["description"],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $comeback = $this->post_data(TABLE_BLOGPOST_MASTER, array($fields_arr));
                $api_fields = array('blog' => array('id' => $_POST['blogpost_id'], 'title' => $_POST['title'], 'body_html' => $_POST["description"]));
                $main_api = array("api_name" => "articles", 'id' => $_POST['blogpost_id']);
                $set_position = $this->cls_get_shopify_list($main_api, $api_fields, 'PUT', 1, array("Content-Type: application/json"));
                $comeback = array("data" => true);
            } else {
                $fields = array(
                    'title' => $_POST['title'],
                    'description' => $_POST["description"],
                );
                $where_query = array(
                    ["", "blogpost_id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_BLOGPOST_MASTER, $fields, $where_query);
                $description = str_replace("data:image/png;base64,", "", $_POST["description"]);

                $api_fields = array('article' => array('id' => $_POST['blogpost_id'], 'title' => $_POST["title"], 'body_html' => $description));
                $main_api = array("api_name" => "articles", 'id' => $_POST['blogpost_id']);
                $set_position = $this->cls_get_shopify_list($main_api, $api_fields, 'PUT', 1, array("Content-Type: application/json"));
                $comeback = array("data" => true, "for_data" => 'blog');
            }
        } else if (isset($_POST["for_data"]) && $_POST["for_data"] == 'page') {
            $id = isset($_POST['page_id']) ? $_POST['page_id'] : '';
            $where_query = array(["", "page_id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_PAGE_MASTER, '*', $where_query);
            if (empty($comeback["data"])) {
                $fields_arr = array(
                    '`page_id`' => $_POST['page_id'],
                    '`description`' => $_POST["description"],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $comeback = $this->post_data(TABLE_PAGE_MASTER, array($fields_arr));
                $api_fields = array('page' => array('id' => $_POST['page_id'], 'title' => $_POST["title"], 'body_html' => $_POST["description"]));
                $main_api = array("api_name" => "pages", 'id' => $_POST['page_id']);
                $set_position = $this->cls_get_shopify_list($main_api, $api_fields, 'PUT', 1, array("Content-Type: application/json"));
                $comeback = array("data" => true);
            } else {
                $fields = array(
                    'title' => $_POST['title'],
                    'description' => $_POST["description"],
                );
                $where_query = array(
                    ["", "page_id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_PAGE_MASTER, $fields, $where_query);
                $api_fields = array('page' => array('id' => $_POST['page_id'], 'title' => $_POST["title"], 'body_html' => $_POST["description"]));
                $main_api = array("api_name" => "pages", 'id' => $_POST['page_id']);
                $set_position = $this->cls_get_shopify_list($main_api, $api_fields, 'PUT', 1, array("Content-Type: application/json"));
                $comeback = array("data" => true, "for_data" => 'page');
            }
        } else if (isset($_POST['for_data']) && $_POST["for_data"] == 'product') {
            $product_id = isset($_POST['product_id']) ? $_POST['product_id'] : '';
            $where_query = array(["", "product_id", "=", "$product_id"]);
            $comeback = $this->select_result(TABLE_PRODUCT_MASTER, '*', $where_query);
            if (empty($comeback["data"])) {
                $fields_arr = array(
                    '`product_id`' => $_POST['product_id'],
                    '`description`' => $_POST["description"],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $comeback = $this->post_data(TABLE_PRODUCT_MASTER, array($fields_arr));
                $api_fields = array('products' => array('product_id' => $_POST['product_id'], 'title' => $_POST["title"], 'body_html' => $_POST["description"]));
                $main_api = array("api_name" => "products", 'id' => $_POST['product_id']);
                $set_position = $this->cls_get_shopify_list($main_api, $api_fields, 'PUT', 1, array("Content-Type: application/json"));
                $comeback = array("data" => true);
            } else {
                $fields = array(
                    'title' => $_POST['title'],
                    'description' => $_POST["description"],
                );
                $where_query = array(
                    ["", "product_id", "=", $product_id],
                );
                $comeback = $this->put_data(TABLE_PRODUCT_MASTER, $fields, $where_query);
                $api_fields = array('product' => array('id' => $_POST['product_id'], 'title' => $_POST["title"], 'body_html' => $_POST["description"]));
                $main_api = array("api_name" => "products", 'id' => $_POST['product_id']);
                $set_position = $this->cls_get_shopify_list($main_api, $api_fields, 'PUT', 1, array("Content-Type: application/json"));
                $comeback = array("data" => true, "for_data" => 'product');
            }
        } else if (isset($_POST['for_data']) && $_POST["for_data"] == 'supplier') {
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            $where_query = array(["", "id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_SUPPLIERS, '*', $where_query);
            if (empty($comeback["data"])) {
                $fields_arr = array(
                    '`id`' => $_POST['id'],
                    '`supplier_type`' => $_POST['supplier_type'],
                    '`supplier_name`' => $_POST['supplier_name'],
                    '`supplier_contact_person`' => $_POST['supplier_contact_person'],
                    '`supplier_email`' => $_POST['supplier_email'],
                    '`supplier_phone`' => $_POST['supplier_phone'],
                    '`supplier_mobile`' => $_POST['supplier_mobile'],
                    '`supplier_address`' => $_POST['supplier_address'],
                    '`supplier_landmark`' => $_POST['supplier_landmark'],
                    '`supplier_city`' => $_POST['supplier_city'],
                    '`supplier_state`' => $_POST['supplier_state'],
                    '`supplier_country`' => $_POST['supplier_country'],
                    '`supplier_pincode`' => $_POST['supplier_pincode'],
                    '`supplier_pan`' => $_POST['supplier_pan'],
                    '`supplier_tin`' => $_POST['supplier_tin'],
                    '`supplier_gst`' => $_POST['supplier_gst'],
                    '`supplier_vat`' => $_POST['supplier_vat'],
                    '`supplier_bank_name`' => $_POST['supplier_bank_name'],
                    '`supplier_bank_account_no`' => $_POST['supplier_bank_account_no'],
                    '`supplier_bank_branch`' => $_POST['supplier_bank_branch'],
                    '`supplier_ifsc_code`' => $_POST['supplier_ifsc_code'],
                    '`supplier_remarks`' => $_POST['supplier_remarks'],
                    '`supplier_status`' => $_POST['supplier_status'],
                    '`created_at`' => date('Y-m-d H:i:s'),
                    '`updated_at`' => date('Y-m-d H:i:s')
                );
                $comeback = $this->post_data(TABLE_PRODUCT_MASTER, array($fields_arr));
                    $comeback = array("data" => true);
            } else {
                $fields = array(
                    '`supplier_type`' => $_POST['supplier_type'],
                    '`supplier_name`' => $_POST['supplier_name'],
                    '`supplier_contact_person`' => $_POST['supplier_contact_person'],
                    '`supplier_email`' => $_POST['supplier_email'],
                    '`supplier_phone`' => $_POST['supplier_phone'],
                    '`supplier_mobile`' => $_POST['supplier_mobile'],
                    '`supplier_address`' => $_POST['supplier_address'],
                    '`supplier_landmark`' => $_POST['supplier_landmark'],
                    '`supplier_city`' => $_POST['supplier_city'],
                    '`supplier_state`' => $_POST['supplier_state'],
                    '`supplier_country`' => $_POST['supplier_country'],
                    '`supplier_pincode`' => $_POST['supplier_pincode'],
                    '`supplier_pan`' => $_POST['supplier_pan'],
                    '`supplier_tin`' => $_POST['supplier_tin'],
                    '`supplier_gst`' => $_POST['supplier_gst'],
                    '`supplier_vat`' => $_POST['supplier_vat'],
                    '`supplier_bank_name`' => $_POST['supplier_bank_name'],
                    '`supplier_bank_account_no`' => $_POST['supplier_bank_account_no'],
                    '`supplier_bank_branch`' => $_POST['supplier_bank_branch'],
                    '`supplier_ifsc_code`' => $_POST['supplier_ifsc_code'],
                    '`supplier_remarks`' => $_POST['supplier_remarks'],
                    '`supplier_status`' => $_POST['supplier_status'],
                    '`created_at`' => date('Y-m-d H:i:s'),
                    '`updated_at`' => date('Y-m-d H:i:s')
                );
                $where_query = array(
                    ["", "id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_SUPPLIERS, $fields, $where_query);

                $comeback = array("data" => true, "for_data" => 'supplier');
                        }
        } else if (isset($_POST['for_data']) && $_POST["for_data"] == 'purchase') {
            $shopinfo = $this->current_store_obj;
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            $where_query = array(["", "id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_PURCHASE, '*', $where_query);
            if (empty($comeback["data"])) {
                $fields_arr = array(
                    '`id`' => $_POST['id'],
                    '`freight`' => $_POST['freight'],
                    '`packing`' => $_POST['packing'],
                    '`insurance`' => $_POST['insurance'],
                    '`po_date`' => $_POST['po_date'],
                    '`supplier_id`' => $_POST['supplier_id'],
                    '`rec_ref_code`' => $_POST['rec_ref_code'],
                    '`supplier_address`' => $_POST['supplier_address'],
                    '`sku`' => $_POST['sku'],
                    '`remarks`' => $_POST['remarks'],
                    '`reverse_charge`' => $_POST['reverse_charge'],
                    '`all_total`' => $_POST['all_total'],
                    '`supplier_invoice_no`' => $_POST['supplier_invoice_no'],
                    '`supplier_invoice_date`' => $_POST['supplier_invoice_date'],
                );
                $comeback = $this->post_data(TABLE_PURCHASE, array($fields_arr));
                $comeback = array("data" => true);
            } else {
                $fields = array(
                    '`freight`' => $_POST['freight'],
                    '`packing`' => $_POST['packing'],
                    '`insurance`' => $_POST['insurance'],
                    '`po_date`' => $_POST['po_date'],
                    '`supplier_id`' => $_POST['supplier_id'],
                    '`rec_ref_code`' => $_POST['rec_ref_code'],
                    '`supplier_address`' => $_POST['supplier_address'],
                    '`remarks`' => $_POST['remarks'],
                    '`reverse_charge`' => $_POST['reverse_charge'],
                    '`supplier_invoice_no`' => $_POST['supplier_invoice_no'],
                    '`supplier_invoice_date`' => $_POST['supplier_invoice_date'],
                );
                $where_query = array(
                    ["", "id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_PURCHASE, $fields, $where_query);
                
                 foreach ($_POST['sku'] as $key => $skuvalue) {
                 $store = $shopinfo[0]['shop_name'];
                    $password = $shopinfo[0]['password'];
                     $category_name = $_POST['category_name'][$key];
                            $sku_id = $_POST['skuid'][$key];
                            $v_sku = $_POST['v_sku'][$key];
                            $qty = $_POST['qty'][$key];
                            $price = $_POST['price'][$key];
                            $taxable = $_POST['taxable'][$key];
                            $cgst = $_POST['cgst'][$key];
                            $sgst = $_POST['sgst'][$key];
                            $igst = $_POST['igst'][$key];
                            $grand_total = $_POST['grand_total'][$key];
                             if ($skuvalue != ''){
                                    $sku = $skuvalue;
                                    $end_point = "https://" . $store . "/admin/api/graphql.json";
                                    $shopify_data_list = cls_api_graphql_call(CLS_API_KEY, $password, $store, $end_point, $sku, 'POST');
                                    $decode_data = json_decode($shopify_data_list["response"]);
                                    $checksku = $decode_data->data->productVariants->edges;
                                    if (!empty($checksku)) {
                                         $barcode = isset($decode_data->data->productVariants->edges[0]->node->barcode) ? $decode_data->data->productVariants->edges[0]->node->barcode : '';
                                        $inventoryItem_id = isset($decode_data->data->productVariants->edges[0]->node->inventoryItem->id) ? $decode_data->data->productVariants->edges[0]->node->inventoryItem->id : '';
                                        $inventoryItem_id = str_replace("gid://shopify/InventoryItem/", "", $inventoryItem_id);

                                        $inventory_level_api = array('api_name' => 'inventory_levels');
                                        $fields = array('inventory_item_ids' => $inventoryItem_id);
                                        $get_level = $this->cls_get_shopify_list($inventory_level_api, $fields, 'GET');

                                        $inventory_levels = $get_level->inventory_levels[0];
                                        $get_location_id = (isset($inventory_levels->location_id) && $inventory_levels->location_id !== '' ) ? $inventory_levels->location_id : '';
                                        $get_available = (isset($inventory_levels->available) && $inventory_levels->available !== '' ) ? $inventory_levels->available : '';
                                        
                                        $id = isset($_POST['id']) ? $_POST['id'] : '';
                                        $where_query = array(["", "id", "=", $sku_id]);
                                        $comeback = $this->select_result(TABLE_SKU_MASTER, '*', $where_query);
                                        $select_qty = (isset($comeback['data'][0]['qty']) && $comeback['data'][0]['qty'] !== '' ) ? $comeback['data'][0]['qty'] : 0 ;
                                      
                                        if($select_qty >= $qty){
                                            $qty = (int)$select_qty - (int)$qty;
                                            $get_available = (int)$get_available - (int)$qty ;
                                        }elseif($select_qty <= $qty){
                                            $qty = (int)$qty - (int)$select_qty;
                                            $get_available = (int)$get_available + (int)$qty ;
                                        }
                                        if ($get_location_id != '') {
                                            $set_api = array('api_name' => 'inventory_levels', 'set' => 'set');
                                            $filed_arr = array('location_id' => $get_location_id, 'inventory_item_id' => $inventoryItem_id, 'available' => (int) $get_available);
                                            $update_inve = $this->cls_get_shopify_list($set_api, $filed_arr, 'POST');
                                            $category_name = $_POST['category_name'][$key];
                                            $sku_id = $_POST['skuid'][$key];
                                            $v_sku = $_POST['v_sku'][$key];
                                            $qty = $_POST['qty'][$key];
                                            $price = $_POST['price'][$key];
                                            $taxable = $_POST['taxable'][$key];
                                            $cgst = $_POST['cgst'][$key];
                                            $sgst = $_POST['sgst'][$key];
                                            $igst = $_POST['igst'][$key];
                                            $grand_total = $_POST['grand_total'][$key];
                                            $fields_arr = array(
                                                '`supplier_id`' => $_POST['supplier_id'],
                                                '`qty`' => $qty,
                                                '`price`' => $price,
                                                '`taxable`' => $taxable,
                                                '`cgst`' => $cgst,
                                                '`sgst`' => $sgst,
                                                '`igst`' => $igst,
                                                '`grand_total`' => $grand_total,
                                                '`created_at`' => date('Y-m-d H:i:s'),
                                                '`updated_at`' => date('Y-m-d H:i:s')
                                            );
                                            $fields_arrwhere_query = array(
                                                ["", "purchase_id", "=", $id], ["AND", "id", "=", $sku_id]
                                            );
                                            $comeback = $this->put_data(TABLE_SKU_MASTER, $fields_arr, $fields_arrwhere_query);
                                        }
                                    }
                                }
                          
                }
                 $fields_purchase = array(
                    '`qty`' => $_POST['total_quantity'],
                    '`taxable`' => $_POST['total_taxable'],
                    '`cgst`' => $_POST['total_cgst'],
                    '`sgst`' => $_POST['total_sgst'],
                    '`igst`' => $_POST['total_igst'],
                    '`grand_total`' => $_POST['grand_amount'],
                     '`all_total`' => $_POST['all_total'],
                );
                $where_query_purchase = array(
                    ["", "id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_PURCHASE, $fields_purchase, $where_query_purchase); 
                

                $comeback = array("data" => true, "for_data" => 'purchase');
            }   
        } else if (isset($_POST['for_data']) && $_POST["for_data"] == 'purchase_return') {
            $shopinfo = $this->current_store_obj;
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            $where_query = array(["", "id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_PURCHASE_RETURN, '*', $where_query);
            if (empty($comeback["data"])) {
                
                $fields_arr = array(
                    '`id`' => $_POST['id'],
                    '`po_type`' => $_POST['po_type'],
                    '`return_date`' => $_POST['returndate'],
                    '`ref_num`' => $_POST['ref_num'],
                    '`qty_ret`' => $_POST['qty_ret'],
                    '`price_ret`' => $_POST['price_ret'],
                    '`reason`' => $_POST['reason'],
                );
                $comeback = $this->post_data(TABLE_PURCHASE_RETURN, array($fields_arr));
               
                $comeback = array("data" => true);
            } else {
                $fields = array(
                    '`po_type`' => $_POST['po_type'],
                    '`ref_num`' => $_POST['ref_num'],
                    '`return_date`' => $_POST['returndate'],
                    '`supplier_id`' => $_POST['supplier_id'],
                    '`qty_ret`' => $_POST['qty_ret'],
                    '`price_ret`' => $_POST['price_ret'],
                    '`reason`' => $_POST['reason'],
                );
                $where_query = array(
                    ["", "id", "=", $id],
                );
                
                $comeback = $this->put_data(TABLE_PURCHASE_RETURN, $fields, $where_query);
            if(!empty($_POST['barcode'])){
                   foreach ($_POST['barcode'] as $key => $barcodevalue) {
                $sku_id = $_POST['sku_id'][$key];
                $category_name = $_POST['category_name'][$key];
                $post_qty = $_POST['qty'][$key];
                $grand_total = $_POST['grand_total'][$key];
                $sku = isset($_POST['sku']) ? $_POST['sku'] : '';
                $supplier_id = isset($_POST['supplier_id']) ? $_POST['supplier_id'] : '';
                $where_query = array(["", "barcode", "=",$barcodevalue], ["AND", "id", "=",$sku_id],["AND", "status", "=",1]);
                $comeback = $this->select_result(TABLE_SKU_MASTER, '*', $where_query);
             
                $getbarcode = (isset($comeback["data"][0]["barcode"]) && !empty($comeback["data"][0]["barcode"])) ?$comeback["data"][0]["barcode"] : '';
                $getsku = (isset($comeback["data"][0]["sku"]) && !empty($comeback["data"][0]["sku"])) ?$comeback["data"][0]["sku"] : '';
                $total = (isset($comeback["data"][0]["total"]) && !empty($comeback["data"][0]["total"])) ?$comeback["data"][0]["total"] : '';
                $cgst = (isset($comeback["data"][0]["cgst"]) && !empty($comeback["data"][0]["cgst"])) ?$comeback["data"][0]["cgst"] : '';
                $sgst = (isset($comeback["data"][0]["sgst"]) && !empty($comeback["data"][0]["sgst"])) ?$comeback["data"][0]["sgst"] : '';
                $igst = (isset($comeback["data"][0]["igst"]) && !empty($comeback["data"][0]["igst"])) ?$comeback["data"][0]["igst"] : '';
                $qty = (isset($comeback["data"][0]["qty"]) && !empty($comeback["data"][0]["qty"])) ?$comeback["data"][0]["qty"] : '';
                $return_qty = (isset($comeback["data"][0]["return_qty"]) && !empty($comeback["data"][0]["return_qty"])) ?$comeback["data"][0]["return_qty"] : 0;
                $grand_total = (isset($comeback["data"][0]["grand_total"]) && !empty($comeback["data"][0]["grand_total"])) ?$comeback["data"][0]["grand_total"] : '';
                $sku_id = (isset($comeback["data"][0]["id"]) && !empty($comeback["data"][0]["id"])) ?$comeback["data"][0]["id"] : '';
                $purchase_id = (isset($comeback["data"][0]["purchase_id"]) && !empty($comeback["data"][0]["purchase_id"]) ?$comeback["data"][0]["purchase_id"] : '');
                if ($barcodevalue == $getbarcode) {
                     $result['dataResult'] = "Success";
                     if ($barcodevalue != ''){
                        $store = $shopinfo[0]['shop_name'];
                        $password = $shopinfo[0]['password'];
                        $end_point = "https://".$store."/admin/api/graphql.json";
                        $shopify_data_list = cls_api_graphql_barcode_call(CLS_API_KEY, $password, $store, $end_point, $barcodevalue, 'POST');
                        $decode_data = json_decode($shopify_data_list["response"]);
                        $checkbarcode = $decode_data->data->productVariants->edges[0]->node->barcode;
                        if (!empty($checkbarcode)) {
                            $inventoryItem_id = isset($decode_data->data->productVariants->edges[0]->node->inventoryItem->id) ? $decode_data->data->productVariants->edges[0]->node->inventoryItem->id : '';
                            $inventoryItem_id = str_replace("gid://shopify/InventoryItem/", "", $inventoryItem_id);

                            $inventory_level_api = array('api_name' => 'inventory_levels');
                            $fields = array('inventory_item_ids' => $inventoryItem_id);
                            $get_level = $this->cls_get_shopify_list($inventory_level_api, $fields, 'GET');
                            $inventory_levels = $get_level->inventory_levels[0];
                            $get_location_id = (isset($inventory_levels->location_id) && $inventory_levels->location_id !== '' ) ? $inventory_levels->location_id : '';
                            $get_available = (isset($inventory_levels->available) && $inventory_levels->available !== '' ) ? $inventory_levels->available : '';
                                                               
                            if ($return_qty >= $post_qty) {
                                    $available_qty = (int)$return_qty - (int)$post_qty;
                                    $get_available = (int)$get_available + (int)$available_qty;
                                } elseif ($return_qty <= $post_qty) {
                                    $available_qty = (int)$post_qty - (int)$return_qty;
                                    $get_available = (int)$get_available - (int)$available_qty;
                                }
                            if ($get_location_id != '') {
                                $set_api = array('api_name' => 'inventory_levels', 'set' => 'set');
                                $filed_arr = array('location_id' => $get_location_id, 'inventory_item_id' => $inventoryItem_id, 'available' => (int) $get_available);
                                $update_inve = $this->cls_get_shopify_list($set_api, $filed_arr, 'POST');
                                 $fields = array(
                                            'return_qty' => $post_qty,
                                        );
                                          $where_query = array(
                                        ["", "id", "=", $sku_id],
                                    );
                                          $comeback = $this->put_data(TABLE_SKU_MASTER, $fields, $where_query);
                                }
                            } else {
                            $result['dataResult'] = "fail";
                            $result['errors']['sku'] = "Invalid Barcode uhu";
                        }
                    }
                }else{
                          $result['dataResult'] = "fail";
                          $result['errors']['sku'] = "Invalid Barcode";
                    }
            }  
  }
                $fields_purchase = array(
                    '`qty`' => $_POST['total_quantity'],
                    '`taxable`' => $_POST['total_taxable'],
                    '`cgst`' => $_POST['total_cgst'],
                    '`sgst`' => $_POST['total_sgst'],
                    '`igst`' => $_POST['total_igst'],
                    '`grand_total`' => $_POST['grand_amount'],
                );
                $where_query_purchase = array(
                    ["", "id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_PURCHASE_RETURN, $fields_purchase, $where_query_purchase); 
                $comeback = array("data" => true, "for_data" => 'purchase_return');
            }
        } else if (isset($_POST['for_data']) && $_POST["for_data"] == 'dealer') {
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            $where_query = array(["", "id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_DEALER, '*', $where_query);
            if (empty($comeback["data"])) {
                $fields_arr = array(
                    '`id`' => $_POST['id'],
                    '`active`' => $_POST['active'],
                    '`discount`' => $_POST['discount'],
                    '`approved`' => $_POST['approved'],
                    '`ifsc_code`' => $_POST['ifsc_code'],
                    '`bank_branch`' => $_POST['bank_branch'],
                    '`bank_account_no`' => $_POST['bank_account_no'],
                    '`bank_name`' => $_POST['bank_name'],
                    '`bank_account_no`' => $_POST['bank_account_no'],
                    '`vat`' => $_POST['vat'],
                    '`tin`' => $_POST['tin'],
                    '`pan`' => $_POST['pan'],
                    '`billing_gst`' => $_POST['billing_gst'],
                    '`gst`' => $_POST['gst'],
                    '`billing_postal_code`' => $_POST['billing_postal_code'],
                    '`postal_code`' => $_POST['postal_code'],
                    '`billing_mobile`' => $_POST['billing_mobile'],
                    '`mobile`' => $_POST['mobile'],
                    '`billing_phone`' => $_POST['billing_phone'],
                    '`phone`' => $_POST['phone'],
                    '`billing_country`' => $_POST['billing_country'],
                    '`country`' => $_POST['country'],
                    '`billing_state`' => $_POST['billing_state'],
                    '`state`' => $_POST['state'],
                    '`billing_city`' => $_POST['billing_city'],
                    '`city`' => $_POST['city'],
                    '`billing_landmark`' => $_POST['billing_landmark'],
                    '`firstname`' => $_POST['firstname'],
                    '`landmark`' => $_POST['landmark'],
                    '`billing_address`' => $_POST['billing_address'],
                    '`address`' => $_POST['address'],
                    '`password`' => $_POST['password'],
                    '`username`' => $_POST['username'],
                    '`email`' => $_POST['email'],
                    '`contact_person`' => $_POST['contact_person'],
                    '`lastname`' => $_POST['lastname'],
                );
                $comeback = $this->post_data(TABLE_DEALER, array($fields_arr));
                $comeback = array("data" => true);
            } else {
                $fields = array(
                    '`active`' => $_POST['active'],
                    '`discount`' => $_POST['discount'],
                    '`approved`' => $_POST['approved'],
                    '`ifsc_code`' => $_POST['ifsc_code'],
                    '`bank_branch`' => $_POST['bank_branch'],
                    '`bank_account_no`' => $_POST['bank_account_no'],
                    '`bank_name`' => $_POST['bank_name'],
                    '`bank_account_no`' => $_POST['bank_account_no'],
                    '`vat`' => $_POST['vat'],
                    '`tin`' => $_POST['tin'],
                    '`pan`' => $_POST['pan'],
                    '`billing_gst`' => $_POST['billing_gst'],
                    '`gst`' => $_POST['gst'],
                    '`billing_postal_code`' => $_POST['billing_postal_code'],
                    '`postal_code`' => $_POST['postal_code'],
                    '`billing_mobile`' => $_POST['billing_mobile'],
                    '`mobile`' => $_POST['mobile'],
                    '`billing_phone`' => $_POST['billing_phone'],
                    '`phone`' => $_POST['phone'],
                    '`billing_country`' => $_POST['billing_country'],
                    '`country`' => $_POST['country'],
                    '`billing_state`' => $_POST['billing_state'],
                    '`state`' => $_POST['state'],
                    '`billing_city`' => $_POST['billing_city'],
                    '`city`' => $_POST['city'],
                    '`billing_landmark`' => $_POST['billing_landmark'],
                    '`firstname`' => $_POST['firstname'],
                    '`landmark`' => $_POST['landmark'],
                    '`billing_address`' => $_POST['billing_address'],
                    '`address`' => $_POST['address'],
                    '`password`' => $_POST['password'],
                    '`username`' => $_POST['username'],
                    '`email`' => $_POST['email'],
                    '`contact_person`' => $_POST['contact_person'],
                    '`lastname`' => $_POST['lastname'],
                );
                $where_query = array(
                    ["", "id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_DEALER, $fields, $where_query);
                $comeback = array("data" => true, "for_data" => 'dealer');
            }
        }else if (isset($_POST['for_data']) && $_POST["for_data"] == 'sales') {
            $shopinfo = $this->current_store_obj;
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            $where_query = array(["", "id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_SALES_ORDER, '*', $where_query);
            if (empty($comeback["data"])) {
                $fields_arr = array(
                    '`id`' => $_POST['id'],
                    '`sales_type`' => $_POST['sales_type'],
//                    '`invoice_type`' => $_POST['invoice_type'],
                    '`sales_date`' => $_POST['sales_date'],
                    '`dos`' => $_POST['dos'],
                    '`packing`' => $_POST['packing'],
                    '`insurance`' => $_POST['insurance'],
                    '`freight`' => $_POST['freight'],
                    '`reverse_charge`' => $_POST['reverse_charge'],
                    '`dealer_id`' => $_POST['dealer_id'],
                    '`ref_no`' => $_POST['ref_no'],
                    '`transport_mode`' => $_POST['transport_mode'],
                    '`vehicle_no`' => $_POST['vehicle_no'],
                    '`billing_address`' => $_POST['billing_address'],
                    '`shipping_address`' => $_POST['shipping_address'],
                    '`remark`' => $_POST['remark'],
                );
                $comeback = $this->post_data(TABLE_SALES_ORDER, array($fields_arr));
                $comeback = array("data" => true);
            } else {
                $fields = array(
                    '`sales_type`' => $_POST['sales_type'],
//                    '`invoice_type`' => $_POST['invoice_type'],
                    '`sales_date`' => $_POST['sales_date'],
                    '`dos`' => $_POST['dos'],
                    '`packing`' => $_POST['packing'],
                    '`insurance`' => $_POST['insurance'],
                    '`freight`' => $_POST['freight'],
                    '`reverse_charge`' => $_POST['reverse_charge'],
                    '`dealer_id`' => $_POST['dealer_id'],
                    '`ref_no`' => $_POST['ref_no'],
                    '`transport_mode`' => $_POST['transport_mode'],
                    '`vehicle_no`' => $_POST['vehicle_no'],
                    '`billing_address`' => $_POST['billing_address'],
                    '`shipping_address`' => $_POST['shipping_address'],
                    '`remark`' => $_POST['remark'],
                );
                $where_query = array(
                    ["", "id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_SALES_ORDER, $fields, $where_query);
                  if(!empty($_POST['sku'])){
                foreach ($_POST['sku'] as $key => $skuvalue) {
                            $store = $shopinfo[0]['shop_name'];
                            $password = $shopinfo[0]['password'];
                            $category_name = $_POST['name'][$key];
                            $sku_id = $_POST['skuid'][$key];
//                            $v_sku = $_POST['v_sku'][$key];
                            $qty = $_POST['qty'][$key];
                            $price = $_POST['mrp'][$key];
                            $taxable = $_POST['taxation'][$key];
                            $cgst = $_POST['cgst'][$key];
                            $sgst = $_POST['sgst'][$key];
                            $igst = $_POST['igst'][$key];
                            $grand_total = $_POST['grandtotal'][$key];
                             if ($skuvalue != ''){
                                    $sku = $skuvalue;
                                    $end_point = "https://" . $store . "/admin/api/graphql.json";
                                    $shopify_data_list = cls_api_graphql_call(CLS_API_KEY, $password, $store, $end_point, $sku, 'POST');
//                                    $shopify_data_list = cls_api_graphql_call(CLS_API_KEY, $password, $store, $end_point, $sku, 'POST');
                                    $decode_data = json_decode($shopify_data_list["response"]);
                                    $checksku = $decode_data->data->productVariants->edges;
                                    if (!empty($checksku)) {
                                         $barcode = isset($decode_data->data->productVariants->edges[0]->node->barcode) ? $decode_data->data->productVariants->edges[0]->node->barcode : '';
                                        $inventoryItem_id = isset($decode_data->data->productVariants->edges[0]->node->inventoryItem->id) ? $decode_data->data->productVariants->edges[0]->node->inventoryItem->id : '';
                                        $inventoryItem_id = str_replace("gid://shopify/InventoryItem/", "", $inventoryItem_id);

                                        $inventory_level_api = array('api_name' => 'inventory_levels');
                                        $fields = array('inventory_item_ids' => $inventoryItem_id);
                                        $get_level = $this->cls_get_shopify_list($inventory_level_api, $fields, 'GET');

                                        $inventory_levels = $get_level->inventory_levels[0];
                                        $get_location_id = (isset($inventory_levels->location_id) && $inventory_levels->location_id !== '' ) ? $inventory_levels->location_id : '';
                                        $get_available = (isset($inventory_levels->available) && $inventory_levels->available !== '' ) ? $inventory_levels->available : '';
                                        
                                        $id = isset($_POST['id']) ? $_POST['id'] : '';
                                        $where_query = array(["", "id", "=", $sku_id]);
                                        $comeback = $this->select_result(TABLE_SALES_SKU_MASTER, '*', $where_query);
                                        $select_qty = (isset($comeback['data'][0]['qty']) && $comeback['data'][0]['qty'] !== '' ) ? $comeback['data'][0]['qty'] : 0 ;
                                        if($select_qty >= $qty){
                                            $qty = (int)$select_qty - (int)$qty;
                                            $get_available = (int)$get_available + (int)$qty ;
                                        }elseif($select_qty <= $qty){
                                            $qty = (int)$qty - (int)$select_qty;
                                            $get_available = (int)$get_available - (int)$qty ;
                                        }
                                        if ($get_location_id != '') {
                                            $set_api = array('api_name' => 'inventory_levels', 'set' => 'set');
                                            $filed_arr = array('location_id' => $get_location_id, 'inventory_item_id' => $inventoryItem_id, 'available' => (int) $get_available);
                                            $update_inve = $this->cls_get_shopify_list($set_api, $filed_arr, 'POST');
                                            $sku_id = $_POST['skuid'][$key];
                                            $barcode = $_POST['barcode'][$key];
                                            $sku = $_POST['sku'][$key];
                                            $qty = $_POST['qty'][$key];
                                            $mrp = $_POST['mrp'][$key];
                                            $discount = $_POST['discount'][$key];
                                            $discount_rate = $_POST['discount_rate'][$key];
                                            $taxation = $_POST['taxation'][$key];
                                            $cgst = $_POST['cgst'][$key];
                                            $sgst = $_POST['sgst'][$key];
                                            $igst = $_POST['igst'][$key];
                                            $grandtotal = $_POST['grandtotal'][$key];
                                            $fields_arr = array(
                                                '`dealer_id`' => $_POST['dealer_id'],
                                                '`qty`' => $qty,
                                                '`mrp`' => $mrp,
                                                '`taxation`' => $taxation,
                                                '`cgst`' => $cgst,
                                                '`sgst`' => $sgst,
                                                '`igst`' => $igst,
                                                '`grandtotal`' => $grandtotal,
                                                '`created_at`' => date('Y-m-d H:i:s'),
                                                '`updated_at`' => date('Y-m-d H:i:s')
                                            );
                                            $fields_arrwhere_query = array(
                                                ["", "sales_id", "=", $id], ["AND", "id", "=", $sku_id]
                                            );
                                            $comeback = $this->put_data(TABLE_SALES_SKU_MASTER, $fields_arr, $fields_arrwhere_query);
                                        }
                                    }
                                }
                }
                  }

                  $fields_purchase = array(
                    '`qty`' => $_POST['total_qty'],
                      '`all_total`' => $_POST['all_total'],
                    '`discount`' => $_POST['total_discount'],
                    '`taxation`' => $_POST['total_taxable'],
                    '`cgst`' => $_POST['total_cgst'],
                    '`sgst`' => $_POST['total_sgst'],
                    '`igst`' => $_POST['total_igst'],
                    '`grandtotal`' => $_POST['grand_amount'],
                );
                $where_query_purchase = array(
                    ["", "id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_SALES_ORDER, $fields_purchase, $where_query_purchase); 
                $comeback = array("data" => true, "for_data" => 'sales');
            }
        }else if (isset($_POST['for_data']) && $_POST["for_data"] == 'sales_return') {
            $shopinfo = $this->current_store_obj;
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            $where_query = array(["", "id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_SALES_ORDER_RETURN, '*', $where_query);
            if (empty($comeback["data"])) {
                $fields_arr = array(
                    '`id`' => $_POST['id'],
                    '`so_type`' => $_POST['po_type'],
                    '`return_date`' => $_POST['return_date'],
                    '`ref_num`' => $_POST['ref_num'],
                    '`qty_ret`' => $_POST['qty_ret'],
                    '`price_ret`' => $_POST['price_ret'],
                    '`reason`' => $_POST['reason'],
                    '`dealer_id`' => $_POST['dealer_id'],
                );
                $comeback = $this->post_data(TABLE_SALES_ORDER_RETURN, array($fields_arr));
                $comeback = array("data" => true);
            } else {
                $fields = array(
                    '`id`' => $_POST['id'],
                    '`so_type`' => $_POST['po_type'],
                    '`return_date`' => $_POST['return_date'],
                    '`ref_num`' => $_POST['ref_num'],
//                    '`qty_ret`' => $_POST['qty_ret'],
//                    '`price_ret`' => $_POST['price_ret'],
                    '`reason`' => $_POST['reason'],
                    '`dealer_id`' => $_POST['dealer_id'],
                );
                $where_query = array(
                    ["", "id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_SALES_ORDER_RETURN, $fields, $where_query);  
                if(!empty($_POST['barcode'])){
                
                foreach ($_POST['barcode'] as $key => $barcodevalue) {
                $sku_id = $_POST['skuid'][$key];
                $category_name = $_POST['category_name'][$key];
                $post_qty = $_POST['qty'][$key];
                $grand_total = $_POST['grandtotal'][$key];
                $sku = isset($_POST['sku']) ? $_POST['sku'] : '';
                $dealer_id = isset($_POST['dealer_id']) ? $_POST['dealer_id'] : '';
                $where_query = array(["", "barcode", "=",$barcodevalue], ["AND", "id", "=",$sku_id],["AND", "status", "=",1]);
                $comeback = $this->select_result(TABLE_SALES_SKU_MASTER, '*', $where_query);
             
                $getbarcode = (isset($comeback["data"][0]["barcode"]) && !empty($comeback["data"][0]["barcode"])) ?$comeback["data"][0]["barcode"] : '';
                $getsku = (isset($comeback["data"][0]["sku"]) && !empty($comeback["data"][0]["sku"])) ?$comeback["data"][0]["sku"] : '';
                $total = (isset($comeback["data"][0]["total"]) && !empty($comeback["data"][0]["total"])) ?$comeback["data"][0]["total"] : '';
                $cgst = (isset($comeback["data"][0]["cgst"]) && !empty($comeback["data"][0]["cgst"])) ?$comeback["data"][0]["cgst"] : '';
                $sgst = (isset($comeback["data"][0]["sgst"]) && !empty($comeback["data"][0]["sgst"])) ?$comeback["data"][0]["sgst"] : '';
                $igst = (isset($comeback["data"][0]["igst"]) && !empty($comeback["data"][0]["igst"])) ?$comeback["data"][0]["igst"] : '';
                $qty = (isset($comeback["data"][0]["qty"]) && !empty($comeback["data"][0]["qty"])) ?$comeback["data"][0]["qty"] : '';
                $return_qty = (isset($comeback["data"][0]["return_qty"]) && !empty($comeback["data"][0]["return_qty"])) ?$comeback["data"][0]["return_qty"] : 0;
                $grand_total = (isset($comeback["data"][0]["grand_total"]) && !empty($comeback["data"][0]["grand_total"])) ?$comeback["data"][0]["grand_total"] : '';
                $sku_id = (isset($comeback["data"][0]["id"]) && !empty($comeback["data"][0]["id"])) ?$comeback["data"][0]["id"] : '';
                $purchase_id = (isset($comeback["data"][0]["purchase_id"]) && !empty($comeback["data"][0]["purchase_id"]) ?$comeback["data"][0]["purchase_id"] : '');
                if ($barcodevalue == $getbarcode) {
                     $result['dataResult'] = "Success";
                     if ($barcodevalue != ''){
                        $store = $shopinfo[0]['shop_name'];
                        $password = $shopinfo[0]['password']; 
                        $end_point = "https://".$store."/admin/api/graphql.json";
                        $shopify_data_list = cls_api_graphql_barcode_call(CLS_API_KEY, $password, $store, $end_point, $barcodevalue, 'POST');
               
                        $decode_data = json_decode($shopify_data_list["response"]);
                        $checkbarcode = $decode_data->data->productVariants->edges[0]->node->barcode;
                        if (!empty($checkbarcode)) {
                            $inventoryItem_id = isset($decode_data->data->productVariants->edges[0]->node->inventoryItem->id) ? $decode_data->data->productVariants->edges[0]->node->inventoryItem->id : '';
                            $inventoryItem_id = str_replace("gid://shopify/InventoryItem/", "", $inventoryItem_id);

                            $inventory_level_api = array('api_name' => 'inventory_levels');
                            $fields = array('inventory_item_ids' => $inventoryItem_id);
                            $get_level = $this->cls_get_shopify_list($inventory_level_api, $fields, 'GET');
                            $inventory_levels = $get_level->inventory_levels[0];
                            $get_location_id = (isset($inventory_levels->location_id) && $inventory_levels->location_id !== '' ) ? $inventory_levels->location_id : '';
                            $get_available = (isset($inventory_levels->available) && $inventory_levels->available !== '' ) ? $inventory_levels->available : '';
                           
                            if ($return_qty >= $post_qty) {
                                    $available_qty = (int)$return_qty - (int)$post_qty;
                                    $get_available = (int)$get_available - (int)$available_qty;
                                } elseif ($return_qty <= $post_qty) {
                                    $available_qty = (int)$post_qty - (int)$return_qty;
                                    $get_available = (int)$get_available + (int)$available_qty;
                                }
                            if ($get_location_id != '') {
                                $set_api = array('api_name' => 'inventory_levels', 'set' => 'set');
                                $filed_arr = array('location_id' => $get_location_id, 'inventory_item_id' => $inventoryItem_id, 'available' => (int) $get_available);
                                $update_inve = $this->cls_get_shopify_list($set_api, $filed_arr, 'POST');
                                 $fields = array(
                                            'return_qty' => $post_qty,
                                        );
                                          $where_query = array(
                                        ["", "id", "=", $sku_id],
                                    );
                                          $comeback = $this->put_data(TABLE_SALES_SKU_MASTER, $fields, $where_query);
                                }
                            } else {
                            $result['dataResult'] = "fail";
                            $result['errors']['sku'] = "Invalid Barcode uhu";
                        }
                    }
                }else{
                          $result['dataResult'] = "fail";
                          $result['errors']['sku'] = "Invalid Barcode";
                    }
            } 
                }
                $fields_purchase = array(
                    '`qty`' => $_POST['total_qty'],
                    '`taxable`' => $_POST['total_taxable'],
                    '`discount`' => $_POST['total_discount'],
                    '`cgst`' => $_POST['total_cgst'],
                    '`sgst`' => $_POST['total_sgst'],
                    '`igst`' => $_POST['total_igst'],
                    '`grand_total`' => $_POST['grand_amount'],
                );
                $where_query_purchase = array(
                    ["", "id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_SALES_ORDER_RETURN, $fields_purchase, $where_query_purchase); 
                $comeback = array("data" => true, "for_data" => 'sales_retrun');
            }
        }  else if (isset($_POST['for_data']) && $_POST["for_data"] == 'dealers_orders_update') {
            $id = isset($_POST['id']) ? $_POST['id'] : '';      

            $where_query = array(["", "id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_DEALER, '*', $where_query);
            if (empty($comeback["data"])) {
                $fields_arr = array(
                    '`id`' => $_POST['id'],
                    '`dealer_status`' => $_POST['dealer_id'],
                );
                $comeback = $this->post_data(TABLE_DEALER, array($fields_arr));
                $comeback = array("data" => true);
            } else {
                $fields = array(
                    '`dealer_status`' => $_POST['dealer_id'],
                );
                $where_query = array(
                    ["", "id", "=", $id],
                );
                $comeback = $this->put_data(TABLE_DEALER, $fields, $where_query);
                $comeback = array("data" => true, "for_data" => 'dealers_orders_update');
            }
        } 
        else {
            echo "error";
        }
        
        return $comeback;
    }

    function blogpost_select() {
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "blogpost_id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_BLOGPOST_MASTER, '*', $where_query_arr);
        if (!empty($comeback)) {
            $description = isset($comeback['data']->description) ? $comeback['data']->description : '';
            $title = isset($comeback['data']->title) ? $comeback['data']->title : '';
            $return_arary["description"] = $description;
            $return_arary["title"] = $title;
            $comeback = array("outcome" => "true", "data" => $return_arary);
        }
        return $comeback;
    }

    function supplier_select() {
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_SUPPLIERS, '*', $where_query_arr);
        if (!empty($comeback)) {
            $supplier_type = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_type"] : '';
            $supplier_name = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_name"] : '';
            $supplier_contact_person = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_contact_person"] : '';
            $supplier_email = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_email"] : '';
            $supplier_phone = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_phone"] : '';
            $supplier_mobile = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_mobile"] : '';
            $supplier_address = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_address"] : '';
            $supplier_landmark = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_landmark"] : '';
            $supplier_city = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_city"] : '';
            $supplier_state = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_state"] : '';
            $supplier_country = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_country"] : '';
            $supplier_pincode = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_pincode"] : '';
            $supplier_pan = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_pan"] : '';
            $supplier_tin = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_tin"] : '';
            $supplier_gst = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_gst"] : '';
            $supplier_vat = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_vat"] : '';
            $supplier_bank_name = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_bank_name"] : '';
            $supplier_bank_account_no = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_bank_account_no"] : '';
            $supplier_bank_branch = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_bank_branch"] : '';
            $supplier_ifsc_code = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_ifsc_code"] : '';
            $supplier_remarks = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_remarks"] : '';
            $supplier_status = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_status"] : '';

            $return_arary["supplier_type"] = $supplier_type;
            $return_arary["supplier_name"] = $supplier_name;
            $return_arary["supplier_contact_person"] = $supplier_contact_person;
            $return_arary["supplier_email"] = $supplier_email;
            $return_arary["supplier_phone"] = $supplier_phone;
            $return_arary["supplier_mobile"] = $supplier_mobile;
            $return_arary["supplier_address"] = $supplier_address;
            $return_arary["supplier_landmark"] = $supplier_landmark;
            $return_arary["supplier_city"] = $supplier_city;
            $return_arary["supplier_state"] = $supplier_state;
            $return_arary["supplier_country"] = $supplier_country;
            $return_arary["supplier_pincode"] = $supplier_pincode;
            $return_arary["supplier_pan"] = $supplier_pan;
            $return_arary["supplier_tin"] = $supplier_tin;
            $return_arary["supplier_gst"] = $supplier_gst;
            $return_arary["supplier_vat"] = $supplier_vat;
            $return_arary["supplier_bank_name"] = $supplier_bank_name;
            $return_arary["supplier_bank_account_no"] = $supplier_bank_account_no;
            $return_arary["supplier_bank_branch"] = $supplier_bank_branch;
            $return_arary["supplier_ifsc_code"] = $supplier_ifsc_code;
            $return_arary["supplier_remarks"] = $supplier_remarks;
            $return_arary["supplier_status"] = $supplier_status;
            $comeback = array("outcome" => "true", "data" => $return_arary);
        }
        return $comeback;
    }

     function purhcase_select() {
        $shopinfo = $this->current_store_obj; 
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_PURCHASE, '*', $where_query_arr);
        if (!empty($comeback["data"])) {
            $freight = !empty($comeback["data"]) ? $comeback["data"]["0"]["freight"] : '';
            $packing = !empty($comeback["data"]) ? $comeback["data"]["0"]["packing"] : '';
            $insurance = !empty($comeback["data"]) ? $comeback["data"]["0"]["insurance"] : '';
            $po_date = !empty($comeback["data"]) ? $comeback["data"]["0"]["po_date"] : '';
            $supplier_id = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_id"] : '';
            $rec_ref_code = !empty($comeback["data"]) ? $comeback["data"]["0"]["rec_ref_code"] : '';
            $supplier_address = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_address"] : '';
            $remarks = !empty($comeback["data"]) ? $comeback["data"]["0"]["remarks"] : '';
            $reverse_charge = !empty($comeback["data"]) ? $comeback["data"]["0"]["reverse_charge"] : '';
            $supplier_invoice_no = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_invoice_no"] : '';
            $supplier_invoice_date = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_invoice_date"] : '';


            $return_arary["freight"] = $freight;
            $return_arary["packing"] = $packing;
            $return_arary["insurance"] = $insurance;
            $return_arary["po_date"] = $po_date;
            $return_arary["supplier_id"] = $supplier_id;
            $return_arary["rec_ref_code"] = $rec_ref_code;
            $return_arary["supplier_address"] = $supplier_address;
            $return_arary["remarks"] = $remarks;
            $return_arary["reverse_charge"] = $reverse_charge;
            $return_arary["supplier_invoice_no"] = $supplier_invoice_no;
            $return_arary["supplier_invoice_date"] = $supplier_invoice_date;
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            $where_query_arr = array(["", "purchase_id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_SKU_MASTER, '*', $where_query_arr);

            if(!empty($comeback["data"])){
            $data = !empty($comeback["data"]) ? $comeback["data"] : '';

            $str = '';
            foreach ($data as $blogpost_obj) {
                
                $str .= '<tr>';
                $str .= '<td><input type="hidden" name="skuid[]" value="' . $blogpost_obj['id'] . '">' . $blogpost_obj['id'] . '</td>';
                $str .= '<td><input type="text" readonly class="form-control skuInput set_sku"  name="sku[]" value= "' . $blogpost_obj['sku'] . '"/></td>';
                $str .= '<td><input type="text"  class="form-control category_name" readonly name="category_name[]" value="' . $blogpost_obj['category_name'] . '"> </td>';
                $str .= '<td><input type="text" readonly class="form-control vendor" name="v_sku[]" value="' . $blogpost_obj['v_sku'] . '"> </td>';
                $str .= '<td class="cls-text-section "><input type="number" class="form-control qty " name="qty[]" value="' . $blogpost_obj['qty'] . '"><input type="number" class="form-control price" name="price[]"  step=0.01 value="' . $blogpost_obj['price'] . '"> </td>';
                $str .= '<td><input type="number" readonly class="form-control total"  name="taxable[]" step=0.01 value="' . $blogpost_obj['taxable'] . '"> </td>';
//                $str .= '<td> <input type="number"  class="form-control taxable_rate"  name="taxable_rate[]" ></td>';
                $str .= '<td><input type="number" readonly class="form-control cgst"  name="cgst[]"  step=0.01 value="' . $blogpost_obj['cgst'] . '"> </td>';
                $str .= '<td><input type="number" readonly class="form-control sgst" name="sgst[]"  step=0.01 value="' . $blogpost_obj['sgst'] . ' "> </td>';
                $str .= '<td><input type="number" readonly class="form-control igst"  name="igst[]"  step=0.01 value="' . $blogpost_obj['igst'] . '">  </td>';
                $str .= '<td><input type="number" readonly  class="form-control total_amount"  name="grand_total[]"  step=0.01 value="' . $blogpost_obj['grand_total'] . '" />  </td>';
//                $str .= '<td> <button class="btnDeleteRow btn-zap" type="button" >× </button></td>';
                $str .= '</tr>';
            }
            $return_arary['str'] = $str;
            }
           
            $comeback = array("outcome" => "true", "data" => $return_arary);
                }
       
        return $comeback;
    }

    function dealer_select() {
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_DEALER, '*', $where_query_arr);

        if (!empty($comeback["data"])) {
            $firstname = !empty($comeback["data"]) ? $comeback["data"]["0"]["firstname"] : '';
            $lastname = !empty($comeback["data"]) ? $comeback["data"]["0"]["lastname"] : '';
            $contact_person = !empty($comeback["data"]) ? $comeback["data"]["0"]["contact_person"] : '';
            $email = !empty($comeback["data"]) ? $comeback["data"]["0"]["email"] : '';
            $username = !empty($comeback["data"]) ? $comeback["data"]["0"]["username"] : '';
            $password = !empty($comeback["data"]) ? $comeback["data"]["0"]["password"] : '';
            $same_address = !empty($comeback["data"]) ? $comeback["data"]["0"]["same_address"] : '';
            $address = !empty($comeback["data"]) ? $comeback["data"]["0"]["address"] : '';
            $landmark = !empty($comeback["data"]) ? $comeback["data"]["0"]["landmark"] : '';
            $city = !empty($comeback["data"]) ? $comeback["data"]["0"]["city"] : '';
            $state = !empty($comeback["data"]) ? $comeback["data"]["0"]["state"] : '';
            $country = !empty($comeback["data"]) ? $comeback["data"]["0"]["country"] : '';
            $phone = !empty($comeback["data"]) ? $comeback["data"]["0"]["phone"] : '';
            $mobile = !empty($comeback["data"]) ? $comeback["data"]["0"]["mobile"] : '';
            $postal_code = !empty($comeback["data"]) ? $comeback["data"]["0"]["postal_code"] : '';
            $gst = !empty($comeback["data"]) ? $comeback["data"]["0"]["gst"] : '';
            $pan = !empty($comeback["data"]) ? $comeback["data"]["0"]["pan"] : '';
            $tin = !empty($comeback["data"]) ? $comeback["data"]["0"]["tin"] : '';
            $vat = !empty($comeback["data"]) ? $comeback["data"]["0"]["vat"] : '';
            $bank_name = !empty($comeback["data"]) ? $comeback["data"]["0"]["bank_name"] : '';
            $bank_account_no = !empty($comeback["data"]) ? $comeback["data"]["0"]["bank_account_no"] : '';
            $bank_branch = !empty($comeback["data"]) ? $comeback["data"]["0"]["bank_branch"] : '';
            $ifsc_code = !empty($comeback["data"]) ? $comeback["data"]["0"]["ifsc_code"] : '';
            $approved = !empty($comeback["data"]) ? $comeback["data"]["0"]["approved"] : '';
            $discount = !empty($comeback["data"]) ? $comeback["data"]["0"]["discount"] : '';
            $active = !empty($comeback["data"]) ? $comeback["data"]["0"]["active"] : '';
            $billing_address = !empty($comeback["data"]) ? $comeback["data"]["0"]["billing_address"] : '';
            $billing_landmark = !empty($comeback["data"]) ? $comeback["data"]["0"]["billing_landmark"] : '';
            $billing_city = !empty($comeback["data"]) ? $comeback["data"]["0"]["billing_city"] : '';
            $billing_state = !empty($comeback["data"]) ? $comeback["data"]["0"]["billing_state"] : '';
            $billing_country = !empty($comeback["data"]) ? $comeback["data"]["0"]["billing_country"] : '';
            $billing_phone = !empty($comeback["data"]) ? $comeback["data"]["0"]["billing_phone"] : '';
            $billing_mobile = !empty($comeback["data"]) ? $comeback["data"]["0"]["billing_mobile"] : '';
            $billing_postal_code = !empty($comeback["data"]) ? $comeback["data"]["0"]["billing_postal_code"] : '';
            $billing_gst = !empty($comeback["data"]) ? $comeback["data"]["0"]["billing_gst"] : '';
            $status = !empty($comeback["data"]) ? $comeback["data"]["0"]["status"] : '';

            $return_arary["firstname"] = $firstname;
            $return_arary["lastname"] = $lastname;
            $return_arary["contact_person"] = $contact_person;
            $return_arary["email"] = $email;
            $return_arary["username"] = $username;
            $return_arary["password"] = $password;
            $return_arary["same_address"] = $same_address;
            $return_arary["address"] = $address;
            $return_arary["landmark"] = $landmark;
            $return_arary["city"] = $city;
            $return_arary["state"] = $state;
            $return_arary["country"] = $country;
            $return_arary["phone"] = $phone;
            $return_arary["mobile"] = $mobile;
            $return_arary["postal_code"] = $postal_code;
            $return_arary["gst"] = $gst;
            $return_arary["pan"] = $pan;
            $return_arary["tin"] = $tin;
            $return_arary["vat"] = $vat;
            $return_arary["bank_name"] = $bank_name;
            $return_arary["bank_account_no"] = $bank_account_no;
            $return_arary["bank_branch"] = $bank_branch;
            $return_arary["ifsc_code"] = $ifsc_code;
            $return_arary["approved"] = $approved;
            $return_arary["discount"] = $discount;
            $return_arary["active"] = $active;
            $return_arary["billing_address"] = $billing_address;
            $return_arary["billing_landmark"] = $billing_landmark;
            $return_arary["billing_city"] = $billing_city;
            $return_arary["billing_state"] = $billing_state;
            $return_arary["billing_country"] = $billing_country;
            $return_arary["billing_phone"] = $billing_phone;
            $return_arary["billing_mobile"] = $billing_mobile;
            $return_arary["billing_postal_code"] = $billing_postal_code;
            $return_arary["billing_gst"] = $billing_gst;
            $return_arary["status"] = $status;

            $comeback = array("outcome" => "true", "data" => $return_arary);
        }
        return $comeback;
    }

    function sales_select() {
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_SALES_ORDER, '*', $where_query_arr);
        if (!empty($comeback["data"])) {
            $sales_type = !empty($comeback["data"]) ? $comeback["data"]["0"]["sales_type"] : '';
            $invoice_type = !empty($comeback["data"]) ? $comeback["data"]["0"]["invoice_type"] : '';
            $sales_date = !empty($comeback["data"]) ? $comeback["data"]["0"]["sales_date"] : '';
            $dos = !empty($comeback["data"]) ? $comeback["data"]["0"]["dos"] : '';
            $packing = !empty($comeback["data"]) ? $comeback["data"]["0"]["packing"] : '';
            $insurance = !empty($comeback["data"]) ? $comeback["data"]["0"]["insurance"] : '';
            $freight = !empty($comeback["data"]) ? $comeback["data"]["0"]["freight"] : '';
            $reverse_charge = !empty($comeback["data"]) ? $comeback["data"]["0"]["reverse_charge"] : '';
            $dealer_id = !empty($comeback["data"]) ? $comeback["data"]["0"]["dealer_id"] : '';
            $ref_no = !empty($comeback["data"]) ? $comeback["data"]["0"]["ref_no"] : '';
            $transport_mode = !empty($comeback["data"]) ? $comeback["data"]["0"]["transport_mode"] : '';
            $vehicle_no = !empty($comeback["data"]) ? $comeback["data"]["0"]["vehicle_no"] : '';
            $billing_address = !empty($comeback["data"]) ? $comeback["data"]["0"]["billing_address"] : '';
            $shipping_address = !empty($comeback["data"]) ? $comeback["data"]["0"]["shipping_address"] : '';
            $remark = !empty($comeback["data"]) ? $comeback["data"]["0"]["remark"] : '';

            $return_arary["sales_type"] = $sales_type;
            $return_arary["invoice_type"] = $invoice_type;
            $return_arary["sales_date"] = $sales_date;
            $return_arary["dos"] = $dos;
            $return_arary["packing"] = $packing;
            $return_arary["insurance"] = $insurance;
            $return_arary["freight"] = $freight;
            $return_arary["reverse_charge"] = $reverse_charge;
            $return_arary["dealer_id"] = $dealer_id;
            $return_arary["ref_no"] = $ref_no;
            $return_arary["transport_mode"] = $transport_mode;
            $return_arary["vehicle_no"] = $vehicle_no;
            $return_arary["billing_address"] = $billing_address;
            $return_arary["shipping_address"] = $shipping_address;
            $return_arary["remark"] = $remark;
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            $where_query_arr = array(["", "sales_id", "=", "$id"]);
            $comeback = $this->select_result(TABLE_SALES_SKU_MASTER, '*', $where_query_arr);
              if(!empty($comeback["data"])){
                              $data = !empty($comeback["data"]) ? $comeback["data"] : '';

            $str = '';
            $startnum = 1;
            foreach ($data as $blogpost_obj) {
                $str .= '<tr>';
                $str .= '<td><input type="hidden" readonly name="skuid[]" value="' . $blogpost_obj['id'] . '">'.$startnum .'</td>';
                $startnum++;
                $str .= '<td><input type="text" readonly class="form-control set_barcode"  name="barcode[]" value="' . $blogpost_obj['barcode'] . '"/></td>';
                $str .= '<td><input type="text" readonly class="form-control skuInput sku set_sku"  name="sku[]" value="' . $blogpost_obj['sku'] . '"/></td>';
                $str .= '<td><input type="text" readonly class="form-control category_name" name="name[]" value="' . $blogpost_obj['name'] . '"> </td>';
//              $str += '<td><input type="text"  class="form-control vendor" name="v.sku[]" >  </td>';
                $str .= '<td><input type="text" readonly class="form-control"  name="metal[]" value="' . $blogpost_obj['metal'] . '">  </td>';
                $str .= '<td><input type="text" readonly class="form-control"  name="weight[]" value="' . $blogpost_obj['weight'] . '" >  </td>';
                $str .= '<td class="cls-text-section"><input type="number" class="form-control qtySale "  name="qty[]" value="' . $blogpost_obj['qty'] . '"><input type="number"  class="form-control mrp"  name="mrp[]"  step=0.01 value="' . $blogpost_obj['mrp'] . '"> </td>';
                $str .= '<td> <input type="number"  class="form-control discount_rate"  name="discount_rate[]" step=0.1 value="' . $blogpost_obj['discount_rate'] . '"> </td>';
                $str .= '<td><input type="number" readonly class="form-control discount" name="discount[]"  step=0.01 value="' . $blogpost_obj['discount'] . '">  </td>';
                $str .= '<td><input type="number"  class="form-control taxation" readonly name="taxation[]"  step=0.01 value="' . $blogpost_obj['taxation'] . '">  </td>';
                $str .= '<td><input type="number"  class="form-control cgst" readonly name="cgst[]"  step=0.01 value="' . $blogpost_obj['cgst'] . '">   </td>';
                $str .= '<td><input type="number"  class="form-control sgst" readonly name="sgst[]"  step=0.01 value="' . $blogpost_obj['sgst'] . '">  </td>';
                $str .= '<td><input type="number"  class="form-control igst" readonly name="igst[]"  step=0.01 value="' . $blogpost_obj['igst'] . '">   </td>';
                $str .= '<td><input type="number"  class="form-control grandtotal" readonly name="grandtotal[]" step=0.01 value="' . $blogpost_obj['grandtotal'] . '">   </td>';
                $str .= '</tr>'; 
            }
            $return_arary['str'] = $str;
              }
            $comeback = array("outcome" => "true", "data" => $return_arary);
        }
        return $comeback;
    }

    function sales_return_select() {
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_SALES_ORDER_RETURN, '*', $where_query_arr);
        if (!empty($comeback["data"])) {

            $so_type = !empty($comeback["data"]) ? $comeback["data"]["0"]["so_type"] : '';
            $dealer_id = !empty($comeback["data"]) ? $comeback["data"]["0"]["dealer_id"] : '';
            $return_date = !empty($comeback["data"]) ? $comeback["data"]["0"]["return_date"] : '';
            $ref_num = !empty($comeback["data"]) ? $comeback["data"]["0"]["ref_num"] : '';
            $qty_ret = !empty($comeback["data"]) ? $comeback["data"]["0"]["qty_ret"] : '';
            $price_ret = !empty($comeback["data"]) ? $comeback["data"]["0"]["price_ret"] : '';
            $grand_total = !empty($comeback["data"]) ? $comeback["data"]["0"]["grand_total"] : '';
            $reason = !empty($comeback["data"]) ? $comeback["data"]["0"]["reason"] : '';
            $return_arary["so_type"] = $so_type;
            $return_arary["dealer_id"] = $dealer_id;
            $return_arary["return_date"] = $return_date;
            $return_arary["ref_num"] = $ref_num;
            $return_arary["qty_ret"] = $qty_ret;
            $return_arary["price_ret"] = $price_ret;
            $return_arary["grand_total"] = $grand_total;
            $return_arary["reason"] = $reason;
            
            $sales_id = isset($_POST['sales_id']) ? $_POST['sales_id'] : '';
            $where_query_arr = array(["", "sales_id", "=", "$sales_id"]);
            $comeback = $this->select_result(TABLE_SALES_SKU_MASTER, '*', $where_query_arr);
            if(!empty($comeback["data"])){
            $str = '';
            $startnum = 1;
            foreach ($comeback["data"] as $blogpost_obj) {            
                $return_qty = (isset($blogpost_obj['return_qty']) && $blogpost_obj['return_qty'] !== 0) ? $blogpost_obj['return_qty'] : 0;
                if($return_qty > 0){
                $where_query = array(["", "id", "=", "$sales_id"]);
                $comeback = $this->select_result(TABLE_SALES_ORDER, '*', $where_query);
                $return_array['sales_date'] = (isset($comeback["data"][0]["sales_date"]) && !empty($comeback["data"][0]["sales_date"])) ? $comeback["data"][0]["sales_date"] : '';
                $return_array['voucher_no'] = (isset($comeback["data"][0]["voucher_no"]) && !empty($comeback["data"][0]["voucher_no"])) ? $comeback["data"][0]["voucher_no"] : '';
                $str .= '<tr>';
                $str .= '<td><input type="hidden" readonly name="skuid[]" value="' . $blogpost_obj['id'] . '">' . $startnum . '</td>';
                $startnum++;
                $str .= '<td><input type="text" readonly class="form-control set_barcode"  name="barcode[]" value="' . $blogpost_obj['barcode'] . '"/></td>';
                $str .= '<td><input type="text" readonly class="form-control skuInput sku set_sku"  name="sku[]" value="' . $blogpost_obj['sku'] . '"/></td>';
                $str .= '<td><input type="text" readonly class="form-control category_name" name="category_name[]" value="' . $blogpost_obj['name'] . '"> </td>';
                $str .= '<td><input type="text"  class="form-control vendor" name="v.sku[]" >  </td>';
                $str .= '<td><input type="text" readonly class="form-control"  name="metal[]" value="' . $blogpost_obj['metal'] . '">  </td>';
                $str .= '<td><input type="text"  class="form-control"  name="invchallan_no[]" readonly value=' . $return_array['sales_date'] . ' >  </td>';
                $str .= '<td><input type="text"  class="form-control"  name="invchallan_no[]" readonly value=' . $return_array['voucher_no'] . ' >  </td>';
                $str .= '<td><input type="text" readonly class="form-control"  name="weight[]" value="' . $blogpost_obj['weight'] . '" >  </td>';
                $str .= '<td class="cls-text-section"><input type="number" class="form-control qtySale "  name="qty[]" value="' . $blogpost_obj['return_qty'] . '"><input type="number"  class="form-control mrp"  name="mrp[]"  step=0.01 value="' . $blogpost_obj['mrp'] . '"> </td>';
                $str .= '<input type="number"  class="form-control disacount"  name="discount_rate[]" step=0.1 value="' . $blogpost_obj['discount_rate'] . '">';
                $str .= '<input type="number" readonly class="form-control discount" name="discount[]"  step=0.01 value="' . $blogpost_obj['discount'] . '">';
                $str .= '<td><input type="number"  class="form-control taxation" readonly name="taxation[]"  step=0.01 value="' . $blogpost_obj['taxation'] . '">  </td>';
                $str .= '<td><input type="number"  class="form-control cgst" readonly name="cgst[]"  step=0.01 value="' . $blogpost_obj['cgst'] . '">   </td>';
                $str .= '<td><input type="number"  class="form-control sgst" readonly name="sgst[]"  step=0.01 value="' . $blogpost_obj['sgst'] . '">  </td>';
                $str .= '<td><input type="number"  class="form-control igst" readonly name="igst[]"  step=0.01 value="' . $blogpost_obj['igst'] . '">   </td>';
                $str .= '<td><input type="number"  class="form-control grandtotal" readonly name="grandtotal[]" step=0.01 value="' . $blogpost_obj['grandtotal'] . '">   </td>';
                $str .= '</tr>';
            }
            }   
            $return_arary['str'] = $str;
            }

            $comeback = array("outcome" => "true", "data" => $return_arary);
        }
        return $comeback;
    }

    function dealers_orders_update_select() {
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_DEALER, '*', $where_query_arr);

        if (!empty($comeback["data"])) {
            $dealer_id = !empty($comeback["data"]) ? $comeback["data"]["0"]["dealer_status"] : '';

            $return_arary["dealer_id"] = $dealer_id;
            $comeback = array("outcome" => "true", "data" => $return_arary);
        }
        return $comeback;
    }

    function page_select() {
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "page_id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_PAGE_MASTER, '*', $where_query_arr);
        if (!empty($comeback)) {
            $description = isset($comeback['data']->description) ? $comeback['data']->description : '';
            $title = isset($comeback['data']->title) ? $comeback['data']->title : '';
            $return_arary["description"] = $description;
            $return_arary["title"] = $title;
            $comeback = array("outcome" => "true", "data" => $return_arary);
        }
        return $comeback;
    }

    function collection_select() {
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "collection_id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_COLLECTION_MASTER, '*', $where_query_arr);
        if (!empty($comeback)) {
            $description = isset($comeback['data']->description) ? $comeback['data']->description : '';
            $title = isset($comeback['data']->title) ? $comeback['data']->title : '';
            $return_arary["description"] = $description;
            $return_arary["title"] = $title;
            $comeback = array("outcome" => "true", "data" => $return_arary);
        }
        return $comeback;
    }

    function product_select() {
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $product_id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "product_id", "=", "$product_id"]);
        $comeback = $this->select_result(TABLE_PRODUCT_MASTER, '*', $where_query_arr);
        if (!empty($comeback)) {
            $description = isset($comeback['data']->description) ? $comeback['data']->description : '';
            $image = isset($comeback['data']->image) ? $comeback['data']->image : '';
            $title = isset($comeback['data']->title) ? $comeback['data']->title : '';
            $return_arary["description"] = $description;
            $return_arary["image"] = $image;
            $return_arary["title"] = $title;
            $comeback = array("outcome" => "true", "data" => $return_arary);
        }
        return $comeback;
    }

    function make_api_data_blogpostData($api_data_list) {
        $shopinfo = $this->current_store_obj;
        $tr_html = '<tr class="Polaris-ResourceList__ItemWrapper"> <td colspan="5"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Data not found</p></center></td></tr>';
        $prifix = '<td>';
        $sufix = '</td>';
        $html = '';
        foreach ($api_data_list as $blogpost_obj) {
            foreach ($blogpost_obj as $i => $blogpost) {
                $html .= '<tr>';
                $image = ($blogpost->image == '') ? CLS_NO_IMAGE : $blogpost->image->src;
                $html .= $prifix . '<img src="' . $image . '" width="50px" height="50px" >' . $sufix;
                $html .= $prifix . $blogpost->id . $sufix;
                $html .= $prifix . $blogpost->title . $sufix;
                $html .= $prifix . $blogpost->author . $sufix;
                $html .= $prifix .
                        '<div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                        <div class="Polaris-ButtonGroup__Item">
                                            <a href="" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="View">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628c-.092-.229-2.317-5.628-7.929-5.628s-7.837 5.399-7.929 5.628c-.094.239-.094.505 0 .744.092.229 2.317 5.628 7.929 5.628s7.837-5.399 7.929-5.628c.094-.239.094-.505 0-.744m-7.929 4.372c-2.209 0-4-1.791-4-4s1.791-4 4-4c2.21 0 4 1.791 4 4s-1.79 4-4 4m0-6c-1.104 0-2 .896-2 2s.896 2 2 2c1.105 0 2-.896 2-2s-.895-2-2-2" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="Polaris-ButtonGroup__Item">
                                            <a href="blogpost_edit.php?blogpost_id=' . $blogpost->id . '" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="Edit">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 469.331 469.331"><path d="M438.931,30.403c-40.4-40.5-106.1-40.5-146.5,0l-268.6,268.5c-2.1,2.1-3.4,4.8-3.8,7.7l-19.9,147.4   c-0.6,4.2,0.9,8.4,3.8,11.3c2.5,2.5,6,4,9.5,4c0.6,0,1.2,0,1.8-0.1l88.8-12c7.4-1,12.6-7.8,11.6-15.2c-1-7.4-7.8-12.6-15.2-11.6   l-71.2,9.6l13.9-102.8l108.2,108.2c2.5,2.5,6,4,9.5,4s7-1.4,9.5-4l268.6-268.5c19.6-19.6,30.4-45.6,30.4-73.3   S458.531,49.903,438.931,30.403z M297.631,63.403l45.1,45.1l-245.1,245.1l-45.1-45.1L297.631,63.403z M160.931,416.803l-44.1-44.1   l245.1-245.1l44.1,44.1L160.931,416.803z M424.831,152.403l-107.9-107.9c13.7-11.3,30.8-17.5,48.8-17.5c20.5,0,39.7,8,54.2,22.4   s22.4,33.7,22.4,54.2C442.331,121.703,436.131,138.703,424.831,152.403z" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="Polaris-ButtonGroup__Item">
                                         <a href="" class="Polaris-Button">
                                                <span class="Polaris-Button__Content tip" data-hover="Delete">
                                                    <span class="Polaris-Icon">
                                                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg>
                                                    </span>
                                                </span>
                                         </a>       
                                        </div>
                    </div> ' . $sufix;

                $html .= '</tr>';
            }
        }
        return $html;
    }

    function get_store_blog() {
        $login_user_id = $_SESSION['login_user_id'];
        $shopify_api = array("api_name" => $_POST['shopify_api']);
        $data_blog = $this->cls_get_shopify_list($shopify_api, '', 'GET');
        $total_record_blog = count($data_blog->articles);
        foreach ($data_blog->articles as $article) {
            $mysql_date = date('Y-m-d H:i:s');
            $fields = 'blogpost_id';
            $where_query = array(["", "blogpost_id", "=", "$article->id"]);
            $options_arr = array('single' => true);
            $comeback = $this->select_result(TABLE_BLOGPOST_MASTER, $fields, $where_query, $options_arr);
            if (isset($comeback["data"]->blogpost_id) && $comeback["data"]->blogpost_id == $article->id) {
                $response = array("data" => true,
                    "total_record_blog" => intval($total_record_blog),
                );
                continue;
            }
            $img_src = (isset($article->image) && $article->image == '') ? '' : $article->image->src;
            $fields_arr = array(
                '`id`' => '',
                '`blogpost_id`' => $article->id,
                'image' => $img_src,
                'title' => $article->title,
                '`store_user_id`' => $login_user_id,
                '`description`' => str_replace("'", "\'", $article->body_html),
                '`created_at`' => $mysql_date,
                '`updated_at`' => $mysql_date
            );
            $result = $this->post_data(TABLE_BLOGPOST_MASTER, array($fields_arr));
            $response = json_decode($result);
        }
        $response = array(
            "data" => 'true',
            "total_record_blog" => intval($total_record_blog),
            "response" => $response
        );
        return $response;
    }
    
    function take_table_shopify_data() {
        $response = array('result' => 'fail', 'msg' => __('Something went wrong'));
        if (isset($_POST['store']) && $_POST['store'] != '') {
            $shopinfo = $this->current_store_obj;
              
            $per_page = defined('CLS_PAGE_PER') ? CLS_PAGE_PER : 10;

            $limit = isset($_POST['limit']) ? $_POST['limit'] : $per_page;
            $pageno = isset($_POST['pageno']) ? $_POST['pageno'] : '1';
            $offset = $limit * ($pageno - 1);

            $search_word = (isset($_POST['search_key']) && $_POST['search_key'] != '') ? $_POST['search_key'] : NULL;
            $select_seller = (isset($_POST['select_seller']) && $_POST['select_seller'] != '') ? $_POST['select_seller'] : NULL;

            $get_table_arr = $this->take_table_listing_data($_POST['listing_id'], $limit, $offset, $search_word, $select_seller);

            $filtered_count = $get_table_arr['filtered_count'];
            $total_prod_cnt = $get_table_arr['total_prod_cnt'];
            $table_data_arr = $get_table_arr['data_arr'];
           
            $tr_html = call_user_func(array($this, 'make_table_data_' . $_POST['listing_id']), $table_data_arr, $pageno, $get_table_arr['api_name']);
            
            $total_page = ceil($filtered_count / $limit);
            $pagination_html = $this->pagination_btn_html($total_page, $pageno, $_POST['pagination_method'], $_POST['listing_id']);
            $response = array(
                "outcome" => 'true',
                "recordsTotal" => intval($total_prod_cnt),
                "recordsFiltered" => intval($filtered_count),
                'pagination_html' => $pagination_html,
                'html' => $tr_html
            );
        }
        return $response;
    }
    function make_table_data_search_sales_editData($table_data_arr, $pageno, $table_name) {
        $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr> <td colspan="6"><center><p>Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
               $where_query = array(["", "id", "=", $dataObj->sales_id]);
                $comeback = $this->select_result(TABLE_SALES_ORDER, '*', $where_query);
                $return_comeback = (isset($comeback["data"][0]) && $comeback["data"][0] != '') ? $comeback["data"][0] : '';

                $sales_date = (isset($return_comeback['sales_date']) && !empty($return_comeback['sales_date'])) ? $return_comeback['sales_date'] : '-';
                $qty = (isset($dataObj->qty) && !empty($dataObj->qty)) ? $dataObj->qty : '-';
                $sku = (isset($dataObj->sku) && !empty($dataObj->sku)) ? $dataObj->sku : '-';
                $mrp = (isset($dataObj->mrp) && !empty($dataObj->mrp)) ? $dataObj->mrp : '-';
                $barcode = (isset($dataObj->barcode) && !empty($dataObj->barcode)) ? $dataObj->barcode : 0;
                $grand_total = (isset($dataObj->grandtotal) && !empty($dataObj->grandtotal)) ? $dataObj->grandtotal : '-';
                $tr_html.='<tr>';
                $tr_html.='<td><input class="chkbox CheckBox" type="checkbox" id="chkbox_'.$dataObj->id.'" name="chkbox" value="1"></td>';
                $tr_html.='<td>' . $barcode . '</td>';
                $tr_html.='<td>' . $sku . '</td>';
                $tr_html.='<td>' . $qty . '</td>';
                $tr_html.='<td>' . $mrp . '</td>';
                $tr_html.='<td>' . $grand_total . '</td>';
                $tr_html.='<td>' . $sales_date . '</td>';
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }
    
    function make_table_data_collectionData($table_data_arr, $pageno, $table_name) {
        $shopinfo = $this->current_store_obj;
        $total_record = $table_data_arr->num_rows;       
        $tr_html = '<tr> <td colspan="11"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
              
                $dataObj = (object) $dataObj;
                $where_query = array(["", "id", "=", $dataObj->supplier_id]);
                $comeback = $this->select_result(TABLE_SUPPLIERS, '*', $where_query);
                $comeback = $comeback["data"]["0"];

                $comeback = (object)$comeback;
                $supplier_name = !empty($comeback) ? $comeback->supplier_name : '-';
                $tr_html.='<tr>';
                $tr_html.='<td>' . $dataObj->id . '</td>';
                $tr_html.='<td>' . $dataObj->voucher_no .'</td>';
                $tr_html.='<td>' . $dataObj->po_date . '</td>';
                $tr_html.='<td>' . $comeback->supplier_name . '</td>';
                $tr_html.='<td>' . $dataObj->qty . '</td>';
                $tr_html.='<td>' . $dataObj->taxable . '</td>';
                $tr_html.='<td>' . $dataObj->cgst . '</td>';
                $tr_html.='<td>' . $dataObj->sgst . '</td>';
                $tr_html.='<td>' . $dataObj->igst . '</td>';
                $tr_html.='<td>' . $dataObj->all_total . '</td>';

                if ($dataObj->status == '1') {
                    $svg_icon_status = CLS_SVG_EYE;
                    $data_hover = 'View';
                }
                $after_delete_pageno = $pageno;
                if ($total_record == 1) {
                    $after_delete_pageno = $pageno - 1;
                }
                $tr_html.='
            	<td>
                <a href="purchase_listing_edit.php?pid=' . $dataObj->id . '&sid=' . $dataObj->supplier_id . '&store=' . $_SESSION['store'] . '" >
                <span class="edit">
                <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-edit" ></i>
                </button>
                </span>
                </a>
                <a href="purchase_view.php?id=' . $dataObj->id . '&sid=' . $dataObj->supplier_id . '&store=' . $_SESSION['store'] . '" >
                <span class="view" data-id=' . $dataObj->id . '>
                <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-eye"></i>
                </button>
                </span> 
                </a>
                 <a href="purchase_print.php?id=' . $dataObj->id . '&sid=' . $dataObj->supplier_id . '&store=' . $_SESSION['store'] . '"  target="_blank">
                <span class="view" data-id=' . $dataObj->id . '>
                <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-print"></i>
                </button>
                </span> 
                </a>
                <span data-hover="Delete" onclick="update_status(' . $dataObj->id . ',this,\'' . TABLE_PURCHASE . '\',\''.TABLE_SKU_MASTER.'\',\'' . TABLE_PURCHASE_RETURN . '\')">
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-trash"></i>
                        </button>
                </span>
                </td>';
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }

    function get_store_collection() {
        $login_user_id = $_SESSION['login_user_id'];
        $shopify_api = array("api_name" => $_POST['shopify_api']);
        $data_collection = $this->cls_get_shopify_list($shopify_api, '', 'GET');
        $total_record_collection = count($data_collection->custom_collections);
        foreach ($data_collection->custom_collections as $collection) {
            $mysql_date = date('Y-m-d H:i:s');
            $fields = 'collection_id';
            $where_query = array(["", "collection_id", "=", "$collection->id"]);
            $options_arr = array('single' => true);
            $comeback = $this->select_result(TABLE_COLLECTION_MASTER, $fields, $where_query, $options_arr);

            if (isset($comeback["data"]->collection_id) && $comeback["data"]->collection_id == $collection->id) {
                $response = array("data" => true, "total_record_collection" => intval($total_record_collection));
                continue;
            }
            $fields_arr = array(
                '`id`' => '',
                '`collection_id`' => $collection->id,
                '`title`' => $collection->title,
                '`store_user_id`' => $login_user_id,
                '`description`' => str_replace("'", "\'", $collection->body_html),
                '`created_at`' => $mysql_date,
                '`updated_at`' => $mysql_date
            );
            $result = $this->post_data(TABLE_COLLECTION_MASTER, array($fields_arr));
            $response = json_decode($result);
        }
        $response = array(
            "data" => 'true',
            "total_record_collection" => intval($total_record_collection),
            "response" => $response
        );
        return $response;
    }

    function make_table_data_blogpostData($table_data_arr, $pageno, $table_name) {
        $shopinfo = $this->current_store_obj;
        $total_record = $table_data_arr->num_rows;
       
        $tr_html = '<tr> <td colspan="11"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
                $where_query = array(["", "id", "=", "$dataObj->supplier_id"]);
                $comeback = $this->select_result(TABLE_SUPPLIERS, '*', $where_query);
                $supplier_name = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_name"] : '-';
                $tr_html.='<tr>';
                $tr_html.='<td>' . $dataObj->id . '</td>';
                $tr_html.='<td>' . $dataObj->voucher_no .'</td>';
                $tr_html.='<td>' . $dataObj->return_date . '</td>';
                $tr_html.='<td>' . $supplier_name . '</td>';
                $tr_html.='<td>' . $dataObj->qty . '</td>';
                $tr_html.='<td>' . $dataObj->taxable . '</td>';
                $tr_html.='<td>' . $dataObj->cgst . '</td>';
                $tr_html.='<td>' . $dataObj->sgst . '</td>';
                $tr_html.='<td>' . $dataObj->igst . '</td>';
                $tr_html.='<td>' . $dataObj->grand_total . '</td>';
                if ($dataObj->status == '1') {
                    $svg_icon_status = CLS_SVG_EYE;
                    $data_hover = 'View';
                }
                $after_delete_pageno = $pageno;
                if ($total_record == 1) {
                    $after_delete_pageno = $pageno - 1;
                }
                $tr_html.='
            	<td>
                        <a href="puchase_return_edit.php?pid=' . $dataObj->id . '&sid=' . $dataObj->supplier_id . '&purchase_id='.$dataObj->purchase_id.'&store=' .$_SESSION['store'] . '" >
                        <span class="edit" data-id=' . $dataObj->id . '>
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-edit" ></i>
                        </button>
                        </span>
                        </a>
                        <a href="purchase_return_view.php?id=' . $dataObj->id . '&sid=' . $dataObj->supplier_id . '&pid=' . $dataObj->sku_id . '&store=' . $_SESSION['store'] . '" >        
                        <span class="view" data-id=' . $dataObj->id . '>
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-eye"></i>
                        </button>
                        </a>
                        
                        <a href="purchase_return_print.php?id=' . $dataObj->id . '&sid=' . $dataObj->supplier_id . '&pid=' . $dataObj->sku_id .'&store=' . $_SESSION['store'] . '"  target="_blank"  >
                       <span class="view" data-id=' . $dataObj->id . '>
                       <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-print"></i>
                       </button>
                       </span> 
                       </a>
                       <span data-hover="Delete" onclick="update_status(' . $dataObj->purchase_id . ',this,\'' . TABLE_PURCHASE . '\',\''.TABLE_SKU_MASTER.'\',\'' . TABLE_PURCHASE_RETURN . '\')">
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-trash"></i>
                        </button>
                        </span>
                    </td>';
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }

    function make_table_data_dealerData($table_data_arr, $pageno, $table_name) {
        $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr> <td colspan="9"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
                if ($dataObj->active == 1) {
                    $active = 'Active';
                } else {
                    $active = "InActive";
                }
                $tr_html.='<tr>';
                $tr_html.='<td>' . $dataObj->id . '</td>';
                $tr_html.='<td>' . $dataObj->firstname . '</td>';
                $tr_html.='<td>' . $dataObj->lastname . '</td>';
                $tr_html.='<td>' . $dataObj->email . '</td>';
                $tr_html.='<td>' . $dataObj->username . '</td>';
                $tr_html.='<td>' . $dataObj->mobile . '</td>';
                $tr_html.='<td>' . $dataObj->city . '</td>';
                $tr_html.='<td>' . $dataObj->state . '</td>';
                $tr_html.='<td>' . $active . '</td>';
                if ($dataObj->status == '1') {
                    $svg_icon_status = CLS_SVG_EYE;
                    $data_hover = 'View';
                }
                $after_delete_pageno = $pageno;
                if ($total_record == 1) {
                    $after_delete_pageno = $pageno - 1;
                }
                $tr_html.='
            	<td>
                   <span class="delete" data-hover="Delete"  onclick="update_status(' . $dataObj->id . ',this,\'' . TABLE_DEALER . '\')">
                            <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-trash"></i>
                            </button>
                            </span>
                            
                            <a href="dealer_edit.php?id=' . $dataObj->id . '&store=' . $_SESSION['store'] . '" >
                            <span class="edit" data-id=' . $dataObj->id . '>
                            <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-edit" ></i>
                            </button>
                            </span>
                            </a>
                    </td>';
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }

    function make_table_data_dealerorderData($table_data_arr, $pageno, $table_name) {
        $shopinfo = $this->current_store_obj;
        $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr> <td colspan="9"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
                if ($dataObj->dealer_status == 1) {
                    $active = 'Under process';
                } else if ($dataObj->dealer_status == 2) {
                    $active = "Cancelled";
                } else if ($dataObj->dealer_status == 3) {
                    $active = "Dispatched";
                } else {
                    $active = "Pending";
                }
                $tr_html.='<tr>';
                $tr_html.='<td>' . $dataObj->id . '</td>';
                $tr_html.='<td>' . $dataObj->firstname . '</td>';
                $tr_html.='<td>' . $dataObj->lastname . '</td>';
                $tr_html.='<td>' . $dataObj->email . '</td>';
                $tr_html.='<td>' . $dataObj->username . '</td>';
                $tr_html.='<td>' . $dataObj->mobile . '</td>';
                $tr_html.='<td>' . $dataObj->created_at . '</td>';
                $tr_html.='<td>' . $active . '</td>';
                if ($dataObj->status == '1') {
                    $svg_icon_status = CLS_SVG_EYE;
                    $data_hover = 'View';
                }
                $after_delete_pageno = $pageno;
                if ($total_record == 1) {
                    $after_delete_pageno = $pageno - 1;
                }
                $tr_html.='
            	<td>
                <a href="dealer_orders_update.php?id=' . $dataObj->id . '&store=' . $_SESSION['store'] . '" >
                            <span class="edit" >
                            <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-edit" ></i>
                            </button>
                            </span>
                            </a>
                <span data-hover="Delete" onclick="update_status(' . $dataObj->id . ',this,\'' . TABLE_DEALER . '\')">
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-trash"></i>
                        </button>
                </span>            
                    </td>';
                
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }

    function make_table_data_salesData($table_data_arr, $pageno, $table_name) {
        $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr> <td colspan="12"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
             
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
                if ($dataObj->invoice_type == 1) {
                    $active = 'On Approval';
                } else {
                    $active = "Direct";
                }
                $where_query = array(["", "id", "=", "$dataObj->dealer_id"]);
                $comeback = $this->select_result(TABLE_DEALER, '*', $where_query);
                $firstname = !empty($comeback["data"]) ? $comeback["data"]["0"]["firstname"] : '-';
                $tr_html.='<tr>';
                $tr_html.='<td>' . $dataObj->id . '</td>';
                $tr_html.='<td>'.$dataObj->voucher_no.'</td>';
                $tr_html.='<td>' . $dataObj->sales_date . '</td>';
                $tr_html.='<td>' . $active . '</td>';
                $tr_html.='<td>' . $firstname . '</td>';
                $tr_html.='<td>' . $dataObj->qty . '</td>';
                $tr_html.='<td>' . $dataObj->taxation . '</td>';
                $tr_html.='<td>' . $dataObj->cgst . '</td>';
                $tr_html.='<td>' . $dataObj->sgst . '</td>';
                $tr_html.='<td>' . $dataObj->igst . '</td>';
                $tr_html.='<td>' . $dataObj->grandtotal . '</td>';
                if ($dataObj->status == '1') {
                    $svg_icon_status = CLS_SVG_EYE;
                    $data_hover = 'View';
                }
                $after_delete_pageno = $pageno;
                if ($total_record == 1) {
                    $after_delete_pageno = $pageno - 1;
                }
                $tr_html.='
            	<td>
                    <a href="sales_edit.php?pid=' . $dataObj->id . '&sid=' . $dataObj->dealer_id . '&store=' . $_SESSION['store'] . '" >
                    <span class="edit" data-id=' . $dataObj->id . '>
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-edit" ></i>
                        </button>
                        </span>
                        </a>
                         <a href="sales_view.php?id=' . $dataObj->id . '&sid=' . $dataObj->dealer_id . '&store=' . $_SESSION['store'] . '" >                 
                        <span class="view" data-id=' . $dataObj->id . '>
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-eye"></i>
                        </button>
                        </a>
                        <a href="sales_print.php?id=' . $dataObj->id . '&sid=' . $dataObj->dealer_id . '&store=' . $_SESSION['store'] . '"  target="_blank">
                       <span class="view" data-id=' . $dataObj->id . '>
                       <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-print"></i>
                       </button>
                       </span> 
                       </a>
                        </span>
                        <span data-hover="Delete" onclick="update_status_sales(' . $dataObj->id . ',this,\'' . TABLE_SALES_ORDER . '\',\''.TABLE_SALES_SKU_MASTER.'\',\'' . TABLE_SALES_ORDER_RETURN . '\')">
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-trash"></i>
                        </button>
                        </span>
                    </td>';
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }
    
    function make_table_data_sales_return_listingData($table_data_arr, $pageno, $table_name) {
        $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr> <td colspan="12"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
                if ($dataObj->so_type == 1) {
                    $active = 'On Approval';
                } else {
                    $active = "Direct";
                }
                $where_query = array(["", "id", "=", "$dataObj->dealer_id"]);
                $comeback = $this->select_result(TABLE_DEALER, '*', $where_query);
                $firstname = !empty($comeback["data"]) ? $comeback["data"]["0"]["firstname"] : '-';
                $qty  = !empty($dataObj->qty ) ? $dataObj->qty  : $dataObj->qty_ret ;
                $tr_html.='<tr>';
                $tr_html.='<td>' . $dataObj->id . '</td>';
                $tr_html.='<td>'.$dataObj->voucher_no.'</td>';
                $tr_html.='<td>' . $dataObj->return_date . '</td>';
                $tr_html.='<td>' . $active . '</td>';
                $tr_html.='<td>' . $firstname . '</td>';
                $tr_html.='<td>' . $qty . '</td>';
                $tr_html.='<td>' . $dataObj->taxable . '</td>';
                $tr_html.='<td>' . $dataObj->cgst . '</td>';
                $tr_html.='<td>' . $dataObj->sgst . '</td>';
                $tr_html.='<td>' . $dataObj->igst . '</td>';
                $tr_html.='<td>' . $dataObj->grand_total . '</td>';
                if ($dataObj->status == '1') {
                    $svg_icon_status = CLS_SVG_EYE;
                    $data_hover = 'View';
                }
                $after_delete_pageno = $pageno;
                if ($total_record == 1) {
                    $after_delete_pageno = $pageno - 1;
                }
                $tr_html.='
            	<td>
                    <a href="sales_return_edit.php?pid=' . $dataObj->id . '&sid=' . $dataObj->dealer_id . '&sku_id='.$dataObj->	sales_sku_id.'&sales_id='.$dataObj->sales_id.'&store=' . $_SESSION['store'] .'" >
                         <span class="edit" data-id=' . $dataObj->id . '>
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-edit" ></i>
                        </button>
                        </span>
                        </a>
                        <a href="sales_return_view.php?id=' . $dataObj->id . '&sid=' . $dataObj->dealer_id . '&sales_sku_id=' . $dataObj->sales_sku_id . '&sales_id=' . $dataObj->sales_id . '&store=' . $_SESSION['store'] .'" >           
                        <span class="view" data-id=' . $dataObj->id . '>
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-eye"></i>
                        </button>
                        </a>
                        <a href="sales_return_print.php?id=' . $dataObj->id . '&sid=' . $dataObj->dealer_id . '&sales_sku_id=' . $dataObj->sales_sku_id . '&store=' . $_SESSION['store'] .'"  target="_blank">
                       <span class="view" data-id=' . $dataObj->id . '>
                       <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-print"></i>
                       </button>
                       </span> 
                       </a>
                        </span>
                                <span data-hover="Delete" onclick="update_status_sales(' . $dataObj->id . ',this,\'' . TABLE_SALES_ORDER . '\',\''.TABLE_SALES_SKU_MASTER.'\',\'' . TABLE_SALES_ORDER_RETURN . '\')">
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-trash"></i>
                        </button>
                        </span>
                    </td>';
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }

    function get_store_pages() {
        $login_user_id = $_SESSION['login_user_id'];
        $shopify_api = array("api_name" => $_POST['shopify_api']);
        $data_pages = $this->cls_get_shopify_list($shopify_api, '', 'GET');
        $total_record_pages = count($data_pages->pages);
        foreach ($data_pages->pages as $pages) {
            $mysql_date = date('Y-m-d H:i:s');
            $fields = 'page_id';
            $where_query = array(["", "page_id", "=", "$pages->id"]);
            $options_arr = array('single' => true);
            $comeback = $this->select_result(TABLE_PAGE_MASTER, $fields, $where_query, $options_arr);
            if (isset($comeback["data"]->page_id) && $comeback["data"]->page_id == $pages->id) {
                $response = array("data" => true, "total_record_pages" => intval($total_record_pages));
                continue;
            }

            $fields_arr = array(
                '`id`' => '',
                '`page_id`' => $pages->id,
                '`title`' => $pages->title,
                '`store_user_id`' => $login_user_id,
                '`description`' => str_replace("'", "\'", $pages->body_html),
                '`created_at`' => $mysql_date,
                '`updated_at`' => $mysql_date
            );
            $result = $this->post_data(TABLE_PAGE_MASTER, array($fields_arr));
            $response = json_decode($result);
        }
        $response = array(
            "data" => 'true',
            "total_record_pages" => intval($total_record_pages),
            "response" => $response
        );
        return $response;
    }

    function make_table_data_pagesData($table_data_arr, $pageno, $table_name) {
        $shop = $this->current_store_obj->store;
        $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr class="Polaris-ResourceList__ItemWrapper"> <td colspan="7"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
                $tr_html.='<tr class="Polaris-ResourceList__ItemWrapper trhover">';
                $tr_html.='<td>' . $dataObj->id . '</td>';
                $tr_html.='<td>' . $dataObj->page_id . '</td>';
                $tr_html.='<td >' . $dataObj->title . '</td>';
                $tr_html.='<td style= "width: 100%;">' . $dataObj->description . '</td>';
                if ($dataObj->status == '1') {
                    $svg_icon_status = CLS_SVG_EYE;
                    $data_hover = 'View';
                }
                $after_delete_pageno = $pageno;
                if ($total_record == 1) {
                    $after_delete_pageno = $pageno - 1;
                }
                $tr_html.='
            <td>
            <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented highlight-text">                   
              <div class="Polaris-ButtonGroup__Item highlight-text">
                <span class="Polaris-Button Polaris-Button--sizeSlim tip loader_show" data-hover="' . $data_hover . '">
                 <a href="page-details.php?page_id=' . $dataObj->page_id . '&view=true&store=' . $_SESSION['store'] . '" >
                        <span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop">
                            ' . $svg_icon_status . '
                        </span>
                    </a>
                </span>
            </div>
            <div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented highlight-text">                   
              <div class="Polaris-ButtonGroup__Item highlight-text">
                <span class="Polaris-Button Polaris-Button--sizeSlim tip loader_show" data-hover="Edit">
                      <a href="pages_edit.php?page_id=' . $dataObj->page_id . '&store=' . $_SESSION['store'] . '" >
                        <span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop">
                            ' . CLS_SVG_EDIT . '
                        </span>
                    </a>
                </span>
            </div>
                  <div class="Polaris-ButtonGroup__Item highlight-text">
                    <span class="Polaris-Button Polaris-Button--sizeSlim tip " data-hover="Delete" onclick="removeFromTable(\'' . TABLE_PAGE_MASTER . '\',' . $dataObj->page_id . ',' . $dataObj->id . ',' . $after_delete_pageno . ', \'pagesData\',\'pages\' ,this)">
                        <a class="history-link" href="javascript:void(0)">
                            <span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop save_loader_show' . $dataObj->page_id . '">
                                ' . CLS_SVG_DELETE . '
                            </span>
                        </a>
                    </span>
                </div>';
                $tr_html.='</div></td></tr>';
            }
        }
        return $tr_html;
    }

    function get_store_product() {
//         $login_user_id = $_SESSION['login_user_id'];
        $shopify_api = array("api_name" => $_POST['shopify_api']);
        $data_pages = $this->cls_get_shopify_list($shopify_api, '', 'GET');
        $total_record_product = count($data_pages->products);
        foreach ($data_pages->products as $product) {
            $mysql_date = date('Y-m-d H:i:s');
            $fields = 'product_id';
            $where_query = array(["", "product_id", "=", "$product->id"]);
            $options_arr = array('single' => true);
            $comeback = $this->select_result(TABLE_PRODUCT_MASTER, $fields, $where_query, $options_arr);
            $img_src = ($product->image == '') ? CLS_NO_IMAGE : $product->image->src;
            foreach ($product->variants as $i => $variants) {
                $main_price = ($variants->position == "1") ? $variants->price : "";
            }
            if (isset($comeback["data"]->product_id) && $comeback["data"]->product_id == $product->id) {
                $response = array("data" => true, "total_record_product" => intval($total_record_product));
                continue;
            }
            $fields_arr = array(
                '`id`' => '',
                '`product_id`' => $product->id,
                '`image`' => $img_src,
                '`title`' => $product->title,
                '`description`' => str_replace("'", "\'", $product->body_html),
                '`vendor`' => $product->vendor,
                '`price`' => $main_price,
                '`store_user_id`' => $login_user_id,
                '`created_at`' => $mysql_date,
                '`updated_at`' => $mysql_date
            );
            $result = $this->post_data(TABLE_PRODUCT_MASTER, array($fields_arr));
            $response = json_decode($result);
        }
        $response = array(
            "data" => 'true',
            "total_record_product" => intval($total_record_product),
            "response" => $response
        );
        return $response;
    }

    function make_table_data_productData($table_data_arr, $pageno, $table_name) {
        $shopinfo = $this->current_store_obj;
        $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr> <td colspan="7"><center><p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
                if ($dataObj->supplier_status == 0) {
                    $status = 'Active';
                } else {
                    $status = "InActive";
                }
                $tr_html.='<tr">';
                $tr_html.='<td>' . $dataObj->id . '</td>';
                $tr_html.='<td>' . $dataObj->supplier_name . '</td>';
                $tr_html.='<td>' . $dataObj->generate_code . '</td>';
                $tr_html.='<td>' . $dataObj->supplier_phone . '</td>';
                $tr_html.='<td>' . $dataObj->supplier_city . '</td>';
                $tr_html.='<td>' . $status . '</td>';
                if ($dataObj->supplier_status == 1) {
                    $svg_icon_status = CLS_SVG_EYE;
                    $data_hover = 'View';
                }
                $after_delete_pageno = $pageno;
                if ($total_record == 1) {
                    $after_delete_pageno = $pageno - 1;
                }
                $tr_html.='
           	<td>
             <span onclick="update_status(' . $dataObj->id . ',this,\'' . TABLE_SUPPLIERS . '\')">
                        <button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-trash"></i>
                        </button>
                        </span>
                        <a href="supplier_listing_edit.php?id=' . $dataObj->id . '&store=' . $_SESSION['store'] . '" >  
                            <span class="edit" data-id=' . $dataObj->id . '>
                            <button  class="btn btn-raised btn-default m-t-15 waves-effect" ><i class="fa fa-edit" ></i>
                            </button>
                            </span>
                        </a>
                </td>';
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }

    function addblog() {

        $response = array('data' => 'fail', 'msg' => __('Something went wrong'));
        $api_fields = array();
        if (isset($_POST['store']) && $_POST['store'] != '') {
            $shopinfo = $this->current_store_obj;
            $image_path = explode(",", $_POST['images']);
            $api_fields = array('article' => array('title' => $_POST["title"], 'body_html' => $_POST["description"]));
            if (isset($_FILES['upload_file']['name']) && $_FILES['upload_file']['name'] != "") {
                $api_fields["article"]['image'] = array('attachment' => trim(end($image_path)));
            }
            $api_articles = array("api_name" => "articles");
            $set_position = $this->cls_get_shopify_list($api_articles, $api_fields, 'POST', 1, array("Content-Type: application/json"));
            $comeback = array("data" => true);
            $mysql_date = date('Y-m-d H:i:s');
            $fields_arr = array(
                '`id`' => '',
                '`blogpost_id`' => $set_position->article->id,
                'title' => $_POST['title'],
                '`store_user_id`' => $shopinfo->login_user_id,
                '`description`' => $_POST['description'],
                '`created_at`' => $mysql_date,
                '`updated_at`' => $mysql_date
            );
            if (isset($set_position->article->image)) {
                $fields_arr['`image`'] = $set_position->article->image->src;
            }

            $result = $this->post_data(TABLE_BLOGPOST_MASTER, array($fields_arr));
            $response = json_decode($result);
        } else {
            
        }
        return $response;
    }

    function addproduct() {
        $response = array('result' => 'fail', 'msg' => __('Something went wrong'));
        $api_fields = array();
        if (isset($_POST['store']) && $_POST['store'] != '') {
            $shopinfo = $this->current_store_obj;
            $image_path = explode(",", $_POST['images']);
            $api_fields = array('product' => array('title' => $_POST["title"], 'body_html' => $_POST["description"]));
            $api_articles = array("api_name" => "products");
            $set_position = $this->cls_get_shopify_list($api_articles, $api_fields, 'POST', 1, array("Content-Type: application/json"));

            if (isset($set_position->product->id)) {
                $mysql_date = date('Y-m-d H:i:s');
                $fields_arr = array(
                    '`id`' => '',
                    '`product_id`' => $set_position->product->id,
                    'title' => $set_position->product->title,
                    '`store_user_id`' => $shopinfo->login_user_id,
                    '`vendor`' => $set_position->product->vendor,
                    '`description`' => $_POST['description'],
                    '`created_at`' => $mysql_date,
                    '`updated_at`' => $mysql_date
                );
                if (isset($_FILES['upload_file']['name']) && ($_FILES['upload_file']['name'] != "")) {
                    $api_fields = array("image" => array("attachment" => trim(end($image_path)), "filename" => $_FILES['upload_file']['name']));
                    $api_articles = array("name" => "products", "id" => $set_position->product->id, "api_name" => "images");
                    $set_image = $this->cls_get_shopify_list($api_articles, $api_fields, 'POST', 1, array("Content-Type: application/json"));
                    if (isset($set_image->image->src)) {
                        $fields_arr['`image`'] = $set_image->image->src;
                    }
                }
                $result = $this->post_data(TABLE_PRODUCT_MASTER, array($fields_arr));
            }
            $response = json_decode($result);
            return $response;
        }
    }

    function addpages() {
        $response = array('result' => 'fail', 'msg' => __('Something went wrong'));
        if (isset($_POST['store']) && $_POST['store'] != '') {
            $shopinfo = $this->current_store_obj;
            $api_fields = array('page' => array('title' => $_POST["title"], 'body_html' => $_POST["description"], 'author' => $_POST["store"]));
            $api_articles = array("api_name" => "pages");
            $set_position = $this->cls_get_shopify_list($api_articles, $api_fields, 'POST', 1, array("Content-Type: application/json"));
            $comeback = array("data" => true);
            $mysql_date = date('Y-m-d H:i:s');
            $fields_arr = array(
                '`id`' => '',
                '`page_id`' => $set_position->page->id,
                'title' => $set_position->page->title,
                '`description`' => $set_position->page->body_html,
                '`store_user_id`' => $shopinfo->login_user_id,
                '`created_at`' => $mysql_date,
                '`updated_at`' => $mysql_date
            );
            $result = $this->post_data(TABLE_PAGE_MASTER, array($fields_arr));
            $response = json_decode($result);
            return $response;
        }
    }

    function addcollections() {
        $response = array('result' => 'fail', 'msg' => __('Something went wrong'));
        if (isset($_POST['store']) && $_POST['store'] != '') {
            $shopinfo = $this->current_store_obj;
            $api_fields = array("custom_collection" => array("title" => $_POST["title"], "body_html" => $_POST["description"]));
            $api_articles = array("api_name" => "custom_collection");
            $set_position = $this->cls_get_shopify_list($api_articles, $api_fields, 'POST', 1, array("Content-Type: application/json"));
            $comeback = array("data" => true);
            $mysql_date = date('Y-m-d H:i:s');
            $fields_arr = array(
                '`id`' => '',
                '`collection_id`' => '',
                'title' => $_POST['title'],
                '`description`' => $_POST['description'],
                '`store_user_id`' => $shopinfo->login_user_id,
                '`created_at`' => $mysql_date,
                '`updated_at`' => $mysql_date
            );
            $result = $this->post_data(TABLE_COLLECTION_MASTER, array($fields_arr));
            $response = json_decode($result);
            return $response;
        }
    }
        function get_supplier_address(){
        $shopinfo = $this->current_store_obj;   
        $supplier_id = isset($_POST['supplier_id']) ? $_POST['supplier_id'] : '';
        $store_user_id = isset($shopinfo[0]['store_user_id']) ? $shopinfo[0]['store_user_id'] : '';
        $where_query = array(["", "id", "=", "$supplier_id"], ["AND", "store_user_id", "=", "$store_user_id"]);
        $comeback = $this->select_result(TABLE_SUPPLIERS, '*', $where_query);
        
        if (!empty($comeback["data"])) {
            $supplier_address = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_address"] : '';
            $return_arary["supplier_address"] = $supplier_address;
            $response = array("outcome" => "true", "data" => $return_arary);
             return $response;
         }
      
    }
    
    function get_billing_address(){
        $shopinfo = $this->current_store_obj;
        $dealer_id = isset($_POST['dealer_id']) ? $_POST['dealer_id'] : '';
        $where_query = array(["", "id", "=", "$dealer_id"]);
        $comeback = $this->select_result(TABLE_DEALER, '*', $where_query);
        $return_array["billing_address"] = (isset($comeback["data"][0]['billing_address']) && $comeback["data"][0]['billing_address'] != '')?$comeback["data"][0]['billing_address'] :'';
        $return_array["shipping_address"] = (isset($comeback["data"][0]['address']) && $comeback["data"][0]['address'] != '')?$comeback["data"][0]['address'] :'';
        return $return_array;
    }

    function make_table_data_searchData($table_data_arr, $pageno, $table_name) {
      $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr> <td colspan="6"><center><p>Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
                $sku = (isset($dataObj->sku) && !empty($dataObj->sku)) ? $dataObj->sku : '-';
                $qty = (isset($dataObj->barcode) && !empty($dataObj->barcode)) ? $dataObj->barcode : '-';
                $price = (isset($dataObj->price) && !empty($dataObj->price)) ? $dataObj->price : '-';
                $voucher_no = (isset($dataObj->voucher_no) && !empty($dataObj->voucher_no)) ? $dataObj->voucher_no : '-';
                $tr_html.='<tr>';
                $tr_html.='<td><input class="chkboxforPurchase CheckBox" type="checkbox" id="chkbox_'.$dataObj->id.'" name="chkbox" value="1"></td>';
                $tr_html.='<td>' . $sku . '</td>';
                $tr_html.='<td>' . $qty . '</td>';
                $tr_html.='<td>' . $price . '</td>';
                $tr_html.='<td>' . $voucher_no . '</td>';
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }
    
        function make_table_data_searchEditData($table_data_arr, $pageno, $table_name) {
        $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr> <td colspan="6"><center><p>Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = ''; 
           
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
               
                $where_query = array(["", "id", "=", $dataObj->purchase_id]);
                $comeback = $this->select_result(TABLE_PURCHASE, '*', $where_query);
                $return_comeback = (isset($comeback["data"][0]) && $comeback["data"][0] != '')?$comeback["data"][0]:'';
        
                $po_date = (isset($return_comeback['po_date']) && !empty($return_comeback['po_date'])) ? $return_comeback['po_date'] : '-';
                $sku = (isset($dataObj->sku) && !empty($dataObj->sku)) ? $dataObj->sku : '-';
                $barcode = (isset($dataObj->barcode) && !empty($dataObj->barcode)) ? $dataObj->barcode : '-';
                $qty = (isset($dataObj->qty) && !empty($dataObj->qty)) ? $dataObj->qty : '-';
                $price = (isset($dataObj->price) && !empty($dataObj->price)) ? $dataObj->price : '-';
                $voucher_no = (isset($dataObj->voucher_no) && !empty($dataObj->voucher_no)) ? $dataObj->voucher_no : '-';
                $tr_html.='<tr>';
                $tr_html.='<td><input class="chkboxforPurchase CheckBox" type="checkbox" id="chkbox_'.$dataObj->id.'" name="chkbox" value="1"></td>';
                $tr_html.='<td>' . $sku . '</td>';
                $tr_html.='<td>' . $barcode . '</td>';
                $tr_html.='<td>' . $qty . '</td>';
                $tr_html.='<td>' . $price . '</td>';
                $tr_html.='<td>' . $voucher_no . '</td>';
                $tr_html.='<td>' . $po_date . '</td>';
                $tr_html.='</tr>';
            }
             return $tr_html;  
        }
        }
   function make_table_data_search_sales_Data($table_data_arr, $pageno, $table_name) {
       $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr> <td colspan="6"><center><p>Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
                $where_query = array(["", "id", "=", $dataObj->sales_id]);
                $comeback = $this->select_result(TABLE_SALES_ORDER, '*', $where_query);
                $return_comeback = (isset($comeback["data"][0]) && $comeback["data"][0] != '') ? $comeback["data"][0] : '';

                $sales_date = (isset($return_comeback['sales_date']) && !empty($return_comeback['sales_date'])) ? $return_comeback['sales_date'] : '-';
                $qty = (isset($dataObj->qty) && !empty($dataObj->qty)) ? $dataObj->qty : '-';
                $sku = (isset($dataObj->sku) && !empty($dataObj->sku)) ? $dataObj->sku : '-';
                $mrp = (isset($dataObj->mrp) && !empty($dataObj->mrp)) ? $dataObj->mrp : '-';
                $barcode = (isset($dataObj->barcode) && !empty($dataObj->barcode)) ? $dataObj->barcode : 0;
                $grand_total = (isset($dataObj->grandtotal) && !empty($dataObj->grandtotal)) ? $dataObj->grandtotal : '-';
                $tr_html.='<tr>';
                $tr_html.='<td><input class="chkbox CheckBox" type="checkbox" id="chkbox_'.$dataObj->id.'" name="chkbox" value="1"></td>';
                $tr_html.='<td>' . $barcode . '</td>';
                $tr_html.='<td>' . $sku . '</td>';
                $tr_html.='<td>' . $qty . '</td>';
                $tr_html.='<td>' . $mrp . '</td>';
                $tr_html.='<td>' . $grand_total . '</td>';
                $tr_html.='<td>' . $sales_date . '</td>';
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }
    function purhcase_return_select() {
        $shopinfo = $this->current_store_obj;
        $comeback = array('result' => 'fail', 'msg' => CLS_SOMETHING_WENT_WRONG);
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query_arr = array(["", "id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_PURCHASE_RETURN, '*', $where_query_arr);
        if (!empty($comeback["data"])) {
            $po_type = !empty($comeback["data"]) ? $comeback["data"]["0"]["po_type"] : '';
            $supplier_id = !empty($comeback["data"]) ? $comeback["data"]["0"]["supplier_id"] : '';
            $return_date = !empty($comeback["data"]) ? $comeback["data"]["0"]["return_date"] : '';
            $ref_num = !empty($comeback["data"]) ? $comeback["data"]["0"]["ref_num"] : '';
            $qty_ret = !empty($comeback["data"]) ? $comeback["data"]["0"]["qty_ret"] : '';
            $price_ret = !empty($comeback["data"]) ? $comeback["data"]["0"]["price_ret"] : '';
            $reason = !empty($comeback["data"]) ? $comeback["data"]["0"]["reason"] : '';
//            $purchase_id = !empty($comeback["data"]) ? $comeback["data"]["0"]["purchase_id"] : '';
            
            $return_arary["po_type"] = $po_type;
            $return_arary["supplier_id"] = $supplier_id;
            $return_arary["return_date"] = $return_date;
            $return_arary["ref_num"] = $ref_num;
            $return_arary["qty_ret"] = $qty_ret;
            $return_arary["price_ret"] = $price_ret;
            $return_arary["reason"] = $reason;
//            $return_arary["purchase_id"] = $purchase_id;
            
            $purchase_id = isset($_POST['purchase_id']) ? $_POST['purchase_id'] : '';
            $where_query_arr = array(["", "purchase_id", "=", "$purchase_id"]);
            $comeback = $this->select_result(TABLE_SKU_MASTER, '*', $where_query_arr);

            if(!empty($comeback["data"])){
            $data = !empty($comeback["data"]) ? $comeback["data"] : '';

            $str = '';
            $startnum = 1;
            foreach ($data as $blogpost_obj) {
                $return_qty = (isset($blogpost_obj['return_qty']) && $blogpost_obj['return_qty'] !== 0) ? $blogpost_obj['return_qty'] : 0;
                if($return_qty > 0){
                   $str .= '<tr>';
                $str .= '<input type="hidden"  value="' . $blogpost_obj['id'] . '" name="sku_id[]">';
                $str .= '<td>' . $startnum . '</td>';
                $startnum++;
                $str .= '<td><input type="text" class="form-control skuInput"  name="barcode[]" readonly  value="' . $blogpost_obj['barcode'] . '" ></td>';
                $str .= '<td><input type="text" class="form-control skuInput"  name="sku[]" readonly  value="' . $blogpost_obj['sku'] . '" ></td>';
                $str .= '<td><input type="text"  class="form-control category_name" name="category_name[]"  readonly  value="' . $blogpost_obj['category_name'] . '"> </td>';
                $str .= '<td><input type="text"  class="form-control" name="v_sku[]" readonly  value="' . $blogpost_obj['v_sku'] . '" >  </td>';
                $str .= '<td><input type="text"  class="form-control" name="return_date[]" readonly >  </td>';
                $str .= '<td class="cls-text-section "><input type="number" class="form-control qty " name="qty[]"  value="' . $blogpost_obj['return_qty'] . '"><input type="number" class="form-control price " name="price[]"  value="' . $blogpost_obj['price'] . '" step=0.01> </td>';
                $str .= '<td><input type="number"  class="form-control total" readonly name="taxable[]"  value="' . $blogpost_obj['taxable'] . '" step=0.01> </td>';
                $str .= '<td><input type="number"  class="form-control cgst"  name="cgst[]" readonly  value="' . $blogpost_obj['cgst'] . '" step=0.01> </td>';
                $str .= '<td><input type="number"  class="form-control sgst" name="sgst[]" readonly  value="' . $blogpost_obj['sgst'] . '" step=0.01>  </td>';
                $str .= '<td><input type="number"  class="form-control igst"  name="igst[]" readonly  value="' . $blogpost_obj['igst'] . '"  step=0.01>  </td>';
                $str .= '<td><input type="number"  class="form-control total_amount"  name="grand_total[]" readonly   value="' . $blogpost_obj['grand_total'] . '" >   </td>';
//                $str .= '<td> <button class="btnDeleteRow btn-zap" type="button" value="' . $blogpost_obj['id'] . '">× </button></td>';
                $str .= '</tr>'; 
                }
                 
            }
            $return_arary['str'] = $str;
             }
            $comeback = array("outcome" => "true", "data" => $return_arary);
                }
        return $comeback;
    }
     function multipleSelectRecord (){
     $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query = array(["", "id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_SALES_SKU_MASTER, '*', $where_query);
        $sales_id = (isset($comeback["data"][0]["sales_id"]) && !empty($comeback["data"][0]["sales_id"])) ? $comeback["data"][0]["sales_id"] : '';
        $return_array['barcode'] = (isset($comeback["data"][0]["barcode"]) && !empty($comeback["data"][0]["barcode"])) ? $comeback["data"][0]["barcode"] : '';
        $return_array['sku'] = (isset($comeback["data"][0]["sku"]) && !empty($comeback["data"][0]["sku"])) ? $comeback["data"][0]["sku"] : '';
        $return_array['name'] = (isset($comeback["data"][0]["name"]) && !empty($comeback["data"][0]["name"])) ? $comeback["data"][0]["name"] : '';
        $return_array['return_qty'] = (isset($comeback["data"][0]["return_qty"]) && !empty($comeback["data"][0]["return_qty"])) ? $comeback["data"][0]["return_qty"] : 0;
        $return_array['weight'] = (isset($comeback["data"][0]["weight"]) && !empty($comeback["data"][0]["weight"])) ? $comeback["data"][0]["weight"] : 0;
        $return_array['metal'] = (isset($comeback["data"][0]["metal"]) && !empty($comeback["data"][0]["metal"])) ? $comeback["data"][0]["metal"] : '';
        $return_array['qty'] = (isset($comeback["data"][0]["qty"]) && !empty($comeback["data"][0]["qty"])) ? $comeback["data"][0]["qty"] : 0;
        $return_array['mrp'] = (isset($comeback["data"][0]["mrp"]) && !empty($comeback["data"][0]["mrp"])) ? $comeback["data"][0]["mrp"] : 0;
        $return_array['discount'] = (isset($comeback["data"][0]["discount"]) && !empty($comeback["data"][0]["discount"])) ? $comeback["data"][0]["discount"] : 0;
        $return_array['taxation'] = (isset($comeback["data"][0]["taxation"]) && !empty($comeback["data"][0]["taxation"])) ? $comeback["data"][0]["taxation"] : 0;
        $return_array['cgst'] = (isset($comeback["data"][0]["cgst"]) && !empty($comeback["data"][0]["cgst"])) ? $comeback["data"][0]["cgst"] : 0;
        $return_array['sgst'] = (isset($comeback["data"][0]["sgst"]) && !empty($comeback["data"][0]["sgst"])) ? $comeback["data"][0]["sgst"] : 0;
        $return_array['igst'] = (isset($comeback["data"][0]["igst"]) && !empty($comeback["data"][0]["igst"])) ? $comeback["data"][0]["igst"] : 0;
        $return_array['grandtotal'] = (isset($comeback["data"][0]["grandtotal"]) && !empty($comeback["data"][0]["grandtotal"])) ? $comeback["data"][0]["grandtotal"] : 0;
        $return_array['id'] = $id;
        $return_array['remain_qty'] = $return_array['qty'] - $return_array['return_qty'];
        $where_query = array(["", "id", "=", "$sales_id"]);
        $comeback = $this->select_result(TABLE_SALES_ORDER, '*', $where_query);
        $return_array['sales_date'] = (isset($comeback["data"][0]["sales_date"]) && !empty($comeback["data"][0]["sales_date"])) ? $comeback["data"][0]["sales_date"] : '';
        $return_array['voucher_no'] = (isset($comeback["data"][0]["voucher_no"]) && !empty($comeback["data"][0]["voucher_no"])) ? $comeback["data"][0]["voucher_no"] : '';
        $response = array("outcome" => "true", "data" => $return_array);
        return $response;
    }

     function selectForPurchase (){
         $id = isset($_POST['id']) ? $_POST['id'] : '';
        $where_query = array(["", "id", "=", "$id"]);
        $comeback = $this->select_result(TABLE_SKU_MASTER, '*', $where_query);
        $return_array['sku']= (isset($comeback["data"][0]["sku"]) &&!empty($comeback["data"][0]["sku"])) ? $comeback["data"][0]["sku"] : '';
          $purchase_id= (isset($comeback["data"][0]["purchase_id"]) &&!empty($comeback["data"][0]["purchase_id"])) ? $comeback["data"][0]["purchase_id"] : '';
        $return_array['barcode']= (isset($comeback["data"][0]["barcode"]) &&!empty($comeback["data"][0]["barcode"])) ? $comeback["data"][0]["barcode"] : '';
        $return_array['v_sku']= (isset($comeback["data"][0]["v_sku"]) &&!empty($comeback["data"][0]["v_sku"])) ? $comeback["data"][0]["v_sku"] : '';
         $return_array['category_name'] = (isset($comeback["data"][0]["category_name"]) &&!empty($comeback["data"][0]["category_name"])) ? $comeback["data"][0]["category_name"] : '';
         $return_array['qty'] = (isset($comeback["data"][0]["qty"]) &&!empty($comeback["data"][0]["qty"])) ? $comeback["data"][0]["qty"] : '';
         $return_array['price'] = (isset($comeback["data"][0]["price"]) &&!empty($comeback["data"][0]["price"])) ? $comeback["data"][0]["price"] : 0;
         $return_array['return_qty'] = (isset($comeback["data"][0]["return_qty"]) &&!empty($comeback["data"][0]["return_qty"])) ? $comeback["data"][0]["return_qty"] : 0;
         $return_array['discount'] = (isset($comeback["data"][0]["discount"]) &&!empty($comeback["data"][0]["discount"])) ? $comeback["data"][0]["discount"] : 0;
         $return_array['taxable'] = (isset($comeback["data"][0]["taxable"]) &&!empty($comeback["data"][0]["taxable"])) ? $comeback["data"][0]["taxable"] : 0;
         $return_array['cgst'] = (isset($comeback["data"][0]["cgst"]) &&!empty($comeback["data"][0]["cgst"])) ? $comeback["data"][0]["cgst"] : 0;
         $return_array['sgst'] = (isset($comeback["data"][0]["sgst"]) &&!empty($comeback["data"][0]["sgst"])) ? $comeback["data"][0]["sgst"] : 0;
         $return_array['igst'] = (isset($comeback["data"][0]["igst"]) &&!empty($comeback["data"][0]["igst"])) ? $comeback["data"][0]["igst"] : 0;
         $return_array['grand_total'] = (isset($comeback["data"][0]["grand_total"]) && !empty($comeback["data"][0]["grand_total"])) ? $comeback["data"][0]["grand_total"] : 0;
         $return_array['id'] = $id;
         $return_array['remain_qty'] =  $return_array['qty'] - $return_array['return_qty'];
          $where_query = array(["", "id", "=", "$purchase_id"]);
        $comeback = $this->select_result(TABLE_PURCHASE_RETURN, '*', $where_query);
          $return_array['return_date']= (isset($comeback["data"][0]["return_date"]) &&!empty($comeback["data"][0]["return_date"])) ? $comeback["data"][0]["return_date"] : '';
        $response = array("outcome" => "true", "data" => $return_array);
        return $response;
    }
   function get_category_name(){
    $shopinfo = $this->current_store_obj;
        $store_user_id = $shopinfo[0]['store_user_id'];
        $store = $shopinfo[0]['shop_name'];
        $password = $shopinfo[0]['password'];
        $sku = isset($_POST['sku']) ? $_POST['sku'] : '';
        $end_point = "https://".$store."/admin/api/graphql.json";
        $shopify_data_list = cls_api_graphql_call(CLS_API_KEY, $password, $store, $end_point, $sku, 'POST');
        $decode_data = json_decode($shopify_data_list["response"]);
        $return_array['check_sku'] = (isset($decode_data->data->productVariants->edges) && !empty($decode_data->data->productVariants->edges)) ?$decode_data->data->productVariants->edges : '';
        $return_array['category_name'] = (isset($decode_data->data->productVariants->edges[0]->node->product->collections->edges[0]->node->handle) && !empty($decode_data->data->productVariants->edges[0]->node->product->collections->edges[0]->node->handle))?$decode_data->data->productVariants->edges[0]->node->product->collections->edges[0]->node->handle:'';
        $return_array['vendor'] = (isset($decode_data->data->productVariants->edges[0]->node->product->vendor) && !empty($decode_data->data->productVariants->edges[0]->node->product->vendor))?$decode_data->data->productVariants->edges[0]->node->product->vendor:'';
        $response = array("outcome" => "true", "data" => $return_array);
        return $response;
   }
   function get_sku_category_name(){
   
       $shopinfo = $this->current_store_obj;
        $store_user_id = $shopinfo[0]['store_user_id'];
        $store = $shopinfo[0]['shop_name'];
        $password = $shopinfo[0]['password'];
        $sku = isset($_POST['sku']) ? $_POST['sku'] : '';
        $end_point = "https://".$store."/admin/api/graphql.json";
        $shopify_data_list = cls_api_graphql_barcode_call(CLS_API_KEY, $password, $store, $end_point, $sku, 'POST');
        $decode_data = json_decode($shopify_data_list["response"]);
        $return_array['check_sku'] = (isset($decode_data->data->productVariants->edges) && !empty($decode_data->data->productVariants->edges)) ?$decode_data->data->productVariants->edges : '';
        $return_array['vendor'] = (isset($decode_data->data->productVariants->edges[0]->node->product->vendor) && !empty($decode_data->data->productVariants->edges[0]->node->product->vendor))?$decode_data->data->productVariants->edges[0]->node->product->vendor:'';
        $return_array['category_name'] = (isset($decode_data->data->productVariants->edges[0]->node->product->collections->edges[0]->node->handle) && !empty($decode_data->data->productVariants->edges[0]->node->product->collections->edges[0]->node->handle))?$decode_data->data->productVariants->edges[0]->node->product->collections->edges[0]->node->handle:'';
        $return_array['sku'] = (isset($decode_data->data->productVariants->edges[0]->node->sku) && !empty($decode_data->data->productVariants->edges[0]->node->sku))?$decode_data->data->productVariants->edges[0]->node->sku:'';
        $return_array['price'] = (isset($decode_data->data->productVariants->edges[0]->node->price) && !empty($decode_data->data->productVariants->edges[0]->node->price))?$decode_data->data->productVariants->edges[0]->node->price:'';
        $response = array("outcome" => "true", "data" => $return_array);
        return $response;
   }
     function make_table_data_purchase_skuData($table_data_arr, $pageno, $table_name) {
      $total_record = $table_data_arr->num_rows;
        $tr_html = '<tr> <td colspan="6"><center><p>Records not found</p></center></td></tr>';
        if ($table_data_arr->num_rows > 0) {
            $tr_html = '';
            foreach ($table_data_arr as $dataObj) {
                $dataObj = (object) $dataObj;
                
                $sku = (isset($dataObj->sku) && !empty($dataObj->sku)) ? $dataObj->sku : '-';
                $mrp = (isset($dataObj->price) && !empty($dataObj->price)) ? $dataObj->price : '-';
                $qty = (isset($dataObj->qty) && !empty($dataObj->qty)) ? $dataObj->qty : '-';
                $sgst = (isset($dataObj->sgst) && !empty($dataObj->sgst)) ? $dataObj->sgst : 0;
                $cgst = (isset($dataObj->cgst) && !empty($dataObj->cgst)) ? $dataObj->cgst : 0;
                $igst = (isset($dataObj->igst) && !empty($dataObj->igst)) ? $dataObj->igst : 0;
                $grand_total = (isset($dataObj->grand_total) && !empty($dataObj->grand_total)) ? $dataObj->grand_total : 0;
                $tr_html.='<tr>';
                $tr_html.='<td>'.$sku.'</td>';
                $tr_html.='<td>' . $mrp . '</td>';
                $tr_html.='<td>' . $qty . '</td>';
                $tr_html.='<td>' . $sgst . '</td>';
                $tr_html.='<td>' . $cgst . '</td>';
                $tr_html.='<td>' . $igst . '</td>';
                $tr_html.='<td>' . $grand_total . '</td>';
                $tr_html.='<td><button type="submit" name="" id="" class="btn  btn-raised bg-teal waves-effect">items </button></td>';
                $tr_html.='</tr>';
            }
        }
        return $tr_html;
    }
    function fill_modal_sku(){
        $shopinfo = $this->current_store_obj;
        $id = (isset($_POST['sku']) && !empty($_POST['sku']) ? $_POST['sku'] : '');
        $where_query_arr = array(["", "id", "=", $id]);
        $comeback = $this->select_result(TABLE_SKU_MASTER, '*', $where_query_arr);
        $table_data_arr = $comeback["data"];
        $tr_html = '';
        foreach ($table_data_arr as $dataObj) {
            $dataObj = (object) $dataObj;
            $sku = (isset($dataObj->sku) && !empty($dataObj->sku)) ? $dataObj->sku : '-';
            $mrp = (isset($dataObj->price) && !empty($dataObj->price)) ? $dataObj->price : '-';
            $barcode = (isset($dataObj->barcode) && !empty($dataObj->barcode)) ? $dataObj->barcode : '-';

            $tr_html.='<tr>';
            $tr_html.='<td><input class="CheckBox" type="checkbox"  name="chkbox" value="1"></td>';
            $tr_html.='<td>' . $sku . '</td>';
            $tr_html.='<td>' . $barcode . '</td>';
            $tr_html.='<td><a target="_blank" href="" title="">
                        <i class="fa fa-download" aria-hidden="true"></i>
                    </a></td>';
            $tr_html.='</tr>';
        }
        return $tr_html;
    }

    function sign_in() {
         $comeback = $this->select_result(TABLE_SUPPLIERS, '*');
         $email=isset($_POST['email']) ? $_POST['email'] : '';
	$password=isset($_POST['password']) ? $_POST['password'] : '';
        $where_query_arr = array(["", "supplier_email", "=", $email],["AND","supplier_password","=","$password"]);
        $comeback = $this->select_result(TABLE_SUPPLIERS, '*', $where_query_arr);
        if(empty($comeback['data'])){
             $result['dataResult'] = "fail";
            $result['errors'] = "Login Invalid";
        }else{
             $result['dataResult'] = "success";
            $result['errors'] = "Login Successfully";
        }
       $response = array("outcome" => "true", "data" => $result);
       return $response;
        
    }
    
    function update_status(){
        if(isset($_POST['table']) && !empty($_POST['table'])){
            $purchase_id = (isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : '');
            $fields = array(
                    '`status`' => 0,
                );
                $where_query = array(
                    ["", "id", "=", $purchase_id],
                );
               $comeback =  $this->put_data($_POST['table'], $fields, $where_query);
        }
         if(isset($_POST['tables']) && !empty($_POST['tables'])) {
            $purchase_id = (isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : ''); 
                   $fields_status = array(
                        '`status`' => 0,
                    );
                    $where_query_status = array(
                        ["", "purchase_id", "=", $purchase_id],
                    );
                $comeback =  $this->put_data($_POST['tables'], $fields_status, $where_query_status);   
                
        }       
         if(isset($_POST['return_tables']) && !empty($_POST['return_tables'])){
                 $purchase_id = (isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : ''); 
                   $fields_return_status = array(
                        '`status`' => 0,
                    );
                    $where_return_query_status = array(
                        ["", "purchase_id", "=", $purchase_id],
                    );
                    $comeback =  $this->put_data($_POST['return_tables'], $fields_return_status, $where_return_query_status); 
        }
                    return $comeback;    
    }
    function update_status_sales(){
         if(isset($_POST['sales']) && !empty($_POST['sales'])){
                $sales_id = (isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : ''); 
                   $fields_sales_status = array(
                        '`status`' => 0,
                    );
                    $where_sales_query_status = array(
                        ["", "id", "=", $sales_id],
                    );
                    $comeback =   $this->put_data($_POST['sales'], $fields_sales_status, $where_sales_query_status); 
        }  if(isset($_POST['sales_sku']) && !empty($_POST['sales_sku'])){
                $sales_id = (isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : ''); 
                   $fields_sales_status = array(
                        '`status`' => 0,
                    );
                    $where_sales_query_status = array(
                        ["", "sales_id", "=", $sales_id],
                    );
                    $comeback =  $this->put_data($_POST['sales_sku'], $fields_sales_status, $where_sales_query_status); 
        }
          if(isset($_POST['sales_return']) && !empty($_POST['sales_return'])){
                $sales_id = (isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : ''); 
                   $fields_sales_status = array(
                        '`status`' => 0,
                    );
                    $where_sales_query_status = array(
                        ["", "sales_id", "=", $sales_id],
                    );
                    $comeback =  $this->put_data($_POST['sales_return'], $fields_sales_status, $where_sales_query_status); 
        }
         return $comeback;    
        
    }
    function get_state(){
        $shopinfo = $this->current_store_obj;
        $countries_id = (isset($_POST['countries_id']) && !empty($_POST['countries_id']) ? $_POST['countries_id'] : '');
        $where_query_arr = array(["", "country_id", "=", $countries_id]);
        $comeback = $this->select_result(TABLE_STATE, '*', $where_query_arr);
       $table_data_arr= (isset($comeback["data"]) &&!empty($comeback["data"])) ? $comeback["data"]: '';
         $tr_html = '';
       $tr_html.='<option>Select State</option>';
       foreach ($table_data_arr as $dataObj) {
        
           $tr_html.='<option data-id="'.$dataObj["id"].'" value="'.$dataObj["name"].'">' . $dataObj['name'] . '</option>';
       }
        $response = array("outcome" => "true", "data" => $tr_html);
     
        return $response;
    }
    
     function fill_sales_modal_sku(){
        $id = (isset($_POST['sku']) && !empty($_POST['sku']) ? $_POST['sku'] : '');
        $where_query_arr = array(["", "id", "=", $id]);
        $comeback = $this->select_result(TABLE_SALES_SKU_MASTER, '*', $where_query_arr);
        $table_data_arr = $comeback["data"];
        $tr_html = '';
        foreach ($table_data_arr as $dataObj) {
            $dataObj = (object) $dataObj;
            $sku = (isset($dataObj->sku) && !empty($dataObj->sku)) ? $dataObj->sku : '-';
            $mrp = (isset($dataObj->price) && !empty($dataObj->price)) ? $dataObj->price : '-';
            $barcode = (isset($dataObj->barcode) && !empty($dataObj->barcode)) ? $dataObj->barcode : '-';

            $tr_html.='<tr>';
            $tr_html.='<td><input class="CheckBox" type="checkbox"  name="chkbox" value="1"></td>';
            $tr_html.='<td>' . $sku . '</td>';
            $tr_html.='<td>' . $barcode . '</td>';
            $tr_html.='<td><a target="_blank" href="" title="">
                        <i class="fa fa-download" aria-hidden="true"></i>
                    </a></td>';
            $tr_html.='</tr>';
        }
        return $tr_html;
    }
}

