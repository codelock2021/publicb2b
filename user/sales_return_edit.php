<!doctype html>
<html class="no-js " lang="en">
  <?php
    $id = isset($_GET['pid']) ? $_GET['pid'] : '';
    $sid = isset($_GET['sid']) ? $_GET['sid'] : '';
    $sku_id = isset($_GET['sku_id']) ? $_GET['sku_id'] : '';
    $sales_id = isset($_GET['sales_id']) ? $_GET['sales_id'] : '';
    include_once 'cls_header.php';
    $obj_Client_functions = new Client_functions($_SESSION['store']);
    ?>
    <script>
        var id = "<?php echo $id; ?>";
        var sku_id = "<?php echo $sku_id; ?>";
        var sales_id = "<?php echo $sales_id; ?>";
    </script>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>
    <!-- Right Sidebar -->
    <!-- Top Bar -->
    <?php  include 'topbar.php';
            include 'sidebar.php';
            include 'ri8sidebar.php';                                          
    ?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-11 col-md-11 col-sm-12">
               <h1>Create Sales Return</h1>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12">
               <a href="sales-return-listing.php?store=<?php echo $_SESSION['store']; ?>" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
                <!-- Input -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <form class="m-t" id="register_frm" name="register_frm" method="POST"  enctype="multipart/form-data" onsubmit="">
                                <input type="hidden" id="" name="id" value="<?php echo $id; ?>">
                                <input type="hidden" id="" name="for_data" value="<?php echo 'sales_return'; ?>">
                               <form name="frm" id="purchase_return_create">
                                <div class="body">
                                    <div class="row clearfix">
                                         <div class="col-lg-12 col-md-12">
                                            <label>POType :</label>
                                            <input name="po_type" type="radio" class="with-gap" id="radio_3" value="0" checked="true"/>
                                            <label for="radio_3">Direct</label>
                                            <input name="po_type" type="radio" class="with-gap" id="radio_2" value="1"/>
                                            <label for="radio_2">On Approval</label>
                                            <span class="error po_type"></span>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <?php
                                                    $sid = isset($_GET['sid']) ? $_GET['sid'] : '';
                                                     $where_query = array(["", "id", "=", "$sid"]);
                                                    $comeback = $obj_Client_functions->select_result(TABLE_DEALER, '*',$where_query);
                                                    ?>
                                                    <div class="form-group">
                                                        <select class="form-control show-tick" name="dealer_id" id="dealer_id">
                                                        <?php
                                                        foreach ($comeback["data"] as $data) {
                                                            $data = (object) $data;
                                                           $options .='<option value="' . $data->id . '">' . $data->firstname . '('. $data->generate_code.')</option>';
                                                        }
                                                        echo $options;
                                                        ?>
                                                        </select>
														
                                                    </div>
                                                    <span class="error dealer_id"></span>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <button type="button" class="btn  btn-raised bg-teal waves-effect" tableId="purchase_return_create" id="searchbtn" style="display:none">Search </button>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <label>Return Date</label>
                                                            <input type="date" class="form-control" placeholder="Return Date" name="return_date"  id="return_date"/>
                                                        </div>
                                                        <span class="error return_date"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" placeholder="Ref Num" name="ref_num" id="ref_num"/>
                                                        </div>
                                                        <span class="error ref_num"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control qty_set" placeholder="Qty" name="qty_ret" id="qty_ret"/>
                                                        </div>
                                                        <span class="error qty_ret"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control price_set" placeholder="Grand Total" name="price_ret" id="price_ret" />
                                                        </div>
                                                        <span class="error price_ret"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <textarea rows="4" class="form-control no-resize" placeholder="Reason" name="reason" id="reason"></textarea>
                                                            </div>
                                                            <span class="error reason"></span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable"  data-search-listing="true"  tableId="0"  id="search_sales_editData" data-search="title"   data-from="table" data-apiName="products" >
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>SKU</th>
                                                            <th>Barcode</th>
                                                            <th>Qty</th>
                                                            <th>Price</th>
                                                            <th>Voucher No.</th>
                                                            <th>GRN Date  </th>
                                                        </tr>
                                                    </thead>

                                                    <tbody id="allsearchData">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                     <div class="row clearfix purchase-return-skutable" style="display: none;">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="card">
                                                <div class="body">
                                                         <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                        <thead>
                                                           <tr>
                                                                <th>SNo.</th>
                                                                <th>Barcode </th>
                                                                <th>SKU</th>
                                                                <th>Name</th>
                                                                <th>V.SKU</th>
                                                                <th>Metal</th>
                                                                <th>Invoice/Challan Date</th>
                                                                <th>Invoice/Challan No</th>
                                                                <th>G. Weight</th>
                                                                <th>Qty/MRP</th>
                                                                <!--<th>Dis </th>-->
                                                                <th>Taxable</th>
                                                                <th>CGST</th>
                                                                <th>SGST</th>
                                                                <th>IGST</th>
                                                                <th>Sales Price  </th>
                                                                <th>#</th>
                                                            </tr>
                                                            
                                                        </thead><tbody class="dataresult">

                                                        </tbody>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>Total</th>
                                                            <th><input type="text"  class="form-control total_qty" name="total_qty" readonly></th>
                                                            <input type="hidden"  class="form-control total_discount" name="total_discount" readonly>
                                                            <th><input type="text"  class="form-control total_taxable" name="total_taxable" readonly></th>
                                                            <th><input type="number"  class="form-control total_cgst" name="total_cgst" readonly></th>
                                                            <th><input type="text"  class="form-control total_sgst" name="total_sgst" readonly></th>
                                                            <th><input type="number"  class="form-control total_igst" name="total_igst" readonly></th>
                                                            <th><input type="number"  class="form-control grand_amount" name="grand_amount" readonly></th>
                                                            <th></th>   
                                                        </tr>
                                                         </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <button type="submit" name="submit" class="btn  btn-raised bg-teal waves-effect">Update </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
</section>
</div>
</div>
  
</body>
</html>

<script>
    var routine_name = 'sales_return_select';
    get_sales_return_value(routine_name, store, id,sales_id, "sales_return");
    $('table[data-search-listing="true"]').each(function () {
        var tableId = $(this).attr('id');
         SearchEditData(tableId,sales_id);
         
    });
</script>
