<!doctype html>
<html class="no-js " lang="en">
  <?php
    $id = isset($_GET['id']) ? $_GET['id'] : '';
    include_once 'cls_header.php';
    $obj_Client_functions = new Client_functions($_SESSION['store']);
    ?>
    <script>
        var id = "<?php echo $id; ?>";
    </script>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>
    <!-- Right Sidebar -->
    <!-- Top Bar -->
    <?php  include 'topbar.php';
            include 'sidebar.php';
            include 'ri8sidebar.php';                                          
    ?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-11 col-md-11 col-sm-12">
               <h1>Dealer Orders</h1>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12">
               <a href="dealer-order.php?store=<?php echo $_SESSION['store'];?>" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                          <form class="m-t" id="register_frm" name="register_frm" method="POST"  enctype="multipart/form-data" onsubmit="">
                                <input type="hidden" id="" name="id" value="<?php echo $id; ?>">
                                <input type="hidden" id="" name="for_data" value="<?php echo 'dealers_orders_update'; ?>">
                        <div class="body">
                          
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Status</label>
                                       <select class="form-control show-tick" name="dealer_id" id="status">
                                              <?php
                                                $where_query_arr = array(["", "id", "=", "$id"]);
                                                $comeback = $obj_Client_functions->select_result(TABLE_DEALER, '*',$where_query_arr);
                                                $comeback = (object)$comeback["data"][0];
                                               
                                                $comeback = (isset($comeback->dealer_status) && $comeback->dealer_status != '') ? $comeback->dealer_status:'';  
                                              ?>
                                            <option value="0"  <?php if($comeback=="0") {echo "selected";}?>> Pending</option>
                                            <option value="1" <?php if($comeback=="1") {echo "selected";}?>>Under process</option>
                                            <option value="2"  <?php if($comeback=="2") {echo "selected";}?>>cancelled</option>
                                            <option value="3"  <?php if($comeback=="3") {echo "selected";}?>>Dispatched</option>
                                        </select>
                                        <span class="error dealer_id"></span>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" name="submit"  class="btn  btn-raised bg-teal waves-effect">Update </button>
                        </div>
                      <form>
                </div>
            </div>
        </div>
        <!-- #END# Input --> 
     
    </div>
</section>
</div>
</div>
  
</body>
</html>

<script>
    var routine_name = 'dealers_orders_update_select';
    get_dealers_orders_update_value(routine_name, store, "dealers_orders_update");
</script>
