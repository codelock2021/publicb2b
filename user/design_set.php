<html class="no-js " lang="en">
    <?php
    include_once 'cls_header.php';
    $obj_Client_functions = new Client_functions($_SESSION['store']);
    ?>
    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore Nexa...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>
        <!-- Right Sidebar -->
        <!-- Top Bar -->
        <?php
        include 'topbar.php';
        include 'sidebar.php';
        include 'ri8sidebar.php';
        ?>

        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-11 col-md-11 col-sm-12">
                        <h1>Create Goods Receive Note
                        </h1>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-12">
                        <a href="puchase-return.php" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <!-- Input -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <form name="frm" id="purchase_return_create">
                                <div class="body">
                                    <div class="row clearfix">
                                       <div class="col-sm-12">
                                            <label>So Type :</label>
                                            <input name="so_type" type="radio" class="with-gap" id="radio_4" value="0">
                                            <label for="radio_4">Direct</label>
                                            <input name="so_type" type="radio" class="with-gap" id="radio_1" value="1">
                                            <label for="radio_1">On Approval</label>
                                            <span class="error so_type"></span>
                                </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="row">
                                                    <?php
                                                    $comeback = $obj_Client_functions->select_result(TABLE_DEALER, '*');
                                                    ?>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <select class="form-control show-tick" name="dealer_id">
                                                                <?php
                                                                foreach ($comeback["data"] as $data) {
                                                                    $data = (object) $data;
                                                                    $options .='<option value="' . $data->id . '" >' . $data->firstname . '</option>';
                                                                }
                                                                echo $options;
                                                                ?>
                                                            </select>
                                                            <span class="error dealer_id"></span>
                                                        </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <button type="button" class="btn  btn-raised bg-teal waves-effect" tableId="purchase_return_create" id="searchbtn">Search </button>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="date" class="form-control" placeholder="Return Date" name="return_date" id="return_date"/>
                                        </div>
                                        <span class="error return_date"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Ref No."  name="ref_num"/>
                                        </div>
                                        <span class="error ref_num"></span>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Qty" name="qty_ret"/>
                                        </div>
                                        <span class="error qty_ret"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Grand Total" name="price_ret"/>
                                        </div>
                                        <span class="error price_ret"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea rows="4" class="form-control no-resize" placeholder="Reason" name="reason"></textarea>
                                        </div>
                                        <span class="error reason"></span>
                                    </div>
                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable"   tableId="purchase_return_create"  id="search_sales_Data" data-search="title"  data-listing="true" data-from="table" data-apiName="products" >
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>SKU</th>
                                                            <th>Qty</th>
                                                            <th>Price</th>
                                                            <th>Return Date</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody id="allsearchData">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <button type="submit" name="submit" class="btn  btn-raised bg-teal waves-effect">Create </button>
                                            </div>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Input --> 

    </div>
</section>
</body>
</html>
