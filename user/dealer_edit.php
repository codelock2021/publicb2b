<!doctype html>
<html class="no-js " lang="en">
   <?php
    $id = isset($_GET['id']) ? $_GET['id'] : '';
    include_once 'cls_header.php';
    $obj_Client_functions = new Client_functions($_SESSION['store']);
    ?>
    <script>
        var id = "<?php echo $id; ?>";
    </script>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>
    <!-- Right Sidebar -->
    <!-- Top Bar -->
    <?php  include 'topbar.php';
include 'sidebar.php';
include 'ri8sidebar.php';
    ?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-11 col-md-11 col-sm-12">
                <h1>Create Dealer/Retailer</h1>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12">
               <a href="dealers-listing.php?store=<?php echo $_SESSION['store'];?>" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                   <form class="m-t" id="register_frm" name="register_frm" method="POST"  enctype="multipart/form-data" onsubmit="">
                                <input type="hidden" id="" name="id" value="<?php echo $id; ?>">
                                <input type="hidden" id="" name="for_data" value="<?php echo 'dealer'; ?>">
                        <div class="body">
                          
                            <div class="row clearfix">
                                    <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Firstname" name="firstname" id="firstname"/>
                                        </div>
                                        <span class="error firstname"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Lastname" name="lastname" id="lastname"/>
                                        </div>
                                        <span class="error lastname"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Contact Person" name="contact_person" id="contact_person" />
                                        </div>
                                        <span class="error contact_person"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Email" name="email" id="email"/>
                                        </div>
                                        <span class="error email"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" class="form-control" placeholder="Username" name="username" id="username"/>
                                        </div>
                                        <span class="error username"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" class="form-control" placeholder="Password" name="password" id="password"/>
                                        </div>
                                        <span class="error password"></span>
                                    </div>
                                </div>
                            <div class="col-sm-12">
                                <input type="checkbox" id="basic_checkbox_2" class="filled-in" name="same_address" value="1" id="same_address">
                                <label for="basic_checkbox_2"> Same Address</label>
                            </div>
                        </br>
                            <div class="col-sm-6">
                                    <div class="form-group">
                                      <label><b><h5>SHIPPING ADDRESS</h5></b></label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><b><h5>BILLING ADDRESS</h5></b></label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Address" name="address" id="address"/>
                                        </div>
                                        <span class="error address"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Address" name="billing_address" id="billing_address"/>
                                        </div>
                                        <span class="error billing_address"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Landmark" name="landmark" id="landmark"/>
                                        </div>
                                        <span class="error landmark"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Landmark" name="billing_landmark" id="billing_landmark"/>
                                        </div>
                                        <span class="error billing_landmark"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="City" name="city" id="city"/>
                                        </div>
                                        <span class="error city"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="City" name="billing_city"  id="billing_city"/>
                                        </div>
                                        <span class="error billing_city"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="State" name="state" id="state" />
                                        </div>
                                        <span class="error state"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="State" name="billing_state" id="billing_state" />
                                        </div>
                                        <span class="error billing_state"></span>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Country" name="country" id="country"/>
                                        </div>
                                        <span class="error country"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Country" name="billing_country" id="billing_country"  />
                                        </div>
                                        <span class="error billing_country"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Phone" name="phone" id="phone" />
                                        </div>
                                        <span class="error phone"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Phone" name="billing_phone" id="billing_phone" />
                                        </div>
                                        <span class="error billing_phone"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Mobile" name="mobile" id="mobile" />
                                        </div>
                                        <span class="error mobile"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Mobile" name="billing_mobile" id="billing_mobile"/>
                                        </div>
                                        <span class="error billing_mobile"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Postal Code" name="postal_code" id="postal_code" />
                                        </div>
                                        <span class="error postal_code"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Postal Code" name="billing_postal_code" id="billing_postal_code" />
                                        </div>
                                        <span class="error billing_postal_code"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="GST"  name="gst" id="gst"/>
                                        </div>
                                        <span class="error gst"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="GST" name="billing_gst" id="billing_gst"/>
                                        </div>
                                        <span class="error billing_gst"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="PAN" name="pan" id="pan" />
                                        </div>
                                        <span class="error pan"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="TIN" name="tin" id="tin"/>
                                        </div>
                                        <span class="error tin"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Vat" name="vat" id="vat"/>
                                        </div>
                                        <span class="error vat"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Bank Name" name="bank_name" id="bank_name"/>
                                        </div>
                                        <span class="error bank_name"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Bank Account No" name="bank_account_no"  id="bank_account_no"/>
                                        </div>
                                        <span class="error bank_account_no"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Bank Branch" name="bank_branch" id="bank_branch" />
                                        </div>
                                        <span class="error bank_branch"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Ifsc Code" name="ifsc_code" id="ifsc_code"/>
                                        </div>
                                        <span class="error ifsc_code"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="approved" id="approved">
                                              <?php
                                                $where_query_arr = array(["", "id", "=", "$id"]);
                                                $comeback = $obj_Client_functions->select_result(TABLE_DEALER, '*',$where_query_arr);
                                                $comeback = (object)$comeback["data"][0];
                                                $comeback = (isset($comeback->approved) && $comeback->approved != '') ? $comeback->approved:'';  
                                              ?>
                                            <option value="" >Approved</option>
                                            <option value="0" <?php if($comeback=="0") {echo "selected";}?>>No</option>
                                            <option value="1" <?php if($comeback=="1") {echo "selected";}?>>Yes</option>
                                        </select>
                                    </div>
                                    <span class="error approved"></span>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Discount" name="discount" id="discount"/>
                                        </div>
                                        <span class="error discount"></span>
                                    </div>
                                </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="active" id="active">
                                            <option value="">Status</option>
                                              <?php
                                                $where_query_arr = array(["", "id", "=", "$id"]);
                                                $comeback = $obj_Client_functions->select_result(TABLE_DEALER, '*',$where_query_arr);
                                                $comeback = (object)$comeback["data"][0];
                                                $comeback = (isset($comeback->active) && $comeback->active != '') ? $comeback->active:'';  
                                              ?>
                                               <option value="1" <?php if($comeback=="1") {echo "selected";}?>>Active</option>
                                              <option value="0" <?php if($comeback=="0") {echo "selected";}?>>InActive</option>
                                        </select>
                                    </div>
                                      <span class="error status"></span>
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn  btn-raised bg-teal waves-effect">Create </button>
                        </div>
                      <form>
                </div>
            </div>
        </div>
        <!-- #END# Input --> 
     
    </div>
</section>
</div>
</div>
</body>
</html>
<script>
    var routine_name = 'dealer_select';
    get_dealer_value(routine_name, id, "dealer");
</script>
