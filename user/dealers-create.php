<!doctype html>
<html class="no-js " lang="en">
<?php
include_once 'cls_header.php';
?>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>
    <!-- Right Sidebar -->
    <!-- Top Bar -->
    <?php  include 'topbar.php';
include 'sidebar.php';
include 'ri8sidebar.php';
    ?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-11 col-md-11 col-sm-12">
                <h1>Create Dealer/Retailer

                </h1>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12">
               <a href="dealers-listing.php?store=<?php echo $_SESSION['store'];?>" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <form name="frm" id="dealer" method="POST">
                        <div class="body">
                          
                            <div class="row clearfix">
                                    <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Firstname" name="firstname" />
                                        </div>
                                        <span class="error firstname"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Lastname" name="lastname"/>
                                        </div>
                                        <span class="error lastname"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Contact Person" name="contact_person" />
                                        </div>
                                        <span class="error contact_person"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Email" name="email"/>
                                        </div>
                                        <span class="error email"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Username" name="username"/>
                                        </div>
                                        <span class="error username"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" class="form-control" placeholder="Password" name="password"/>
                                        </div>
                                        <span class="error password"></span>
                                    </div>
                                </div>
                            <div class="col-sm-12">
                                <input type="checkbox" id="same_address" class="filled-in" name="same_address" value="1">
                                <label for="same_address"> Same Address</label>
                            </div>
                        </br>
                            <div class="col-sm-6">
                                    <div class="form-group">
                                      <label><b><h5>SHIPPING ADDRESS</h5></b></label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><b><h5>BILLING ADDRESS</h5></b></label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Address" name="address" id="address"/>
                                        </div>
                                        <span class="error address"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Address" name="billing_address" id="billing_address"/>
                                        </div>
                                        <span class="error billing_address"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Landmark" name="landmark" id="landmark"/>
                                        </div>
                                        <span class="error landmark"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Landmark" name="billing_landmark" id="billing_landmark"/>
                                        </div>
                                        <span class="error billing_landmark"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="City" name="city" id="city"/>
                                        </div>
                                        <span class="error city"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="City" name="billing_city" id="billing_city" />
                                        </div>
                                        <span class="error billing_city"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="State" name="state" id="state" />
                                        </div>
                                        <span class="error state"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="State" name="billing_state" id="billing_state" />
                                        </div>
                                        <span class="error billing_state"></span>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Country" name="country" id="country"/>
                                        </div>
                                        <span class="error country"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Country" name="billing_country" id="billing_country"  />
                                        </div>
                                        <span class="error billing_country"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" class="form-control" placeholder="Phone" name="phone" id="phone" />
                                        </div>
                                        <span class="error phone"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Phone" name="billing_phone"  id="billing_phone"/>
                                        </div>
                                        <span class="error billing_phone"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" class="form-control" placeholder="Mobile" name="mobile" id="mobile"/>
                                        </div>
                                        <span class="error mobile"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Mobile" name="billing_mobile" id="billing_mobile"/>
                                        </div>
                                        <span class="error billing_mobile"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Postal Code" name="postal_code" id="postal_code"/>
                                        </div>
                                        <span class="error postal_code"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Postal Code" name="billing_postal_code" id="billing_postal_code" />
                                        </div>
                                        <span class="error billing_postal_code"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="GST"  name="gst" id="gst"/>
                                        </div>
                                        <span class="error gst"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="GST" name="billing_gst" id="billing_gst"/>
                                        </div>
                                        <span class="error billing_gst"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="PAN" name="pan" />
                                        </div>
                                        <span class="error pan"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="TIN" name="tin"/>
                                        </div>
                                        <span class="error tin"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Vat" name="vat"/>
                                        </div>
                                        <span class="error vat"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Bank Name" name="bank_name" />
                                        </div>
                                        <span class="error bank_name"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Bank Account No" name="bank_account_no" />
                                        </div>
                                        <span class="error bank_account_no"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Bank Branch" name="bank_branch" />
                                        </div>
                                        <span class="error bank_branch"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Ifsc Code" name="ifsc_code"/>
                                        </div>
                                        <span class="error ifsc_code"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="approved">
                                            <option value="">Approved</option>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                    <span class="error approved"></span>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Discount" name="discount"/>
                                        </div>
                                        <span class="error discount"></span>
                                    </div>
                                </div>
                                  <div class="col-sm-4">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="active">
                                            <option value="">Active</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                      <span class="error status"></span>
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn  btn-raised bg-teal waves-effect">Create </button>
                        </div>
                      <form>
                </div>
            </div>
        </div>
        <!-- #END# Input --> 
     
    </div>
</section>
</div>
</div>
  
</body>


</html>