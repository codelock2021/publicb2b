  <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
            </div>
            <div class="info-container">
                <div class="btn-group user-helper-dropdown">
                  
                </div>

            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active open"><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-home"></i><span> B2B Management </span></a>
                    <ul class="ml-menu">
                        <li><a href="supplier-listing.php?store=<?php echo $_SESSION['store'];?>">Suppliers</a></li>
                        <li><a href="purchase-listing.php?store=<?php echo $_SESSION['store'];?>">Purchase</a></li>
                        <li><a href="puchase-return.php?store=<?php echo $_SESSION['store'];?>">Purchase Return</a></li>
                        <li><a href="dealers-listing.php?store=<?php echo $_SESSION['store'];?>">Dealers/Retailers</a></li>
                        <li><a href="sales-listing.php?store=<?php echo $_SESSION['store'];?>">Sales </a></li>
                        <li><a href="sales-return-listing.php?store=<?php echo $_SESSION['store'];?>">Sales Return </a></li>
                        <li><a href="dealer-order.php?store=<?php echo $_SESSION['store'];?>">Dealer/Retailer Orders  </a></li>
                        <!-- <li><a href="barcode-listing.php">Barcode Tracking </a></li> -->
                    </ul>
                </li>
              <!--   <li><a href=""><i class="zmdi zmdi-delicious"></i><span>Widgets</span> </a></li>
                <li><a href=""><i class="zmdi zmdi-email"></i><span>Inbox</span> </a></li>
                <li><a href=""><i class="zmdi zmdi-blogger"></i><span>Blogger</span> </a></li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-shopping-cart"></i><span>Ecommerce</span> </a>
                    <ul class="ml-menu">
                        <li><a href="ec-dashboard.html">Dashboard</a></li>
                        <li><a href="ec-product.html">Product</a></li>
                        <li><a href="ec-product-List.html">Product List</a></li>
                        <li><a href="ec-product-detail.html">Product detail</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-swap-alt"></i><span>User Interface (UI)</span> </a>
                    <ul class="ml-menu">
                        <li><a href="typography.html">Typography</a></li>
                        <li><a href="helper-classes.html">Helper Classes</a></li>
                        <li><a href="alerts.html">Alerts</a></li>
                        <li><a href="animations.html">Animations</a></li>
                        <li><a href="badges.html">Badges</a></li>
                        <li><a href="breadcrumbs.html">Breadcrumbs</a></li>
                        <li><a href="buttons.html">Buttons</a></li>
                        <li><a href="collapse.html">Collapse</a></li>
                        <li><a href="colors.html">Colors</a></li>
                        <li><a href="dialogs.html">Dialogs</a></li>
                        <li><a href="icons.html">Icons</a></li>
                        <li><a href="labels.html">Labels</a></li>
                        <li><a href="list-group.html">List Group</a></li>
                        <li><a href="media-object.html">Media Object</a></li>
                        <li><a href="modals.html">Modals</a></li>
                        <li><a href="notifications.html">Notifications</a></li>
                        <li><a href="pagination.html">Pagination</a></li>
                        <li><a href="preloaders.html">Preloaders</a></li>
                        <li><a href="progressbars.html">Progress Bars</a></li>
                        <li><a href="range-sliders.html">Range Sliders</a></li>
                        <li><a href="sortable-nestable.html">Sortable & Nestable</a></li>
                        <li><a href="tabs.html">Tabs</a></li>
                        <li><a href="waves.html">Waves</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-assignment"></i><span>Forms</span> </a>
                    <ul class="ml-menu">
                        <li><a href="basic-form-elements.html">Basic Form Elements</a></li>
                        <li><a href="advanced-form-elements.html">Advanced Form Elements</a></li>
                        <li><a href="form-examples.html">Form Examples</a></li>
                        <li><a href="form-validation.html">Form Validation</a></li>
                        <li><a href="form-wizard.html">Form Wizard</a></li>
                        <li><a href="form-editors.html">Editors</a></li>
                        <li><a href="form-summernote.html">Summernote</a></li>
                        <li><a href="form-upload.html">File Upload</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-grid"></i><span>Tables</span> </a>
                    <ul class="ml-menu">
                        <li><a href="normal-tables.html">Normal Tables</a></li>
                        <li><a href="jquery-datatable.html">Jquery Datatables</a></li>
                        <li><a href="editable-table.html">Editable Tables</a></li>
                        <li><a href="footable.html">Foo Tables</a></li>
                        <li><a href="table-color.html">Tables Color</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-image"></i><span>Medias</span> </a>
                    <ul class="ml-menu">
                        <li><a href="image-gallery.html">Image Gallery</a></li>
                        <li><a href="carousel.html">Carousel</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-chart"></i><span>Charts</span> </a>
                    <ul class="ml-menu">
                        <li><a href="echart.html">E Chart</a></li>
                        <li><a href="morris.html">Morris</a></li>
                        <li><a href="flot.html">Flot</a></li>
                        <li><a href="chartjs.html">ChartJS</a></li>
                        <li><a href="sparkline.html">Sparkline</a></li>
                        <li><a href="jquery-knob.html">Jquery Knob</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-lock"></i><span>Authentication</span> </a>
                    <ul class="ml-menu">
                        <li><a href="sign-in.html">Sign In</a></li>
                        <li><a href="sign-up.html">Sign Up</a></li>
                        <li><a href="forgot-password.html">Forgot Password</a></li>
                        <li><a href="404.html">Page 404</a></li>
                        <li><a href="500.html">Page 500</a></li>
                        <li><a href="page-offline.html">Page Offline</a></li>
                        <li><a href="locked.html">Locked Screen</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-copy"></i><span>Extra Pages</span> </a>
                    <ul class="ml-menu">
                        <li><a href="blank.html">Blank Page</a></li>
                        <li><a href="events.html">Calendar</a></li>
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="timeline.html">Timeline</a></li>
                        <li><a href="chat.html">Chat</a></li>
                        <li><a href="contact.html">Contact list</a></li>
                        <li><a href="pricing.html">Pricing</a></li>
                        <li><a href="invoices.html">Invoices</a></li>
                        <li><a href="search-results.html">Search Results</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-map"></i><span>Maps</span> </a>
                    <ul class="ml-menu">
                        <li><a href="google.html">Google Map</a></li>
                        <li><a href="yandex.html">YandexMap</a></li>
                        <li><a href="jvectormap.html">jVectorMap</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-trending-down"></i><span>Multi Level Menu</span> </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="javascript:void(0);"> <span>Menu Item</span> </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);"> <span>Menu Item - 2</span> </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle"> <span>Level - 2</span> </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="javascript:void(0);"> <span>Menu Item</span> </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="menu-toggle"> <span>Level - 3</span> </a>
                                    <ul class="ml-menu">
                                        <li>
                                            <a href="javascript:void(0);"> <span>Level - 4</span> </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="changelogs.html"><i class="zmdi zmdi-time-restore"></i><span>Changelogs</span> </a></li>
                <li class="header">LABELS</li>
                <li><a href="javascript:void(0);"><i class="zmdi zmdi-chart-donut col-red"></i><span>Important</span> </a></li>
                <li><a href="javascript:void(0);"><i class="zmdi zmdi-chart-donut col-amber"></i><span>Warning</span> </a></li>
                <li><a href="javascript:void(0);"><i class="zmdi zmdi-chart-donut col-blue"></i><span>Information</span> </a></li> -->
            </ul>
        </div>
        <!-- #Menu -->
    </aside>