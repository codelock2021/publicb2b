<?php
include_once '../append/connection.php';
include_once ABS_PATH . '/user/cls_functions.php';
include_once ABS_PATH . '/cls_shopifyapps/config.php';

$default_shop = 'managedashboard.myshopify.com';
if ((isset($_GET['store']) && $_GET['store'] != '') || isset($default_shop)) {
   $store = isset($_GET['store']) ? $_GET['store'] : $default_shop;
    $functions = new Client_functions($store);
    $current_user = $functions->get_store_detail_obj();
} else {
    header('Location: https://www.shopify.com/admin/apps');
    exit;
}
$view = (isset($_GET["view"]) && $_GET["view"]) ? $_GET["view"] : FALSE;
?>  
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo CLS_SITE_NAME; ?></title>
        <link rel="stylesheet" href="<?php // echo main_url('assets/css/shopify_clients.css');  ?>" />
        <link rel="stylesheet" href="<?php // echo main_url('assets/css/shopify_client.css');  ?>" />
        <link rel="stylesheet" href="<?php // echo main_url('assets/css/polaris.min.css');  ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/spectrum.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/owl.carousel.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo main_url('assets/css/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/bootstrap-tagsinput.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/select2.min.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo main_url('assets/plugins/bootstrap-select/css/bootstrap-select.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/editor.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/plugins/morrisjs/morris.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/main.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/color_skins.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/authentication.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo main_url('assets/css/custom-style.css'); ?>" rel="stylesheet">
        <style>
           @import url('https://fonts.googleapis.com/css2?family=Oi&display=swap&family=Goblin+One&display=swap&family=Dancing+Script&display=swap&family=Dancing+Script&family=Goblin+One&family=Pacifico&display=swap&family=Caveat&display=swap&family=Martel:wght@200&display=swap&family=Satisfy&display=swap&family=Courgette&display=swap&family=Secular+One&display=swap');  
        </style>
              <script> var store = "<?php echo $store; ?>"; </script>
             
            <?php  $_SESSION['store'] = $store; ?>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script src="<?php // echo main_url('assets/js/jquery-2.1.1.js');  ?>"></script>
        <script src="<?php echo main_url('assets/bundles/bootstrap-select.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/jquery-ui.min.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/spectrum.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/owl.carousel.min.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/popper.min.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/star_rating.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/ckeditor/ckeditor.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/bootstrap-tagsinput.min.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/select2.full.min.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/pages/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/shopify_client.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/shopify_custom.js'); ?>"></script>
        <script src="<?php echo main_url('assets/bundles/vendorscripts.bundle.js'); ?>"></script>
        <script src="<?php echo main_url('assets/bundles/jvectormap.bundle.js'); ?>"></script>
        <script src="<?php echo main_url('assets/bundles/sparkline.bundle.js'); ?>"></script>
        <script src="<?php echo main_url('assets/bundles/knob.bundle.js'); ?>"></script>
        <script src="<?php echo main_url('assets/bundles/mainscripts.bundle.js'); ?>"></script>
        <script src="<?php echo main_url('assets/js/pages/charts/jquery-knob.min.js'); ?>"></script>
           
 
   