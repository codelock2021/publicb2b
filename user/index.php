<!doctype html>
<html class="no-js " lang="en">
<?php
include 'cls_header.php';
?>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>

    <!-- Top Bar -->
    <?php  include 'topbar.php';
include 'sidebar.php';
include 'ri8sidebar.php';
    ?>
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <h1>Suppliers
                    </h1>
            </div>
                              
  <div class="col-lg-2 col-md-2 col-sm-12">
                     <ul class="header-dropdown">
                         <a type="button" href="supplier-create.php" class="btn  btn-raised bg-teal waves-effect">Create Supplier</a>
                        </ul>
                </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix"></div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                    </div>
                    <div class="body"> 
                        <input type="hidden" name="limit" value="<?php echo CLS_PAGE_PER; ?>" id="productDatalimit" selected="selected">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable"  id="productData" data-search="title"  data-listing="true" data-from="table" data-apiName="products" >
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Ref No</th>
                                        <th>Phone</th>
                                        <th>City</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    <th>#</th>
                                        <th>Name</th>
                                        <th>Ref No</th>
                                        <th>Phone</th>
                                        <th>City</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody id="allproductData">
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
        </div>
    </section>
</body>
<!-- Jquery DataTable Plugin Js -->
    <script src="../assets/bundles/datatablescripts.bundle.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.flash.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

    <!-- <script src="../assets/bundles/mainscripts.bundle.js"></script> -->
    <!-- Custom Js -->
    <script src="../assets/js/pages/tables/jquery-datatable.js"></script>


</html>