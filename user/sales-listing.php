<!doctype html>
<html class="no-js " lang="en">
<?php
include 'cls_header.php';
?>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>

    <!-- Top Bar -->
    <?php  include 'topbar.php';
include 'sidebar.php';
include 'ri8sidebar.php';
    ?>
<section class="content">
        <div class="block-header">
             <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <h1>Sales</h1>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                     <ul class="header-dropdown">
                        <a type="button" href="direct-order.php?store=<?php echo $_SESSION['store'];?>" class="btn  btn-raised bg-blue waves-effect">Set Direct Order</a>
                            <a type="button" href="sales-order-create.php?store=<?php echo $_SESSION['store'];?>" class="btn  btn-raised bg-teal waves-effect">Create Sales Order</a>
                        </ul>
                </div>
            </div>
        <div class="container-fluid">
            <div class="row clearfix"></div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
<!--                 <div class="header">
                     <div><i class="fa fa-search" aria-hidden="true"></i> </div>
                        <div class="col-sm-3 form-group">
                            <div class="form-line">
                                <input  class="form-control"  id="salesDataSearchKeyword" name="salesData" onkeyup="js_loadShopifyDATA('salesData')" aria-invalid="false" placeholder="Search salesData" autocomplete="off">                                            
                            </div>
                        </div>
                    </div> -->
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable"  id="salesData" data-search="qty"  data-listing="true" data-from="table" data-apiName="sales">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th  class="col-1">Voucher No.</th>
                                        <th  class="col-1">Sales Date</th>
                                        <th  class="col-1">Sales Type</th>
                                        <th  class="col-1">Dealer Name</th>
                                        <th  class="col-1">Qty</th>
                                        <th>Taxable</th>
                                        <th  class="col-1">CGST</th>
                                        <th  class="col-1">SGST</th>
                                        <th  class="col-1"> IGST</th>
                                        <th  class="col-1">Grand Total</th>
                                        <th>Action</th>
                                    </tr>
                                       <tr id="filter" class="filter">
                                        <th></th>
                                        <th><input type="text" class="form-control" name="" data-apiname="sales-order"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="po_date" data-apiname="sales-order"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="sales_type" data-apiname="sales-order"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="dealer_id" data-apiname="sales-order"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="qty" data-apiname="sales-order"  data-from="table"></th>
                                        <th></th>
                                        <th><input type="text" class="form-control" name="cgst" data-apiname="sales-order"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="sgst" data-apiname="sales-order"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="igst" data-apiname="sales-order"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="grand_total" data-apiname="sales-order"  data-from="table"></th>
                                            <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                          <th>#</th>
                                        <th>Voucher No.</th>
                                        <th>Sales Date</th>
                                        <th>Sales Type</th>
                                        <th>Dealer Name</th>
                                        <th>Qty</th>
                                        <th>Taxable</th>
                                        <th>CGST</th>
                                        <th>SGST</th>
                                        <th>IGST</th>
                                        <th>Grand Total</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody id="allsalesData"></tbody>
                            </table>
                        </div>
                          <div class="cls-page-pagination mb-4" id="salesDataPagination">
                                <input type="hidden" name="current_page" id="currentPage" value="1">                                              
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</body>
<!-- Jquery DataTable Plugin Js -->
    <script src="../assets/bundles/datatablescripts.bundle.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.flash.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

    <!-- <script src="../assets/bundles/mainscripts.bundle.js"></script> -->
    <!-- Custom Js -->
    <script src="../assets/js/pages/tables/jquery-datatable.js"></script>


</html>