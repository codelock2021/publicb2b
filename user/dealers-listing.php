<!doctype html>
<html class="no-js " lang="en">
<?php
include 'cls_header.php';
?>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>

    <!-- Top Bar -->
    <?php  include 'topbar.php';
include 'sidebar.php';
include 'ri8sidebar.php';
    ?>

  <section class="content">
        <div class="block-header">
               <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <h1>Dealers/Retailers</h1>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12">
                     <ul class="header-dropdown">
                            <a type="button" href="dealers-create.php?store=<?php echo $_SESSION['store'];?>" class="btn  btn-raised bg-teal waves-effect">Create Dealer/Retailer</a>
                        </ul>
                </div>
            </div>
        <div class="container-fluid">
            <div class="row clearfix"></div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
<!--                     <div class="header">
                     <div><i class="fa fa-search" aria-hidden="true"></i> </div>
                        <div class="col-sm-3 form-group">
                            <div class="form-line">
                                <input  class="form-control"  id="dealerDataSearchKeyword" name="dealerData" onkeyup="js_loadShopifyDATA('dealerData')" aria-invalid="false" placeholder="Search dealerData" autocomplete="off">                                            
                            </div>
                        </div>
                    </div> -->
                    <div class="body">
                        <div class="table-responsive">
                             <input type="hidden" name="limit" value="<?php echo CLS_PAGE_PER; ?>" id="dealerDatalimit" selected="selected">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable"  id="dealerData" data-search="firstname"  data-listing="true" data-from="table" data-apiName="dealer">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th  class="col-1">Firstname</th>
                                        <th  class="col-1">Lastname</th>
                                        <th>Email</th>
                                        <th>Username</th>
                                        <th>Mobile</th>
                                        <th  class="col-1">City</th>
                                        <th>State </th> 
                                        <th>Active</th>
                                        <th>Action</th>
                                    </tr>
                                     <tr id="filter" class="filter">
                                        <th></th>
                                        <th><input type="text" class="form-control" name="firstname" data-apiname="dealer"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="lastname"  data-apiname="dealer"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="email"     data-apiname="dealer"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="username"  data-apiname="dealer" data-from="table"></th>
                                        <th><input type="text" class="form-control" name="mobile"    data-apiname="dealer"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="city"      data-apiname="dealer"  data-from="table"> </th>
                                        <th><input type="text" class="form-control" name="state"      data-apiname="dealer"  data-from="table"> </th>
                                        <th><select class="form-control show-tick"  name="dealer_status" data-apiname="dealer"  data-from="table">
                                            <option value="">All</option>
                                            <option value="0">Active</option>
                                            <option value="1">InActive</option>
                                      </select></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Email</th>
                                        <th>Username</th>
                                        <th>Mobile</th>
                                        <th>City</th>
                                        <th>State </th>
                                        <th>Active</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody id="alldealerData">
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="cls-page-pagination mb-4" id="dealerDataPagination">
                                <input type="hidden" name="current_page" id="currentPage" value="1">                                              
                        </div>  
                    </div>
                </div>
            </div>
        </div>
      
        </div>
    </section>

</body>
<!-- Jquery DataTable Plugin Js -->
    <script src="../assets/bundles/datatablescripts.bundle.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.flash.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

    <!-- <script src="../assets/bundles/mainscripts.bundle.js"></script> -->
    <!-- Custom Js -->
    <script src="../assets/js/pages/tables/jquery-datatable.js"></script>


</html>
<script>

</script>