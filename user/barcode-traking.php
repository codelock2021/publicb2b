<!doctype html>
<html class="no-js " lang="en">
<?php
include_once 'cls_header.php';
?>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>
    <!-- Right Sidebar -->
    <!-- Top Bar -->
    <?php  include 'topbar.php';
include 'sidebar.php';
include 'ri8sidebar.php';
    ?>

   
   
    <!-- Main Content -->
  
  
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h1>Create Barcode Tracking
                </h1>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
               <a href="barcode-listing.php" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                       <div class="col-lg-4 col-md-6 col-sm-12">
                     <ul class="header-dropdown">
                            <a type="button" href="update-barcode-tracking.php" class="btn  btn-raised bg-teal waves-effect">Start now </a>
                        </ul>
                </div>
                       
                        
        </div>
        <!-- #END# Input --> 
     
    </div>
</section>
</div>
</div>
  
</body>


</html>