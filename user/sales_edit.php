<!doctype html>
<html class="no-js " lang="en">
  <?php
    $id = isset($_GET['pid']) ? $_GET['pid'] : '';
    $sid = isset($_GET['sid']) ? $_GET['sid'] : '';
    include_once 'cls_header.php';
    $obj_Client_functions = new Client_functions($_SESSION['store']);
    ?>
    <script>
        var id = "<?php echo $id; ?>";
    </script>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"></div>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>
    <!-- Right Sidebar -->
    <!-- Top Bar -->
    <?php  include 'topbar.php';
include 'sidebar.php';
include 'ri8sidebar.php';
    ?>

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-11 col-md-11 col-sm-12">
               <h1>Create Sales Order</h1>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12">
               <a href="sales-listing.php?store=<?php echo $_SESSION; ?>" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                          <form class="m-t" id="register_frm" name="register_frm" method="POST"  enctype="multipart/form-data" onsubmit="">
                                <input type="hidden" id="" name="id" value="<?php echo $id; ?>">
                                <input type="hidden" id="" name="for_data" value="<?php echo 'sales'; ?>">
                        <div class="body">
                          
                            <div class="row clearfix">
                                 <div class="col-sm-6">
                                            <label>Sales Type :</label>
                                            <input name="sales_type" type="radio" class="with-gap" id="sales_type_tax"  value="0" />
                                            <label for="radio_1"> Tax Invoice </label>
                                            <input name="sales_type" type="radio" class="with-gap" id="sales_type_delivery" value="1"/>
                                            <label for="radio_2"> Delivery Callan</label>
                                <span class="error sales_type"></span>
                                </div>
                                  <div class="col-sm-6">
                                            <label>Invoice Type :</label>
                                            <input name="invoice_type" type="radio" class="with-gap" id="invoice_type" value="" />
                                            <label for="radio_4"> Exclude TAX</label>
                                </div>
                            </br>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Date of Supply</label>
                                        <div class="form-line">
                                            <input type="date" class="form-control" placeholder="Date of Supply" name="dos" id="dos"/>
                                        </div>
                                        <span class="error dos"></span>
                                    </div>
                                </div>
                                </br>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Sales Date</label>
                                        <div class="form-line">
                                            <input type="date" class="form-control" placeholder="Sales Date" name="sales_date" id="sales_date" />
                                        </div>
                                        <span class="error sales_date"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Packing and Forwarding" name="packing" id="packing"/>
                                        </div>
                                        <span class="error packing"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Insurance" name="insurance" id="insurance" />
                                        </div>
                                        <span class="error insurance"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Freight" name="freight" id="freight"/>
                                        </div>
                                        <span class="error freight"></span>
                                    </div>
                                </div>
                                 
                                 
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <select class="form-control show-tick" name="dealer_id" id="dealer_id">
                                             <?php
                                                $options = '<option value="">Supplier Name</option>';
                                                $comeback = $obj_Client_functions->select_result(TABLE_DEALER, '*');
                                                foreach($comeback["data"] as $data){
                                                    $data = (object)$data;
                                                    $selectedoption = ($sid==$data->id) ? 'selected ' : '';
                                                    $options .= '<option '.$selectedoption.' value='.$data->id.'>' .$data->firstname. '</option>';
                                                    ?>
                                               <?php }
                                               echo $options;
                                                ?>
                                        </select>
                                        <span class="error dealer_id"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                       <div class="form-group">
                                           <select class="form-control show-tick" name="reverse_charge" id="reverse_charge">
                                            <option value="">Reverse Charge</option>
                                            <?php                                  
                                            $comeback = $obj_Client_functions->select_result(TABLE_SALES_ORDER, '*');
                                            $comeback = (object)$comeback["data"][0];
                                            $comeback = (isset($comeback->reverse_charge) && $comeback->reverse_charge != '') ? $comeback->reverse_charge:'';
                                           
                                            ?>
                                              <option value="1" <?php if($comeback=="1") {echo "selected";}?>>No</option>
                                              <option value="0" <?php if($comeback=="0") {echo "selected";}?>>Yes</option>
                                        </select>
                                    </div>
                                     <span class="error reverse_charge"></span>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Ref No" name="ref_no" id="ref_no"/>
                                        </div>
                                        <span class="error ref_no"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Transport Mode" name="transport_mode" id="transport_mode" />
                                        </div>
                                        <span class="error transport_mode"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Vehicle No" name="vehicle_no" id="vehicle_no" />
                                        </div>
                                        <span class="error vehicle_no"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea rows="4" class="form-control no-resize" placeholder="Billing Address" name="billing_address" id="billing_address"></textarea>
                                        </div>
                                        <span class="error billing_address"></span>
                                    </div>
                                </div>
                                 <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea rows="4" class="form-control no-resize" placeholder="Shipping Address" name="shipping_address" id="shipping_address"></textarea>
                                        </div>
                                        <span class="error shipping_address"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea rows="4" class="form-control no-resize" placeholder="Remark" name="remark" id="remark"></textarea>
                                        </div>
                                        <span class="error remark"></span>
                                    </div>
                                </div>
                            </div>
                            
       <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card">
                                        <div class="body">
                                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                            <thead>
                                                <tr>
                                                        <th>SNo.</th>
                                                        <th>Barcode </th>
                                                        <th>SKU</th>
                                                        <th>Name</th>
                                                        <th>Metal</th>
                                                        <th>G. Weight</th>
                                                        <th>Qty/MRP </th>
                                                        <th>Dis. Rate </th>
                                                        <th>Dis </th>
                                                        <th>Taxable</th>
                                                        <th>CGST</th>
                                                        <th>SGST</th>
                                                        <th>IGST</th>
                                                        <th>Sales Price  </th>
                                                </tr>
                                            </thead>
                                             <tbody class="edit-sales">
                                            </tbody>
                                            <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th><input type="hidden"  class="form-control all_total" name="all_total" ></th>
                                                            <th>Total</th>
                                                            <th><input type="text"  class="form-control total_qty" name="total_qty" ></th>
                                                            <th>&nbsp;</th>
                                                            <th><input type="text"  class="form-control total_discount" name="total_discount" ></th>
                                                            <th><input type="text"  class="form-control total_taxable" name="total_taxable" ></th>
                                                            <th><input type="number"  class="form-control total_cgst" name="total_cgst" ></th>
                                                            <th><input type="text"  class="form-control total_sgst" name="total_sgst" ></th>
                                                            <th><input type="number"  class="form-control total_igst" name="total_igst" ></th>
                                                            <th><input type="number"  class="form-control grand_amount" name="grand_amount" ></th>
                                                        </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                               <div class="row clearfix">
                       
                               <div class="col-sm-4">
                                <div class="form-group">
                                    <button type="submit" name="submit" class="btn  btn-raised bg-teal waves-effect">Update </button>
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>
                      <form>
                </div>
            </div>
        </div>
        <!-- #END# Input --> 
     
    </div>
</section>
</div>
</div>
</body>
</html>
<script>
    var routine_name = 'sales_select';
    get_sales_value(routine_name,  id, "sales");
</script>
