<!doctype html>
<html class="no-js " lang="en">
    <html class="no-js " lang="en">
        <?php
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $sid = isset($_GET['sid']) ? $_GET['sid'] : '';
        $sku_id = isset($_GET['sales_sku_id']) ? $_GET['sales_sku_id'] : '';
        $sales_id = isset($_GET['sales_id']) ? $_GET['sales_id'] : '';
        include_once 'cls_header.php';
        $obj_Client_functions = new Client_functions($_SESSION['store']);
        ?>
        <body class="theme-orange">
            <!-- Page Loader -->
            <div class="page-loader-wrapper">
                <div class="loader">
                    <div class="line"></div>
                    <div class="line"></div>
                    <div class="line"></div>
                    <p>Please wait...</p>
                    <div class="m-t-30"></div>
                </div>
            </div>

            <!-- Overlay For Sidebars -->
            <div class="overlay"></div>
            <!-- Search  -->
            <div class="search-bar">
                <div class="search-icon"> <i class="material-icons">search</i> </div>
                <input type="text" placeholder="Explore Nexa...">
                <div class="close-search"> <i class="material-icons">close</i> </div>
            </div>

            <!-- Top Bar -->
            <?php
            include 'topbar.php';
            include 'sidebar.php';
            include 'ri8sidebar.php';
            ?>
            <section class="content">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 col-sm-12">
                            <h2>Return Sales Dealer: "The Jewellery House " </h2>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12">
                            <ul class="header-dropdown">
                                <a type="button" href="sales-return-listing.php?store=<?php echo $_SESSION['store'];?>" class="btn  btn-raised bg-teal waves-effect">Back</a>
                            </ul>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <!-- Input -->
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <form class="m-t" id="register_frm" name="register_frm" method="POST"  enctype="multipart/form-data" onsubmit="">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-2 col-md-2">
                                                    <div class="col-sm-2">
                                                        <label>Format</label>
                                                        <input name="pdf" type="radio" class="with-gap" id="radio_0" value="1" checked="true"/>
                                                        <label for="radio_0">PDF</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3">
                                                    <div class="col-lg-4">
                                                        <label>Print Price</label>
                                                        <input  name="print_price" type="radio" class="with-gap"  value="1" checked id="radio_4"/>
                                                        <label for="radio_4">Yes</label>
                                                        <input  name="print_price" type="radio" class="with-gap"  value="0" id="radio_3" />
                                                        <label for="radio_3">No</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-1">
                                                    <div class="col-lg-2">
                                                        <label>Weight</label>
                                                        <input  name="weight" type="radio" class="with-gap"  value="1" checked="true" id="radio_1"/> 
                                                        <label for="radio_1">Yes</label>
                                                        <input  name="weight" type="radio" class="with-gap"  value="0" id="radio_2" />
                                                        <label for="radio_2">No</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-1">
                                                    <div class="col-lg-2">
                                                        <label>Metal</label>
                                                        <input type="radio" id="age1" name="metal"  class="with-gap"  value="0" checked>
                                                        <label for="age1">Yes</label>
                                                        <input type="radio" id="age12" name="metal"  class="with-gap"  value="1" >
                                                        <label for="age12">No</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3">
                                                    <div class="col-lg-5">
                                                        <label>Type</label>
                                                        <input  name="type" type="radio" class="with-gap"  value="0" checked="true" id="radio_7"/>
                                                        <label for="radio_7">  Original Copy</label>
                                                        <input  name="type" type="radio" class="with-gap"  value="1" id="radio_8" />
                                                        <label for="radio_8">   Office Copy</label>
                                                        <input  name="type" type="radio" class="with-gap"  value="2" id="radio_9" />
                                                        <label for="radio_9">   Dupilcate Copy</label>
                                                        <input  name="type" type="radio" class="with-gap"  value="3" id="radio_80" />
                                                        <label for="radio_80">   Packing List</label>
                                                        <input  name="type" type="radio" class="with-gap"  value="4" id="radio_08" />
                                                        <label for="radio_08">   Transporter Copy</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a href="sales_return_print.php?id=<?php  echo $id; ?>&dealer_id=<?php  echo $sid; ?>&sales_sku_id=<?php  echo $sku_id; ?>?store=<?php echo $_SESSION['store'];?>" target="_blank"  class="btn  btn-raised bg-teal waves-effect">Voucher Print </a>
                                            </div>
                                        </div>
                                        <div class="body">
                                            <?php
                                            $where_query_arr = array(["", "id", "=", "$sid"]);
                                            $comeback = $obj_Client_functions->select_result(TABLE_DEALER, '*', $where_query_arr);
                                            foreach ($comeback["data"] as $data) {
                                                $data = (object) $data;
                                            }
                                            ?>
                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable"  id="" data-search="title"  data-listing="true" data-from="table" data-apiName="purchase">
                                                <tbody>

                                                    <tr><th>Dealer Name	</th><td class="dealer_name">
                                                            <?php
                                                            if ($sid == $data->id) {
                                                                $dealer_name = (isset($data->firstname) && $data->firstname != '' ) ? $data->firstname : '';
                                                                echo $dealer_name;
                                                            }
                                                            ?></td></tr>
                                                    <tr><th>Return Date</th><td class="pur_date"><?php
                                                            $where_query_arr = array(["", "id", "=", "$id"]);
                                                            $comeback = $obj_Client_functions->select_result(TABLE_SALES_ORDER_RETURN, '*', $where_query_arr);
                                                            $comeback = (object) $comeback["data"][0];
                                                            $so_type = (isset($comeback->so_type) && $comeback->so_type !== '') ? $comeback->so_type : 0;
                                                            if ($comeback->so_type == 1) {
                                                                $so_type = "On Approval ";
                                                            } else {
                                                                $so_type = "Direct ";
                                                            }

                                                            $reason = (isset($comeback->reason) && $comeback->reason !== '') ? $comeback->reason : 0;
                                                            $ref_num = (isset($comeback->ref_num) && $comeback->ref_num !== '') ? $comeback->ref_num : 0;
                                                            $return_date = (isset($comeback->return_date) && $comeback->return_date !== '') ? $comeback->return_date : 0;
                                                            $qty = (isset($comeback->qty) && $comeback->qty !== '') ? $comeback->qty : 0;
                                                            $discount = (isset($comeback->discount) && $comeback->discount !== '') ? $comeback->discount : 0;
                                                            $cgst = (isset($comeback->cgst) && $comeback->cgst !== '') ? $comeback->cgst : 0;
                                                            $sgst = (isset($comeback->sgst) && $comeback->sgst !== '') ? $comeback->sgst : 0;
                                                            $igst = (isset($comeback->igst) && $comeback->igst !== '') ? $comeback->igst : 0;
                                                            $freight = (isset($comeback->freight) && $comeback->freight !== '') ? $comeback->freight : 0;
                                                            $insurance = (isset($comeback->insurance) && $comeback->insurance !== '') ? $comeback->insurance : 0;
                                                            $packing = (isset($comeback->packing) && $comeback->packing !== '') ? $comeback->packing : 0;
                                                            $grand_total = (isset($comeback->grand_total) && $comeback->grand_total !== '') ? $comeback->grand_total : 0;
                                                            $taxable = (isset($comeback->taxable) && $comeback->taxable !== '') ? $comeback->taxable : 0;
                                                            echo $return_date;
                                                            ?></td></tr>
                                                    <tr><th>So Type</th><td><?php echo $so_type; ?> </td></tr>
                                                    <tr><th>Ref No.	</th><td class="qty"> <?php echo $ref_num; ?> </td></tr>
                                                    <tr><th>Reason</th><td class="qty"> <?php echo $reason; ?> </td></tr>
                                                    
                                                    <tr><th>Qty</th><td class="qty"> <?php echo $qty; ?> </td></tr>
                                                    <tr><th>Product Value</th><td class="purchase_value">   <?php echo $taxable; ?></td></tr>
                                                    <tr><th>CGST</th><td class="cgst">   <?php echo $cgst; ?></td></tr>
                                                    <tr><th>SGST</th><td class="sgst">   <?php echo $sgst; ?></td></tr>
                                                    <tr><th>IGST</th><td class="igst"> <?php echo $igst; ?></td>  </tr>
                                                    <tr><th>Freight</th><td class="freight">   <?php echo $freight; ?></td></tr>
                                                    <tr><th>Insurance</th><td class="insurancee">   <?php echo $insurance; ?></td></tr>
                                                    <tr><th>Packing And Forwarding	</th><td class="packing">   <?php echo $packing; ?></td></tr>
                                                    <tr><th>Discount</th><td class="packing">    <?php echo $discount; ?> </td></tr>
                                                    <tr><th>Grand Total</th><td class="grandtotal">   <?php echo $grand_total; ?></td></tr>
                                                </tbody>
                                            </table>
                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable"  id="" data-search="title"  data-listing="true" data-from="table" data-apiName="purchase">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th class="col-lg-1" >SKU</th>
                                                        <th>Purchase Value	</th>
                                                        <th class="col-lg-1">Qty</th>
                                                        <th class="col-lg-1">Discount</th>
                                                        <th>CGST</th>
                                                        <th>SGST</th>
                                                        <th>IGST</th>
                                                        <th> Total Product Value	</th>
                                                        <th class="col-lg-1">Detail</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th></th>
                                                        <th class="col-lg-1" >Total</th>
                                                        <th>	</th>
                                                        <th class="col-lg-1"><?php echo $qty; ?></th>
                                                        <th class="col-lg-1"><?php echo $discount; ?></th>
                                                        <th> <?php echo $cgst; ?></th>
                                                        <th> <?php echo $sgst; ?></th>
                                                        <th> <?php echo $igst; ?></th>
                                                        <th> <?php echo $grand_total; ?>	</th>
                                                        <th class="col-lg-1"></th>
                                                    </tr>
                                                </tfoot>
                                                <tbody id="allcollectionData">
                                                <?php
                                                    $where_query_arr = array(["", "sales_id", "=", "$sales_id"], ["AND", "id", "=", "$sku_id"]);

                                                    $comeback = $obj_Client_functions->select_result(TABLE_SALES_SKU_MASTER, '*', $where_query_arr);

                                                    $i=1;
                                                    foreach ($comeback["data"] as $comeback) {
                                                        $comeback = (object) $comeback;
                                                        $id = (isset($comeback->id) && $comeback->id !== '') ? $comeback->id : 0;
                                                        $return_qty = (isset($comeback->return_qty) && $comeback->return_qty !== '') ? $comeback->return_qty : 0;
                                                        $sku = (isset($comeback->sku) && $comeback->sku !== '') ? $comeback->sku : 0;
                                                        $price = (isset($comeback->mrp) && $comeback->mrp !== '') ? $comeback->mrp : 0;
                                                        $discount  = (isset($comeback->discount) && $comeback->discount !== '') ? $comeback->discount : 0;
                                                        $sgst = (isset($comeback->sgst) && $comeback->sgst !== '') ? $comeback->sgst : 0;
                                                        $cgst = (isset($comeback->cgst) && $comeback->cgst !== '') ? $comeback->cgst : 0;
                                                        $igst = (isset($comeback->igst) && $comeback->igst !== '') ? $comeback->igst : 0;
                                                        $grand_total = (isset($comeback->grandtotal) && $comeback->grandtotal !== '') ? $comeback->grandtotal : 0;
                                                     
                                                        ?>
                                                     <tr>
                                                        <th><?php echo $i++; ?></th>
                                                        <th class="col-lg-1" ><?php echo $sku; ?></th>
                                                        <th><?php echo $price; ?></th>
                                                        <th class="col-lg-1"><?php  echo $return_qty;?></th>
                                                        <th class="col-lg-1"><?php  echo $discount;?></th>
                                                        <th><?php  echo $cgst;?></th>
                                                        <th><?php echo $sgst;?></th>
                                                        <th><?php  echo $igst ?></th>
                                                        <th><?php echo $grand_total; ?></th>
                                                        <th class="col-lg-1 "><a type="button" name="" id="" data-sku="<?php echo $id; ?>" class="btn  btn-raised bg-teal waves-effect salesModalClick"  data-toggle="modal" data-target="#largeModal" >items </a></th>
                                                    </tr>
                                                   <?php  } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- #END# Input --> 
                    </div>
                </div>
                      <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Print MRP Label </h4>
            </div>
            <div class="modal-body table-responsive">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable"  id="purchase_skuData" data-search="title"  data-listing="false" data-from="table" data-apiName="purchase">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>SKU</th>
                            <th>Barcode	</th>
                            <th>Action</th>
                            </tr>
                    </thead>
                    <tbody class="fill_sku">
                         
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-raised bg-teal waves-effect">PRINT</button>
                <button type="button" class="btn bg-deep-orange waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
            </section>  

        </body>
        <!-- Jquery DataTable Plugin Js -->
        <script src="../assets/bundles/datatablescripts.bundle.js"></script>
        <script src="../assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
        <script src="../assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
        <script src="../assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
        <script src="../assets/plugins/jquery-datatable/buttons/buttons.flash.min.js"></script>
        <script src="../assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
        <script src="../assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>
        <!-- <script src="../assets/bundles/mainscripts.bundle.js"></script> -->
        <!-- Custom Js -->
        <script src="../assets/js/pages/tables/jquery-datatable.js"></script>


    </html>