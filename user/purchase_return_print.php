
<!doctype html>
<html class="no-js " lang="en">
    <head> 
        <link rel="stylesheet" href="https://1dd265088a6cef288739-152492db8df9591a73e16991ac6a30fb.ssl.cf2.rackcdn.com/generator/generator.min.css" rel="stylesheet">
</head>
  <?php
    include_once 'cls_header.php';
    $id = isset($_GET['id']) ? $_GET['id'] : '';
    $sid = isset($_GET['sid']) ? $_GET['sid'] : '';
    $sku_id = isset($_GET['sku_id']) ? $_GET['sku_id'] : '';
    $obj_Client_functions = new Client_functions($_SESSION['store']);
    $fields = '*';
    $where_query = array(["", "id", "=", "$id"]);
    $comeback = $obj_Client_functions->select_result(TABLE_PURCHASE_RETURN, $fields, $where_query);
    $comeback = (isset($comeback['data'][0]) && !empty($comeback['data'][0]) ? $comeback['data'][0] : '');
    $reverse_charge = (isset($comeback['reverse_charge']) && $comeback['reverse_charge'] == 1) ? "Yes" : "NO";
    $supplier_invoice_no = (isset($comeback['supplier_invoice_no']) && $comeback['supplier_invoice_no'] != '') ? $comeback['supplier_invoice_no'] : '';
    $supplier_invoice_date = (isset($comeback['supplier_invoice_date']) && $comeback['supplier_invoice_date'] != '') ? $comeback['supplier_invoice_date'] : '';
    $qty_ret = (isset($comeback['qty_ret']) && $comeback['qty_ret'] != '') ? $comeback['qty_ret'] : '';
    $price_ret = (isset($comeback['price_ret']) && $comeback['price_ret'] != '') ? $comeback['price_ret'] : '';
    $cgst = (isset($comeback['cgst']) && $comeback['cgst'] != '') ? $comeback['cgst'] : '';
    $sgst = (isset($comeback['sgst']) && $comeback['sgst'] != '') ? $comeback['sgst'] : '';
    $igst = (isset($comeback['igst']) && $comeback['igst'] != '') ? $comeback['igst'] : '';
    $grand_total = (isset($comeback['grand_total']) && $comeback['grand_total'] != '') ? $comeback['grand_total'] : '';
    $taxable = (isset($comeback['taxable']) && $comeback['taxable'] != '') ? $comeback['taxable'] : '';
    $number = $grand_total;
   
     $fields = '*';
    $where_query = array(["", "id", "=", "$sid"]);
    $comeback = $obj_Client_functions->select_result(TABLE_SUPPLIERS, $fields, $where_query);
     $comeback = (isset($comeback['data'][0]) && !empty($comeback['data'][0]) ? $comeback['data'][0] : '');
    $supplier_name = (isset($comeback['supplier_name']) && $comeback['supplier_name'] != '') ? $comeback['supplier_name'] : '';
    $supplier_mobile = (isset($comeback['supplier_mobile']) && $comeback['supplier_mobile'] != '') ? $comeback['supplier_mobile'] : '';
    $supplier_address = (isset($comeback['supplier_address']) && $comeback['supplier_address'] != '') ? $comeback['supplier_address'] : '';
    $supplier_city = (isset($comeback['supplier_city']) && $comeback['supplier_city'] != '') ? $comeback['supplier_city'] : '';
    $supplier_state = (isset($comeback['supplier_state']) && $comeback['supplier_state'] != '') ? $comeback['supplier_state'] : '';
    $supplier_pan = (isset($comeback['supplier_pan']) && $comeback['supplier_pan'] != '') ? $comeback['supplier_pan'] : '';
    $supplier_type = (isset($comeback['supplier_type']) && $comeback['supplier_type'] != '') ? $comeback['supplier_pan'] : '';
    
    if ($supplier_type == 0) {
            $supplier_type = 'Un-registered';
    } elseif ($supplier_type == 1) {
            $supplier_type = 'Registered';
    } else {
            $supplier_type = 'Composition';
    }
        

    
?>
 
    <body class="theme-orange">
        <div class="container priotout-page-font" style="margin-top:40px">
            <div class="row">
                <div class="col-sm">
                    <img src="../assets/images/logo.png" alt="" height="50" weight="40">  
                    </br>
                    <hr>
                </div>
                <div class="col-sm">
                    <b>TISTABENE RETAILS LLP</b><br>
                    <b>GSTN: </b>08AALFT2070N1ZP<br>
                    <b>PAN: </b>AALFT2070N<br>
                    <b>STATE & CODE:</b><p> RAJASTHAN (08)</p>
                </div>
                <div class="col-sm">
                    <p><b><h6>GOODS RETURN RECEIPT NOTE</h6></b>(DUPILCATE COPY)</p> 
                    <div class="supplier_grn_date"><b>GRN DATE:</b> <div class="supplier_grn_date_ans"></div></div>
                    <div class="supplier_grn_no"><b>GRN NO.:</b> <div class="supplier_grn_no_ans"></div></div>
                    <div class="supplier_reverse_charge"><b>REVERSE CHARGE:</b> <div class="supplier_reverse_charge_ans"><?php  echo $reverse_charge;?></div></div>
                </div>

            </div>
            <div class="row">
                <div class="col-sm">
                    <div class="supplier_details"><b><h6>SUPPLIER DETAILS</h6></b><div class="supplier_details_ans"></div></div>
                    <div class="supplier_invoice_no"><b>Supplier Invoice No:</b> <?php  echo $supplier_invoice_no;?></div>
                    <div class="supplier_invoice_date"><b>Supplier Invoice Date</b><?php echo $supplier_invoice_date;?></div>
                    <div class="supplier_name"><b>Name:</b><?php echo $supplier_name;?></div>
                    <div class="supplier_mob_no"><b>Mobile No.:</b><?php  echo $supplier_mobile;?></div>
                    <div class="supplier_address"><b>Address:</b><?php echo $supplier_address; ?></div>
                    <div class="supplier_city"><b>City:</b><?php echo $supplier_city;?> </div>
                    <div class="supplier_state"><b>State:</b><?php echo $supplier_state; ?> </div>
                    <div class="supplier_pan"><b>PAN:</b> <?php echo $supplier_pan; ?></div>
                    <div class="supplier_gstn"><b>GSTN:</b><?php echo $supplier_type;?> </div>
                </div>
                <div class="col-sm">
                </div>
                <div class="col-sm">
                    <div class="supplier_details"><b><h6>CONSIGNER DETAILS</h6></b> <div class="supplier_consigner_details_ans"></div></div>
                      <div class="supplier_name"><b>Name:</b><?php echo $supplier_name;?></div>
                    <div class="supplier_mob_no"><b>Mobile No.:</b><?php  echo $supplier_mobile;?></div>
                    <div class="supplier_address"><b>Address:</b><?php echo $supplier_address; ?></div>
                    <div class="supplier_city"><b>City:</b><?php echo $supplier_city;?> </div>
                    <div class="supplier_state"><b>State:</b><?php echo $supplier_state; ?> </div>
                    <div class="supplier_pan"><b>PAN:</b> <?php echo $supplier_pan; ?></div>
                    <div class="supplier_gstn"><b>GSTN:</b><?php echo $supplier_type;?> </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 printoutpage-cls">
                <table class="table table-hover " border="1" style="margin-top:30px">
                    <thead>
                        <tr>
                            <th>S No</th>
                            <th>Product</th>
                            <th>SKU  No</th>
                            <th>Supplier product code</th>
                            <th>HSN code</th>
                            <th>Gr. Wt. </th>
                            <th>qty</th>
                            <th>UOM</th>
                            <th>Rate /Item (INR)</th>
                            <th>Amount</th>
                            <th>Taxable</th>
                            <th>CGST</th>
                            <th>SGST</th>
                            <th>IGST</th>
                            <th>Total Amount</th>
                        </tr>
                    </thead>
                    <tbody id="voucherprint">  
                         <?php
                        $where_query = array(["", "id", "=", "$id"]);
                    $comeback = $obj_Client_functions->select_result(TABLE_PURCHASE_RETURN, '*', $where_query);

                    $comeback = (isset($comeback['data']) && !empty($comeback['data']) ? $comeback['data'] : '');
                    $i=1;
                        foreach ($comeback as $value) {
                             $sku_id = (isset($value['sku_id']) && $value['sku_id'] != '') ? $value['sku_id'] : '';
                            $where_query = array(["", "id", "=", "$sku_id"]);
                            $comeback = $obj_Client_functions->select_result(TABLE_SKU_MASTER, '*', $where_query);
                            $comeback = (isset($comeback['data'][0]) && !empty($comeback['data'][0]) ? $comeback['data'][0] : '');
                          
                            $sku = (isset($comeback['sku']) && $comeback['sku'] != '') ? $comeback['sku'] : '';
                            $qty = (isset($value['qty']) && $value['qty'] != '') ? $value['qty'] : '';
                            $price = (isset($value['price']) && $value['price'] != '') ? $value['price'] : '';
                            $taxable = (isset($value['taxable']) && $value['taxable'] != '') ? $value['taxable'] : '';
                            $cgst = (isset($value['cgst']) && $value['cgst'] != '') ? $value['cgst'] : '';
                            $sgst = (isset($value['sgst']) && $value['sgst'] != '') ? $value['sgst'] : '';
                            $igst = (isset($value['igst']) && $value['igst'] != '') ? $value['igst'] : '';
                            $grand_total = (isset($value['grand_total']) && $value['grand_total'] != '') ? $value['grand_total'] : '';
                            $prifix = '<td>';
                            $sufix = '</td>';
                            $html = '';
                            $html .= '<tr>';
                            $html .= $prifix . $i++ . $sufix;
                            $html .= $prifix . '' . $sufix;
                            $html .= $prifix . $sku . $sufix;
                            $html .= $prifix . '' . $sufix;
                            $html .= $prifix .''  . $sufix;
                            $html .= $prifix . '' . $sufix;
                            $html .= $prifix . $qty . $sufix;
                            $html .= $prifix . '' . $sufix;
                            $html .= $prifix . $price . $sufix;
                            $html .= $prifix . $price.$sufix;
                            $html .= $prifix . $taxable . $sufix;
                            $html .= $prifix . $cgst . $sufix;
                            $html .= $prifix . $sgst . $sufix;
                            $html .= $prifix . $igst . $sufix;
                            $html .= $prifix . $grand_total . $sufix;
                            $html .= '</tr>';
                        echo $html;
                        }
                        ?>
<!--                       <th></th>
                           <th></th>
                           <th></th>
                           <th></th>
                           <th></th>
                           <th> </th>
                           <th><?php echo $qty_ret; ?></th>
                           <th></th>
                           <th><?php echo $price_ret;?></th>
                           <th></th>
                           <th><?php echo $taxable; ?></th>
                           <th><?php  echo $cgst;?></th>
                           <th><?php  echo $sgst;?></th>
                           <th><?php  echo $igst;?></th>
                           <th><?php echo $grand_total; ?></th>-->
                    </tbody>
                    <tfoot>
                         <?php
                            $where_query = array(["", "id", "=", "$id"]);
                    $comeback = $obj_Client_functions->select_result(TABLE_PURCHASE_RETURN, '*', $where_query);

                    $comeback = (isset($comeback['data'][0]) && !empty($comeback['data'][0]) ? $comeback['data'][0] : '');
                    $price = (isset($comeback['price']) && $comeback['price'] != '') ? $comeback['price'] : '';
                    $qty = (isset($comeback['qty']) && $comeback['qty'] != '') ? $comeback['qty'] : '';
                    $cgst = (isset($comeback['cgst']) && $comeback['cgst'] != '') ? $comeback['cgst'] : '';
                    $sgst = (isset($comeback['sgst']) && $comeback['sgst'] != '') ? $comeback['sgst'] : '';
                    $igst = (isset($comeback['igst']) && $comeback['igst'] != '') ? $comeback['igst'] : '';
                    $grand_total = (isset($comeback['grand_total']) && $comeback['grand_total'] != '') ? $comeback['grand_total'] : '';
                    $taxable = (isset($comeback['taxable']) && $comeback['taxable'] != '') ? $comeback['taxable'] : '';
                     
                        ?>
                        <tr>
                            <th>Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Gr. Wt.</th>
                            <th><?php echo $qty;?></th>
                            <th></th>
                            <th><?php echo $price;?></th>
                            <th></th>
                            <th><?php echo $taxable; ?></th>
                            <th><?php echo $cgst; ?></th>
                            <th><?php echo $sgst; ?></th>
                            <th><?php echo $igst; ?></th>
                            <th><?php echo $grand_total; ?></th>
                        </tr>
                    </tfoot>
                </table> 
            </div>
            <div class="row"><div class="col-sm"><p>Remark:-</p></div></div>
            <hr>
            <div class="row">
                <div class="col-sm">
                    <div class="supplier_amount"><b>Total Amount Before Tax</b><?php echo $taxable; ?></div>
                    <div class="supplier_cgst"><b>Add: CGST</b><?php echo $cgst; ?></div>
                    <div class="supplier_sgst"><b>Add: SGST</b><?php echo $sgst;?></div>
                    <div class="supplier_igst"><b>Add: IGST</b><?php echo $igst;?></div>
                    <div class="supplier_total_amount"><b>Total Tax Amount</b></div>
                    <hr>
                    <div class="supplier_total_amount_tax"><b>Total Amount After Tax</b> <?php  echo $grand_total;?></div>
                    <hr>
                    <div class="supplier_reverse_charge"><b>GST on Reverse Charges </b></div>
                </div>
                <div class="col-sm">
                </div>
                <div class="col-sm">
                    <div class="supplier_total_amount_tax"><b>Total Amount After Tax</b> <?php  echo $grand_total;?></div>
                    <div class="supplier_roundoff"><b>Round Off</b> <div class="supplier_roundoff_ans"></div></div>
                    <hr>
                    <div class="supplier_grand_total"><b>GRAND TOTAL</b> <?php echo $grand_total; ?></div>
                    <div class="supplier_total_amount_word"><b>Invoice Amount in Words:</b> <?php  
                    $no = floor($number);
                    $point = round($number - $no, 2) * 100;
                    $hundred = null;
                    $digits_1 = strlen($no);
                    $i = 0;
                    $str = array();
                    $words = array('0' => '', '1' => 'one', '2' => 'two',
                        '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
                        '7' => 'seven', '8' => 'eight', '9' => 'nine',
                        '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
                        '13' => 'thirteen', '14' => 'fourteen',
                        '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
                        '18' => 'eighteen', '19' => 'nineteen', '20' => 'twenty',
                        '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
                        '60' => 'sixty', '70' => 'seventy',
                        '80' => 'eighty', '90' => 'ninety');
                    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
                    while ($i < $digits_1) {
                        $divider = ($i == 2) ? 10 : 100;
                        $number = floor($no % $divider);
                        $no = floor($no / $divider);
                        $i += ($divider == 10) ? 1 : 2;
                        if ($number) {
                            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                            $str [] = ($number < 21) ? $words[$number] .
                                    " " . $digits[$counter] . $plural . " " . $hundred :
                                    $words[floor($number / 10) * 10]
                                    . " " . $words[$number % 10] . " "
                                    . $digits[$counter] . $plural . " " . $hundred;
                        } else
                            $str[] = null;
                    }
                    $str = array_reverse($str);
                    $result = implode('', $str);
                    echo $result . "Rupees  ";
?></div>
                </div>
            </div>  
            <hr>
            <div class="row">
                <div class="col-sm">
                    <div class="col-sm-12"><div><b>Address:</b>344-A,Near Seva Sadan, Bees Dukan, Adarsh Nagar,Jaipur-302004</div></div>
                </div>
                <div class="col-sm">
                    <div class="col-sm-12"><div><b>Phone:</b> +91-141-4040744 |<b> Email: </b>accounts@tistabene.com | <b>Web: </b>www.tistabene.com</div></div>
                </div>
            </div>
    </body>
    <script>
    window.print()
    
    </script>