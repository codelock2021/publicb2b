<html class="no-js " lang="en">
    <?php
    include_once 'cls_header.php';
    $obj_Client_functions = new Client_functions($_SESSION['store']);
    ?>
    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore Nexa...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>
        <!-- Right Sidebar -->
        <!-- Top Bar -->
        <?php
        include 'topbar.php';
        include 'sidebar.php';
        include 'ri8sidebar.php';
        ?>

        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-11 col-md-11 col-sm-12">
                        <h1>Create Goods Return Note
                        </h1>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-12">
                        <a href="puchase-return.php?store=<?php echo $_SESSION['store'];?>" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid " >
                <!-- Input -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <form name="frm" id="purchase_return_create">
                                <div class="body">
                                    <div class="row clearfix">
<!--                                        <div class="col-lg-12 col-md-12">
                                            <label>POType :</label>
                                            <input name="po_type" type="radio" class="with-gap" id="radio_3" value="0" checked="true"/>
                                            <label for="radio_3">Direct</label>
                                            <input name="po_type" type="radio" class="with-gap" id="radio_2" value="1"/>
                                            <label for="radio_2">On Approval</label>
                                            <span class="error po_type"></span>
                                        </div>-->
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <?php
                                                   $shopinfo = $obj_Client_functions->get_store_detail_obj();
                                                    $store_user_id = $shopinfo[0]['store_user_id'];
                                                    $where_query = array(["", "store_user_id", "=", "$store_user_id"]);
                                                    $comeback = $obj_Client_functions->select_result(TABLE_SUPPLIERS, '*', $where_query);
                                                    ?>
                                                    <div class="form-group">
                                                        <select class="form-control show-tick" name="supplier_id" id="supplier_id">
                                                            <?php
                                                            foreach ($comeback["data"] as $data) {
                                                                $data = (object) $data;
                                                               $options .='<option value="' . $data->id . '" >' . $data->supplier_name . '('. $data->generate_code.')</option>';
                                                            }
                                                            echo $options;
                                                            ?>

                                                        </select>
                                                    </div>
                                                    <span class="error supplier_id"></span>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <button type="button" class="btn  btn-raised bg-teal waves-effect" tableId="purchase_return_create" id="searchbtn">Search </button>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <label>Return Date</label>
                                                            <input type="date" class="form-control" placeholder="Return Date" name="return_date"  id="return_date"/>
                                                        </div>
                                                        <span class="error return_date"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" placeholder="Ref Num" name="ref_num"/>
                                                        </div>
                                                        <span class="error ref_num"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control qty_set" placeholder="Qty" name="qty_ret" />
                                                        </div>
                                                        <span class="error qty_ret"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control price_set" placeholder="Grand Total" name="price_ret" />
                                                        </div>
                                                        <span class="error price_ret"></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <textarea rows="4" class="form-control no-resize" placeholder="Reason" name="reason"></textarea>
                                                            </div>
                                                            <span class="error reason"></span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable"   tableId="purchase_return_create"  id="searchData" data-search="title"  data-listing="true" data-from="table" data-apiName="products" >
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>SKU</th>
                                                            <th>Barcode</th>
                                                            <th>Price</th>
                                                            <th>Voucher No.</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody id="allsearchData">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                           <div class="row clearfix purchase-return-skutable" style="display: none;">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="card">
                                                <div class="body">
                                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                        <thead>
                                                            <tr>
                                                                <th>SNo.</th>
                                                                <th>Barcode</th>
                                                                <th>SKU</th>
                                                                <th>name</th>
                                                                <th>V. SKU  </th>
                                                                <th>qty /Price</th>
                                                                <th>  Taxable </th>
                                                                <th>CGST</th>
                                                                <th>SGST</th>
                                                                <th>IGST    </th>
                                                                <th>Total    </th>
                                                                <th>#</th>
                                                            </tr>
                                                            
                                                        </thead>

                                                        <tbody class="dataresult">
                                                        </tbody>

                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>Total</th>
                                                            <th><input type="text"  class="form-control total_quantity"  name="total_quantity" value="0">  </th>
                                                            <th><input type="text"  class="form-control total_taxable" readonly=""name="total_taxable" ></th>
                                                            <th><input type="text"  class="form-control total_cgst" readonly="" name="total_cgst" ></th>
                                                            <th><input type="text"  class="form-control total_sgst" readonly=""name="total_sgst" ></th>
                                                            <th><input type="text"  class="form-control total_igst" readonly="" name="total_igst" ></th>
                                                            <th><input type="number"  class="form-control grand_amount" readonly="" name="grand_amount" ></th>
                                                            <th></th>   
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                        <div class="row clearfix">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="number" class="form-control" id="numberDemo" placeholder="Add Rows?" />
                                                        <span class="addrows" id="addSku_purchase_return">
                                                            <i class="fa fa-plus-circle circlewidth"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <button type="submit" name="submit" class="btn  btn-raised bg-teal waves-effect">Create </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Input --> 
</section>
</body>
</html>
