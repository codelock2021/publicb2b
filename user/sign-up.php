﻿<!doctype html>
<html class="no-js " lang="en">
<?php
include_once 'cls_header.php';
?>
<body class="theme-orange">
<div class="authentication">
    <div class="card">
        <div class="body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header slideDown">
                        
                        <h1> Admin</h1>
                        <ul class="list-unstyled l-social">
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-facebook-box"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-linkedin-box"></i></a></li>                            
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a></li>
                        </ul>
                    </div>                        
                </div>
                <form class="col-lg-12" id="sign_in" method="POST">
                    <h5 class="title">Register a new membership</h5>
                    <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control">
                                <label class="form-label">Name Surname</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control">
                                <label class="form-label">Email Address</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control">
                                <label class="form-label">Password</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control">
                                <label class="form-label">Confirm Password</label>
                            </div>
                        </div>
                    <div>
                         <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
                        <label for="terms">I read and agree to the <a href="javascript:void(0);">terms of usage</a>.</label>
                    </div>                        
                </form>
                <div class="col-lg-12">                        
                    <a href="javascript:void(0);" class="btn btn-raised btn-primary waves-effect">SIGN UP</a>                        
                </div>
                <div class="col-lg-12 m-t-20">
                    <a href="sign-in.php">You already have a membership?</a>
                </div>                    
            </div>
        </div>
    </div>
</div>

</html>
