	<!doctype html>
<html class="no-js " lang="en">
<?php
include 'cls_header.php';
?>
<body class="theme-orange">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <!--<div class="m-t-30"></div>-->
        </div>
    </div>

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>

    <!-- Top Bar -->
    <?php  include 'topbar.php';
include 'sidebar.php';
include 'ri8sidebar.php';
    ?>
<section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h1>Dealer Orders</h1> 
                </div>
              
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix"></div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable"  id="dealerorderData" data-search="title"  data-listing="true" data-from="table" data-apiName="dealer">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Order ID</th>
                                        <th>Dealer Name</th>
                                        <th>Dealer Email</th>
                                        <th>Dealer Address</th>
                                        <th>Grand Total</th>
                                        <th>Order  Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                      <tr id="filter" class="filter">
                                        <th></th>
                                        <th><input type="text" class="form-control" name="order_id"  data-apiname="dealer"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="dealer_id" data-apiname="dealer"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="email" data-apiname="dealer"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="delaer_address" data-apiname="dealer"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="grand_total" data-apiname="dealer"  data-from="table"></th>
                                        <th><input type="text" class="form-control" name="order_datee" data-apiname="dealer"  data-from="table"></th>
                                        <th><select class="form-control show-tick" name="supplier_status" data-apiname="dealer"  data-from="table">
                                            <option value="" >All</option>
                                            <option value="0">Active</option>
                                            <option value="1">InActive</option>
                                        </select>
                                            </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                       <th>#</th>
                                        <th>Order ID</th>
                                        <th>Dealer Name</th>
                                        <th>Dealer Email</th>
                                        <th>Dealer Address</th>
                                        <th>Grand Total</th>
                                        <th>Order  Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody id="alldealerorderData"></tbody>
                            </table>
                        </div>
                          <div class="cls-page-pagination mb-4" id="dealerorderDataPagination">
                                <input type="hidden" name="current_page" id="currentPage" value="1">                                              
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        
        </div>
    </section>

</body>
<!-- Jquery DataTable Plugin Js -->
    <script src="../assets/bundles/datatablescripts.bundle.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.flash.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
    <script src="../assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

    <!-- <script src="../assets/bundles/mainscripts.bundle.js"></script> -->
    <!-- Custom Js -->
    <script src="../assets/js/pages/tables/jquery-datatable.js"></script>


</html>