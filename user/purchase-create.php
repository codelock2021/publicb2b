<!doctype html>
<html class="no-js " lang="en">
    <?php
    include_once 'cls_header.php';
    $obj_Client_functions = new Client_functions($_SESSION['store']);
    
    ?>
    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore Nexa...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>
        <!-- Right Sidebar -->
        <!-- Top Bar -->
        <?php
        include 'topbar.php';
        include 'sidebar.php';
        include 'ri8sidebar.php';
        ?>

        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-11 col-md-11 col-sm-12">
                        <h1>Create Goods Receive Note
                        </h1>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-12">
                        <a href="purchase-listing.php?store=<?php echo $_SESSION['store'];?>" type="button" class="btn  btn-raised bg-teal waves-effect">Back</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <!-- Input -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <form name="form" id="purchase_create" enctype="multipart/form-data">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-1 col-md-2">
                                            <div class="col-sm-2">
                                                <label>P.O.Type</label>
                                                <input name="po_type" type="radio" class="with-gap" id="radio_3"  value="1" checked />
                                                <label for="radio_3">Direct</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <div class="col-sm-2">
                                                <label>Taxation</label>
                                                <input  name="taxation" type="radio" class="with-gap" id="radio_4" value="1" checked/>
                                                <label for="radio_4">Exclude TAX</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" placeholder="Freight" name="freight" id="freight"/>
                                                </div>
                                                <span class="error freight"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" placeholder="Packing" name="packing" id="packing"/>
                                                </div>
                                                <span class="error packing"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" placeholder="Insurance" name="insurance" id="insurance"/>
                                                </div>
                                                <span class="error insurance"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label >Purchase Date</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="date" class="form-control" placeholder="Purchase Date" name="po_date"/>
                                                </div>
                                                <span class="error po_date"></span>
                                            </div>
                                        </div>

                                        <?php
                                        $shopinfo = $obj_Client_functions->get_store_detail_obj();
                                        $store_user_id = $shopinfo[0]['store_user_id'];
                                        $where_query = array(["", "store_user_id", "=","$store_user_id"]);
                                        $comeback = $obj_Client_functions->select_result(TABLE_SUPPLIERS, '*',$where_query);
                                        ?>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <select class="form-control show-tick" name="supplier_id" id="supplier_id">
                                                    <?php
                                                    $options = '<option value="">Supplier Name</option>';
                                                    foreach ($comeback["data"] as $data) {
                                                        $data = (object) $data;
                                                        $options .='<option value="' . $data->id . '" >' . $data->supplier_name . '('. $data->generate_code.')</option>';
                                                    }
                                                    echo $options;
                                                    ?>
                                                </select>
                                            </div>
                                            <span class="error supplier_id"></span>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <textarea rows="4" class="form-control no-resize" id="supplier_address" placeholder="Supplier Address" name="supplier_address"></textarea>
                                                </div>
                                                <span class="error supplier_address"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <textarea rows="4" class="form-control no-resize" placeholder="Remarks" name="remarks"></textarea>
                                                </div>
                                                <span class="error remarks"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <select class="form-control show-tick" name="reverse_charge">
                                                        <option value="">Reverse Charge</option>
                                                        <option value="0">No</option>
                                                        <option value="1">Yes</option>
                                                    </select>
                                                </div>
                                                <span class="error reverse_charge"></span>
                                            </div> 
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" placeholder="Supplier Invoice No" name="supplier_invoice_no" />
                                                </div>
                                                <span class="error supplier_invoice_no"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label >Supplier Invoice Date</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="date" class="form-control" placeholder="Supplier Invoice Date" name="supplier_invoice_date"/>
                                                </div>
                                                <span class="error supplier_invoice_date"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" placeholder="Rec. Ref. Code" name="rec_ref_code" />
                                                </div>
                                                <span class="error rec_ref_code"></span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="card">
                                                <div class="body">
                                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                        <thead>
                                                            <tr>
                                                                <th>SNo.</th>
                                                                <th>SKU</th>
                                                                <th>Category</th>
                                                                <th>V. SKU  </th>
                                                                <th>Qty/Our.Price   </th>
                                                                <th>Taxable</th>
                                                                <th>Tax Rate</th>   
                                                                <th>CGST</th>
                                                                <th>SGST</th>
                                                                <th>IGST    </th>
                                                                <th>Total Amt   </th>
                                                                <th>#</th>
                                                            </tr>
                                                            <tr class="cls">
                                                                <td>1</td>
                                                                <td> <input type="text" class="form-control set_sku" name="sku[]" >                                                  
                                                                    <span class="error sku"></span>
                                                                </td>
                                                                <td> <input type="text"  class="form-control category_name" readonly="" name="category_name[]" >                                                  
                                                                </td>
                                                                <td> <input type="text"  class="form-control vendor" name="v_sku[]" >                                                  
                                                                </td>
                                                                 <td class="cls-text-section"> <input type="number"  class="form-control qty"  name="qty[]" >  	
                                                                    <input type="number"  class="form-control price"  name="price[]" step=0.01 > 	
                                                                </td>	
                                                                <td> <input type="number"  class="form-control total"  name="taxable[]"   step=0.01 >                                                  	
                                                                </td>	
                                                                  <td> <input type="number"  class="form-control taxable_rate"  name="taxable_rate[]"  >                                                  	
                                                                </td>	
                                                                <td> <input type="number"  class="form-control cgst"  name="cgst[]"  step=0.01>                                                  	
                                                                </td>	
                                                                <td> <input type="number"  class="form-control sgst" name="sgst[]"  step=0.01>                                                  	
                                                                </td>	
                                                                <td> <input type="number"  class="form-control igst" name="igst[]"  step=0.01>                                                  	
                                                                </td>	
                                                                <td> <input type="number"  class="form-control total_amount"  name="grand_total[]"  step=0.01 >             
                                                                <td> <button class="btnDeleteRowCls btn-zap" type="button">× </button>                                                
                                                                </td>
                                                            </tr>
                                                        </thead>

                                                        <tbody class="dataresult">
                                                        </tbody>

                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th>Total</th>
                                                            <th><input type="number"  class="form-control total_quantity" name="total_quantity"  step=0.01 > </th>	
                                                            <th><input type="number"  class="form-control total_taxable" name="total_taxable"  step=0.01></th>	
                                                            <th>&nbsp;</th>	
                                                            <th><input type="number"  class="form-control total_cgst" name="total_cgst"  step=0.01></th>	
                                                            <th><input type="number"  class="form-control total_sgst" name="total_sgst"  step=0.01></th>	
                                                            <th><input type="number"  class="form-control total_igst" name="total_igst"  step=0.01 ></th>	
                                                            <th><input type="number"  class="form-control grand_amount" name="grand_amount"  step=0.01></th>	
                                                            <input type="hidden"  class="form-control all_total" name="all_total" >
                                                            <th></th>   
                                                        </tr>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="row clearfix">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="number" class="form-control" id="numberDemo" placeholder="Add Rows?" />
                                                        <span class="addrows" id="addSku">
                                                            <i class="fa fa-plus-circle circlewidth"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <button type="submit" name="submit"  id="purchase-create" class="btn  btn-raised bg-teal waves-effect">Create </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- #END# Input --> 
            </div>
        </section>
    </body>
</html>